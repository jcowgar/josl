#
# Copyright (c) 2008-2022 Jeremy Cowgar.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#   * Neither the name of Jeremy Cowgar nor the names of its contributors may
#     be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

# Edit config.mak for your system
include main.mak
include config.mak

all: scmid.mak $(PLATFORM)

$(PLATFORMS) clean:
	cd src && $(MAKE) $@

scmid.mak:
	echo "SCMID=\"`git rev-parse HEAD|cut -b1-12`\"" > scmid.mak

indent:
	cd include && $(MAKE) $@
	cd src && $(MAKE) $@

test: $(PLATFORM)
	cd tests && ../src/josl josl.j

api-docs manual publican publican-all:
	cd docs && make $@

copy-files: $(PLATFORM)
	$(MKDIR) $(DEST_DIR)
	$(MKDIR) $(DEST_DIR)/bin
	$(MKDIR) $(DEST_DIR)/lib
	$(MKDIR) $(DEST_DIR)/include
	$(MKDIR) $(DEST_DIR)/include/josl
	$(MKDIR) $(DEST_DIR)/share
	$(MKDIR) $(DEST_DIR)/share/docs
	$(MKDIR) $(DEST_DIR)/share/docs/josl
	$(MKDIR) $(DEST_DIR)/share/docs/josl/examples
	$(MKDIR) $(DEST_DIR)/share/docs/josl/tests
	$(MKDIR) $(DEST_DIR)/share/josl
	$(MKDIR) $(DEST_DIR)/share/josl/$(VERSION_MAJOR)
	$(MKDIR) $(DEST_DIR)/share/josl/$(VERSION_MAJOR).$(VERSION_MINOR)
	$(MKDIR) $(DEST_DIR)/share/josl/$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)
	$(STRIP) src/josl$(EXE)
ifeq ($(PLATFORM),mingw)
	$(UPX) src/josl$(EXE)
	$(INSTALL_EXEC) dist/libpcre-0.dll $(DEST_DIR)/bin
endif
	$(INSTALL_EXEC) src/josl$(EXE) $(DEST_DIR)/bin
	$(INSTALL_DATA) src/libjosl.a $(DEST_DIR)/lib
	$(INSTALL_DATA) include/josl.h $(DEST_DIR)/include
	$(INSTALL_DATA) include/josl/*.h $(DEST_DIR)/include/josl
	$(INSTALL_DATA) lib/*.j $(DEST_DIR)/share/josl/$(VERSION)
	$(INSTALL_DATA) examples/*.j $(DEST_DIR)/share/docs/josl/examples
	$(INSTALL_DATA) tests/*.j $(DEST_DIR)/share/docs/josl/tests

dist: $(PLATFORM)
	$(MAKE) copy-files DEST_DIR=dist/$(PACKAGE_NAME)-$(PLATFORM)-$(VERSION)
	$(RM) dist/$(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).tar.gz
	$(RM) dist/$(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).tar.bz2
	$(RM) dist/$(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).zip
ifneq ($(PLATFORM),mingw)
	cd dist && $(TARGZ) $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).tar.gz $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION)
	cd dist && $(TARBZ2) $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).tar.bz2 $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION)
endif
	cd dist && $(ZIP) $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION).zip $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION)
	cd dist && $(RMDIR_RECURSE) $(PACKAGE_NAME)-$(PLATFORM)-$(VERSION)

install:
	$(MAKE) copy-files DEST_DIR=$(INSTALL_DIR)

none:
	@echo "Makefile use:"
	@echo "    make PLATFORM"
	@echo "where PLATFORM is one of:"
	@echo "    $(PLATFORMS)"

.PHONY: all $(PLATFORMS) clean test docs dist install

