Josl is an easy to use, embedable, stack based scripting language.

Hello World

    main "Hello, World!" println

Hello World x 5

    main 5 times "Hello, World!" println next

Hello Someone (name from command line)

    main 0 argv "Hello, {0}!" format println

Recursive fibonacci

    fib dup 1 > if dup 2- fib swap 1- fib +

	ensure-params argc 0= if
	  "usage: fib.j number" println 0 exit

	main ensure-params 0 argv >int fib

Includes a wide range of built-in words include file system,
socket, regular expression, foreign function interface and more.
