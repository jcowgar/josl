<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
                  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<chapter id="functional-words"
         xmlns:xi="http://www.w3.org/2001/XInclude">
  <title>Functional Words</title>

  <para>
    Josl does not strive at all to be a pure functional language, however
    we have borrowed a few common words from our functional friends. These
    functional words will work on Lists or Arrays.
  </para>

  <section id="functional-general">
    <title>General</title>

    <section id="functional-map">
      <title>map</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>map ( al:content q:quotation -- al:result )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Apply <varname>quotation</varname> to every element of the list or
        array <varname>content</varname> creating a new array or list as 
        the <varname>result</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/functional-map-1.ses"
                                  parse="text"/></programlisting>

      <bridgehead>Versioning</bridgehead>
      <itemizedlist>
        <listitem>
          <para>
            Added in version <emphasis>0.5.0</emphasis>.
          </para>
        </listitem>
      </itemizedlist>
    </section>

    <section id="functional-filter">
      <title>filter</title>
      <bridgehead>Synopsis</bridgehead>
      <para>
        <function>filter ( al:content q:quotation -- al:result )</function>
      </para>

      <bridgehead>Description</bridgehead>
      <para>
        Create a new array or list containing only the values from
        <varname>content</varname> where <varname>quotation</varname>
        returns true.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/functional-filter-1.ses"
                                  parse="text"/></programlisting>

      <bridgehead>Versioning</bridgehead>
      <itemizedlist>
        <listitem>
          <para>
            Added in version <emphasis>0.5.0</emphasis>.
          </para>
        </listitem>
      </itemizedlist>
    </section>

    <section id="functional-fold">
      <title>fold</title>
      <bridgehead>Synopsis</bridgehead>
      <para>
        <function>fold ( al:content q:quotation -- al:result )</function>
      </para>


      <bridgehead>Description</bridgehead>
      <para>
        Starting with the first two values from <varname>content</varname>,
        iterate from left to right calling <varname>quotation</varname>
        on successive values from <varname>content</varname>. The result 
        will be one value from the final call to 
        <varname>quotation</varname>.
      </para>
      <para>
        If <varname>content</varname> contains no elements, 
        <literal>NULL</literal> will be the result. If 
        <varname>content</varname> contains only one element,
        the first element from <varname>content</varname> will be
        the result.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/functional-fold-1.ses"
                                  parse="text"/></programlisting>
      
      <bridgehead>Versioning</bridgehead>
      <itemizedlist>
        <listitem>
          <para>
            Added in version <emphasis>0.5.0</emphasis>.
          </para>
        </listitem>
      </itemizedlist>
    </section>

    <section id="functional-foldr">
      <title>foldr</title>
      <bridgehead>Synopsis</bridgehead>
      <para>
        <function>foldr ( al:content q:quotation -- al:result )</function>
      </para>

      <bridgehead>Description</bridgehead>
      <para>
        Starting with the last two values from <varname>content</varname>,
        iterate from right to left calling <varname>quotation</varname>
        on successive values from <varname>content</varname>. The result will
        be one value from the final call to <varname>quotation</varname>.
      </para>
      <para>
        If <varname>content</varname> contains no elements, 
        <literal>NULL</literal> will be the result. If 
        <varname>content</varname> contains only one element,
        the first element from <varname>content</varname> will be
        the result.
      </para>
      
      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/functional-foldr-1.ses"
                                  parse="text"/></programlisting>
      
      <bridgehead>Versioning</bridgehead>
      <itemizedlist>
        <listitem>
          <para>
            Added in version <emphasis>0.5.0</emphasis>.
          </para>
        </listitem>
      </itemizedlist>
    </section>
  </section>
</chapter>
