<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
                  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<chapter id="string-words">
  <title>String Words</title>

  <section id="string-general-notes">
    <title>General Notes</title>

    <itemizedlist>
      <listitem>
        <para>
          When duplicating a string via the stack word <function>dup</function> it's
          memory is not actually duplicated, only a pointer to it's memory. Therefore
          passing strings around on the stack is very fast. When a change operation is
          requested on a string, a new string with the changed content will be created
          leaving the original intact.
        </para>
      </listitem>
      <listitem>
        <para>
          All character indexes and positions are a zero based index.
        </para>
      </listitem>
      <listitem>
        <para>
          All equality and comparison words function on strings. They will not be
          gone over here. These words include:
        </para>
        <itemizedlist>
          <listitem><para><function>=</function></para></listitem>
          <listitem><para><function>not=</function></para></listitem>
          <listitem><para><function>&lt;</function></para></listitem>
          <listitem><para><function>&gt;</function></para></listitem>
          <listitem><para><function>&lt;=</function></para></listitem>
          <listitem><para><function>&gt;=</function></para></listitem>
          <listitem><para><function>between?</function></para></listitem>
        </itemizedlist>
      </listitem>
      <listitem>
        <para>
          String words fail gracefully. For example, say you have a string
          <literal>"Hello"</literal> and you issue the <function>delete</function> word
          to delete from positions <literal>20</literal> to <literal>25</literal>. Some
          languages would through an index error or some other type of error. With Josl
          it assumes that you wanted to remove the range of <literal>20-25</literal> and
          that range does not exist, therefore it doesn't have to do any work, it's
          already "not" there, thus, the task was completed with success.
        </para>
      </listitem>
    </itemizedlist>
  </section>

  <section id="string-basic">
    <title>Basic</title>

    <section id="string-len">
      <title>len</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>len ( s -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Retrieve the length of <varname>s</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-len-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-delete">
      <title>delete</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>delete ( s:value i:start i:end -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Delete the content of <varname>value</varname> between the character indexes
        of <varname>start</varname> and <varname>end</varname>.
      </para>
      <para>
        If <varname>start</varname> is greater than the length of
        <varname>value</varname> nothing will be deleted. If <varname>end</varname> is
        greater than the length of <varname>value</varname> then the <varname>end</varname>
        will be adjusted to be the actual ending position of <varname>value</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-delete-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-insert">
      <title>insert</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>insert ( s:content s:new i:index -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Insert <varname>new</varname> into the string <varname>content</varname> at
        the character index <varname>index</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-insert-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-replace">
      <title>replace</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>replace ( s:value s:new i:start i:end -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Replace from <varname>start</varname> to <varname>end</varname> in the
        string <varname>value</varname> with the content from <varname>new</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-replace-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-reverse">
      <title>reverse</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>reverse ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Reverse the string <varname>s</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-reverse-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-translate">
      <title>translate</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>translate ( s:value s:map -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Translate all pairs from <varname>map</varname> in <varname>value</varname>.
        <varname>map</varname> must be an even number of characters.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-translate-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

  <section id="string-concatenation">
    <title>Concatenation</title>

    <section id="string-repeat">
      <title>repeat</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>repeat ( s:what i:times -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Repeat <varname>what</varname> <varname>times</varname> creating a new string.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-repeat-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-append">
      <title>append</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>append ( s:first s:second -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Append <varname>second</varname> onto the end of <varname>first</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-append-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-append-hash">
      <title>append#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>append# ( s:... i:count -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Append the top <varname>count</varname> values onto each other.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-append-hash-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>

      <warning>
        <title>Not yet implemented!</title>
        <para>
          <function>append#</function> is not yet implemented.
        </para>
      </warning>
    </section>

    <section id="string-join">
      <title>join</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>join ( s:first s:second s:by -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Append <varname>second</varname> onto the end of <varname>first</varname>
        placing <varname>by</varname> between the two.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-join-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-join-hash">
      <title>join#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>join ( s:... s:by i:count -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Append the top <varname>count</varname> values onto each other placing
        <varname>by</varname> between each other.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-join-hash-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>

      <warning>
        <title>Not yet implemented!</title>
        <para>
          <function>join#</function> is not yet implemented.
        </para>
      </warning>
    </section>

    <section id="string-format">
      <title>format</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>format ( ... s:format -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Format items from the stack according to the <varname>format</varname>. The
        number of items removed from the stack depends on the number of items requested
        in the <varname>format</varname> string.
      </para>
      <para>
        All text in the <varname>format</varname> will be copied verbatim to the new
        string except for content contained in special place holders. A place holder
        is defined by cury brackets (<literal>{}</literal>).
      </para>
      <para>
        The bare minimum for a place holder is an integer number which states which
        place from the stack to pull the associated value from. Zero is the
        <varname>TOS</varname>, one is the <varname>NOS</varname> and so on.
      </para>

      <note>
        <title>What's removed from the stack?</title>
        <para>
          <function>format</function> will remove the deepest value accessed by the
          format string all the way to the <varname>TOS</varname>. This means if you
          have ten values on the stack and in the <varname>format</varname> string you
          use only <literal>{9}</literal>, all items from that position down are removed
          even though they are not accessed.
        </para>
      </note>

      <bridgehead>Example - Simple</bridgehead>
      <programlisting><xi:include href="../examples/string-format-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>

      <bridgehead>Advanced Formatting</bridgehead>
      <para>
        Advanced formatting is specified by placing a colon (<literal>:</literal>) after
        the position number and then using the following flags and format characters.
      </para>

      <itemizedlist>
        <listitem>
          <para>
            Flags (in any order) which modify the specification:
            <itemizedlist>
              <listitem>
                <para>
                  <literal>-</literal>, left adjustment
                </para>
              </listitem>
              <listitem>
                <para>
                  <literal>+</literal>, number will always be printed with a sign
                </para>
              </listitem>
              <listitem>
                <para>
                  <literal>space</literal>, if the first character is not a sign, a space
                  will be prefixed
                </para>
              </listitem>
              <listitem>
                <para>
                  <literal>0</literal>, padding to the field width
                </para>
              </listitem>
            </itemizedlist>
          </para>
        </listitem>
        <listitem>
          <para>
            A number, specifies the minimum field width. The padding is normally a
            space but if a <literal>0</literal> flag is specified, padding will become
            a zero.
          </para>
        </listitem>
        <listitem>
          <para>
            A period followed by a number, specifies the precision
          </para>
        </listitem>
      </itemizedlist>

      <table class="html">
        <caption>Format Character Conversions</caption>
        <thead>
          <tr>
            <td>Character</td>
            <td>Description</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>d,i</td>
            <td>an integer signed decimal notation</td>
          </tr>
          <tr>
            <td>o</td>
            <td>an integer unsigned octal notation (without a leading zero)</td>
          </tr>
          <tr>
            <td>x,X</td>
            <td>
              an integer unsigned hexadecimal notation (without a leading
              <literal>0x</literal> or <literal>0X</literal>).
            </td>
          </tr>
          <tr>
            <td>u</td>
            <td>an integer unsigned decimal notation</td>
          </tr>
          <tr>
            <td>s</td>
            <td>a string value</td>
          </tr>
          <tr>
            <td>f</td>
            <td>
              a double value decimal notation. The default precision is 6. A
              precision of 0 will suppress the decimal point.
            </td>
          </tr>
          <tr>
            <td>e,E</td>
            <td>
              a double value decimal notation in the form of [-]m.dddddde+/-xx
              or [-]m.ddddddE+/-xx. The default precision is 6. A precision of
              0 will suppress the decimal point.
            </td>
          </tr>
          <tr>
            <td>g,G</td>
            <td>
              a double value. Format e,E is used if the exponent is less than -4 or
              greater than or equal to the precision, otherwise the f format is used.
            </td>
          </tr>
          <tr>
            <td>p</td>
            <td>a pointer value</td>
          </tr>
        </tbody>
      </table>

      <bridgehead>Example - Advanced</bridgehead>
      <programlisting><xi:include href="../examples/string-format-2.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>

    </section>

  </section>

  <section id="string-case">
    <title>Case Conversion</title>

    <section id="string-lower">
      <title>lower</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>lower ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Convert <varname>s</varname> to lower case.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-lower-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-upper">
      <title>upper</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>upper ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Convert <varname>s</varname> to upper case.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-upper-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-title">
      <title>title</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>title ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Convert <varname>s</varname> to title case (<literal>Mr. John Doe</literal>).
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-title-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-capitalize">
      <title>capitalize</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>capitalize ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Capitalize <varname>s</varname>. (<literal>The dog was brown.</literal>)
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-capitalize-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

  <section id="string-extraction">
    <title>Extraction</title>

    <warning>
      <title>Possible Changes</title>
      <para>
        When lists and arrays are introduced, it's possible that <function>left</function>
        and <function>right</function> words will change to <function>tail</function>
        and <function>head</function> functioning on strings, lists and arrays.
      </para>
      <para>
        Josl strives to keep a minimum number of functional words. One should not have
        to remember <function>left</function> gets X characters from a string but
        <function>head</function> gets X items from a list. They perform the same function
        and should have the same name.
      </para>
    </warning>

    <section id="string-at">
      <title>at</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>at ( s i -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Retrieve the ASCII character code from <varname>s</varname> at the character
        index <varname>i</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-at-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-left">
      <title>left</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>left ( s:value i:count -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Get <varname>count</varname> characters from the left side of
        <varname>value</varname>. If <varname>count</varname> is greater than the length
        of <varname>value</varname> then the entire content of <varname>value</varname>
        is returned.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-left-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-right">
      <title>right</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>right ( s:value i:count -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Get <varname>count</varname> characters from the right side of
        <varname>value</varname>. If <varname>count</varname> is greater than the length
        of <varname>value</varname> then the entire content of <varname>value</varname>
        is returned.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-right-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-mid">
      <title>mid</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>mid ( s:value i:start i:end -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Get the string from <varname>value</varname> that begins from
        <varname>start</varname> and ends at the character index <varname>end</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-mid-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-trim">
      <title>trim</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>trim ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Trim all white space off the head and tail of <varname>s</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-trim-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-trim-head">
      <title>trim-head</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>trim-head ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Trim all white space off the head of <varname>s</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-trim-head-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-trim-tail">
      <title>trim-tail</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>trim-tail ( s -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Trim all white space off the tail of <varname>s</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-trim-tail-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="split">
      <title>split</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>split ( s:value s:by -- s?... i:count )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Split <varname>value</varname> by the string <varname>by</varname>.
        <function>split</function> can place a variable number of items on the stack,
        therefore, it places on the <varname>TOS</varname> the number of items
        split from <varname>value</varname> as <varname>count</varname>.
      </para>
      <para>
        <varname>count</varname> will always be at least one. For example, say you split
        the string <literal>John</literal>. Nothing is split, but the original value
        of <literal>John</literal> is placed back onto the stack by
        <function>split</function>, therefore <varname>count</varname> will be one.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-split-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-split-hash">
      <title>split#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>split# ( s:value s:by i:max -- s?... i:count )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Split <varname>max</varname> times the <varname>value</varname> by
        <varname>by</varname>. Splitting <varname>max</varname> times means that a split
        will occur at most <varname>max</varname> times. If one, the result can
        possibly have two values as a result of the one split. For example,
        <literal>"John Doe" " " 1 split#</literal> will split <literal>John Doe</literal>
        once resulting in two values on the stack, <literal>John</literal> and
        <literal>Doe</literal>. <varname>max</varname> is not to be confused with the
        maximum number of items on the stack.
      </para>
      <para>
        See <function>split</function> for more information.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-split-hash-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-splitany">
      <title>splitany</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>splitany ( s:value s:by -- s?... i:count )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Split <varname>value</varname> by <emphasis>any character</emphasis> in the
        string <varname>by</varname>.
      </para>
      <para>
        See <function>splitany</function> for more information.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-splitany-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-splitany-hash">
      <title>splitany#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>splitany# ( s:value s:by i:max -- s?... i:count )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Split <varname>max</varname> times the <varname>value</varname> by
        <emphasis>any character</emphasis> in the string <varname>by</varname>.
      </para>
      <para>
        See <function>split</function> and <function>split#</function> for more
        information.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-splitany-hash-1.ses" parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-peel">
      <title>peel</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>peel ( s:value -- s:rest i:first )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Peel the first character from <varname>value</varname> putting the rest of
        the string and the peeled character back on the stack as <varname>rest</varname>
        and <varname>first</varname>. <varname>first</varname> will be the ASCII
        character code. When the end of the string is reached, <literal>0</literal>
        (NULL terminator) will naturally be the last character peeled from
        <varname>value</varname>, thus it's easy to use peel in a condition based loop.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-peel-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>

      <bridgehead>Example - Using <function>peel</function> in a decision loop</bridgehead>
      <programlisting><xi:include href="../examples/string-peel-2.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-cut">
      <title>cut</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>cut ( s:value i:position -- s:first s:last )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Cut <varname>value</varname> into two strings at the character index
        <varname>position</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-cut-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

  <section id="string-ascii">
    <title>ASCII Characters</title>

    <section id="string-asc">
      <title>asc</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>asc ( s:character -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Return the ASCII integer code of <varname>character</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-asc-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-chr">
      <title>chr</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>chr ( i:ascii-code -- s )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Convert the integer <varname>ascii-code</varname> to a string character.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-chr-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

  <section id="string-searching">
    <title>Searching</title>

    <section id="string-find">
      <title>find</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>find ( s:haystack is:needle -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Find the character index position of <varname>needle</varname> in
        <varname>haystack</varname>. If <varname>needle</varname> was not found the
        result will be <literal>-1</literal>.
      </para>
      <para>
        <varname>needle</varname> can be a string or an integer representing a valid
        ASCII character code.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-find-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-rfind">
      <title>rfind</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>rfind ( s:haystack is:needle -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Find the character index position of <varname>needle</varname> in
        <varname>haystack</varname> searching from the rear of <varname>haystack</varname>
        forward. If <varname>needle</varname> was not found, the result will be
        <literal>-1</literal>.
      </para>
      <para>
        <varname>needle</varname> can be a string or an integer representing a valid
        ASCII character code.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-rfind-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-find-hash">
      <title>find#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>find# ( s:haystack is:needle i:start -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Find the character index position of <varname>needle</varname> in
        <varname>haystack</varname> starting the search at character index position
        <varname>start</varname>. If <varname>needle</varname> was not found the result
        will be <literal>-1</literal>.
      </para>
      <para>
        <varname>needle</varname> can be a string or an integer representing a valid
        ASCII character code.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-find-hash-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-rfind-hash">
      <title>rfind#</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>rfind# ( s:haystack is:needle i:start -- i )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Find the character index position of <varname>needle</varname> in
        <varname>haystack</varname> searching from the rear of <varname>haystack</varname>
        forward starting at character index <varname>start</varname>. If
        <varname>needle</varname> was not found the result will be <literal>-1</literal>.
      </para>
      <para>
        <varname>needle</varname> can be a string or an integer representing a valid
        ASCII character code.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-rfind-hash-1.ses" parse="text"
        xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-prefix-question">
      <title>prefix?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>prefix? ( s:haystack s:prefix -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Check if <varname>haystack</varname> begins with <varname>prefix</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-prefix-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-suffix-question">
      <title>suffix?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>suffix? ( s:haystack s:suffix -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Check if <varname>haystack</varname> ends with <varname>suffix</varname>.
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-suffix-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

  <section id="string-character-testing">
    <title>Character Testing</title>

    <warning>
      <title>Possible Changes</title>
      <para>
        Currently the character testing words test either an ASCII character code in the
        form of an integer or the <emphasis>first</emphasis> character found in a string.
        The later form may change to test to ensure <emphasis>all</emphasis> characters
        in the string conform to the test condition.
      </para>
    </warning>

    <section id="string-alnum-question">
      <title>alnum?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>alnum? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> an alphabetical character or digit?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-alnum-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-alpha-question">
      <title>alpha?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>alpha? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> an alphabetical character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-alpha-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-cntrl-question">
      <title>cntrl?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>cntrl? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a control character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-cntrl-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-digit-question">
      <title>digit?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>digit? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a digit?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-digit-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-graph-question">
      <title>graph?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>graph? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a printing character (except space)?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-graph-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-lower-question">
      <title>lower?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>lower? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a lower case character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-lower-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-print-question">
      <title>print?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>print? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a printing character (including space)?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-print-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-punct-question">
      <title>punct?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>punct? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a punctuation character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-punct-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-space-question">
      <title>space?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>space? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a space, formfeed, newline, carriage return, tab
        or vertical tab character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-space-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-upper-question">
      <title>upper?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>upper? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> an upper case character?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-upper-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>

    <section id="string-xdigit-question">
      <title>xdigit?</title>
      <bridgehead>Synopsis</bridgehead>
      <para><function>xdigit? ( si:value -- b )</function></para>

      <bridgehead>Description</bridgehead>
      <para>
        Is <varname>value</varname> a hexadecimal digit?
      </para>

      <bridgehead>Example</bridgehead>
      <programlisting><xi:include href="../examples/string-xdigit-question-1.ses"
        parse="text" xmlns:xi="http://www.w3.org/2001/XInclude" /></programlisting>
    </section>
  </section>

</chapter>
