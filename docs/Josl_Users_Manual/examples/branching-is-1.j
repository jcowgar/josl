main
  5 is
    1 do "one" end
    3 do "three" end
    5 do "five" end
  else
    "unknown"
  end

  # Result is: "five"
