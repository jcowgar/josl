-variable myBoss mySpouse

main
  "John" myBoss !
  "Jane" mySpouse !

  "John" is
    "Jim"    do "Howdy, Jim!" end
    myBoss   do "Good evening sir!" end
    mySpouse do "Hey sweety!" end
  else
    "Hey"
  end

# Result is: "Good evening sir!"
