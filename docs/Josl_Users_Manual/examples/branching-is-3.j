main
  10 is
    1 do "one" end
    5 do "five" end
    { 5 > } do "more than five" end
  end

# Result is "more than five"
