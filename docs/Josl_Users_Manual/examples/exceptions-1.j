main@failed?
  is
    AlreadyExists?
      do
        "Directory already exists" println
        resume-this
      end

    PermissionDenied?
      do
        "Permission was denied to create the directory"
        println
        1 exit
      end
  else
    "An unknown exception was thrown: {0}" format println
    2 exit
  end

main
  "hello-world" dir-create
  "program is done" println
