# Say hello to someone
#
# ( s -- )
say-hello "Hello, {0}!" format println

# Main application entry point
main argc if 0 argv else "World" end say-hello