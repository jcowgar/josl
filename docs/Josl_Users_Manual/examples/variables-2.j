#
# Greeter Library (greeter.j)
#

-public set-name get-name say-hello
-variable name

set-name name !
get-name name @
say-hello name @ "Hello, {0}!" format println
