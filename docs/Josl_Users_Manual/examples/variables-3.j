-include greeter.j as g

main
  "John" g.set-name
  g.say-hello
  g.get-name "Name is set to '{0}'" format println
