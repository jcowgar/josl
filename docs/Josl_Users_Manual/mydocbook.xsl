<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="/usr/share/sgml/docbook/xsl-stylesheets/xhtml/chunkfast.xsl"/>
  <xsl:param name="html.stylesheet" select="'docbook.css'"/>
  <xsl:param name="admon.graphics" select="1"/>
  <xsl:param name="chapter.autolabel" select="1"/>
  <xsl:param name="section.label.includes.component.label" select="1"/>
  <xsl:param name="section.autolabel" select="1"/>
  <xsl:param name="use.id.as.filename" select="1"/>
  <xsl:param name="toc.max.depth" select="3"/>
  <xsl:param name="generate.section.toc.level" select="2"/>
</xsl:stylesheet>
