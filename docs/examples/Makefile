#
# Copyright (c) 2008-2022 Jeremy Cowgar.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#   * Neither the name of Jeremy Cowgar nor the names of its contributors may
#     be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

include ../../main.mak
include ../../config.mak

JOSL = ../../src/josl$(EXE)

EXAMPLES = \
	interactive-session-1.ses \
	constant-1.ses \
	rename-1.ses \
	array-capacity-1.ses \
	array-resize-1.ses \
	array-append-1.ses \
	array-sort-1.ses \
	boolean-equal-1.ses \
	boolean-one-equal-1.ses \
	boolean-zero-equal-1.ses \
	boolean-minus-one-equal-1.ses \
	boolean-not-equal-1.ses \
	boolean-not-1.ses \
	boolean-greater-than-1.ses \
	boolean-less-than-1.ses \
	boolean-greater-than-or-equal-1.ses \
	boolean-less-than-or-equal-1.ses \
	boolean-between-question-1.ses \
	boolean-and-question-1.ses \
	boolean-or-question-1.ses \
	branching-if-1.ses \
	branching-if-2.ses \
	branching-if-3.ses \
	branching-star-if-1.ses \
	branching-vector-1.ses \
	branching-begin-1.ses \
	branching-while-1.ses \
	branching-for-1.ses \
	branching-for-2.ses \
	branching-times-1.ses \
	branching-plus-next-1.ses \
	branching-i-2.ses \
	branching-each-1.ses \
	branching-each-rev-1.ses \
	branching-each-kv-1.ses \
	branching-break-1.ses \
	branching-continue-1.ses \
	datetime-clock-1.ses \
	dynamic-parse-1.ses \
	dynamic-exec-word-1.ses \
	dynamic-exec-word-2.ses \
	dynamic-exec-word-3.ses \
	dynamic-curry-1.ses \
	dynamic-curry-2.ses \
	dynamic-curry-question-1.ses \
	dynamic-compose-1.ses \
	system-system-1.ses \
	system-env-1.ses \
	list-list-1.ses \
	list-push-head-1.ses \
	list-push-tail-1.ses \
	list-insert-1.ses \
	list-remove-1.ses \
	list-current-1.ses \
	list-forward-1.ses \
	list-backward-1.ses \
	list-len-1.ses \
	list-pop-head-1.ses \
	list-pop-tail-1.ses \
	list-sort-1.ses \
	math-plus-1.ses \
	math-minus-1.ses \
	math-asterisk-1.ses \
	math-forward-slash-1.ses \
	math-percent-1.ses \
	math-fmod-1.ses \
	math-pipe-1.ses \
	math-ampersand-1.ses \
	math-carot-1.ses \
	math-tilda-1.ses \
	math-double-less-than-1.ses \
	math-double-greater-than-1.ses \
	math-modf-1.ses \
	math-frexp-1.ses \
	math-exp-1.ses \
	math-exp2-1.ses \
	math-expm1-1.ses \
	math-log-1.ses \
	math-log10-1.ses \
	math-log1p-1.ses \
	math-log2-1.ses \
	math-pow-1.ses \
	math-sqrt-1.ses \
	math-acos-1.ses \
	math-asin-1.ses \
	math-atan-1.ses \
	math-atan2-1.ses \
	math-cos-1.ses \
	math-sin-1.ses \
	math-tan-1.ses \
	math-radians-1.ses \
	math-degrees-1.ses \
	math-cosh-1.ses \
	math-sinh-1.ses \
	math-tanh-1.ses \
	math-tests-even-1.ses \
	math-tests-odd-1.ses \
	math-min-1.ses \
	math-max-1.ses \
	math-one-plus-1.ses \
	math-one-minus-1.ses \
	math-two-plus-1.ses \
	math-two-minus-1.ses \
	math-ceil-1.ses \
	math-floor-1.ses \
	math-round-1.ses \
	math-round-to-1.ses \
	math-abs-1.ses \
	math-negate-1.ses \
	math-pi-1.ses \
	math-2pi-1.ses \
	math-half-pi-1.ses \
	math-e-1.ses \
	io-println-1.ses \
	io-file-1.ses \
	type-integer-question-1.ses \
	type-double-question-1.ses \
	type-string-question-1.ses \
	type-dict-question-1.ses \
	type-array-question-1.ses \
	type-list-question-1.ses \
	type-word-question-1.ses \
	type-pointer-question-1.ses \
	type-null-question-1.ses \
	type-variable-question-1.ses \
	type-type-1.ses \
	type-greater-than-int-1.ses \
	type-greater-than-dbl-1.ses \
	type-greater-than-str-1.ses \
	stack-swap-1.ses \
	stack-2swap-1.ses \
	stack-3swap-1.ses \
	stack-swap-pound-1.ses \
	stack-roll-1.ses \
	stack-minus-roll-1.ses \
	stack-place-holder-1.ses \
	stack-dip-1.ses \
	stack-2dip-1.ses \
	string-len-1.ses \
	string-delete-1.ses \
	string-at-1.ses \
	string-insert-1.ses \
	string-replace-1.ses \
	string-reverse-1.ses \
	string-translate-1.ses \
	string-repeat-1.ses \
	string-append-1.ses \
	string-append-hash-1.ses \
	string-join-1.ses \
	string-join-hash-1.ses \
	string-format-1.ses \
	string-format-2.ses \
	string-lower-1.ses \
	string-upper-1.ses \
	string-title-1.ses \
	string-capitalize-1.ses \
	string-left-1.ses \
	string-right-1.ses \
	string-mid-1.ses \
	string-trim-1.ses \
	string-trim-head-1.ses \
	string-trim-tail-1.ses \
	string-split-1.ses \
	string-split-hash-1.ses \
	string-splitany-1.ses \
	string-splitany-hash-1.ses \
	string-peel-1.ses \
	string-peel-2.ses \
	string-cut-1.ses \
	string-asc-1.ses \
	string-chr-1.ses \
	string-find-1.ses \
	string-rfind-1.ses \
	string-find-hash-1.ses \
	string-rfind-hash-1.ses \
	string-prefix-question-1.ses \
	string-suffix-question-1.ses \
	string-alnum-question-1.ses \
	string-alpha-question-1.ses \
	string-cntrl-question-1.ses \
	string-digit-question-1.ses \
	string-graph-question-1.ses \
	string-lower-question-1.ses \
	string-print-question-1.ses \
	string-punct-question-1.ses \
	string-space-question-1.ses \
	string-upper-question-1.ses \
	string-xdigit-question-1.ses \
	filesys-dir-question-1.ses \
	filesys-file-exists-question-1.ses \
	filesys-readable-question-1.ses \
	filesys-writable-question-1.ses \
	filesys-executable-question-1.ses \
	filesys-file-ctime-1.ses \
	filesys-file-mtime-1.ses \
	filesys-file-atime-1.ses \
	regex-re-compile-1.ses \
	regex-regexp-1.ses \
	regex-regsub-1.ses \
	regex-regextract-1.ses \
	sock-serv-by-name-1.ses \
	sock-serv-by-port-1.ses \
	sock-host-by-name-1.ses \
	sock-host-by-addr-1.ses \
	functional-map-1.ses \
	functional-filter-1.ses \
	functional-fold-1.ses \
	functional-foldr-1.ses

all: $(EXAMPLES)

%.ses: %.j
	cat $< | $(JOSL) --example | sed -e s/\n\r/\n/ > $@

