-variable ary

main
  10 array ary !            # Create an array to contain 10 values
  "John" 0 ary @ !          # Set element 0 to John
  "Jane" 1 ary @ !          # Set element 1 to Jane
  "Jim"  2 ary @ !          # Set element 2 to Jim
  ary @ len println         # 10 (3-9 are NULL but exist in the array)
  1 ary @ @ println         # Prints "Jane"
