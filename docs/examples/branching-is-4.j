main
  "Dr. John" is
    "Jane"              do "Jane, help!" end
    r/Dr. [A-Z][a-z]+/  do "Doctor, I need help!" end
    r/Mr. [A-Z][a-z]+/  do "Mister, can you help me?" end
    r/Mrs. [A-Z][a-z]+/ do "Miss, can you please help me?" end
  end

# Result is: "Doctor, I need help!"
