-variable person

main
  dict person !                           # Create the dictionary,
                                          # assign to person variable
  "John" "name" person @ !                # Set name to John
  51 "age" person @ !                     # Set age to 51
  "name" person @ @                       # Get the name
  "age"  person @ @                       # Get the age
  "{1} is {0} years old." format println  # Print a message
  "age" person @ has-key? if              # Does it have the age key?
    "age" person @ remove                 # Remove it
  end
  person @ keys each println next         # name, age
  person @ values each println next       # John, 51
