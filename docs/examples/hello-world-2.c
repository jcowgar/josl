#include <stdio.h>
/*
 * Say hello to someone
 *
 * Parameters: char *name
 */
void say_hello (const char *name)
{
  printf ("Hello, %s!\n", name);
}

/*
 * Main application entry point
 */
int main (int argc, char **argv)
{
  if (argc == 1) /* 1 because in C 0 is always app name */
    say_hello ("World");
  else
    say_hello (argv[1]);

  return 0;
}
