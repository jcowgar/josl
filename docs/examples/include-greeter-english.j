#
# English Greeter Library (greeter-english.j)
#

-public say-hello # make say-hello accessible to others

say-hello "Hello, {0}!" format println
