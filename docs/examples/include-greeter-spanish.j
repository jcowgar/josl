#
# Spanish Greeter Library (greeter-spanish.j)
#

-public say-hello # make say-hello accessible to others

say-hello "Hola, {0}!" format println
