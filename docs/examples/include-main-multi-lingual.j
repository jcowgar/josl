#
# Multi-lingual greeter application
#

-include greeter-english.j as en
-include greeter-spanish.j as es

main
  "John" en.say-hello # Greet in English
  "John" es.say-hello # Greet in Spanish
