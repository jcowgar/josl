=== Boolean Words

Boolean words work on all intrinsic types. This means that you can compare two
integers, two doubles, two strings, two pointers or two anything. Further, words
such as `<`, `>` and `between?` work on all intrinsic types as well.
`"John" "Anne" "William" between?`
is perfectly valid and will result in a true value as `John`
is somewhere in between `Anne` and `William`.

==== Star Variants

Many comparators have a "star" variant that will leave the compared values
on the stack. For example, the word signature for `>` is:
`> ( x1 x2 -- b )` however, it's star variant has a
signature of: `*> ( x1 x2 -- x1 x2 b )`. This will
come in handy when you wish to test a value then use it. The star
variants are cleaner, less code and more efficient than using a duplicate
word such as `2dup > if ... end`.

These star variants are not described in each word description. They
are included in the section title and as a separate word signature to
show their presence.

==== Equality

===== = and *=

*Synopsis*

----
= ( x1 x2 -- b )
*= ( x1 x2 -- x1 x2 b )
----

Is `x1` equal to `x2`?

*Example*

[source,conf]
----
include::examples/boolean-equal-1.ses[]
----

WARNING: Different types are not equal. This makes sense in most cases, however,
this may change for some types. For example, 1 as an integer and 1.0 as a double,
should they be considered equal?
