#
# Famous apple refactor example.
#
# Situation, you've written an application that tracks the number of apples you currently
# have. Now in the 11th hour you realize you must track green apples and red apples.
# Everything has to change, or does it?
#
# The original application looked like:
#
#  -variable apples
#  main
#    20 apples !
#    apples @ println
#
# The new application does not change any core code.

-variable
  color     # pointer to the current tally
  reds      # tally of red apples
  greens    # tally of green apples

red    reds color !     # set apple-type to red
green  greens color !   # set apple-type to green
apples color @          # address of the current apple tally

main
  red                   # Set the apple pointer to the reds

  # Our main application
  20 apples !
  apples @ println

  # Now, let's work on green apples
  green                 # Set our apples tally to green
  10 apples !           # Add 10 apples to our green apple count
  apples @ println      # Show how many green apples we have

  # To prove we are tracking multiple apple types, let's see
  # the red count again.
  red
  apples @ println

