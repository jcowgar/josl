#!/usr/bin/env josl

# ==========================================================================================
# Join two paths into one
#
# ( path1 path2 -- path )

file-join swap "/" join

# ==========================================================================================
# Determine if filea is newer than fileb
#
# ( filenameA filenameB -- boolean )

file-newer? file-mtime swap file-mtime <

# ==========================================================================================
# Determine if the given filename is a relative directory, i.e. "." or ".."
#
# ( filename -- boolean )

is-relative-dir? dup "." = swap ".." = or?

# ==========================================================================================
# Backup files from source to dest. Only files that are outdated are copied.
#
# ( source dest -- )

backup 
  2dup dup dir? 0= if dir-create drop else drop end
  dir-open
  while dup dir-next dup null? 0= do
    dup is-relative-dir? if
      drop
    else
      dup 4 pick file-join
      swap 3 pick file-join
      over dir? if
        backup
      else
        1 pick print "... " print
        2dup file-newer? if
          file-copy 0= if "failed" else "copied" end
        else
          2drop "skipped"
        end println
      end
    end
  end drop
  dir-close
  2drop

# ==========================================================================================
# Ensure that the proper command line arguments have been sent to our
# program. If not, a usage message is displayed and the program is terminated.
#
# ( -- )

ensure-params argc 2 = not if "usage: backup.j source dest" println 1 exit

# ==========================================================================================
# Our main entry point

main ensure-params 0 argv 1 argv backup
