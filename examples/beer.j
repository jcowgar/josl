bottles-of-beer
  times
    ii ii 1-
    "{1} bottles of beer on the wall"+
    "{1} bottles of beer."+
    "Take one down, pass it around"+
    "{0} bottles of beer on the wall" format println
  next

main 99 bottles-of-beer
