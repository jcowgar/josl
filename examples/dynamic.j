#
# Example of making new words during runtime.
#

make-inc-word dup "inc-by-{0} {1} +" format parse

main
   5 make-inc-word
  10 make-inc-word

  1 inc-by-10 println
  2 inc-by-5 println
