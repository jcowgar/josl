#
# Fibonacci Example
#

# Include our simple benchmarking tools for ease of showing a time difference
-include bench.j

# Compute the Fibonacci number to N in an optimized loop.
#
# ( i -- i )

fib-opt
  dup 1= if 0 return end
  dup 2 = if 1 return end
  0 1 rot
  while dup 2 > do
    2 pick 2 pick + 2swap nip swap rot 1-
  end
  drop +

# Compute the Fibonacci number to N in a recursive manner.
#
# While easy to read, this is not the most efficient way due to the
# recursive function call.
#
# ( i -- i )

fib-rec dup 1 > if dup 2- fib-rec swap 1- fib-rec +

# Benchmark a fibonacci word
#
# ( i:count w:word s:name -- )

bench-fib "{0}: " format print bench-start exec-word bench-stop "{1}, {0} seconds" format println

ensure-params argc not if "usage: josl fibo.j #" println 0 exit
main ensure-params
  0 argv >int dup # get the fib number from the command line
                  # dup it because we are going to run 2 fib calculations

  @fib-opt "optimized" bench-fib
  @fib-rec "recursive" bench-fib

