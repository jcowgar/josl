#
# Fixed Width Parser
#

integer 0
double  1
string  2

get-field swap cut swap trim rot vector >int >dbl >str end
parse-line swap times get-field >t next drop

#
# Example Use (person fixed width parser)
#
person-layout integer 3 string 10 2
show-person 2 times >s next "{0} is {1} years old" format println

main
  person-layout "John Doe   13" parse-line show-person
  person-layout "Jane Smith112" parse-line show-person

