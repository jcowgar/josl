#
# Show off some various for loops
#

nl       "\n" print
sp       " " print
saystart print "start: " print
sayend   ":end" println

simple "simple: " saystart 10 0 for i print sp next sayend
nested "nested: " saystart nl
  5 1 for
    10 5 for
      j print ":" print i print sp
    next
    nl
  next
  sayend

main simple simple simple nested
