#
# Helper functions to assist in making a greeter application
#

-public say-hello say-goodbye

# ( sName -- )
say-hello "Hello, " print print "!" println

# ( sName -- )
say-goodbye "Goodbye, " print print "!" println

