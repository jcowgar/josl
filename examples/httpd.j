#!/usr/bin/env josl
#
# Simple HTTP server for example purposes
#
# Note: This is not designed for or to be used in production.
# It is soley for an example application using various words
# defined in Josl, especially as a simple socket server.
#
# Try accessing http://localhost:8080/http.j or add a few
# HTML or image files to the current directory and access
# those.
#
# You can change what IP and Port are bound as well as the
# document root:
#
# Usage: josl http.j [-docroot root] [-bind-port port-num] [-bind-addr addr]
#

-include io.j

-variable docroot bind-addr bind-port mime-types

error-404
  "<html><head><title>404 Not Found</title></head>".
  "<body>".
  "<h1>404 Not Found</h1><p>{0} was not found on this server.</p>".
  "</body>".
  "</html>"

dir-page
  "<html>".
  "<head><title>Directory Listing {1}</title></head>".
  "<body>".
  "<h1>Directory Listing {1}</h1>".
  "<ul>{0}</ul>".
  "</body>".
  "</html>"

dir-entry "<li><a href='{0}'>{0}</a></li>"

response-string
  "HTTP/1.0 {0}"+
  "Content-Type: {1}"+
  "Connection: close"+
  "Content-length: {2}"+
  ""+
  "{3}"

# ( s:filename -- s:extension )
file-extension dup 46 rfind dup -1= if drop "" else 1+ over len mid end

# ( d:dict s:value s:key -- d:dict )
,d 2 pick !

# ( s:filename -- s:mime-type )
get-mime-type file-extension mime-types @ @ dup null?
  if drop "default" mime-types @ @ end

# ( sk:client-socket -- )
handle-request
  dup 512 swap read-string " " 2 split# 3 = if
    drop nip dup "/" = if
      docroot @ "{0}/index.html" format dup
      file-exists? 1= if nip else drop end
    else
      docroot @ "{0}{1}" format
    end

    dup is
      { dir? }
        do
          sbuf over dir-open
          while dup dir-next dup null? 0= do
            dir-entry format 2 pick append
          end drop dir-close
          >str dir-page format dup len "text/html" 200
        end
      { file-exists? }
        do
          dup read-file drop dup len rot get-mime-type 200
        end
    else
      drop error-404 format dup len "text/html" 404
    end

    response-string format
    0 2 pick write
  end
  close

main
  "." docroot !
  "0.0.0.0" bind-addr !
  8080 bind-port !
  argc times
    i argv
    dup "-docroot"   = if i 1+ argv docroot ! end
    dup "-bind-addr" = if i 1+ argv bind-addr ! end
    "-bind-port"     = if i 1+ argv >int bind-port ! end
  2 +next
  docroot @ bind-addr @ bind-port @ "docroot={2}, bind={1}:{0}" format println

  dict "text/html"  "htm" ,d
       "text/html"  "html" ,d
       "text/css"   "css" ,d
       "image/png"  "png" ,d
       "image/jpeg" "jpg" ,d
       "image/jpeg" "jpeg" ,d
       "text/plain" "default" ,d
  mime-types !

  IPPROTO-TCP SOCK-STREAM AF-INET socket
  bind-port @ bind-addr @ 2 pick bind 0= not if
    bind-port @ bind-addr @ "Could not bind {0}:{1}" format println 1 exit
  else
    "waiting..." println
  end
  while 0 over listen 0= do
    dup accept handle-request
  end close
