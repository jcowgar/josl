#
# Example of the if word
#

# When at the tail end of a function, if no endif is found then the
# if is terminated at the end of the function

endless 1 if "1 is true... endless" println

main
  "start" println
  1 if "1 is true... ended" println end
  endless
  "done" println
