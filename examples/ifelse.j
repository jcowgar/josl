main
    # Should enter the "if" parof the if
    1 if
        "Good, 1 is true."
    else
        "Hm, 1 is not true?"
    end println

    # Should enter the "else" part of the if
    0 if
        "Hm, 0 is true?"
    else
        "Good, 0 is not true."
    end println
