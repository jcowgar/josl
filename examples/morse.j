#
# Simple morse code translator
#

-variable morse-table

dup>t dup >t
dup>s >s dup>t

# ( ... i:pair-count -- d )
>dict dict >t times dup>s ! next >s

morse-init
    ".-"    "a" "-..."  "b" "-.-."  "c" "-.."   "d"
    "."     "e" "..-."  "f" "--."   "g" "...."  "h" 
    ".."    "i" ".---"  "j" ".-."   "k" ".-.."  "l"
    "--"    "m" "--."   "n" "---"   "o" ".--."  "p"
    "--.-"  "q" ".-."   "r" "..."   "s" "-"     "t"
    "..-"   "u" "...-"  "v" ".--"   "w" "-..-"  "x"
    "-.--"  "y" "--.."  "z"
    "-----" "0" ".----" "1" "..---" "2" "...--" "3"
    "....-" "4" "....." "5" "-...." "6" "--..." "7"
    "---.." "8" "----." "9"
    ".-.-.-"  "." "--..--"  "," "..--.."  "?"
    ".----."  "'" "-.-.--"  "!" "-..-."   "/"
    "-.--."   "(" "-.--.-"  ")" ".-..."   "&"
    "---..."  ":" "-.-.-."  "s" "-...-"   "="
    ".-.-."   "+" "-....-"  "-" "..--.-"  "_"
    ".-..-." "\"" "...-..-" "$" ".--.-."  "@"
    "   " " " "\n" "\n" 56 >dict morse-table !

morse # ( s -- )
  morse-table @ null? if morse-init end
  lower while peel dup do
    chr morse-table @ @ dup null? if drop "?" end
    print dup len 0 > if " " print end
  end "" println

main argc 0 > if 0 argv else "Hello, World!\nHow are you?" end morse
