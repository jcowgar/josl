#
# Print a file to stdout
#

ensureparams argc 1 < if "usage: pfile filename" println 1 exit

line-by-line
  0 argv "rb" open while
    dup get-str dup is-string?
  do
    print
  end
  drop
  close

easy-way 0 argv readfile drop println

main ensureparams easy-way
