#
# Perform the rot13 translation on a file
#

# One method
rot13translate
  "ANBOCPDQERFSGTHUIVJWKXLYMZNAOBPCQDRESFTGUHVIWJZKYLZM".
  "anbocpdqerfsgthuivjwkxlymznaobpcqdresftguhviwjzkylzm"
  translate println

# Another method
rot13ch
  dup lower? if 84 - 26 % 97 + return end
  dup upper? if 52 - 26 % 65 +
rot13 dup len times peel rot13ch chr print next drop "\n" print

# Rot47 using the second method
rot47ch
  dup 33 79 between? if 47 + return end
  dup 80 126 between? if 47 - end
rot47 dup len times peel rot47ch chr print next drop "\n" print

ensureparams argc 1 < if "usage: rot13 filename" println 1 exit

main ensureparams 0 argv readfile if rot13 else println end

