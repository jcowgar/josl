#
# Simple math (+ - / *)
#

message print println
main 10 10 + "10 + 10 = " message
     20 10 - "20 - 10 = " message
     30 10 * "30 * 10 = " message
     40 10 / "40 / 10 = " message
