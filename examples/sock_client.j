#
# Socket Client example, launch sock_server.j first
#

main@failed?
  is
    ConnectionRefused?
      do
        "Could not connect to the server."+
        "Did you start sock_server.j first?" println
        1 exit
      end

    ConnectionReset?
      do
        "The connection with the server was terminated, exiting."
        println
        1 exit
      end
  else
    "An unhandled exception occured: {0}" format println
  end

main
  IPPROTO-TCP SOCK-STREAM AF-INET socket
  5000 "127.0.0.1" 2 pick sock-open
  begin
    "Say to server (or 'quit'): " print
    STDIN read-line trim dup "quit" = if
      drop 0
    else
      "{0}\n" format 0 2 pick write
      512 over read-string "Server said: {0}" format println
      1
    end
    0=
  until close
