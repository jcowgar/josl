#
# Socket Server example, use sock_client.j to communicate with it.
#

check-null? dup null? if drop 1 else 0 end

handle-request@failed?
  is
    ConnectionReset?
      do
        close # close our client socket
        resume
      end
  else
    "Handle request failed: {0}" format println
  end

handle-request
  "client connected, handling!" println
  begin
    512 over read-string check-null? 0= if
      trim dup "   client sent: {0}" format println
      "Server received: {0}\n" format 0 2 pick write
      1
    else
      0
    end
  0= until close

server@failed? "Server failed: {0}" format println
server
  5000 0 2 pick bind
  0 over listen
  while 1 do
    "waiting for connection (CTRL+C to terminate)... " print
    dup accept handle-request
  end close

main IPPROTO-TCP SOCK-STREAM AF-INET socket server
