-include sqlite3.j

# app wide database handle

-variable db

# Show error information, close the database and exit
#
# ( -- )

show-errors
  db @ sqlite-errcode db @ sqlite-errmsg "{1}: {0}" format println
  0 exit

# Our main application

main
  sqlite-version "SQLite version: {0}" format println

  "hello.db" sqlite-open swap db ! 0= not if show-errors end
  null null null "create table people (name varchar(20))" db @ sqlite-exec drop

  "insert into people values (?)" db @ sqlite-prepare 0= not if show-errors end
  "John Doe" 1 2 pick sqlite-bind "bind: {0}" format println
  dup sqlite-step "step: {0}" format println
  sqlite-finalize "finalize: {0}" format println

  null null null "SELECT name FROM people" db @ sqlite-prepare 0= not if show-errors end
  while dup sqlite-step 100 = do
    0 over sqlite-value "Value: {0}" format println
  end
  sqlite-finalize
  db @ sqlite-close
