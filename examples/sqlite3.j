#
# SQLite v3 binding for Josl
#

-public
  sqlite-open sqlite-close
  sqlite-exec sqlite-prepare sqlite-step sqlite-finalize sqlite-reset
  sqlite-bind sqlite-value
  sqlite-version sqlite-errcode sqlite-errmsg

-ffi-library sqlite3 sqlite3

-ffi-word so-open sqlite3 sqlite3_open ffi-int ffi-string ffi-pointer
-ffi-word so-close sqlite3 sqlite3_close ffi-int ffi-pointer
-ffi-word so-exec sqlite3 sqlite3_exec
  ffi-int ffi-pointer ffi-string ffi-pointer ffi-pointer ffi-pointer
-ffi-word so-prepare sqlite3 sqlite3_prepare
  ffi-int ffi-pointer ffi-string ffi-int ffi-pointer ffi-pointer
-ffi-word so-step sqlite3 sqlite3_step ffi-int ffi-pointer
-ffi-word so-finalize sqlite3 sqlite3_finalize ffi-int ffi-pointer
-ffi-word so-reset sqlite3 sqlite3_reset ffi-int ffi-pointer
-ffi-word so-bind-null sqlite3 sqlite3_bind_null ffi-int ffi-pointer ffi-int
-ffi-word so-bind-int64 sqlite3 sqlite3_bind_int64
  ffi-int ffi-pointer ffi-int ffi-int64
-ffi-word so-bind-double sqlite3 sqlite3_bind_double
  ffi-int ffi-pointer ffi-int ffi-double
-ffi-word so-bind-text sqlite3 sqlite3_bind_text
  ffi-int ffi-pointer ffi-int ffi-string ffi-int ffi-pointer
-ffi-word so-column-type sqlite3 sqlite3_column_type ffi-int ffi-pointer ffi-int
-ffi-word so-column-int64 sqlite3 sqlite3_column_int64
  ffi-int64 ffi-pointer ffi-int
-ffi-word so-column-double sqlite3 sqlite3_column_double
  ffi-double ffi-pointer ffi-int
-ffi-word so-column-text sqlite3 sqlite3_column_text
  ffi-string ffi-pointer ffi-int
-ffi-word so-version sqlite3 sqlite3_libversion ffi-string
-ffi-word so-errcode sqlite3 sqlite3_errcode ffi-int ffi-pointer
-ffi-word so-errmsg sqlite3 sqlite3_errmsg ffi-string ffi-pointer

# Open/create a SQLite database
#
# ( filename -- db status )

sqlite-open    4 malloc tuck swap so-open @ ffi-exec swap @ swap

# Close a SQLite database
#
# ( db -- status )

sqlite-close   so-close @ ffi-exec drop

# Execute a SQL statement directly
#
# ( sql db -- status )

sqlite-exec    so-exec @ ffi-exec

# Prepare a SQL statement for execution
#
# ( sql db -- status )

sqlite-prepare
  null 4 malloc dup >t                   # db sql null ptr
  2swap swap                             # null ptr db sql
  tuck len -rot                          # null ptr sql db len
  so-prepare @ ffi-exec >s @ swap # result-ptr status

# Step through a prepared statement
#
# ( stmt -- status )

sqlite-step so-step @ ffi-exec

# Reset the prepared statement making it ready for later execution
#
# ( stmt -- status )

sqlite-reset so-reset @ ffi-exec

# Finalize the prepared statement closing it for no further use
#
# ( stmt -- status )

sqlite-finalize so-finalize @ ffi-exec

# Bind a value to a positional index in the given statement
#
# ( value position stmt -- result )

sqlite-bind
  2 pick type is
    T-NULL    do so-bind-null end
    T-INTEGER do so-bind-int64 end
    T-DOUBLE  do so-bind-double end
    T-STRING  do rot dup len swap 2swap 0 4 -roll so-bind-text end
    T-BUFFER  do rot >str dup len swap 2swap 0 4 -roll so-bind-text end
  else
    "unknown type: {0}" format StackType? fail
  end @ ffi-exec

# Retrieve the content from an executed statement
#
# ( column-index stmt -- value )

sqlite-value
  2dup so-column-type @ ffi-exec 1- vector so-column-int64 so-column-double so-column-text null null end
  @ ffi-exec

# Retrieve the SQLite version being used
#
# ( -- version-string )

sqlite-version so-version @ ffi-exec

# Retrieve the last error code
#
# ( db -- error-code )

sqlite-errcode so-errcode @ ffi-exec

# Retrieve the last error message
#
# ( db -- error-message )

sqlite-errmsg  so-errmsg @ ffi-exec
