#
# Times loop example. This example also shows a nested loop and retrieving the
# loop index via I
#

main
    10 times
        i print ":" print
        10 times
            i print
            "," print
        next
        "done" println
    next
