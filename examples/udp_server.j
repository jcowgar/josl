#
# UDP Server Example
#
# Hint: use udp_client.j to talk to this server
#

main
  IPPROTO-UDP SOCK-DGRAM AF-INET socket
  27015 0 2 pick bind
  64 over udp-read "Client said: '{0}'" format println
  close
