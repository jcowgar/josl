#
# Example of using a variable
#

# Accessor words that could be used if this file were included
# in another Josl program.

-public set-name get-name

set-name .name!
get-name .name

# ( -- )
say-hello
  "Hello, " print
  .name print          # Print the name variable
  "!" println

# ( -- )
#
# Say hello using our accessor functions
# and use format for an easier word, the prior
# say-hello used an awkward way just so you could
# see exactly what was happening.
#

say-hello2 get-name "Hello (via an accessor word), {0}!" format println

#
# Main application entry word
#

main
  "John" .name!        # Set the name variable to be "John"
  say-hello
  "Jim" set-name
  say-hello2

