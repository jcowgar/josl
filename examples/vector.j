#
# Simple example of using the vector conditional statement
#

morning   "Good morning"
afternoon "Good afternoon"
evening   "Good evening"
greet     vector morning afternoon evening end

main
    2 greet println
