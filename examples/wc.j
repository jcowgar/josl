#
# Word count suitable for counting characters, words and lines
# of a Josl program.
#

-variable char# word# line# inword

outword! 0 inword !
inword!  1 inword !
inword?  inword @ 1=
inc      dup @ 1 + swap !    # Should be built in

content-count
  # Reset all of our counters and states
  0 0 0 0 char# ! word# ! line# ! inword !

  # Iterate through each character in the file
  dup "r" file-open
  while
    dup read-char dup 0 >
  do
    33 255 *between? if   # Is this a Josl word character?
      inword? 0= if          # Are we currently in a Josl word?
        inword!              # No, put us there
        word# inc            # Increment the word counter
      end
      drop                   # Drop our no longer needed ch
    else                     # Not a Josl word character
      outword!               # Make sure we don't think we are in a Josl word
      10 = if                # Do we have a line ending?
        line# inc            # We have a new line as well
      end
    end
    char# inc                # Increment the character counter
  end drop close

  # Show a nice summary of our findings
  char# @ word# @ line# @ "| {0:5i} | {1:5i} | {2:5i} | {3}" format println

header "| Lines | Words | Chars | Filename"+
       "|-------|-------|-------|------------------------------------" println

checkparams argc 0= if "usage: wc filename1 [filename2 ...]" println 1 exit
main checkparams header argc times i argv content-count next
