/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_H
#define JOSL_H

/*!

  \mainpage Josl C API

  Define a VM interpreter structure via josl_init. Call
  josl_parse_string or josl_parse_file. Push values
  and pop values onto and off of the stack via the various
  stack words.

*/

#define JOSL_MAX_STACK 2048
#define JOSL_MAX_TSTACK 1024

#ifndef bool
#define bool int
#define FALSE 0
#define TRUE !FALSE
#endif /* ifndef bool */

#include <josl/hashtable.h>
#include <josl/hashtable_itr.h>

typedef struct hashtable josl_hash;
typedef struct hashtable_itr josl_hash_itr;

typedef long long int64;

typedef struct _josl josl;

#include <tgc.h>

extern tgc_t gc;

#include <josl/error.h>
#include <josl/value.h>

/*! Main VM structure */

struct _josl
{
  josl_hash *intrinsics;        /*!< all intrinsic top level words */
  josl_hash *filewords;         /*!< filename -> hash of words in the file */
  josl_hash *constants;         /*!< constants */
  int excCode;                  /*!< current exception code */
  josl_error_message *errorStack;       /*!< errors are pushed on here. If this
                                           becomes non-NULL, the Josl VM is in
                                           an error state */
  bool debugLevel;              /*!< debug level 0, 1, ... the higher the more output */
  int sp;                       /*!< current position - 1 of TOS */
  josl_value **s;               /*!< our stack */
  int tsp;                      /*!< current position - 1 of TOS for temp stack */
  josl_value **ts;              /*!< our temporary stack */
};

/*! Argument count for args not consumed by main.c */
extern int josl_argc;

/*! Arguments not consumed by main.c */
extern char **josl_argv;

#endif /* JOSL_H */
