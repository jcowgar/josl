/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_ERROR_H
#define JOSL_ERROR_H

/* errno values that are not on all platforms */
#ifndef ENOSTR
#define ENOSTR -10000
#endif

#ifndef ENODATA
#define ENODATA -10001
#endif

#ifndef ETIME
#define ETIME -10002
#endif 

#ifndef ENOSR
#define ENOSR -10003
#endif

typedef enum
{
  josl_ok = 0,
  josl_exc_resume,
  josl_exc_resume_this,
  josl_exc_retry,
  josl_exc_handled,
  josl_err_address_family_not_supported,
  josl_err_address_in_use,
  josl_err_address_not_available,
  josl_err_again,
  josl_err_already_exists,
  josl_err_arg_list_too_big,
  josl_err_bad_address,
  josl_err_bad_file_no,
  josl_err_bad_message,
  josl_err_bad_protocol_option,
  josl_err_broken_pipe,
  josl_err_call_cancelled,
  josl_err_call_stack,
  josl_err_cannot_translate_name,
  josl_err_class_type_not_found,
  josl_err_connection_aborted,
  josl_err_connection_refused,
  josl_err_connection_reset,
  josl_err_connection_timed_out,
  josl_err_cross_device_link,
  josl_err_database_query_refused,
  josl_err_destination_address_required,
  josl_err_device_resource_busy,
  josl_err_dir_not_empty,
  josl_err_disk_quota_exceeded,
  josl_err_escape_char,
  josl_err_exec_format,
  josl_err_ffi_declaration,
  josl_err_ffi_register_failed,
  josl_err_file_too_big,
  josl_err_filename_too_long,
  josl_err_format,
  josl_err_host_is_down,
  josl_err_host_not_found,
  josl_err_host_unreachable,
  josl_err_illegal_byte_sequence,
  josl_err_include_as,
  josl_err_include_filename,
  josl_err_include_namespace,
  josl_err_incomplete_string,
  josl_err_index_out_of_bounds,
  josl_err_interrputed_func,
  josl_err_invalid_argument,
  josl_err_invalid_branch_word,
  josl_err_invalid_handle,
  josl_err_invalid_parameter,
  josl_err_invalid_regex,
  josl_err_invalid_seek,
  josl_err_invalid_word_reference,
  josl_err_io,
  josl_err_io_incomplete,
  josl_err_io_pending,
  josl_err_is_a_directory,
  josl_err_item_is_remote,
  josl_err_memory,
  josl_err_message_too_long,
  josl_err_multiline_string,
  josl_err_name_not_unique,
  josl_err_name_too_long,
  josl_err_network_is_down,
  josl_err_network_is_unreachable,
  josl_err_network_reset,
  josl_err_network_system_not_ready,
  josl_err_no_buffer_space_available,
  josl_err_no_data,
  josl_err_no_link,
  josl_err_no_locks,
  josl_err_no_message,
  josl_err_no_more_results,
  josl_err_no_space_left,
  josl_err_no_spawned_child,
  josl_err_no_streams_available,
  josl_err_no_such_device,
  josl_err_no_such_file,
  josl_err_no_such_process,
  josl_err_no_top_word,
  josl_err_no_tty,
  josl_err_nonauthoritative_host_not_found,
  josl_err_nonrecoverable_error,
  josl_err_not_a_directory,
  josl_err_not_a_stream,
  josl_err_not_boolean,
  josl_err_not_comparable,
  josl_err_not_enough_memory,
  josl_err_not_impl,
  josl_err_not_on_network,
  josl_err_not_socket,
  josl_err_not_user_word,
  josl_err_op_aborted,
  josl_err_op_already_in_progress,
  josl_err_op_in_progress,
  josl_err_op_not_permitted,
  josl_err_op_not_supported,
  josl_err_parse,
  josl_err_parse_file_not_found,
  josl_err_parse_unknown_word,
  josl_err_path_not_found,
  josl_err_permission_denied,
  josl_err_procedure_table_invalid,
  josl_err_protocol_error,
  josl_err_protocol_family_not_supported,
  josl_err_protocol_not_supported,
  josl_err_read_only_fs,
  josl_err_regex_not_supported,
  josl_err_remote_address_changed,
  josl_err_resource_deadlock,
  josl_err_resource_temporarily_unavailable,
  josl_err_result_too_large,
  josl_err_ro_file_system,
  josl_err_service_init_failed,
  josl_err_service_invalid,
  josl_err_service_not_found,
  josl_err_shutdown_in_progress,
  josl_err_socket_already_connected,
  josl_err_socket_not_connected,
  josl_err_socket_type_not_supported,
  josl_err_socket_was_shutdown,
  josl_err_stack_overflow,
  josl_err_stack_size,
  josl_err_stack_type,
  josl_err_stack_underflow,
  josl_err_stale_file_handle,
  josl_err_string_truncated,
  josl_err_system_call_failure,
  josl_err_timer_expired,
  josl_err_too_many_files_opened,
  josl_err_too_many_links,
  josl_err_too_many_processes,
  josl_err_too_many_references,
  josl_err_tstack_overflow,
  josl_err_tstack_underflow,
  josl_err_unknown,
  josl_err_unknown_constant,
  josl_err_unknown_word,
  josl_err_user_quota_exceeded,
  josl_err_variable_undef,
  josl_err_winsock_not_initialized,
  josl_err_winsock_version_error,
  josl_err_winspecific,
  josl_err_wrong_protocol
} josl_error_code;

typedef struct _josl_error_message
{
  const char *filename;
  int lineNo;
  int colNo;
  int code;
  const char *message;
  struct _josl_error_message *prev;
} josl_error_message;

#include <josl.h>
#include <josl/vm.h>

void josl_error_new (josl * interp, const int code, const char *message);
void josl_error_new_word (josl * interp, const josl_word * word,
                          const int code, const char *message);
void josl_error_new_file (josl * interp, const char *filename,
                          const int lineNo, const int colNo,
                          const int code, const char *message);
void josl_error_display (josl * interp);

#endif /* JOSL_ERROR_H */
