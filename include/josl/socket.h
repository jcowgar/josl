/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_SOCKET_H
#define JOSL_SOCKET_H

#ifdef WINDOWS
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#ifdef FREEBSD
#include <netinet/in.h>
#endif /* FREEBSD */
#define SOCKET int
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1
#define SOCKADDR struct sockaddr_in
#define closesocket close
#endif /* WINDOWS */

#ifndef SOCKET_ERROR
#define SOCKET_ERROR (-1)
#endif

#ifdef AF_BTH
#define AF_BLUETOOTH AF_BTH
#endif

#ifndef AF_UNIX
#define AF_UNIX -1
#endif

#ifndef AF_BLUETOOTH
#define AF_BLUETOOTH -1
#endif

#ifndef IPPROTO_GGP
#define IPPROTO_GGP -1
#endif

#ifndef IPPROTO_ND
#define IPPROTO_ND -1
#endif

#define SOCK_BUFF_SIZE 4096

/* Socket Error Codes */
#define ERR_OK                     0
#define ERR_ACCESS                -1
#define ERR_ADDRINUSE             -2
#define ERR_ADDRNOTAVAIL          -3
#define ERR_AFNOSUPPORT           -4
#define ERR_AGAIN                 -5
#define ERR_ALREADY               -6
#define ERR_CONNABORTED           -7
#define ERR_CONNREFUSED           -8
#define ERR_CONNRESET             -9
#define ERR_DESTADDRREQ           -10
#define ERR_FAULT                 -11
#define ERR_HOSTUNREACH           -12
#define ERR_INPROGRESS            -13
#define ERR_INTR                  -14
#define ERR_INVAL                 -15
#define ERR_IO                    -16
#define ERR_ISCONN                -17
#define ERR_ISDIR                 -18
#define ERR_LOOP                  -19
#define ERR_MFILE                 -20
#define ERR_MSGSIZE               -21
#define ERR_NAMETOOLONG           -22
#define ERR_NETDOWN               -23
#define ERR_NETRESET              -24
#define ERR_NETUNREACH            -25
#define ERR_NFILE                 -26
#define ERR_NOBUFS                -27
#define ERR_NOENT                 -28
#define ERR_NOTCONN               -29
#define ERR_NOTDIR                -30
#define ERR_NOTINITIALISED        -31
#define ERR_NOTSOCK               -32
#define ERR_OPNOTSUPP             -33
#define ERR_PROTONOSUPPORT        -34
#define ERR_PROTOTYPE             -35
#define ERR_ROFS                  -36
#define ERR_SHUTDOWN              -37
#define ERR_SOCKTNOSUPPORT        -38
#define ERR_TIMEDOUT              -39
#define ERR_WOULDBLOCK            -40
#define ERR_INVALID_SOCKET        -41

#endif /* JOSL_SOCKET_H */
