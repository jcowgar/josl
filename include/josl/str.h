/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_STR_H
#define JOSL_STR_H

typedef struct
{
  int bufsize;
  int len;
  char *data;
} charbuf;

#include <stdlib.h>
#include <stdarg.h>
#include <josl.h>

unsigned int strhash (void *key);
int strhasheq (void *a, void *b);

int64 string_to_int64 (const char *s);
double string_to_double (const char *s);

char *strdup_printf (const char *fmt, ...);
char *strrstr (const char *s, const char *t);

#if defined(MINGW) || defined(OSX)
char *strndup (char const *s, size_t n);
#endif

charbuf *cbuf_alloc (const int size);
bool cbuf_realloc (charbuf * cb, const int newsize);
bool cbuf_assign (charbuf * cb, const char *str, int count);
bool cbuf_append_ch (charbuf * cb, const int ch);
bool cbuf_append_str (charbuf * cb, const char *str, int count);
bool cbuf_append_printf (charbuf * cb, const char *fmt, ...);
void cbuf_truncate (charbuf * cb, const int position);

#endif /* JOSL_STR_H */
