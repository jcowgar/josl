/*
 * Copyright (c) 2008,,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_VALUE_H
#define JOSL_VALUE_H

#include <ffi.h>

#include <josl.h>
#include <josl/socket.h>
#include <josl/str.h>
#include <josl/vm.h>

/*!

  \defgroup value Values

*/

/*!
  \ingroup value

  Value types

  \sa _josl_value
*/

typedef enum
{
  josl_vt_null,                 /*!< Null */
  josl_vt_integer,              /*!< Integer */
  josl_vt_double,               /*!< Double */
  josl_vt_string,               /*!< String */
  josl_vt_sbuffer,              /*!< String buffer */
  josl_vt_array,                /*!< Array */
  josl_vt_list,                 /*!< List  */
  josl_vt_list_item,            /*!< List item */
  josl_vt_dict,                 /*!< Dictionary */
  josl_vt_dict_it,              /*!< Dictionary iterator */
  josl_vt_pointer,              /*!< Generic pointer */
  josl_vt_variable,             /*!< Variable */
  josl_vt_regex,                /*!< Regular Expression */
  josl_vt_word,                 /*!< Reference to a word */
  josl_vt_socket,               /*!< Socket */
  josl_vt_ffi,                  /*!< Foreign Function Interface */
  josl_vt_loop                  /*!< Internal loop structure */
} josl_value_type;

typedef struct _josl_value josl_value;
typedef struct _josl_array josl_array;
typedef struct _josl_list josl_list;
typedef struct _josl_list_item josl_list_item;
typedef struct _josl_word_ref josl_word_ref;
typedef struct _josl_socket josl_socket;
typedef struct _josl_ffi josl_ffi;

/*!
  \ingroup value

  Josl value

  All josl values are represented by a _josl_value structure. The stack
  is actually an array of _josl_value's.

  \sa josl_value_type;
*/

struct _josl_value
{
  /*!

     Type of value contained in the value union.

     \sa josl_value_type

   */
  josl_value_type type;

  /*!

     Value union

     \sa josl_value_type

   */
  union
  {
    int64 i;                    /*!< josl_vt_integer   */
    double d;                   /*!< josl_vt_double    */
    char *s;                    /*!< josl_vt_string    */
    charbuf *b;                 /*!< josl_vt_sbuffer   */
    josl_array *a;              /*!< josl_vt_array     */
    josl_list *l;               /*!< josl_vt_list      */
    josl_list_item *li;         /*!< josl_vt_list_item */
    josl_hash *hd;              /*!< josl_vt_dict      */
    josl_hash_itr *hditr;       /*!< josl_vt_dict_it   */
    void *p;                    /*!< josl_vt_pointer   */
    josl_value *v;              /*!< josl_vt_variable  */
    void *regex;                /*!< josl_vt_regex     */
    josl_word_ref *wref;        /*!< josl_vt_word      */
    josl_socket *sock;          /*!< josl_vt_socket    */
    josl_ffi *ffi;              /*!< josl_vt_ffi       */
    josl_loop *loop;            /*!< josl_vt_loop      */
  } value;
};

struct _josl_word_ref
{
  int64 word_id;
  josl_top_word *word;
};

struct _josl_array
{
  int64 capacity;
  int64 size;
  josl_value **elems;
};

struct _josl_list_item
{
  josl_value *v;
  josl_list_item *next;
  josl_list_item *prev;
};

struct _josl_list
{
  int64 size;
  josl_list_item *head;
  josl_list_item *tail;
  josl_list_item *cur;
};

struct _josl_socket
{
  struct sockaddr_in *addr;
  SOCKET sock;
};

struct _josl_ffi
{
  int rettype;
  int argc;
  int argtypes[20];
  josl *vm;
  josl_top_word *topword;
  ffi_cif *cif;
  void *(*funcref)(void);
};

#define JOSL_FFI_VOID        0
#define JOSL_FFI_UINT8       1
#define JOSL_FFI_INT8        2
#define JOSL_FFI_UINT16      3
#define JOSL_FFI_INT16       4
#define JOSL_FFI_UINT32      5
#define JOSL_FFI_INT32       6
#define JOSL_FFI_UINT64      7
#define JOSL_FFI_INT64       8
#define JOSL_FFI_FLOAT       9
#define JOSL_FFI_DOUBLE     10
#define JOSL_FFI_UCHAR      11
#define JOSL_FFI_CHAR       12
#define JOSL_FFI_USHORT     13
#define JOSL_FFI_SHORT      14
#define JOSL_FFI_UINT       15
#define JOSL_FFI_INT        16
#define JOSL_FFI_ULONG      17
#define JOSL_FFI_LONG       18
#define JOSL_FF_LONG_DOUBLE 19
#define JOSL_FFI_POINTER    20
#define JOSL_FFI_STRING     21
#define JOSL_FFI_BUFFER     22
#define JOSL_FFI_OPOINTER   23
#define JOSL_FFI_MAX        23

#endif /* JOSL_VALUE_H */
