/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOSL_VM_H
#define JOSL_VM_H

typedef struct _josl_top_word josl_top_word;
typedef struct _josl_word josl_word;
typedef struct _josl_loop josl_loop;

#include <josl.h>
#include <josl/value.h>

/* *INDENT-OFF* */
typedef enum
  {
    /* internal non-parsable words */
    josl_w_push, josl_w_noop, josl_w_variable_access,
    josl_w_variable_assign, josl_w_variable_assign_keep,

    /* Exception Handling */
    josl_w_fail, josl_w_resume, josl_w_resume_this, josl_w_retry,

    /* System Information */
    josl_w_os,

    /* Memory */
    josl_w_mem_malloc,

    /* Dynamic Execution */
    josl_w_ffi_exec, josl_w_ffi_callback,
    josl_w_dynamic_word, josl_w_parse, josl_w_exec_word,
    josl_w_curry, josl_w_2curry, josl_w_3curry, josl_w_curry_pound,
    josl_w_curry_question, josl_w_compose,

    /* branch words */
    josl_w_if, josl_w_else, josl_w_end, josl_w_while, josl_w_do,
    josl_w_begin, josl_w_until, josl_w_each, josl_w_each_kv,
    josl_w_each_rev,
    josl_w_for, josl_w_times, josl_w_next,
    josl_w_plus_next, josl_w_vector,
    josl_w_return, josl_w_break, josl_w_continue,
    josl_w_i, josl_w_j, josl_w_k, josl_w_ii, josl_w_jj, josl_w_kk,
    josl_w_is, josl_w_do_is,

    /* non-dropping branch words */
    josl_w_star_if, josl_w_star_do, josl_w_star_until,

    /* stack words */
    josl_w_depth,
    josl_w_dup, josl_w_2dup, josl_w_3dup, josl_w_dup_pound,
    josl_w_drop, josl_w_2drop, josl_w_3drop, josl_w_drop_pound,
    josl_w_swap, josl_w_2swap, josl_w_3swap, josl_w_swap_pound,
    josl_w_over, josl_w_2over, josl_w_3over, josl_w_rot, josl_w_minus_rot,
    josl_w_2rot, josl_w_minus_2rot, josl_w_3rot, josl_w_minus_3rot, josl_w_roll,
    josl_w_minus_roll, josl_w_pick, josl_w_nip, josl_w_tuck, josl_w_dip,
    josl_w_2dip,
    josl_w_to_temp, josl_w_from_temp, josl_w_to_temp_count, josl_w_from_temp_count,

    /* Boolean */
    josl_w_equal, josl_w_star_equal,
    josl_w_equal_zero, josl_w_equal_one, josl_w_equal_minus_one,
    josl_w_not_equal, josl_w_star_not_equal,
    josl_w_not,
    josl_w_gt, josl_w_star_gt,
    josl_w_lt, josl_w_star_lt,
    josl_w_gt_equal, josl_w_star_gt_equal,
    josl_w_lt_equal, josl_w_star_lt_equal,
    josl_w_between, josl_w_star_between,
    josl_w_and, josl_w_or, josl_w_even, josl_w_odd,

    /* Math */
    josl_w_plus, josl_w_minus, josl_w_multiply, josl_w_divide, josl_w_mod,
    josl_w_one_plus, josl_w_one_minus, josl_w_two_plus, josl_w_two_minus,
    josl_w_negate, josl_w_abs,
    josl_w_min, josl_w_max, josl_w_ceil,
    josl_w_floor, josl_w_round, josl_w_round_to, josl_w_acos, josl_w_asin,
    josl_w_atan, josl_w_atan2, josl_w_cos, josl_w_cosh, josl_w_sin,
    josl_w_sinh, josl_w_tan, josl_w_tanh, josl_w_exp, josl_w_exp2, josl_w_expm1,
    josl_w_frexp,
    josl_w_log, josl_w_log1p, josl_w_log2, josl_w_log10, josl_w_modf, josl_w_pow,
    josl_w_sqrt, josl_w_fmod, josl_w_degrees, josl_w_radians,
    josl_w_rand, josl_w_srand,

    /* Bitmath */
    josl_w_bit_and, josl_w_bit_or, josl_w_bit_xor, josl_w_bit_not, josl_w_shift_right,
    josl_w_shift_left,

    /* Conversion */
    josl_w_to_string, josl_w_to_integer, josl_w_to_double,

    /* Type */
    josl_w_is_integer, josl_w_is_double, josl_w_is_number, josl_w_is_string,
    josl_w_is_pointer, josl_w_is_dict, josl_w_is_array, josl_w_is_list, josl_w_is_null,
    josl_w_is_variable, josl_w_is_word, josl_w_type, josl_w_null,

    /* String */
    josl_w_len, josl_w_append, josl_w_append_pound, josl_w_join,
    josl_w_join_pound, josl_w_left, josl_w_right, josl_w_mid,
    josl_w_lower, josl_w_upper, josl_w_title, josl_w_capitalize,
    josl_w_trim, josl_w_trim_head, josl_w_trim_tail, josl_w_split,
    josl_w_split_pound, josl_w_splitany, josl_w_splitany_pound, josl_w_asc,
    josl_w_chr, josl_w_find, josl_w_rfind, josl_w_find_pound, josl_w_rfind_pound,
    josl_w_cut, josl_w_peel, josl_w_delete, josl_w_replace, josl_w_insert, josl_w_format,
    josl_w_repeat, josl_w_reverse, josl_w_translate, josl_w_is_alnum,
    josl_w_is_alpha, josl_w_is_cntrl, josl_w_is_digit, josl_w_is_graph,
    josl_w_is_lower, josl_w_is_print, josl_w_is_punct, josl_w_is_space,
    josl_w_is_upper, josl_w_is_xdigit, josl_w_has_prefix, josl_w_has_suffix,
    josl_w_at,

    /* Date and Time */
    josl_w_clock, josl_w_time,

    /* Input/Output */
    josl_w_print, josl_w_println,

    josl_w_file_open, josl_w_close, josl_w_eof, josl_w_read_ch, josl_w_read_line,
    josl_w_read_str, josl_w_read_blob, josl_w_write, josl_w_writeln,

    /* System */
    josl_w_argc, josl_w_argv, josl_w_env, josl_w_system, josl_w_exit,

    /* File/Directory Words */
    josl_w_dir_open, josl_w_dir_next, josl_w_dir_close, josl_w_dir_create,
    josl_w_dir_remove, josl_w_file_size, josl_w_file_create_time, josl_w_file_access_time,
    josl_w_file_modify_time, josl_w_is_dir, josl_w_is_readable,
    josl_w_is_writable, josl_w_is_executable, josl_w_file_exists,
    josl_w_file_copy, josl_w_file_remove, josl_w_file_move,

    /* Misc Intrinsic Type Words */
    josl_w_variable, josl_w_dict, josl_w_sized_dict, josl_w_set, josl_w_set_keep,
    josl_w_get, josl_w_remove, josl_w_has_key, josl_w_copy, josl_w_array,
    josl_w_list, josl_w_push_head, josl_w_push_tail, josl_w_current, josl_w_forward,
    josl_w_backward, josl_w_head, josl_w_tail, josl_w_go_head, josl_w_go_tail,
    josl_w_pop_head, josl_w_pop_tail, josl_w_sbuf, josl_w_sized_sbuf,
    josl_w_keys, josl_w_values, josl_w_capacity, josl_w_array_resize,
    josl_w_sort, josl_w_sort_user,
    josl_w_place_0, josl_w_place_1, josl_w_place_2, josl_w_place_3,
    josl_w_place_4, josl_w_place_5, josl_w_place_6, josl_w_place_7,
    josl_w_place_8, josl_w_place_9,

    /* Functional Words */
    josl_w_map, josl_w_filter, josl_w_fold, josl_w_foldr,

    /* Regular Expressions */
    josl_w_re_compile, josl_w_regexp, josl_w_regsub, josl_w_regextract,

    /* Sockets */
    josl_w_sock_socket, josl_w_sock_bind, josl_w_sock_listen,
    josl_w_sock_accept,  josl_w_sock_connect, josl_w_sock_select,
    josl_w_sock_send_to, josl_w_sock_receive_from,
    josl_w_sock_serv_by_name, josl_w_sock_serv_by_port,
    josl_w_sock_host_by_name, josl_w_sock_host_by_addr,

    /* Debugging */
    josl_w_words, josl_w_word_show, josl_w_is_user_word, josl_w_word_dot,
    josl_w_stack_show, josl_w_word_list,

    /* Unit testing */
    josl_w_test, josl_w_test_fail_count, josl_w_test_pass_count,
    josl_w_test_total_count, josl_w_test_report, josl_w_test_verbose,
	josl_w_test_is_verbose,

    /* User words begin with josl_w_user + x */
    josl_w_user,

    /* Compiler Directives */
    josl_w_minus_include, josl_w_minus_public,
    josl_w_minus_constant, josl_w_minus_enum, josl_w_minus_variable,
    josl_w_minus_rename,
    josl_w_minus_ffi_library, josl_w_minus_ffi_word, josl_w_minus_ffi_callback,
  } josl_word_id;

typedef enum
  {
    josl_vis_local,
    josl_vis_public,
    josl_vis_global
  } josl_visibility;

/* *INDENT-ON* */

/*! Executable word */
struct _josl_word
{
  josl_word_id word_id;         /*!< Word id */
  josl_top_word *top;           /*!< Top word this word is associated with */
  char *name;                   /*!< Name of the word */
  josl_word *next;              /*!< Next word in top word definition */
  josl_word *nextexec;          /*!< Next word in line for execution */
  josl_word *jump;              /*!< Stored jump for words that may possibly jump */
  josl_value *value;            /*!< Value that might be associated with this word */
  int lineNo;                   /*!< Line number the word was defined on */
  int colNo;                    /*!< Column number the word was defined on */
};

/*! Top word */
struct _josl_top_word
{
  josl_visibility visibility;   /*!< Visiblity of word */
  char *name;                   /*!< Name of this top word */
  char *filename;               /*!< Filename the word was defined in. */
  int lineNo;                   /*!< Line number the word was defined on */
  josl_word *first;             /*!< First word to execute */
  josl_word *last;              /*!< Last word (used for appending) */
  josl_top_word *excH;          /*!< Associated exception handler */
};

/*! Loop Data */
struct _josl_loop
{
  josl_word_id type;
  int64 finish;                 /*!< Value when this is done */
  bool active;                  /*!< Loop active? */
  int64 current;                /*!< Current loop index */
  josl_value *content;          /*!< Content data pointer (each loop word) */
};

bool josl_init (josl ** interp);

void josl_cleanup (josl ** interp);

void josl_words_core (josl * interp);
void josl_words_io (josl * interp);
void josl_words_system (josl * interp);
void josl_words_filesystem (josl * interp);
void josl_words_socket (josl * interp);
void josl_words_interactive (josl * interp);

josl_word_id josl_word_get_id (josl * interp, const char *word);

void josl_add_word (josl * interp, const char *word);
void josl_add_integer (josl * interp, const char *name, int64 v);
void josl_add_double (josl * interp, const char *name, double v);
void josl_add_string (josl * interp, const char *name, const char *v);
void josl_add_pointer (josl * interp, const char *name, const void *v);

void josl_exec_word (josl * interp, josl_top_word * word);
void josl_exec_named_word (josl * interp, const char *filename,
                           const char *word);

#endif /* JOSL_VM_H */
