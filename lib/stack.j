#
# Copyright (c) 2008,2010 Jeremy Cowgar.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#   * Neither the name of Jeremy Cowgar nor the names of its contributors may
#     be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

-public clear-stack 4dup dup2 dup3 dup4 bury

#
# Additional stack words
#

# Clear the entire stack
#
# ( ... -- )

clear-stack depth drop#

# Duplicate the top four items as a set
#
# ( x1 x2 x3 x4 -- x1 x2 x3 x4 x1 x2 x3 x4 )

4dup 3 pick 3 pick 3 pick 3 pick

# Duplicate the TOS two times
#
# ( x1 -- x1 x1 )

dup2 dup dup

# Duplicate the TOS three times
#
# ( x1 -- x1 x1 x1 )

dup3 dup dup dup

# Duplicate the TOS four times
#
# ( x1 -- x1 x1 x1 x1 )

dup4 dup dup dup dup

# Move the TOS under the third item
#
# ( x1 x2 x3 x4 -- x4 x1 x2 x3 )

bury 3 -roll
