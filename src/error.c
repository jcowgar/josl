/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>

#include <josl.h>
#include <josl/error.h>
#include <josl/str.h>

void
josl_error_new_file (josl * interp, const char *filename, const int lineNo,
                     const int colNo, const int code, const char *message)
{
  josl_error_message *emsg;

  emsg = tgc_alloc (&gc, (sizeof (josl_error_message)));
  if (filename == NULL)
    emsg->filename = NULL;
  else
    emsg->filename = filename;
  emsg->lineNo = lineNo;
  emsg->colNo = colNo;
  emsg->code = code;
  emsg->message = message;

  if (interp->errorStack == NULL)
    emsg->prev = NULL;
  else
    emsg->prev = interp->errorStack;

  interp->errorStack = emsg;
}

void
josl_error_new_word (josl * interp, const josl_word * word, const int code,
                     const char *message)
{
  josl_error_message *emsg;

  emsg = tgc_alloc (&gc, sizeof (josl_error_message));
  emsg->filename = word->top->filename;
  emsg->lineNo = word->lineNo;
  emsg->colNo = word->colNo;
  emsg->code = code;
  emsg->message = strdup_printf ("%s: %s", word->name, message);

  if (interp->errorStack == NULL)
    emsg->prev = NULL;
  else
    emsg->prev = interp->errorStack;

  interp->errorStack = emsg;
}

void
josl_error_new (josl * interp, const int code, const char *message)
{
  josl_error_new_file (interp, NULL, -1, -1, code, message);
}

void
josl_error_display (josl * interp)
{
  int idx;
  josl_error_message *msg;

  msg = interp->errorStack;
  idx = 0;

  while (msg != NULL)
    {
      if (msg->filename == NULL)
        fprintf (stderr, "<%i> %i: %s\n", idx, msg->code, msg->message);
      else
        fprintf (stderr, "<%i> (%s %i,%i) %i: %s\n", idx, msg->filename,
                 msg->lineNo, msg->colNo, msg->code, msg->message);
      msg = msg->prev;
      idx++;
    }
}
