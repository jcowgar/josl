/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tgc.h>

tgc_t gc;

#include "config.h"

#include <josl.h>
#include <josl/parse.h>
#include <josl/str.h>
#include <josl/value.h>

int josl_argc;
char **josl_argv;
bool hidestack = FALSE;
bool example = FALSE;

#if HAVE_LIBREADLINE == 1
#include <readline/readline.h>
#include <readline/history.h>

char historyfile[FILENAME_MAX];

void
init_getline ()
{
  char *homedir;

  using_history ();

  homedir = getenv ("HOME");
  if (homedir != NULL)
    {
      snprintf (historyfile, FILENAME_MAX, "%s/.josl_history", homedir);
      read_history (historyfile);
    }
  else
    historyfile[0] = 0;
}

void
finish_getline ()
{
  if (historyfile[0])
    {
      write_history (historyfile);
    }
}

char *
get_line (const int continuation)
{
  char *top;

  rl_readline_name = "josl";
  if (continuation == 1)
    top = readline (example == TRUE ? "" : ">> ");
  else
    top = readline (example == TRUE ? "" : "> ");

  if (example == FALSE)
    {
      add_history (top);
    }

  return top;
}

#else /* HAVE_LIBREADLINE == 1 */

char linebuffer[1024];

void
init_getline ()
{
}

void
finish_getline ()
{
}

char *
get_line (const int continuation)
{
  if (continuation == 1)
    fprintf (stdout, example == TRUE ? "" : ">> ");
  else
    fprintf (stdout, example == TRUE ? "" : "> ");
  fgets (linebuffer, 1023, stdin);

  return linebuffer;
}

#endif /* HAVE_LIBREADLINE == 1 */

static void
version (const char *exename)
{
  fprintf (stderr, PACKAGE_NAME " v" VERSION " (" SCMID ")\n");
  fprintf (stderr, "Copyright (C) 2008-2022 by Jeremy Cowgar.\n");
}

static void
usage (const char *exename)
{
  version (exename);
  fprintf (stderr, "\n");
  fprintf (stderr, "Usage: josl [-i dir] [-d] [--parse-only] filename.j\n");
  fprintf (stderr, "Standard options:\n");
  fprintf (stderr,
           "  -i dir         add an include directory to the search path.\n");
  fprintf (stderr, "                 Can be used many times.\n");
  fprintf (stderr,
           "  -d             increment the debug level by one. Can be used\n");
  fprintf (stderr,
           "                 many times, each time debug level increases.\n");
  fprintf (stderr,
           "  --parse-only   check syntax and parse only, no execution.\n");
  fprintf (stderr,
           "  --hide-stack   when in interactive mode do not display the\n");
  fprintf (stderr, "                 stack prompt automatically.\n");
  fprintf (stderr, "\n");
  fprintf (stderr, "Other options:\n");
  fprintf (stderr, "  --example      take content from stdin, print and\n");
  fprintf (stderr, "                 display as if it were an example.\n");
  fprintf (stderr, "                 This is useful for creating user\n");
  fprintf (stderr, "                 manuals and documentation.\n");
  fprintf (stderr, "  -h,--help      display this help message.\n");
  fprintf (stderr, "  -v,--version   display version information.\n");
  fprintf (stderr, "\n");
  fprintf (stderr, "Notes:\n");
  fprintf (stderr,
           "  All arguments after the initial filename are not processed.\n");
  fprintf (stderr,
           "  They are assumed to be used by the script itself, not Josl.\n");
}

static void
interactive (const char *exename, josl * interp)
{
  josl_top_word *stackshow, *stackclear;
  josl_hash *str_words;
  charbuf *cmd;
  char *top;
  int its = 0;

  if (example == FALSE)
    version (exename);

  cmd = cbuf_alloc (128);
  josl_parse_string (interp, "josl-top-stack-show stack-show");
  str_words = hashtable_search (interp->filewords, (char *) "<str>");
  stackshow = hashtable_search (str_words, (char *) "josl-top-stack-show");
  if (example == TRUE)
    {
      josl_parse_string (interp, "josl-top-stack-clear depth drop#");
      stackclear =
        hashtable_search (str_words, (char *) "josl-top-stack-clear");
    }

  init_getline ();

  while (1)
    {
      its++;

      if (hidestack == FALSE && its > 1 && top[0] != '.')
        josl_exec_word (interp, stackshow);
      top = get_line (0);
      if (strncmp (top, "exit", 4) == 0)
        {
          finish_getline ();
          return;
        }
      if (top == NULL)
        return;
      if (example == TRUE && top[0] == '.')
        {
          josl_exec_word (interp, stackclear);
          continue;
        }
      if (top[0] == ':' && top[1] == ' ')
        {
          cbuf_assign (cmd, top + 2, -1);

          while (1)
            {
              top = get_line (1);
              if (top[0] == 0 || top[0] == '\n')
                {
                  cbuf_append_ch (cmd, '\n');
                  break;
                }
              if (example == TRUE)
                fprintf (stdout, ">> %s", top);

              cbuf_append_ch (cmd, ' ');
              cbuf_append_str (cmd, top, -1);
            }

          josl_parse_string (interp, cmd->data);

          if (interp->errorStack != NULL)
            {
              finish_getline ();
              return;
            }

        }
      else if (top[0] == '-')
        {
          josl_parse_string (interp, top);
          if (interp->errorStack != NULL)
            {
              josl_error_display (interp);
              interp->errorStack = NULL;
            }
        }
      else
        {
          cbuf_truncate (cmd, 0);
          cbuf_append_printf (cmd, "__josl__top__ %s", top);

          josl_parse_string (interp, cmd->data);
          if (interp->errorStack != NULL)
            {
              josl_error_display (interp);
              interp->errorStack = NULL;
            }
          else
            {
              josl_exec_named_word (interp, "<str>", "__josl__top__");
              if (interp->errorStack != NULL)
                {
                  josl_error_display (interp);
                  interp->errorStack = NULL;
                  /* TODO: clear the stack? */
                }
            }
        }
    }
}

int
main (int argc, char **argv)
{
  int i, sys_args = 0;
  bool parse_only = FALSE, is_interactive = TRUE;
  char *filename = NULL;
  josl *interp = NULL;

  tgc_start (&gc, &argc);

  if (josl_init (&interp) == FALSE)
    {
      fprintf (stderr, "could not initialize Josl\n");
      exit (1);
    }

  for (i = 1; i < argc; i++)
    {
      char *a = argv[i];

      if (strcmp (a, "-d") == 0)
        {
          sys_args += 1;
          interp->debugLevel += 1;
        }
      else if (strcmp (a, "--parse-only") == 0)
        {
          parse_only = TRUE;
          sys_args += 1;
        }
      else if (strcmp (a, "-i") == 0)
        {
          i += 1;

          strncpy (josl_include_paths[josl_include_paths_count++],
                   argv[i], FILENAME_MAX);

          sys_args += 2;
        }
      else if (strcmp (a, "--hide-stack") == 0)
        {
          hidestack = TRUE;
          sys_args += 1;
        }
      else if (strcmp (a, "--example") == 0)
        {
          example = TRUE;
          hidestack = FALSE;
          sys_args += 1;
        }
      else if (strcmp (a, "-h") == 0 || strcmp (a, "--help") == 0)
        {
          usage (argv[0]);
          exit (0);
        }
      else if (strcmp (a, "-v") == 0 || strcmp (a, "--version") == 0)
        {
          version (argv[0]);
          exit (0);
        }
      else if (filename == NULL)
        {
          sys_args += 1;
          filename = a;
          is_interactive = FALSE;
          break;                /* The rest of the arguments are given
                                   to the Josl script */
        }
      else
        break;
    }

  josl_argc = argc - sys_args - 1;
  josl_argv = argv + sys_args + 1;

  josl_words_io (interp);
  josl_words_system (interp);
  josl_words_filesystem (interp);
  josl_words_socket (interp);
  josl_words_interactive (interp);

  if (filename != NULL)
    {
      josl_parse_file (interp, filename);
      if (interp->errorStack != NULL)
        {
          josl_error_display (interp);
          exit (0);
        }
      if (parse_only == FALSE)
        {
          josl_exec_named_word (interp, filename, "main");
        }
    }
  else
    {
      interactive (argv[0], interp);
    }

  if (interp->errorStack != NULL)
    {
      josl_error_display (interp);
    }

  josl_cleanup (&interp);

  tgc_stop (&gc);

  return 0;
}
