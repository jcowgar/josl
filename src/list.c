/*
 * Copyright (c) 2022 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*!
  \defgroup list List Utilities

  List utilities making life a bit easier.

*/

#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <josl/list.h>

/*!
  \ingroup list

  Create a new list

*/

josl_list
*new_list()
{
  josl_list *l;

  l = tgc_alloc(&gc, sizeof (josl_list));
  l->size = 0;
  l->head = NULL;
  l->tail = NULL;
  l->cur = NULL;

  return l;
}

/*!
  \ingroup list

  Append a string onto the end of a list.

*/

void
list_append_string(josl_list *l, char *s)
{
  josl_list_item *li;

  li = tgc_alloc(&gc, sizeof (josl_list_item));
  li->v = tgc_alloc(&gc, sizeof (josl_value));
  li->v->type = josl_vt_string;
  li->v->value.s = s;
  li->next = NULL;
  li->prev = l->tail;

  l->tail = li;
  l->size++;

  if (l->head == NULL)
    {
      l->head = li;
      l->cur = li;
    }
  else
    li->prev->next = li;
}

