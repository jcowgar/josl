/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <tgc.h>
#include <ffi.h>

#if HAVE_PCRE == 1
#include <pcre.h>
#endif

#include "config.h"

#include <josl.h>
#include <josl/error.h>
#include <josl/hashtable.h>
#include <josl/hashtable_itr.h>
#include <josl/parse.h>
#include <josl/str.h>
#include <josl/value.h>
#include <josl/vm.h>
#include <josl/xdl.h>

/*! Possible token types found during parsing. */
typedef enum
{
  /* basics */
  tt_none,                      /*!< */
  tt_eof,                       /*!< End of the file has been reached */
  tt_unknown,                   /*!< */

  /* types */
  tt_integer,                   /*!< Integer literal */
  tt_double,                    /*!< Double literal */
  tt_string,                    /*!< String literal */
  tt_array,                     /*!< Array literal */
  tt_list,                      /*!< List literal */
  tt_dict,                      /*!< Dictionary literal */

  /* syntax */
  tt_constant,                  /*!< Constant */
  tt_word,                      /*!< Word */
  tt_ns_word,                   /*!< Word contained in a namespace */
  tt_top_word,                  /*!< Top Word */
  tt_comment,                   /*!< Line comment */
  tt_word_ref,                  /*!< Word reference */
  tt_regex,                     /*!< Regular expression */
  tt_quotation_start,           /*!< Quotation start { */
  tt_quotation_finish,          /*!< Quotation finish } */

  /* directive tokens */
  tt_include,                   /*!< -include FILENAME */
  tt_include_as,                /*!< -include FILENAME as NAMESPACE */
  tt_public,                    /*!< -public WORD1 [WORD2 ...] */
  tt_constant_def,              /*!< -constant WORD1 VAL1 [WORD2 VAL2 ... ...] */
  tt_rename,                    /*!< -rename FROM TO [... ...] */
  tt_ffi_word,                  /*!< -ffi-word */
  tt_ffi_library                /*!< -ffi-library */
} josl_token_type;

typedef struct _josl_parser
{
  josl *interp;
  josl_hash *words;             /*!< top words */
  josl_hash *namespaces;        /*!< namespace name to hash of words */
  josl_hash *variables;         /*!< variables */

  int lineNo;
  int colNo;
  int pos;

  bool parsingDirective;

  josl_value *token;
  josl_token_type tokenType;
  int tokPos;
  int tokLineNo;
  int tokColNo;
  int tokEndPos;                /* is this necessary? why not use pos? */
  int tokEndLineNo;             /* is this necessary? why not use lineNo? */
  int tokEndColNo;              /* is this necessary? why not use colNo? */
  int tokIntData1;
  int tokIntData2;
  char *tokCharData1;
  char *tokCharData2;

  char *buf;
  char *filename;

  charbuf *tmpBuf;              /* used for temporary string
                                   reading/parsing/formatting */

  /* Branch Stack */
  josl_word *bs[1024];
  int bsp;
} josl_parser;

/* *INDENT-OFF* */
struct
{
  char *name;
  ffi_type *type;
} josl_ffi_types[] =
  {
    { "ffi-void",        &ffi_type_void       },
    { "ffi-uint8",       &ffi_type_uint8      },
    { "ffi-int8",        &ffi_type_sint8      },
    { "ffi-uint16",      &ffi_type_uint16     },
    { "ffi-int16",       &ffi_type_sint16     },
    { "ffi-uint32",      &ffi_type_uint32     },
    { "ffi-int32",       &ffi_type_sint32     },
    { "ffi-uint64",      &ffi_type_uint64     },
    { "ffi-int64",       &ffi_type_sint64     },
    { "ffi-float",       &ffi_type_float      },
    { "ffi-double",      &ffi_type_double     },
    { "ffi-uchar",       &ffi_type_uchar      },
    { "ffi-char",        &ffi_type_schar      },
    { "ffi-ushort",      &ffi_type_ushort     },
    { "ffi-short",       &ffi_type_sshort     },
    { "ffi-uint",        &ffi_type_uint       },
    { "ffi-int",         &ffi_type_sint       },
    { "ffi-ulong",       &ffi_type_ulong      },
    { "ffi-long",        &ffi_type_slong      },
    { "ffi-long-double", &ffi_type_longdouble },
    { "ffi-pointer",     &ffi_type_pointer    },
    { "ffi-string",      &ffi_type_pointer    },
    { "ffi-buffer",      &ffi_type_pointer    },
    { "ffi-out-pointer", &ffi_type_pointer    }
  };

/* *INDENT-ON* */

/* Forward declarations */
static bool josl_parse_get_token (josl_parser * p);
static void josl_parse_putback_token (josl_parser * p);
static const char *str_filename = "<str>";
char josl_include_paths[64][FILENAME_MAX];
int josl_include_paths_count = 0;

#define josl_parse_forward(p) { p->colNo++; p->pos++; }

void
josl_parse_init_include_path ()
{
  char *tok, *env = getenv ("JOSL_INCLUDE_PATH");

  strcpy (josl_include_paths[josl_include_paths_count++], ".");

  /* environmental variable comes before built in path */
  if (env != NULL)
    {
      tok = strtok (env, ":");
      while (tok != NULL)
        {
          strcpy (josl_include_paths[josl_include_paths_count++], tok);
          tok = strtok (NULL, ":");
        }
    }

  strcpy (josl_include_paths[josl_include_paths_count++], JOSL_INC_PATH);
  snprintf (josl_include_paths[josl_include_paths_count++], FILENAME_MAX,
            "%s/%i", JOSL_INC_PATH, VERSION_MAJOR);
  snprintf (josl_include_paths[josl_include_paths_count++], FILENAME_MAX,
            "%s/%i.%i", JOSL_INC_PATH, VERSION_MAJOR, VERSION_MINOR);
  snprintf (josl_include_paths[josl_include_paths_count++], FILENAME_MAX,
            "%s/%i.%i.%i", JOSL_INC_PATH, VERSION_MAJOR, VERSION_MINOR,
            VERSION_PATCH);
}

static char *
josl_parse_find_include (josl * interp, const char *name)
{
  char filename[FILENAME_MAX];
  int i;

  if (access ((char *) name, R_OK) == 0)
    return strdup (name);

  for (i = 0; i < josl_include_paths_count; i++)
    {
      snprintf (filename, FILENAME_MAX, "%s/%s", josl_include_paths[i], name);
      if (access ((char *) filename, R_OK) == 0)
        return strdup (filename);
    }

  return NULL;
}

static josl_top_word *
new_top_word (josl_parser * p)
{
  void *oldTopWord;
  char excName[256];
  josl_top_word *w = tgc_alloc (&gc, sizeof (josl_top_word));

  snprintf (excName, 256, "%s@failed?", p->token->value.s);

  w->name = strdup (p->token->value.s);
  w->filename = strdup (p->filename);
  w->lineNo = p->tokLineNo;
  w->first = NULL;
  w->last = NULL;
  w->excH = hashtable_search (p->words, (void *) excName);
  oldTopWord = hashtable_search (p->words, (void *) w->name);
  if (oldTopWord != NULL)
    {
      if (oldTopWord == (void *) 1)
        {
          w->visibility = josl_vis_public;
        }
      else if (strcmp (w->name, "main") == 0)
        {
          w->visibility = josl_vis_public;
        }
      else
        {
          w->visibility = josl_vis_local;
        }

      hashtable_remove (p->words, (void *) w->name);
      hashtable_insert (p->words, (void *) w->name, w);
    }
  else
    {
      w->visibility = josl_vis_local;
      hashtable_insert (p->words, (void *) w->name, w);
    }

  return w;
}

static josl_word *
new_word (josl_parser * p, josl_top_word * topword, const char *name,
          josl_word_id word_id)
{
  josl_word *w = tgc_alloc (&gc, sizeof (josl_word));
  w->top = topword;
  w->name = name == NULL ? NULL : strdup (name);
  w->word_id = word_id;
  w->next = NULL;
  w->nextexec = NULL;
  w->jump = NULL;
  w->value = NULL;
  w->lineNo = p->tokLineNo;
  w->colNo = p->tokColNo;

  if (word_id == -1)
    {
      josl_word *tw = NULL;
      void *wp = NULL;

      if (p->tokenType == tt_ns_word)
        {
          char *filename;
          josl_hash *fh;
          josl_top_word *tw;
          filename = hashtable_search (p->namespaces, p->tokCharData1);
          if (filename == NULL)
            {
              fprintf (stderr, "couldn't find filename for namespace '%s'\n",
                       p->tokCharData1);
              return NULL;
            }

          fh = hashtable_search (p->interp->filewords, filename);
          if (fh == NULL)
            {
              fprintf (stderr, "couldn't find file function has table\n");
              return NULL;
            }

          w->name = p->tokCharData2;

          tw = hashtable_search (fh, w->name);

          if (tw == NULL)
            {
              fprintf (stderr, "couldn't find word in namespace\n");
              return NULL;
            }
          else if (tw->visibility == josl_vis_local)
            {
              fprintf (stderr,
                       "attempted to access a word defined for local use only");
              return NULL;
            }

          w->value = tgc_alloc (&gc, sizeof (josl_value));
          w->value->type = josl_vt_pointer;
          w->word_id = josl_w_user;
          w->value->value.p = tw;
        }
      else
        {
          wp = hashtable_search (p->words, w->name);
          if (wp == NULL)
            {
              w->word_id = josl_word_get_id (p->interp, w->name);
              if (w->word_id == -1)
                {
                  w->word_id = josl_w_dynamic_word;
                }
            }
          else if ((int) wp < 0)
            {
              w->word_id = -((int) wp);
            }
          else
            {
              w->word_id = josl_w_user;
              w->value = tgc_alloc (&gc, sizeof (josl_value));
              w->value->type = josl_vt_pointer;
              w->value->value.p = wp;
            }
        }
    }

  if (topword->last != NULL)
    {
      topword->last->next = w;
      if (topword->last->nextexec == NULL)
        topword->last->nextexec = w;
      topword->last = w;
    }
  else
    {
      topword->last = w;
      topword->first = w;
    }

  return w;
}

static void
josl_parse_eat_ws (josl_parser * p)
{
  unsigned int ch = 0;

  while ((ch = p->buf[p->pos]) && isspace (ch))
    {
      p->colNo++;
      if (ch == '\n')
        {
          p->colNo = 1;
          p->lineNo++;
        }

      p->pos++;
    }
}

static void
josl_parse_eat_to_eol (josl_parser * p)
{
  unsigned int ch;

  while ((ch = p->buf[p->pos]))
    {
      if (ch == '\n')
        {
          return;
        }

      josl_parse_forward (p);
    }
}

static void
josl_parse_begin_token (josl_parser * p, josl_token_type tokenType)
{
  p->tokPos = p->pos;
  p->tokLineNo = p->lineNo;
  p->tokColNo = p->colNo;
  p->tokenType = tokenType;
  p->token->value.s = NULL;
}

static void
josl_parse_end_token (josl_parser * p)
{
  p->tokEndPos = p->pos;
  p->tokEndColNo = p->colNo - 1;
  p->tokEndLineNo = p->lineNo;
  p->token->type = tt_string;
  p->token->value.s = strndup (p->buf + p->tokPos, p->tokEndPos - p->tokPos);

  switch (p->tokenType)
    {
    case tt_integer:
      {
        char *tmp = p->token->value.s;
        p->token->type = tt_integer;
        p->token->value.i = string_to_int64 (tmp);
      }
      break;

    case tt_double:
      {
        char *tmp = p->token->value.s;
        p->token->type = tt_double;
        p->token->value.d = string_to_double (tmp);
      }
      break;

    case tt_unknown:
    case tt_word:
      {
        int isConst = 1, hasAlpha = 0, idx = 0;
        while (p->token->value.s[idx])
          {
            if (isalpha (p->token->value.s[idx]))
              {
                hasAlpha = 1;
                if (isupper (p->token->value.s[idx]) == 0)
                  {
                    isConst = 0;
                    break;
                  }
              }
            idx++;
          }
        if (hasAlpha == 1
            && (isConst == 1
                || (isupper (p->token->value.s[0]) &&
                    p->token->value.s[strlen (p->token->value.s) - 1] ==
                    '?')))
          p->tokenType = tt_constant;
      }
      break;
    }
}

static void
josl_parse_end_regex_token (josl_parser * p)
{
  p->tokEndPos = p->pos;
  p->tokEndColNo = p->colNo - 1;
  p->tokEndLineNo = p->lineNo;
}

static void
josl_parse_end_token_string (josl_parser * p)
{
  p->tokEndPos = p->pos;
  p->tokEndColNo = p->colNo - 1;
  p->tokEndLineNo = p->lineNo;
}

static void
josl_parse_word (josl_parser * p)
{
  bool is_number = TRUE, is_double = FALSE, has_digits = FALSE;
  bool has_sign = FALSE, is_ns_word = FALSE;
  unsigned int ch, wordStart = 0;

  while ((ch = p->buf[p->pos]) && !isspace (ch))
    {
      if (is_number)
        {
          /* we still have a chance that this is a number */
          if (!isdigit (ch))
            {
              if (ch == '.')
                {
                  if (is_double == TRUE)
                    is_number = FALSE;
                  else
                    is_double = TRUE;
                }
              else if (ch == '+' || ch == '-')
                {
                  if (has_sign == TRUE || has_digits == TRUE
                      || is_double == TRUE)
                    {
                      /* multiple signs, sign after a digit or sign
                         after double */
                      is_number = FALSE;
                    }
                  else
                    has_sign = TRUE;
                }
              else if (ch != ',' && ch != '_')
                is_number = FALSE;
            }
          else
            has_digits = TRUE;
        }
      else if (ch == '.')
        {
          wordStart = p->pos;
          is_ns_word = TRUE;
        }

      josl_parse_forward (p);
    }

  if (is_number && has_digits)
    {
      p->tokenType = is_double ? tt_double : tt_integer;
    }
  else if (is_ns_word)
    {
      p->tokenType = tt_ns_word;

      /* Namespace */
      p->tokCharData1 = strndup (p->buf + p->tokPos, wordStart - p->tokPos);

      /* Word name */
      p->tokCharData2 =
        strndup (p->buf + wordStart + 1, p->pos - wordStart - 1);
    }
}

static void
josl_parse_regex (josl_parser * p)
{
  int state = 0;                /* simple state, 0 = parse regex, 1 = parse modifiers */
  unsigned int ch;

  p->tokIntData1 = 0;
  p->tokIntData2 = 0;

  /* clear the temporary buffer */
  cbuf_truncate (p->tmpBuf, 0);
  while ((ch = p->buf[p->pos]))
    {
      if (state == 0)
        {
          if (ch == '/')
            {
              p->token->value.s = strdup (p->tmpBuf->data);
              josl_parse_forward (p);

              if (isspace (p->buf[p->pos]))
                {
                  return;
                }
              else
                {
                  state = 1;
                  continue;
                }
            }
          cbuf_append_ch (p->tmpBuf, ch);
        }
      else if (isspace (ch) || ch == 0)
        {
          break;
        }
#if HAVE_PCRE == 1
      else
        {
          switch (ch)
            {
            case 'i':
              p->tokIntData1 |= PCRE_CASELESS;
              break;

            case 'm':
              p->tokIntData1 |= PCRE_MULTILINE;
              break;

            case 's':
              p->tokIntData1 |= PCRE_DOTALL;
              break;

            case 'x':
              p->tokIntData1 |= PCRE_EXTENDED;
              break;

            case 'A':
              p->tokIntData1 |= PCRE_ANCHORED;
              break;

            case 'D':
              p->tokIntData1 |= PCRE_DOLLAR_ENDONLY;
              break;

            case 'U':
              p->tokIntData1 |= PCRE_UNGREEDY;
              break;

            case 'S':
              p->tokIntData2 = 1;
              break;
            }
        }
#endif /* HAVE_PCRE == 1 */
      josl_parse_forward (p);
    }
}

static void
josl_parse_string_content (josl_parser * p)
{
  unsigned int ch, lch = 0;
  bool skip_till_next = FALSE;

  /* clear the temporary buffer */
  cbuf_truncate (p->tmpBuf, 0);

  while ((ch = p->buf[p->pos]))
    {
      if (skip_till_next)
        {
          if (ch == '"')
            {
              skip_till_next = FALSE;
            }
          else if (ch == '\n')
            {
              p->lineNo++;
              p->colNo = 1;
              p->pos++;
            }
          else if (!isspace (ch))
            {
              josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                                   p->tokColNo, josl_err_multiline_string,
                                   "next token after a multi-line string continuation "
                                   "should be a string.");

              return;
            }

          /* do nothing normally, just eat the whitespace. */
        }
      else if (lch == '\\')
        {
          switch (ch)
            {
            case 'n':          /* newline */
              ch = '\n';
              break;

            case '\\':         /* backslash */
              ch = '\\';
              break;

            case 't':          /* tab */
              ch = '\t';
              break;

            case 'b':          /* backspace */
              ch = '\b';
              break;

            case '"':          /* double quote */
              ch = '"';
              break;

            case 'r':          /* carriage return */
              ch = '\r';
              break;

            case 'f':          /* form feed */
              ch = '\f';
              break;

            case 'v':          /* vertical tab */
              ch = '\v';
              break;

            case 'a':          /* audible alert */
              ch = '\a';
              break;

            default:
              josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                                   p->tokColNo, josl_err_escape_char,
                                   "invalid escape character");
              break;
            }

          /* Overwrite the previous character, it was a \ */
          p->tmpBuf->data[p->tmpBuf->len - 1] = ch;
          if (ch == '\\')
            ch = 0;             /* get around lch being set to '\' again */
        }
      else if (ch == '"')
        {
          if (p->buf[p->pos + 1] == '.')
            {
              josl_parse_forward (p);
              skip_till_next = TRUE;
            }
          else if (p->buf[p->pos + 1] == '+')
            {
              josl_parse_forward (p);
              skip_till_next = TRUE;
              cbuf_append_ch (p->tmpBuf, '\n');
            }
          else
            {
              p->token->type = tt_string;
              p->token->value.s = strdup (p->tmpBuf->data);
              return;
            }
        }
      else if (ch == '\n')
        {
          josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                               p->tokColNo, josl_err_incomplete_string,
                               "string is not terminated");
          return;
        }
      else
        {
          cbuf_append_ch (p->tmpBuf, ch);
        }

      lch = ch;
      josl_parse_forward (p);
    }

  if (ch == 0)
    {
      josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                           p->tokColNo, josl_err_incomplete_string,
                           "string is not terminated");
    }
}


static bool
josl_parse_directive (josl_parser * p)
{
  int tokPos = p->tokPos;
  int tokLineNo = p->tokLineNo;
  int tokColNo = p->tokColNo;
  josl_word_id word_id = -1;

  if (p->parsingDirective == TRUE)
    {
      return FALSE;
    }

  p->parsingDirective = TRUE;

  word_id = (josl_word_id)
    hashtable_search (p->interp->intrinsics, (void *) p->token->value.s);

  switch (word_id)
    {
    case josl_w_minus_include:
      if (josl_parse_get_token (p) != TRUE)
        {
          josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                               josl_err_include_filename,
                               "-include expects a filename");
          return FALSE;
        }

      if (p->tokenType == tt_ns_word)
        p->tokenType = tt_word;

      if (p->tokenType != tt_word)
        {
          josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                               josl_err_include_filename,
                               "-include expects a filename but next token was not a word type");
          return FALSE;
        }

      p->tokCharData1 = strdup (p->token->value.s);

      josl_parse_get_token (p);
      if (p->tokenType != tt_word || strcmp (p->token->value.s, "as") != 0)
        {
          if (p->tokenType != tt_eof)
            josl_parse_putback_token (p);

          p->tokenType = tt_include;
          p->tokPos = tokPos;
          p->tokLineNo = tokLineNo;
          p->tokColNo = tokColNo;

          return TRUE;
        }

      /* we have an -include FILENAME as NAMESPACE */

      if (josl_parse_get_token (p) != TRUE)
        {
          josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                               josl_err_include_namespace,
                               "error while reading namespace portion of -include");
          return FALSE;
        }

      if (p->tokenType != tt_word)
        {
          josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                               josl_err_include_namespace,
                               "-include X as Y expects a namespace but none was found.");
          return FALSE;
        }

      p->tokenType = tt_include_as;
      p->tokCharData2 = strdup (p->token->value.s);

      p->tokPos = tokPos;
      p->tokLineNo = tokLineNo;
      p->tokColNo = tokColNo;
      return TRUE;

    case josl_w_minus_public:
      /* Read all tokens until a token appears on column 1. */
      while (josl_parse_get_token (p) == TRUE)
        {
          if (p->tokColNo == 1 || p->tokenType == tt_eof)
            {
              josl_parse_putback_token (p);
              break;
            }

          hashtable_insert (p->words, (void *) p->token->value.s, (void *) 1);
        }

      return TRUE;

    case josl_w_minus_constant:
      while (josl_parse_get_token (p) == TRUE)
        {
          char *constName = NULL;
          josl_value *constVal = NULL;
          josl_top_word *constTw = NULL;
          josl_word *constW = NULL;

          if (p->tokenType != tt_word)
            {
              josl_parse_putback_token (p);
              p->tokenType = tt_constant_def;
              return TRUE;
            }

          constTw = new_top_word (p);

          if (josl_parse_get_token (p) == FALSE)
            {
              josl_error_new_file (p->interp, p->filename, tokLineNo,
                                   tokColNo, josl_err_invalid_argument,
                                   "Invalid token found in -constant");
              return FALSE;
            }

          constW = new_word (p, constTw, strdup ("const"), josl_w_push);
          constW->value = tgc_alloc (&gc, sizeof (josl_value));
          constW->value->value = p->token->value;

          switch (p->tokenType)
            {
            case tt_integer:
              constW->value->type = josl_vt_integer;
              break;

            case tt_double:
              constW->value->type = josl_vt_double;
              break;

            default:
              constW->value->type = josl_vt_string;
            }
        }
      return TRUE;

    case josl_w_minus_enum:
      {
        int64 current = 0, incrementor = 1;
        int seenAWord = FALSE, gotStart = FALSE, gotInc = FALSE, multiplier =
          FALSE;

        while (josl_parse_get_token (p) == TRUE)
          {
            if (p->tokColNo == 1 || p->tokenType == tt_eof)
              {
                josl_parse_putback_token (p);
                break;
              }

            switch (p->tokenType)
              {
              case tt_integer:
                if (gotStart == FALSE || seenAWord == TRUE)
                  {
                    current = p->token->value.i;
                    gotStart = TRUE;
                  }
                else if (gotInc == FALSE)
                  {
                    incrementor = p->token->value.i;
                    gotInc = TRUE;
                  }
                break;

              case tt_word:
                {
                  if (strcmp (p->token->value.s, "*") == 0)
                    {
                      multiplier = TRUE;
                    }
                  else if (strcmp (p->token->value.s, "+") == 0)
                    {
                      multiplier = FALSE;
                    }
                  else
                    {
                      josl_top_word *enumTw = new_top_word (p);
                      enumTw->first =
                        new_word (p, enumTw, strdup (p->token->value.s),
                                  josl_w_push);
                      enumTw->first->value =
                        tgc_alloc (&gc, sizeof (josl_value));
                      enumTw->first->value->type = josl_vt_integer;
                      enumTw->first->value->value.i = current;

                      if (multiplier == TRUE)
                        {
                          current *= incrementor;
                        }
                      else
                        {
                          current += incrementor;
                        }

                      seenAWord = TRUE;
                    }
                }
                break;

              default:
                {
                  josl_error_new_file (p->interp, p->filename, tokLineNo,
                                       tokColNo, josl_err_invalid_argument,
                                       "Invalid token found in -enum");
                  return FALSE;
                }
              }
          }
      }
      return TRUE;

    case josl_w_minus_variable:
      /* Read all tokens until a token appears on column 1. */
      while (josl_parse_get_token (p) == TRUE)
        {
          if (p->tokColNo == 1 || p->tokenType == tt_eof)
            {
              josl_parse_putback_token (p);
              break;
            }

          hashtable_insert (p->variables, (void *) p->token->value.s,
                            (void *) 1);
        }
      return TRUE;

    case josl_w_minus_rename:
      /* -rename word-from word-to word-from word-to ... ... */
      while (josl_parse_get_token (p) == TRUE)
        {
          char *from, *to;
          void *tw = NULL;

          if (p->tokenType != tt_word)
            {
              josl_parse_putback_token (p);
              p->tokenType = tt_rename;
              return TRUE;
            }

          from = strdup (p->token->value.s);

          if (josl_parse_get_token (p) == FALSE)
            {
              josl_error_new_file (p->interp, p->filename, tokLineNo,
                                   tokColNo, josl_err_invalid_argument,
                                   "Invalid token found in -rename");
              return FALSE;
            }

          to = p->token->value.s;
          tw = hashtable_search (p->interp->intrinsics, (void *) from);
          if (tw != NULL)
            {
              p->tokenType = tt_rename;
              hashtable_insert (p->words, (void *) to,
                                (void *) (-((int) tw)));
              continue;
            }

          tw = hashtable_search (p->words, (void *) from);
          if (tw != NULL)
            {
              p->tokenType = tt_rename;
              hashtable_remove (p->words, (void *) from);
              hashtable_insert (p->words, (void *) to, tw);
              continue;
            }

          josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                               josl_err_invalid_argument,
                               "Unknown source word found in -rename");
          return FALSE;
        }
      return TRUE;

    case josl_w_minus_ffi_library:
      {
        char *varname, *libname;
        josl_value *var;
        void *dll;

        if (josl_parse_get_token (p) == FALSE ||
            (p->tokenType != tt_word && p->tokenType != tt_ns_word))
          {
            josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                                 josl_err_ffi_declaration,
                                 "syntax error, use -ffi-library VAR-NAME LIB-NAME");
            return FALSE;
          }

        varname = strdup (p->token->value.s);

        if (josl_parse_get_token (p) == FALSE ||
            (p->tokenType != tt_word && p->tokenType != tt_ns_word))
          {
            josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                                 josl_err_ffi_declaration,
                                 "syntax error, use -ffi-library VAR-NAME LIB-NAME");
            return FALSE;
          }

        libname = strdup (p->token->value.s);

        dll = dlopen (libname, 0);
        if (dll == NULL)
          {
            josl_error_new_file (p->interp, p->filename,
                                 tokLineNo, tokColNo,
                                 josl_err_ffi_declaration,
                                 "library could not be loaded");
            return FALSE;
          }

        var = tgc_alloc (&gc, sizeof (josl_value));
        var->type = josl_vt_pointer;
        var->value.p = dll;

        hashtable_insert (p->variables, (void *) varname, (void *) var);

        p->tokenType = tt_ffi_library;
      }
      return TRUE;

    case josl_w_minus_ffi_word:
      {
        char *varname = NULL, *lname = NULL, *cname = NULL;
        ffi_cif *cif;
        ffi_type **argtypes;
        ffi_type *typ, *rettype = NULL;
        int idx, argc = -1, argread = 0;
        josl_value *var;
        josl_ffi *jffi;
        void *dll;
        int storedLineNo, storedColNo, storedPos;

        jffi = tgc_alloc (&gc, sizeof (josl_ffi));
        jffi->rettype = -1;
        jffi->argc = 0;
        jffi->cif = tgc_alloc (&gc, sizeof (ffi_cif));

        while (josl_parse_get_token (p) == TRUE)
          {
            if (p->tokColNo == 1 || p->tokenType == tt_eof)
              {
                josl_parse_putback_token (p);
                break;
              }
            if (varname == NULL)
              {
                varname = strdup (p->token->value.s);
              }
            else if (lname == NULL)
              {
                josl_value *vdll;

                lname = strdup (p->token->value.s);

                vdll = hashtable_search (p->variables, (void *) lname);
                if (vdll == NULL)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         tokLineNo, tokColNo,
                                         josl_err_ffi_declaration,
                                         "library reference could not be found, use -ffi-library first");
                    return FALSE;
                  }

                if (vdll->type != josl_vt_pointer)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         tokLineNo, tokColNo,
                                         josl_err_ffi_declaration,
                                         "found library reference is not a library");
                    return FALSE;
                  }

                dll = vdll->value.p;
              }
            else if (cname == NULL)
              {
                cname = strdup (p->token->value.s);
                jffi->funcref = dlsym (dll, cname);
                if (jffi->funcref == NULL)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         tokLineNo, tokColNo,
                                         josl_err_ffi_declaration,
                                         "could not find function reference");
                    return FALSE;
                  }
              }
            else if (rettype == NULL)
              {
                for (idx = 0; idx < JOSL_FFI_MAX; idx++)
                  {
                    if (strcmp (josl_ffi_types[idx].name,
                                p->token->value.s) == 0)
                      {
                        jffi->rettype = idx;
                        rettype = josl_ffi_types[idx].type;
                        break;
                      }
                  }

                if (jffi->rettype == -1)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         tokLineNo, tokColNo,
                                         josl_err_ffi_declaration,
                                         "invalid ffi return type");
                    return FALSE;
                  }

              }
            else
              {
                josl_parse_putback_token (p);
                break;
              }
          }

        /* store parser state because we are going to read
           ahead and determine how many parameters exist */

        storedLineNo = p->lineNo;
        storedColNo = p->colNo;
        storedPos = p->pos;

        /* count parameters so we can allocate the argtypes
           value correctly, later we will really parse the
           data we have read. */

        while (josl_parse_get_token (p) == TRUE)
          {
            if (p->tokColNo == 1 || p->tokenType != tt_word)
              {
                josl_parse_putback_token (p);
                break;
              }

            jffi->argc++;
          }

        /* restore our parser state */
        p->lineNo = storedLineNo;
        p->colNo = storedColNo;
        p->pos = storedPos;

        if (jffi->argc == 0)
          {
            argtypes = NULL;
          }
        else
          {
            /* allocate and parse our parameters now */
            argtypes = tgc_alloc (&gc, sizeof (ffi_type *) * jffi->argc);

            while (josl_parse_get_token (p) == TRUE)
              {
                if (p->tokColNo == 1 || p->tokenType == tt_eof)
                  {
                    josl_parse_putback_token (p);
                    break;
                  }

                argtypes[argread] = NULL;

                for (idx = 0; idx < JOSL_FFI_MAX; idx++)
                  {
                    if (strcmp (josl_ffi_types[idx].name, p->token->value.s)
                        == 0)
                      {
                        argtypes[argread] = josl_ffi_types[idx].type;
                        break;
                      }
                  }

                if (argtypes[argread] == NULL)
                  {
                    josl_error_new_file (p->interp, p->filename, tokLineNo,
                                         tokColNo, josl_err_ffi_declaration,
                                         "invalid ffi argument type");
                    return FALSE;
                  }

                argread++;

                if (argread == jffi->argc)
                  break;
              }
          }

        if (ffi_prep_cif (jffi->cif, FFI_DEFAULT_ABI, jffi->argc,
                          rettype, argtypes) != FFI_OK)
          {
            josl_error_new_file (p->interp, p->filename, tokLineNo,
                                 tokColNo, josl_err_ffi_declaration,
                                 "ffi failed to initialize");
            return FALSE;
          }

        var = tgc_alloc (&gc, sizeof (josl_value));
        var->type = josl_vt_ffi;
        var->value.p = jffi;

        hashtable_insert (p->variables, (void *) varname, (void *) var);

        p->tokenType = tt_ffi_word;
      }
      return TRUE;

    case josl_w_minus_ffi_callback:
      {
        char *varname = NULL;
        ffi_cif *cif;
        ffi_type **argtypes = NULL;
        ffi_type *typ, *rettype = NULL;
        int idx, argc = -1, argread = 0;
        josl_value *var;
        josl_ffi *jffi;
        int storedLineNo, storedColNo, storedPos;

        jffi = tgc_alloc (&gc, sizeof (josl_ffi));
        jffi->rettype = -1;
        jffi->argc = 0;
        jffi->cif = tgc_alloc (&gc, sizeof (ffi_cif));
        jffi->vm = p->interp;

        while (josl_parse_get_token (p) == TRUE)
          {
            if (p->tokColNo == 1 || p->tokenType == tt_eof)
              {
                josl_parse_putback_token (p);
                break;
              }
            if (varname == NULL)
              {
                varname = strdup (p->token->value.s);
              }
            else if (rettype == NULL)
              {
                for (idx = 0; idx < JOSL_FFI_MAX; idx++)
                  {
                    if (strcmp (josl_ffi_types[idx].name,
                                p->token->value.s) == 0)
                      {
                        jffi->rettype = idx;
                        rettype = josl_ffi_types[idx].type;
                        break;
                      }
                  }

                if (jffi->rettype == -1)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         tokLineNo, tokColNo,
                                         josl_err_ffi_declaration,
                                         "invalid ffi return type");
                    return FALSE;
                  }

              }
            else
              {
                josl_parse_putback_token (p);
                break;
              }
          }

        /* store parser state because we are going to read
           ahead and determine how many parameters exist */

        storedLineNo = p->lineNo;
        storedColNo = p->colNo;
        storedPos = p->pos;

        /* count parameters so we can allocate the argtypes
           value correctly, later we will really parse the
           data we have read. */

        while (josl_parse_get_token (p) == TRUE)
          {
            if (p->tokColNo == 1 || p->tokenType != tt_word)
              {
                josl_parse_putback_token (p);
                break;
              }

            jffi->argc++;
          }

        /* restore our parser state */
        p->lineNo = storedLineNo;
        p->colNo = storedColNo;
        p->pos = storedPos;

        if (jffi->argc == 0)
          {
            argtypes = NULL;
          }
        else
          {
            /* allocate and parse our parameters now */
            argtypes = tgc_alloc (&gc, sizeof (ffi_type *) * jffi->argc);

            while (josl_parse_get_token (p) == TRUE)
              {
                if (p->tokColNo == 1 || p->tokenType == tt_eof)
                  {
                    josl_parse_putback_token (p);
                    break;
                  }

                argtypes[argread] = NULL;
                jffi->argtypes[argread] = -1;

                for (idx = 0; idx < JOSL_FFI_MAX; idx++)
                  {
                    if (strcmp (josl_ffi_types[idx].name, p->token->value.s)
                        == 0)
                      {
                        argtypes[argread] = josl_ffi_types[idx].type;
                        jffi->argtypes[argread] = idx;
                        break;
                      }
                  }

                if (argtypes[argread] == NULL)
                  {
                    josl_error_new_file (p->interp, p->filename, tokLineNo,
                                         tokColNo, josl_err_ffi_declaration,
                                         "invalid ffi argument type");
                    return FALSE;
                  }

                argread++;

                if (argread == jffi->argc)
                  break;
              }
          }

        if (ffi_prep_cif (jffi->cif, FFI_DEFAULT_ABI, jffi->argc,
                          rettype, argtypes) != FFI_OK)
          {
            josl_error_new_file (p->interp, p->filename, tokLineNo,
                                 tokColNo, josl_err_ffi_declaration,
                                 "ffi failed to initialize");
            return FALSE;
          }

        var = tgc_alloc (&gc, sizeof (josl_value));
        var->type = josl_vt_ffi;
        var->value.p = jffi;

        hashtable_insert (p->variables, (void *) varname, (void *) var);

        p->tokenType = tt_ffi_word;
      }
      return TRUE;

    default:
      josl_error_new_file (p->interp, p->filename, tokLineNo, tokColNo,
                           josl_err_unknown, "unknown directive.");
      return FALSE;
    }
}

static bool
josl_parse_get_token (josl_parser * p)
{
  unsigned int ch;

  /* clear the existing token */
  p->tokenType = tt_none;

  josl_parse_eat_ws (p);

  ch = p->buf[p->pos];
  if (p->colNo == 1 && ch == '-')
    {
      bool result;

      josl_parse_begin_token (p, tt_unknown);
      josl_parse_word (p);
      josl_parse_end_token (p);

      josl_parse_directive (p);

      p->parsingDirective = FALSE;

      return TRUE;
    }
  else if (ch == '#')
    {
      josl_parse_begin_token (p, tt_comment);
      josl_parse_eat_to_eol (p);
      josl_parse_end_token (p);
      return TRUE;
    }
  else if (ch == '"')
    {
      josl_parse_forward (p);   /* get rid of the delimiter */
      josl_parse_begin_token (p, tt_string);
      josl_parse_string_content (p);
      josl_parse_end_token_string (p);
      josl_parse_forward (p);   /* get rid of the delimiter */
      return TRUE;
    }
  else if (ch == '@' && !isspace (p->buf[p->pos + 1]))
    {
      josl_parse_forward (p);   /* get rid of the reference operator */
      josl_parse_begin_token (p, tt_word_ref);
      josl_parse_word (p);
      josl_parse_end_token (p);
      return TRUE;
    }
  else if (ch == 'r' && p->buf[p->pos + 1] == '/')
    {
      josl_parse_forward (p);   /* get rid of r */
      josl_parse_forward (p);   /* get rid of / */
      josl_parse_begin_token (p, tt_regex);
      josl_parse_regex (p);
      josl_parse_end_regex_token (p);
      return TRUE;
    }
  else if (ch == '{' && isspace (p->buf[p->pos + 1]))
    {
      josl_parse_begin_token (p, tt_quotation_start);
      josl_parse_forward (p);
      josl_parse_end_token (p);
      return TRUE;
    }
  else if (ch == '}' && isspace (p->buf[p->pos + 1]))
    {
      josl_parse_begin_token (p, tt_quotation_finish);
      josl_parse_forward (p);
      josl_parse_end_token (p);
      return TRUE;
    }
  else if (ch != 0)
    {
      josl_parse_begin_token (p, p->colNo == 1 ? tt_top_word : tt_word);
      josl_parse_word (p);
      josl_parse_end_token (p);
      return TRUE;
    }

  p->tokenType = tt_eof;

  return FALSE;
}

static void
josl_parse_putback_token (josl_parser * p)
{
  p->pos = p->tokPos;
  p->lineNo = p->tokLineNo;
  p->colNo = p->tokColNo;
  p->tokEndPos = p->pos;
  p->tokEndLineNo = p->tokLineNo;
  p->tokEndColNo = p->tokColNo;

  /* TODO: do we need to free data here? Analyze */
}

#define josl_debug_message(interp, ...) if (interp->debugLevel) printf (__VA_ARGS__)
#define TOPWORD_CHECK(p,topword)                                        \
  if (topword == NULL)                                                  \
    {                                                                   \
      josl_error_new_file (p->interp, p->filename, p->tokLineNo,        \
                           p->tokColNo, josl_err_no_top_word,           \
                           "no top word defined for this body word");   \
      return;                                                           \
    }

static void
josl_parse_loop_jumps (josl_parser * p, josl_top_word * top)
{
  josl_word *w = top->first, *tmpw, *backw;
  int i, skipend = 0;

  p->bsp = 0;

  while (w)
    {
      switch (w->word_id)
        {
        case josl_w_if:
        case josl_w_star_if:
        case josl_w_do_is:
          skipend++;
          break;

        case josl_w_times:
        case josl_w_for:
        case josl_w_while:
        case josl_w_do:
        case josl_w_star_do:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_end:
        case josl_w_next:
        case josl_w_plus_next:
          if (w->word_id != josl_w_end && skipend == 0)
            {
              switch (p->bs[p->bsp - 1]->word_id)
                {
                case josl_w_times:
                case josl_w_for:
                case josl_w_while:
                case josl_w_do:
                case josl_w_star_do:
                  p->bsp--;
                  while (p->bs[p->bsp - 1]->word_id == josl_w_continue)
                    {
                      p->bs[p->bsp - 1]->jump = w;
                      p->bsp--;
                    }

                  break;

                default:
                  break;
                }
            }
          else
            {
              skipend--;
            }
          break;

        case josl_w_break:
          tmpw = p->bs[p->bsp - 1];

          switch (tmpw->word_id)
            {
            case josl_w_times:
            case josl_w_for:
              w->jump = tmpw->jump;
              break;

            case josl_w_do:
            case josl_w_star_do:
              w->jump = tmpw->jump;
              break;

            default:
              break;
            }
          break;

        case josl_w_continue:
          tmpw = p->bs[p->bsp - 1];

          switch (tmpw->word_id)
            {
            case josl_w_do:
            case josl_w_star_do:
              tmpw = p->bs[p->bsp - 2];
              if (tmpw->word_id == josl_w_while)
                w->jump = tmpw->next;
              break;

            case josl_w_times:
            case josl_w_for:
            case josl_w_each:
            case josl_w_each_rev:
            case josl_w_each_kv:
              {
                p->bs[p->bsp] = tmpw;
                p->bs[p->bsp - 1] = w;
                p->bsp++;
              }
              break;


            default:
              w->jump = tmpw;
              break;
            }
          break;

        default:
          break;
        }

      w = w->next;
    }
}

static void
josl_parse_link_branches (josl_parser * p, josl_top_word * top)
{
  josl_word *w = top->first, *tmpw;
  int foundLoopJump = 0;

  while (w)
    {
      switch (w->word_id)
        {
        case josl_w_break:
        case josl_w_continue:
          foundLoopJump = 1;
          break;

        case josl_w_is:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_if:
        case josl_w_star_if:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_else:
          tmpw = p->bs[--p->bsp];
          switch (tmpw->word_id)
            {
            case josl_w_if:
            case josl_w_star_if:
              tmpw->jump = w->next;
              p->bs[p->bsp++] = w;
              break;

            case josl_w_is:
              w->word_id = josl_w_noop;
              p->bsp++;
              break;

            default:
              josl_error_new_word (p->interp, w,
                                   josl_err_invalid_branch_word,
                                   "unexpected else");
              return;
            }
          break;

        case josl_w_while:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_do:
        case josl_w_star_do:
          switch (p->bs[p->bsp - 1]->word_id)
            {
            case josl_w_is:
              w->word_id = josl_w_do_is;
              p->bs[p->bsp++] = w;
              break;

            default:
              p->bs[p->bsp++] = w;
              break;
            }
          break;

        case josl_w_vector:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_end:
          tmpw = p->bs[--p->bsp];

          switch (tmpw->word_id)
            {
            case josl_w_if:
            case josl_w_star_if:
              tmpw->jump = w;
              break;

            case josl_w_else:
              tmpw->nextexec = w;
              break;

            case josl_w_noop:
              break;

            case josl_w_do:
            case josl_w_star_do:
              tmpw->jump = w->next;
              tmpw = p->bs[--p->bsp];
              if (tmpw->word_id != josl_w_while)
                {
                  josl_error_new_word (p->interp, w,
                                       josl_err_invalid_branch_word,
                                       "malformed while/do/end loop");
                  return;
                }
              w->nextexec = tmpw->next;
              break;

            case josl_w_vector:
              /* do nothing */
              break;

            case josl_w_is:
              {
                /* must back patch all end words to link to this end */
                josl_word *dois = tmpw;
                while (dois != w)
                  {
                    dois = dois->next;
                    if (dois->word_id == josl_w_do_is
                        && dois->jump->word_id == josl_w_end)
                      {
                        josl_word *doisEnd = dois->jump;
                        doisEnd->nextexec = w;
                        dois->jump = doisEnd->next;
                        dois = doisEnd;
                      }
                  }
              }
              break;

            case josl_w_do_is:
              tmpw->jump = w;
              break;

            default:
              josl_error_new_word (p->interp, w,
                                   josl_err_invalid_branch_word,
                                   "unexpected end...");
              return;
            }
          break;

        case josl_w_return:
          w->nextexec = NULL;
          w->word_id = josl_w_noop;
          break;

        case josl_w_for:
        case josl_w_times:
        case josl_w_each:
        case josl_w_each_rev:
        case josl_w_each_kv:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_i:
        case josl_w_ii:
          {
            int i, found = 0;

            w->value = NULL;

            /* Look back through the branch stack finding the first
               for or times loop */
            for (i = p->bsp - 1; i >= 0; i--)
              {
                if (p->bs[i]->word_id == josl_w_for ||
                    p->bs[i]->word_id == josl_w_times)
                  {
                    found = 1;
                    break;
                  }
              }

            if (found == 0)
              {
                josl_error_new_word (p->interp, w,
                                     josl_err_invalid_branch_word,
                                     "i must be contained within a for/times loop");
                return;
              }
          }
          break;

        case josl_w_j:
        case josl_w_jj:
          {
            int i, found = 0;

            w->value = NULL;

            /* Look back through the branch stack finding the second
               for or times loop */
            for (i = p->bsp - 1; i >= 0; i--)
              {
                if (p->bs[i]->word_id == josl_w_for ||
                    p->bs[i]->word_id == josl_w_times)
                  {
                    found++;
                    if (found == 2)
                      {
                        break;
                      }
                  }
              }

            if (found != 2)
              {
                josl_error_new_word (p->interp, w,
                                     josl_err_invalid_branch_word,
                                     "i must be contained within a for/times loop");
                return;
              }
          }
          break;

        case josl_w_k:
        case josl_w_kk:
          {
            int i, found = 0;

            w->value = NULL;

            /* Look back through the branch stack finding the second
               for or times loop */
            for (i = p->bsp - 1; i >= 0; i--)
              {
                if (p->bs[i]->word_id == josl_w_for ||
                    p->bs[i]->word_id == josl_w_times)
                  {
                    found++;
                    if (found == 3)
                      {
                        break;
                      }
                  }
              }

            if (found != 3)
              {
                josl_error_new_word (p->interp, w,
                                     josl_err_invalid_branch_word,
                                     "i must be contained within a for/times loop");
                return;
              }
          }
          break;

        case josl_w_next:
        case josl_w_plus_next:
          tmpw = p->bs[--p->bsp];

          switch (tmpw->word_id)
            {
            case josl_w_for:
            case josl_w_times:
            case josl_w_each:
            case josl_w_each_rev:
            case josl_w_each_kv:
              tmpw->jump = w->next;
              w->nextexec = tmpw->next;
              w->jump = w->next;
              break;

            default:
              josl_error_new_word (p->interp, w,
                                   josl_err_invalid_branch_word,
                                   "unexpected next");
              return;
            }
          break;

        case josl_w_begin:
          p->bs[p->bsp++] = w;
          break;

        case josl_w_until:
        case josl_w_star_until:
          tmpw = p->bs[--p->bsp];
          if (tmpw->word_id != josl_w_begin)
            {
              josl_error_new_word (p->interp, w,
                                   josl_err_invalid_branch_word,
                                   "unexpected until");
              return;
            }

          w->jump = tmpw->next;
          break;

        default:
          ;
        }

      w = w->next;
    }

  if (foundLoopJump == 1)
    {
      josl_parse_loop_jumps (p, top);
    }
}

static void
josl_parse (josl_parser * p)
{
  josl_top_word **wordStack = NULL;
  josl_top_word *topword = NULL;
  josl_word *word = NULL, *prevword = NULL;
  josl_word_id wordId;
  josl_value *var = NULL;
  int wsp = -1;

  while (josl_parse_get_token (p) == TRUE)
    {
      if (word != NULL)
        {
          prevword = word;
          word = NULL;
        }

      switch (p->tokenType)
        {
        case tt_word:
        case tt_ns_word:
          TOPWORD_CHECK (p, topword);

          /* Is this word a variable? */
          var = hashtable_search (p->variables, (void *) p->token->value.s);
          if (var == NULL)
            {
              word = new_word (p, topword, p->token->value.s, -1);
            }
          else
            {
              if (var == (void *) 1)
                {
                  /* Variable is not yet created. */
                  var = tgc_alloc (&gc, sizeof (josl_value));
                  var->type = josl_vt_null;
                  hashtable_remove (p->variables, (void *) p->token->value.s);
                  hashtable_insert (p->variables,
                                    (void *) p->token->value.s, (void *) var);
                }

              wordId = josl_w_variable;
              word = new_word (p, topword, p->token->value.s, wordId);
              word->value = var;
            }
          break;

        case tt_top_word:
          if (topword != NULL && topword->first != NULL)
            {
              /* TODO: detect during normal parsing if I need to do this */
              josl_parse_link_branches (p, topword);
              if (p->interp->errorStack != NULL)
                return;

              /* tail call optimization */
              if (topword->last->word_id == josl_w_user &&
                  topword->last->value->value.p == topword)
                {
                  josl_word *tmpw = topword->first;
                  while (tmpw->next != NULL && tmpw->next->next != NULL)
                    tmpw = tmpw->next;
                  tmpw->nextexec = topword->first;
                  tmpw->next = NULL;
                  topword->last = tmpw;
                }
            }

          topword = new_top_word (p);
          break;

        case tt_comment:
          break;

        case tt_constant:
          {
            josl_word_id pushWordId;
            josl_value *constVal = hashtable_search (p->interp->constants,
                                                     (void *) p->token->value.
                                                     s);

            if (constVal == NULL)
              {
                josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                                     p->tokColNo,
                                     josl_err_unknown_constant,
                                     "unknown constant");
                return;
              }

            word = new_word (p, topword, p->token->value.s, josl_w_push);
            word->value = constVal;
          }
          break;

        case tt_integer:
          word =
            new_word (p, topword, strdup_printf ("%d", p->token->value.i),
                      josl_w_push);
          word->value = tgc_alloc (&gc, sizeof (josl_value));
          word->value->type = josl_vt_integer;
          word->value->value.i = p->token->value.i;
          break;

        case tt_double:
          word =
            new_word (p, topword, strdup_printf ("%f", p->token->value.i),
                      josl_w_push);
          word->value = tgc_alloc (&gc, sizeof (josl_value));
          word->value->type = josl_vt_double;
          word->value->value.d = p->token->value.d;
          break;

        case tt_string:
          word = new_word (p, topword, strdup ("<str>"), josl_w_push);
          word->value = tgc_alloc (&gc, sizeof (josl_value));
          word->value->type = josl_vt_string;
          word->value->value.s = strdup (p->token->value.s);
          break;

        case tt_regex:
#if HAVE_PCRE == 1
          {
            const char *errmsg = NULL;
            int erroffset = 0;
            word = new_word (p, topword, p->token->value.s, josl_w_push);
            word->value = tgc_alloc (&gc, sizeof (josl_value));
            word->value->type = josl_vt_regex;
            word->value->value.regex =
              pcre_compile (p->token->value.s, p->tokIntData1, &errmsg,
                            &erroffset, NULL);
            if (word->value->value.regex == NULL)
              {
                josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                                     p->tokColNo,
                                     josl_err_invalid_regex, errmsg);
                return;
              }
          }
#else
          josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                               p->tokColNo,
                               josl_err_regex_not_supported,
                               "Regular expressions not support in this compilation of Josl");
          return;
#endif
          break;

        case tt_word_ref:
          {
            josl_word_id wid;
            josl_word_ref *wref;

            wid = josl_word_get_id (p->interp, p->token->value.s);

            wref = tgc_alloc (&gc, sizeof (josl_word_ref));
            if (wid == -1)
              {
                wref->word = hashtable_search (p->words, p->token->value.s);
                if (wref->word == NULL)
                  {
                    josl_error_new_file (p->interp, p->filename,
                                         p->tokLineNo, p->tokColNo,
                                         josl_err_invalid_word_reference,
                                         "invalid word reference");
                    return;
                  }
              }
            else
              wref->word_id = wid;

            word = new_word (p, topword, p->token->value.s, josl_w_push);
            word->value = tgc_alloc (&gc, sizeof (josl_value));
            word->value->type = josl_vt_word;
            word->value->value.wref = wref;
          }
          break;

        case tt_quotation_start:
          {
            if (wordStack == NULL)
              wordStack = tgc_alloc (&gc, sizeof (josl_top_word) * 20);
            wsp++;
            wordStack[wsp] = topword;
            topword = tgc_alloc (&gc, sizeof (josl_top_word));
            topword->name = strdup_printf ("quot%p", topword);
            topword->filename = p->filename;
            topword->lineNo = p->tokLineNo;
            topword->first = NULL;
            topword->last = NULL;
          }
          break;

        case tt_quotation_finish:
          {
            josl_top_word *quot = topword;
            topword = wordStack[wsp];

            word = new_word (p, topword, topword->name, josl_w_push);
            word->value = tgc_alloc (&gc, sizeof (josl_value));
            word->value->type = josl_vt_word;
            word->value->value.wref = tgc_alloc (&gc, sizeof (josl_word_ref));
            word->value->value.wref->word = quot;
            word->value->value.wref->word_id = josl_w_user;
            wsp--;
          }
          break;

        case tt_rename:
        case tt_ffi_library:
        case tt_ffi_word:
          break;

        case tt_include:
        case tt_include_as:
          {
            char *filename = josl_parse_find_include (p->interp,
                                                      (const char *)
                                                      p->tokCharData1);

            if (filename == NULL)
              {
                josl_error_new_file (p->interp, p->filename, p->tokLineNo,
                                     p->tokColNo,
                                     josl_err_parse_file_not_found,
                                     "could not find include file");
                return;
              }

            /* Only parse if we have not already */
            if (hashtable_search (p->interp->filewords, filename) == NULL)
              {
                josl_parse_file (p->interp, filename);
                if (p->interp->errorStack != NULL)
                  return;
              }

            if (p->tokenType == tt_include)
              {
                /* Copy exported word definitions into our local words */
                josl_hash *fw;
                josl_hash_itr *itr;

                fw = hashtable_search (p->interp->filewords, filename);
                itr = hashtable_iterator (fw);

                do
                  {
                    char *name = hashtable_iterator_key (itr);
                    josl_top_word *tw = hashtable_iterator_value (itr);

                    hashtable_insert (p->words, (void *) name, (void *) tw);
                  }
                while (hashtable_iterator_advance (itr));
              }
            else
              {
                hashtable_insert (p->namespaces,
                                  (void *) p->tokCharData2,
                                  (void *) p->tokCharData1);
              }
          }
          break;

        case tt_eof:
          break;

        case tt_unknown:
          break;

        default:
          break;
        }
    }

  if (topword != NULL)
    {
      josl_parse_link_branches (p, topword);
      if (p->interp->errorStack != NULL)
        return;
    }
}

bool did_inc_path_init = FALSE;

static void
josl_parse_init (josl * vm, const char *filename, josl_parser * p)
{
  if (did_inc_path_init == FALSE)
    {
      josl_parse_init_include_path ();
      did_inc_path_init = TRUE;
    }

  p->words = hashtable_search (vm->filewords, (void *) filename);
  if (p->words == NULL)
    {
      p->words = create_hashtable (0, strhash, strhasheq);
      hashtable_insert (vm->filewords, (void *) filename, p->words);
    }

  p->interp = vm;
  p->filename = strdup (filename);
  p->namespaces = create_hashtable (3, strhash, strhasheq);
  p->variables = create_hashtable (5, strhash, strhasheq);
  p->parsingDirective = FALSE;
  p->lineNo = 1;
  p->colNo = 1;
  p->pos = 0;
  p->token = tgc_alloc (&gc, sizeof (josl_value));
  p->token->type = tt_none;
  p->tokenType = tt_none;
  p->tokLineNo = 0;
  p->tokColNo = 0;
  p->tokEndColNo = 0;
  p->tokPos = 0;
  p->tokIntData1 = 0;
  p->tokIntData2 = 0;
  p->tokCharData1 = NULL;
  p->tokCharData2 = NULL;
  p->buf = NULL;
  p->tmpBuf = cbuf_alloc (256);
  p->bsp = 0;
}

static void
josl_parse_cleanup (josl_parser * p)
{
}

void
josl_parse_string (josl * interp, const char *str)
{
  josl_parser p;

  josl_parse_init (interp, str_filename, &p);
  p.buf = strdup (str);
  if (p.buf == NULL)
    {
      josl_parse_cleanup (&p);
      josl_error_new (interp, josl_err_parse,
                      strdup
                      ("couldn't allocate enough memory for string buffer."));
      return;
    }

  josl_parse (&p);
}

void
josl_parse_file (josl * interp, const char *filename)
{
  FILE *fh;
  size_t len;
  size_t readlen;
  josl_parser p;

  josl_parse_init (interp, filename, &p);

  fh = fopen (filename, "rb");
  if (fh == NULL)
    {
      josl_parse_cleanup (&p);
      josl_error_new (interp, josl_err_parse,
                      strdup_printf ("couldn't open file '%s'", filename));
      return;
    }

  fseek (fh, 0L, SEEK_END);
  len = ftell (fh) + 1;
  fseek (fh, 0L, 0L);

  p.buf = tgc_alloc (&gc, len * sizeof (char));
  if (p.buf == NULL)
    {
      josl_parse_cleanup (&p);
      josl_error_new (interp, josl_err_parse,
                      strdup
                      ("couldn't allocate enough memory for file buffer."));
      return;
    }

  readlen = fread (p.buf, sizeof (char), len, fh);
  if (readlen != len - 1)
    {
      josl_parse_cleanup (&p);
      josl_error_new (interp, josl_err_parse,
                      strdup_printf
                      ("file size is %d but could only read %d bytes.",
                       len, readlen));
      return;
    }

  p.buf[readlen] = 0;

  fclose (fh);
  josl_parse (&p);
}
