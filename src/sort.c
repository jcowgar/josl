/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <josl.h>
#include <josl/error.h>
#include <josl/sort.h>
#include <josl/value.h>

int
josl_value_compare (const josl_value *va, const josl_value *vb, void *userdata)
{
  if (userdata != NULL)
    return josl_value_user_compare (va, vb, userdata);

  switch (va->type)
    {
    case josl_vt_null:
      switch (vb->type)
        {
        case josl_vt_null:
          return 0;

        default:
          break;
        }
      break;

    case josl_vt_integer:
      switch (vb->type)
        {
        case josl_vt_null:
          return 1;

        case josl_vt_integer:
          if (va->value.i < vb->value.i) return -1;
          if (va->value.i == vb->value.i) return 0;
          return 1;

        case josl_vt_double:
          return (va->value.i > vb->value.d) - (va->value.i < vb->value.d);

        default:
          break;
        }
      break;

    case josl_vt_double:
      switch (vb->type)
        {
        case josl_vt_null:
          return 1;

        case josl_vt_integer:
          return (va->value.d > vb->value.i) - (va->value.d < vb->value.i);

        default:
          break;
        }
      break;

    case josl_vt_string:
      {
        int result = 0;
        switch (vb->type)
          {
          case josl_vt_null:
          case josl_vt_integer:
          case josl_vt_double:
            return 1;

          case josl_vt_string:
            result = strcmp (va->value.s, vb->value.s);
            break;

          case josl_vt_sbuffer:
            result = strcmp (va->value.s, vb->value.b->data);
            break;
          }
        /* needed for BSDs as they may return -2, -3, etc... */
        if (result > 0) return 1;
        if (result < 0) return -1;
        return 0;
      }

    case josl_vt_sbuffer:
      {
        int result = 0;
        switch (vb->type)
          {
          case josl_vt_null:
          case josl_vt_integer:
          case josl_vt_double:
            return 1;

          case josl_vt_string:
            result = strcmp (va->value.b->data, vb->value.s);
            break;

          case josl_vt_sbuffer:
            result = strcmp (va->value.b->data, vb->value.b->data);
            break;

          default:
            return 0;
            break;
          }
        /* needed for BSDs as they may return -2, -3, etc... */
        if (result > 0) return 1;
        if (result < 0) return -1;
        return 0;
      }

    default:
      break;
    }

  if (va->type < vb->type)
    return -1;
  else if (va->type > vb->type)
    return 1;

  return 0;
}

int
josl_value_user_compare (const josl_value *va, const josl_value *vb,
                         void *userdata)
{
  josl_user_sort *us = (josl_user_sort *) userdata;
  josl_value *tmp;

  us->interp->s[us->interp->sp]->type = va->type;
  us->interp->s[us->interp->sp++]->value = va->value;
  us->interp->s[us->interp->sp]->type = vb->type;
  us->interp->s[us->interp->sp++]->value = vb->value;

  josl_exec_word (us->interp, us->topword);

  tmp = us->interp->s[--us->interp->sp];

  return tmp->value.i;
}

/*
 * This is the actual sort function. Notice that it returns the new
 * head of the list. (It has to, because the head will not
 * generally be the same element after the sort.) So unlike sorting
 * an array, where you can do
 *
 *     sort(myarray);
 *
 * you now have to do
 *
 *     list = listsort(mylist);
 *
 * Function adapted from:
 *    http://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html
 *
 * Original copyright which applies to the josl_list_sort function:
 *
 * =====================================================================
 * This file is copyright 2001 Simon Tatham.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL SIMON TATHAM BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * =====================================================================
 */

josl_list_item *
josl_list_sort (josl_list_item * list, int is_circular, int is_double, void *userdata)
{
  josl_list_item *p, *q, *e, *tail, *oldhead;
  int insize, nmerges, psize, qsize, i;

  /*
   * Silly special case: if `list' was passed in as NULL, return
   * NULL immediately.
   */
  if (!list)
    return NULL;

  insize = 1;

  while (1)
    {
      p = list;
      oldhead = list;		       /* only used for circular linkage */
      list = NULL;
      tail = NULL;

      nmerges = 0;  /* count number of merges we do in this pass */

      while (p)
        {
          nmerges++;  /* there exists a merge to be done */
          /* step `insize' places along from p */
          q = p;
          psize = 0;
          for (i = 0; i < insize; i++)
            {
              psize++;
              if (is_circular)
                q = (q->next == oldhead ? NULL : q->next);
              else
                q = q->next;
              if (!q) break;
            }

          /* if q hasn't fallen off end, we have two lists to merge */
          qsize = insize;

          /* now we have two lists; merge them */
          while (psize > 0 || (qsize > 0 && q))
            {
              /* decide whether next element of merge comes from p or q */
              if (psize == 0)
                {
                  /* p is empty; e must come from q. */
                  e = q; q = q->next; qsize--;
                  if (is_circular && q == oldhead)
                    q = NULL;
		}
              else if (qsize == 0 || !q)
                {
                  /* q is empty; e must come from p. */
                  e = p; p = p->next; psize--;
                  if (is_circular && p == oldhead)
                    p = NULL;
		}
              else if (josl_value_compare(p->v, q->v, userdata) <= 0)
                {
                  /* First element of p is lower (or same);
                   * e must come from p. */
                  e = p; p = p->next; psize--;
                  if (is_circular && p == oldhead) p = NULL;
		}
              else
                {
                  /* First element of q is lower; e must come from q. */
                  e = q; q = q->next; qsize--;
                  if (is_circular && q == oldhead) q = NULL;
		}

              /* add the next element to the merged list */
              if (tail)
                {
                  tail->next = e;
		}
              else
                {
                  list = e;
		}
              if (is_double)
                {
                  /* Maintain reverse pointers in a doubly linked list. */
                  e->prev = tail;
                }
              tail = e;
            }

          /* now p has stepped `insize' places along, and q has too */
          p = q;
        }
      if (is_circular)
        {
          tail->next = list;
          if (is_double)
            list->prev = tail;
	}
      else
        tail->next = NULL;

      /* If we have done only one merge, we're finished. */
      if (nmerges <= 1)   /* allow for nmerges==0, the empty list case */
        return list;

      /* Otherwise repeat, merging lists twice the size */
      insize *= 2;
    }
}

void
josl_qsort(josl_value ** data, int n, void *userdata)
{
  register int i, j;
  josl_value *v, *t;

  if (n <= 1)
    return;

  // Partition elements
  v = data[0];
  i = 0;
  j = n;

  for(;;)
    {
      while(josl_value_compare (data[++i], v, userdata) == -1 && i < n) { }
      while(josl_value_compare (data[--j], v, userdata) == 1) { }

      if(i >= j)
        break;

      t = data[i];
      data[i] = data[j];
      data[j] = t;
    }

  t = data[i-1];
  data[i-1] = data[0];
  data[0] = t;

  josl_qsort(data, i-1, userdata);
  josl_qsort(data+i, n-i, userdata);
}
