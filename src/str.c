/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*!
  \defgroup str String Utilities

  String utilities making life a bit easier.

*/

#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <josl/str.h>

/*!
  \ingroup str

  String hash function.

*/

unsigned int
strhash (void *key)
{
  unsigned long hash = 5381;
  int c;

  char *str = (char *) key;

  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;    /* hash * 33 + c */

  return hash;
}

int
strhasheq (void *a, void *b)
{
  return strcmp ((char *) a, (char *) b) == 0;
}

typedef enum
{
  pn_skip_white,
  pn_read_digits,
  pn_read_decimal,
  pn_done
} pn_state;

/*!
  \ingroup str

  Convert a string (decorated or not) to a 64-bit integer.

  Decorations can contain commas and underscores. For example, 58467,
  58,467 and 58_467 will all parse correctly to 58467.

  \param s String to parse integer from.

  \sa string_to_double

*/

int64
string_to_int64 (const char *s)
{
  int ch;
  int64 limit = LLONG_MAX / 10;
  pn_state state = pn_skip_white;
  bool is_negative = FALSE;
  int64 value = 0;

  if (s == NULL)
    return 0;

  while (state != pn_done)
    {
      ch = (unsigned char) *s++;

      switch (state)
        {
        case pn_skip_white:
          if (isspace (ch))
            {
              /* do nothing at all */
            }
          else if (ch == '-')
            {
              state = pn_read_digits;
              is_negative = TRUE;
            }
          else if (ch == '+')
            {
              state = pn_read_digits;
            }
          else if (isdigit (ch))
            {
              state = pn_read_digits;
              value = ch - '0';
            }
          else
            {
              state = pn_done;
            }
          break;

        case pn_read_digits:
          if (isdigit (ch))
            {
              ch -= '0';

              if ((value < limit) || ((LLONG_MAX - value * 10) >= ch))
                {
                  value *= 10;
                  value += ch;
                }
              else
                {
                  value = LLONG_MAX;
                  state = pn_done;
                }
            }
          else if (ch == ',' || ch == '_')
            {
              /* do nothing */
            }
          else
            {
              state = pn_done;
            }
          break;

        default:
          break;
        }
    }

  if (is_negative)
    {
      value *= -1;
    }

  return value;
}

/*!
  \ingroup str

  Convert a string (decorated or not) to a double.

  Decorations can contain commas and underscores. For example, 58467.887348,
  58,467.887,348 and 58_467.887_348 will all parse correctly to 58467.887348.
  Further, decorations can be mixed. 58,467.887_348 is valid.

  \param s String to parse integer from.

  \sa string_to_double

*/

double
string_to_double (const char *s)
{
  int ch;
  double limit = DBL_MAX / 10;
  pn_state state = pn_skip_white;
  bool is_negative = FALSE;
  double value = 0;
  double inc = 0.1;

  if (s == NULL)
    return 0;

  while (state != pn_done)
    {
      ch = (unsigned char) *s++;

      switch (state)
        {
        case pn_skip_white:
          if (isspace (ch))
            {
              /* do nothing at all */
            }
          else if (ch == '-')
            {
              state = pn_read_digits;
              is_negative = TRUE;
            }
          else if (ch == '+')
            {
              state = pn_read_digits;
            }
          else if (ch == '.')
            {
              state = pn_read_decimal;
            }
          else if (isdigit (ch))
            {
              state = pn_read_digits;
              value = ch - '0';
            }
          else
            {
              state = pn_done;
            }
          break;

        case pn_read_digits:
          if (isdigit (ch))
            {
              ch -= '0';

              if ((value < limit) || ((DBL_MAX - value * 10) >= ch))
                {
                  value *= 10;
                  value += ch;
                }
              else
                {
                  value = DBL_MAX;
                  state = pn_done;
                }
            }
          else if (ch == ',' || ch == '_')
            {
              /* do nothing */
            }
          else if (ch == '.')
            {
              state = pn_read_decimal;
            }
          else
            {
              state = pn_done;
            }
          break;

        case pn_read_decimal:
          if (isdigit (ch))
            {
              value += (ch - '0') * inc;
              inc *= 0.1;
            }
          else if (ch == ',' || ch == '_')
            {
              /* do nothing */
            }
          else
            {
              state = pn_done;
            }
          break;

        default:
          break;
        }
    }

  if (is_negative)
    {
      value *= -1.0;
    }

  return value;
}

/*!
  \ingroup str

  Duplicate a printf style formatted string.

 */

char *
strdup_printf (const char *fmt, ...)
{
  /* Guess we need no more than 100 bytes. */
  int n, size = 100;
  char *msg;
  va_list ap;

  if ((msg = tgc_alloc (&gc, size)) == NULL)
    return NULL;

  while (1)
    {
      /* Try to print in the allocated space. */
      va_start (ap, fmt);
      n = vsnprintf (msg, size, fmt, ap);
      va_end (ap);

      /* If that worked, return the string. */
      if (n > -1 && n < size)
        break;

      /* otherwise, try again with more space. */
      if (n > -1)               /* glibc 2.1 */
        size = n + 1;           /* precisely what is needed */
      else                      /* glibc 2.0 */
        size *= 2;              /* twice the old size */
      if ((msg = tgc_realloc (&gc, msg, size)) == NULL)
        return NULL;
    }

  return msg;
}

/*!
  \ingroup str

  Perform a strstr search but backward.

 */

char *
strrstr (const char *s, const char *t)
{
  size_t i, j, tlen = strlen (t);


  for (i = strlen (s); i >= tlen; i--)
    {
      for (j = 0; j < tlen && s[i - tlen + j] == t[j]; j++)
        ;
      if (j == tlen)
        return (char *) (s + i - tlen);
    }


  return NULL;
}

#if defined(MINGW) || defined(OSX)       \

/*!
  \ingroup str

  Duplicate up to n characters.

*/

char *
strndup (char const *s, size_t n)
{
  size_t slen = strlen (s);
  size_t len = slen > n ? n : slen;
  char *new = tgc_alloc (&gc, len + 1);

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return memcpy (new, s, len);
}
#endif

/*!
  \ingroup str

  Allocate a new character buffer (growable string).

*/

charbuf *
cbuf_alloc (const int size)
{
  charbuf *cb;
  cb = tgc_alloc (&gc, sizeof (charbuf));
  if (cb == NULL)
    return NULL;

  cb->data = tgc_alloc (&gc, sizeof (char) * size);
  if (cb->data == NULL)
    {
      return NULL;
    }

  cb->bufsize = size;
  cb->len = 0;
  cb->data[0] = 0;

  return cb;
}

/*!
  \ingroup str

  Grow the character buffer to a new size.

*/

bool
cbuf_realloc (charbuf * cb, const int newsize)
{
  if (newsize == 0)
    {
      cb->len = 0;
      cb->data[0] = 0;
      return TRUE;
    }
  else if (newsize > cb->bufsize)
    {
      char *newdata = tgc_realloc (&gc, cb->data, newsize);
      if (newdata == NULL)
        return FALSE;
      cb->data = newdata;
      cb->bufsize = newsize;
    }

  return TRUE;
}

/*!
  \ingroup str

  Assign the string to the character buffer. This overwrites any existing data.

*/

bool
cbuf_assign (charbuf * cb, const char *str, int count)
{
  if (count == -1)
    count = strlen (str);

  if (cb->bufsize < count)
    {
      if (cbuf_realloc (cb, count) == FALSE)
        return FALSE;
    }

  memcpy (cb->data, str, count);
  cb->len = count;
  cb->data[count] = 0;

  return TRUE;
}

/*!
  \ingroup str

  Append a single character onto the end of the character buffer.

*/

bool
cbuf_append_ch (charbuf * cb, const int ch)
{
  if (cb->len + 1 > cb->bufsize)
    {
      if (cbuf_realloc (cb, cb->bufsize + 1) == FALSE)
        return FALSE;
    }

  cb->data[cb->len] = ch;
  cb->data[++cb->len] = 0;

  return TRUE;
}

/*!
  \ingroup str

  Append a string onto the end of the character buffer. The first
  count characters will be copied. If count is -1, the entire string
  is copied.

*/

bool
cbuf_append_str (charbuf * cb, const char *str, int count)
{
  if (count < 0)
    count = strlen (str);

  if (cb->len + count > cb->bufsize)
    {
      if (cbuf_realloc (cb, cb->len + (count * 2)) == FALSE)
        return FALSE;
    }

  memcpy (&cb->data[cb->len], str, count);
  cb->len += count;
  cb->data[cb->len] = 0;

  return TRUE;
}

bool
cbuf_append_printf (charbuf * cb, const char *fmt, ...)
{
  /* Guess we need no more than 100 bytes. */
  int n, size = 100;
  char *msg;
  bool result;
  va_list ap;

  if ((msg = tgc_alloc (&gc, size)) == NULL)
    return FALSE;

  while (1)
    {
      /* Try to print in the allocated space. */
      va_start (ap, fmt);
      n = vsnprintf (msg, size, fmt, ap);
      va_end (ap);

      /* If that worked, return the string. */
      if (n > -1 && n < size)
        break;

      /* otherwise, try again with more space. */
      if (n > -1)               /* glibc 2.1 */
        size = n + 1;           /* precisely what is needed */
      else                      /* glibc 2.0 */
        size *= 2;              /* twice the old size */
      if ((msg = tgc_realloc (&gc, msg, size)) == NULL)
        return FALSE;
    }

  result = cbuf_append_str (cb, msg, -1);

  return result;
}


/*!
  \ingroup str

  Truncate the contents of a character buffer.

*/

void
cbuf_truncate (charbuf * cb, const int position)
{
  cb->len = position;
  cb->data[position] = 0;
}
