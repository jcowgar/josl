/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*!

  \file vm.c

  Virtual Machine (initialization, interaction with and cleanup of).

  \defgroup init Initialization
  \defgroup intrinsic_setup Intrinsic Setup

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <josl.h>
#include <josl/hashtable.h>
#include <josl/str.h>
#include <josl/vm.h>

/*!
  \ingroup init

  Initialize a Josl VM

  \param interp Josl interpreter structure
  \return TRUE if interp was properly initialized otherwise FALSE

  \sa josl_cleanup
*/

bool
josl_init (josl ** interp)
{
  int idx;
  josl *newInterp;

  newInterp = tgc_alloc (&gc, sizeof (josl));
  newInterp->s = tgc_alloc (&gc, sizeof (void *) * JOSL_MAX_STACK);
  newInterp->sp = 0;
  newInterp->ts = tgc_alloc (&gc, sizeof (void *) * JOSL_MAX_TSTACK);
  newInterp->tsp = 0;
  newInterp->errorStack = NULL;
  newInterp->excCode = josl_ok;
  newInterp->debugLevel = 0;
  newInterp->intrinsics = create_hashtable (300, strhash, strhasheq);
  newInterp->filewords = create_hashtable (10, strhash, strhasheq);
  newInterp->constants = create_hashtable (50, strhash, strhasheq);

  for (idx = 0; idx < JOSL_MAX_STACK; idx++)
    newInterp->s[idx] = tgc_alloc (&gc, sizeof (josl_value));
  for (idx = 0; idx < JOSL_MAX_TSTACK; idx++)
    newInterp->ts[idx] = tgc_alloc (&gc, sizeof (josl_value));

  *interp = newInterp;

  josl_words_core (*interp);

  return TRUE;
}

/*!
  \ingroup init

  Cleanup all resources used by the Josl VM

  interp will be set to NULL.

  \param interp Josl interpreter structure to cleanup

  \sa josl_init

*/
void
josl_cleanup (josl ** interp)
{
  *interp = NULL;
}

#define josl_add_core_word(x,y,z) hashtable_insert (x->intrinsics, \
  (void *) strdup (y), (void *) z)

/*!
  \ingroup intrinsic_setup

  Activate core words.

  Core words include these word groups:
  - Stack
  - Boolean
  - Math
  - Bitmath
  - String
  - Conversion
  - Variables

  Core words are activated automatically with the initialization call josl_init

*/

void
josl_words_core (josl * interp)
{
  /* Parsing Words */
  josl_add_core_word (interp, "'0", josl_w_place_0);
  josl_add_core_word (interp, "'1", josl_w_place_1);
  josl_add_core_word (interp, "'2", josl_w_place_2);
  josl_add_core_word (interp, "'3", josl_w_place_3);
  josl_add_core_word (interp, "'4", josl_w_place_4);
  josl_add_core_word (interp, "'5", josl_w_place_5);
  josl_add_core_word (interp, "'6", josl_w_place_6);
  josl_add_core_word (interp, "'7", josl_w_place_7);
  josl_add_core_word (interp, "'8", josl_w_place_8);
  josl_add_core_word (interp, "'9", josl_w_place_9);

  /* Type Constants */
  josl_add_integer (interp, "T-NULL", josl_vt_null);
  josl_add_integer (interp, "T-INTEGER", josl_vt_integer);
  josl_add_integer (interp, "T-DOUBLE", josl_vt_double);
  josl_add_integer (interp, "T-STRING", josl_vt_string);
  josl_add_integer (interp, "T-BUFFER", josl_vt_sbuffer);
  josl_add_integer (interp, "T-ARRAY", josl_vt_array);
  josl_add_integer (interp, "T-LIST", josl_vt_list);
  josl_add_integer (interp, "T-DICTIONARY", josl_vt_dict);

  /* Exception Handling */
  josl_add_core_word (interp, "fail", josl_w_fail);
  josl_add_core_word (interp, "resume", josl_w_resume);
  josl_add_core_word (interp, "resume-this", josl_w_resume_this);
  josl_add_core_word (interp, "retry", josl_w_retry);

  /* Exception Constants */
  josl_add_integer (interp, "Again?", josl_err_again);
  josl_add_integer (interp, "AlreadyExists?", josl_err_already_exists);
  josl_add_integer (interp, "ArgumentListTooBig?", josl_err_arg_list_too_big);
  josl_add_integer (interp, "BadAddress?", josl_err_bad_address);
  josl_add_integer (interp, "BadFileNumber?", josl_err_bad_file_no);
  josl_add_integer (interp, "BadMessage?", josl_err_bad_message);
  josl_add_integer (interp, "BrokenPipe?", josl_err_broken_pipe);
  josl_add_integer (interp, "BrokenPipe?", josl_err_broken_pipe);
  josl_add_integer (interp, "CrossDeviceLink?", josl_err_cross_device_link);
  josl_add_integer (interp, "DeviceOrResourceBusy?",
                    josl_err_device_resource_busy);
  josl_add_integer (interp, "DirectoryNotEmpty?", josl_err_dir_not_empty);
  josl_add_integer (interp, "ExecFormat?", josl_err_exec_format);
  josl_add_integer (interp, "FFIRegisterFailed?",
                    josl_err_ffi_register_failed);
  josl_add_integer (interp, "FileTooBig?", josl_err_file_too_big);
  josl_add_integer (interp, "FilenameTooLong?", josl_err_filename_too_long);
  josl_add_integer (interp, "IllegalByteSequence?",
                    josl_err_illegal_byte_sequence);
  josl_add_integer (interp, "IndexOutOfBounds?",
                    josl_err_index_out_of_bounds);
  josl_add_integer (interp, "InterruptedFunction?",
                    josl_err_interrputed_func);
  josl_add_integer (interp, "InvalidArgument?", josl_err_invalid_argument);
  josl_add_integer (interp, "InvalidSeek?", josl_err_invalid_seek);
  josl_add_integer (interp, "Io?", josl_err_io);
  josl_add_integer (interp, "IsADirectory?", josl_err_is_a_directory);
  josl_add_integer (interp, "Memory?", josl_err_memory);
  josl_add_integer (interp, "NameNotUnique?", josl_err_name_not_unique);
  josl_add_integer (interp, "NoLink?", josl_err_no_link);
  josl_add_integer (interp, "NoLocksAvailable?", josl_err_no_locks);
  josl_add_integer (interp, "NoMessage?", josl_err_no_message);
  josl_add_integer (interp, "NoSpaceLeft?", josl_err_no_space_left);
  josl_add_integer (interp, "NoSpawnedProcesses?", josl_err_no_spawned_child);
  josl_add_integer (interp, "NoStreamsAvailable?",
                    josl_err_no_streams_available);
  josl_add_integer (interp, "NoSuchDevice?", josl_err_no_such_device);
  josl_add_integer (interp, "NoSuchDevice?", josl_err_no_such_device);
  josl_add_integer (interp, "NoSuchFile?", josl_err_no_such_file);
  josl_add_integer (interp, "NoSuchProcess?", josl_err_no_such_process);
  josl_add_integer (interp, "NoTty?", josl_err_no_tty);
  josl_add_integer (interp, "NotADirectory?", josl_err_not_a_directory);
  josl_add_integer (interp, "NotAStream?", josl_err_not_a_stream);
  josl_add_integer (interp, "NotBoolean?", josl_err_not_boolean);
  josl_add_integer (interp, "NotComparable?", josl_err_not_comparable);
  josl_add_integer (interp, "NotEnoughMemory?", josl_err_not_enough_memory);
  josl_add_integer (interp, "NotOnNetwork?", josl_err_not_on_network);
  josl_add_integer (interp, "OperationNotPermitted?",
                    josl_err_op_not_permitted);
  josl_add_integer (interp, "PathNotFound?", josl_err_path_not_found);
  josl_add_integer (interp, "PermissionDenied?", josl_err_permission_denied);
  josl_add_integer (interp, "ProtocolError?", josl_err_protocol_error);
  josl_add_integer (interp, "ReadOnlyFileSystem?", josl_err_read_only_fs);
  josl_add_integer (interp, "ReadOnlyFileSystem?", josl_err_ro_file_system);
  josl_add_integer (interp, "RegexNotSupported?",
                    josl_err_regex_not_supported);
  josl_add_integer (interp, "RemoteAddressChanged?",
                    josl_err_remote_address_changed);
  josl_add_integer (interp, "ResourceDeadlock?", josl_err_resource_deadlock);
  josl_add_integer (interp, "ResultTooLarge?", josl_err_result_too_large);
  josl_add_integer (interp, "StackOverflow?", josl_err_stack_overflow);
  josl_add_integer (interp, "StackType?", josl_err_stack_type);
  josl_add_integer (interp, "StackUnderflow?", josl_err_stack_underflow);
  josl_add_integer (interp, "StringTruncated?", josl_err_string_truncated);
  josl_add_integer (interp, "TempStackOverflow?", josl_err_tstack_overflow);
  josl_add_integer (interp, "TempStackUnderflow?", josl_err_tstack_underflow);
  josl_add_integer (interp, "TimerExpired?", josl_err_timer_expired);
  josl_add_integer (interp, "TooManyFilesOpened?",
                    josl_err_too_many_files_opened);
  josl_add_integer (interp, "TooManyLinks?", josl_err_too_many_links);
  josl_add_integer (interp, "TooManyLinks?", josl_err_too_many_links);

  josl_add_core_word (interp, "malloc", josl_w_mem_malloc);

  /* Dynamic Execution */
  josl_add_core_word (interp, "ffi-exec", josl_w_ffi_exec);
  josl_add_core_word (interp, "ffi-callback", josl_w_ffi_callback);
  josl_add_core_word (interp, "parse", josl_w_parse);
  josl_add_core_word (interp, "exec-word", josl_w_exec_word);
  josl_add_core_word (interp, "curry", josl_w_curry);
  josl_add_core_word (interp, "2curry", josl_w_2curry);
  josl_add_core_word (interp, "3curry", josl_w_3curry);
  josl_add_core_word (interp, "curry#", josl_w_curry_pound);
  josl_add_core_word (interp, "curry?", josl_w_curry_question);
  josl_add_core_word (interp, "compose", josl_w_compose);

  /* Compiler Directives */
  josl_add_core_word (interp, "-public", josl_w_minus_public);
  josl_add_core_word (interp, "-include", josl_w_minus_include);
  josl_add_core_word (interp, "-constant", josl_w_minus_constant);
  josl_add_core_word (interp, "-enum", josl_w_minus_enum);
  josl_add_core_word (interp, "-variable", josl_w_minus_variable);
  josl_add_core_word (interp, "-rename", josl_w_minus_rename);
  josl_add_core_word (interp, "-ffi-library", josl_w_minus_ffi_library);
  josl_add_core_word (interp, "-ffi-word", josl_w_minus_ffi_word);
  josl_add_core_word (interp, "-ffi-callback", josl_w_minus_ffi_callback);

  /* System Information */
  josl_add_core_word (interp, "os", josl_w_os);

  /* Branching Words */
  josl_add_core_word (interp, "begin", josl_w_begin);
  josl_add_core_word (interp, "do", josl_w_do);
  josl_add_core_word (interp, "else", josl_w_else);
  josl_add_core_word (interp, "end", josl_w_end);
  josl_add_core_word (interp, "each", josl_w_each);
  josl_add_core_word (interp, "each-kv", josl_w_each_kv);
  josl_add_core_word (interp, "each-rev", josl_w_each_rev);
  josl_add_core_word (interp, "for", josl_w_for);
  josl_add_core_word (interp, "if", josl_w_if);
  josl_add_core_word (interp, "while", josl_w_while);
  josl_add_core_word (interp, "times", josl_w_times);
  josl_add_core_word (interp, "next", josl_w_next);
  josl_add_core_word (interp, "+next", josl_w_plus_next);
  josl_add_core_word (interp, "until", josl_w_until);
  josl_add_core_word (interp, "i", josl_w_i);
  josl_add_core_word (interp, "j", josl_w_j);
  josl_add_core_word (interp, "k", josl_w_k);
  josl_add_core_word (interp, "ii", josl_w_ii);
  josl_add_core_word (interp, "jj", josl_w_jj);
  josl_add_core_word (interp, "kk", josl_w_kk);
  josl_add_core_word (interp, "vector", josl_w_vector);
  josl_add_core_word (interp, "is", josl_w_is);
  josl_add_core_word (interp, "return", josl_w_return);
  josl_add_core_word (interp, "break", josl_w_break);
  josl_add_core_word (interp, "continue", josl_w_continue);

  /* Non-dropping branch words */
  josl_add_core_word (interp, "*if", josl_w_star_if);
  josl_add_core_word (interp, "*do", josl_w_star_do);
  josl_add_core_word (interp, "*until", josl_w_star_until);

  /* Stack */
  josl_add_core_word (interp, "depth", josl_w_depth);
  josl_add_core_word (interp, "dup", josl_w_dup);
  josl_add_core_word (interp, "2dup", josl_w_2dup);
  josl_add_core_word (interp, "3dup", josl_w_3dup);
  josl_add_core_word (interp, "dup#", josl_w_dup_pound);
  josl_add_core_word (interp, "drop", josl_w_drop);
  josl_add_core_word (interp, "2drop", josl_w_2drop);
  josl_add_core_word (interp, "3drop", josl_w_3drop);
  josl_add_core_word (interp, "drop#", josl_w_drop_pound);
  josl_add_core_word (interp, "swap", josl_w_swap);
  josl_add_core_word (interp, "2swap", josl_w_2swap);
  josl_add_core_word (interp, "3swap", josl_w_3swap);
  josl_add_core_word (interp, "swap#", josl_w_swap_pound);
  josl_add_core_word (interp, "over", josl_w_over);
  josl_add_core_word (interp, "2over", josl_w_2over);
  josl_add_core_word (interp, "3over", josl_w_3over);
  josl_add_core_word (interp, "rot", josl_w_rot);
  josl_add_core_word (interp, "-rot", josl_w_minus_rot);
  josl_add_core_word (interp, "2rot", josl_w_2rot);
  josl_add_core_word (interp, "-2rot", josl_w_minus_2rot);
  josl_add_core_word (interp, "3rot", josl_w_3rot);
  josl_add_core_word (interp, "-3rot", josl_w_minus_3rot);
  josl_add_core_word (interp, "roll", josl_w_roll);
  josl_add_core_word (interp, "-roll", josl_w_minus_roll);
  josl_add_core_word (interp, "pick", josl_w_pick);
  josl_add_core_word (interp, "nip", josl_w_nip);
  josl_add_core_word (interp, "tuck", josl_w_tuck);
  josl_add_core_word (interp, "dip", josl_w_dip);
  josl_add_core_word (interp, "2dip", josl_w_2dip);
  josl_add_core_word (interp, ">t", josl_w_to_temp);
  josl_add_core_word (interp, ">s", josl_w_from_temp);
  josl_add_core_word (interp, ">t#", josl_w_to_temp_count);
  josl_add_core_word (interp, ">s#", josl_w_from_temp_count);

  /* Boolean */
  josl_add_core_word (interp, "=", josl_w_equal);
  josl_add_core_word (interp, "*=", josl_w_star_equal);
  josl_add_core_word (interp, "0=", josl_w_equal_zero);
  josl_add_core_word (interp, "1=", josl_w_equal_one);
  josl_add_core_word (interp, "-1=", josl_w_equal_minus_one);
  josl_add_core_word (interp, "not=", josl_w_not_equal);
  josl_add_core_word (interp, "*not=", josl_w_star_not_equal);
  josl_add_core_word (interp, "not", josl_w_not);
  josl_add_core_word (interp, ">", josl_w_gt);
  josl_add_core_word (interp, "*>", josl_w_star_gt);
  josl_add_core_word (interp, "<", josl_w_lt);
  josl_add_core_word (interp, "*<", josl_w_star_lt);
  josl_add_core_word (interp, ">=", josl_w_gt_equal);
  josl_add_core_word (interp, "*>=", josl_w_star_gt_equal);
  josl_add_core_word (interp, "<=", josl_w_lt_equal);
  josl_add_core_word (interp, "*<=", josl_w_star_lt_equal);
  josl_add_core_word (interp, "between?", josl_w_between);
  josl_add_core_word (interp, "*between?", josl_w_star_between);
  josl_add_core_word (interp, "and?", josl_w_and);
  josl_add_core_word (interp, "or?", josl_w_or);

  /* Math */
  josl_add_core_word (interp, "even?", josl_w_even);
  josl_add_core_word (interp, "odd?", josl_w_odd);
  josl_add_core_word (interp, "+", josl_w_plus);
  josl_add_core_word (interp, "-", josl_w_minus);
  josl_add_core_word (interp, "*", josl_w_multiply);
  josl_add_core_word (interp, "/", josl_w_divide);
  josl_add_core_word (interp, "%", josl_w_mod);
  josl_add_core_word (interp, "1+", josl_w_one_plus);
  josl_add_core_word (interp, "1-", josl_w_one_minus);
  josl_add_core_word (interp, "2+", josl_w_two_plus);
  josl_add_core_word (interp, "2-", josl_w_two_minus);
  josl_add_core_word (interp, "negate", josl_w_negate);
  josl_add_core_word (interp, "abs", josl_w_abs);
  josl_add_core_word (interp, "min", josl_w_min);
  josl_add_core_word (interp, "max", josl_w_max);
  josl_add_core_word (interp, "ceil", josl_w_ceil);
  josl_add_core_word (interp, "floor", josl_w_floor);
  josl_add_core_word (interp, "round", josl_w_round);
  josl_add_core_word (interp, "round-to", josl_w_round_to);
  josl_add_core_word (interp, "acos", josl_w_acos);
  josl_add_core_word (interp, "asin", josl_w_asin);
  josl_add_core_word (interp, "atan", josl_w_atan);
  josl_add_core_word (interp, "atan2", josl_w_atan2);
  josl_add_core_word (interp, "cos", josl_w_cos);
  josl_add_core_word (interp, "cosh", josl_w_cosh);
  josl_add_core_word (interp, "sin", josl_w_sin);
  josl_add_core_word (interp, "sinh", josl_w_sinh);
  josl_add_core_word (interp, "tan", josl_w_tan);
  josl_add_core_word (interp, "tanh", josl_w_tanh);
  josl_add_core_word (interp, "exp", josl_w_exp);
  josl_add_core_word (interp, "exp2", josl_w_exp2);
  josl_add_core_word (interp, "expm1", josl_w_expm1);
  josl_add_core_word (interp, "frexp", josl_w_frexp);
  josl_add_core_word (interp, "log", josl_w_log);
  josl_add_core_word (interp, "log1p", josl_w_log1p);
  josl_add_core_word (interp, "log2", josl_w_log2);
  josl_add_core_word (interp, "log10", josl_w_log10);
  josl_add_core_word (interp, "modf", josl_w_modf);
  josl_add_core_word (interp, "pow", josl_w_pow);
  josl_add_core_word (interp, "sqrt", josl_w_sqrt);
  josl_add_core_word (interp, "fmod", josl_w_fmod);
  josl_add_core_word (interp, "radians", josl_w_radians);
  josl_add_core_word (interp, "degrees", josl_w_degrees);
  josl_add_core_word (interp, "rand", josl_w_rand);
  josl_add_core_word (interp, "seed-rand", josl_w_srand);
  josl_add_double (interp, "PI", 3.1415926535897932384);
  josl_add_double (interp, "2PI", 6.28318530717958647692);
  josl_add_double (interp, "PI/2", 1.57079632679489661923);
  josl_add_double (interp, "E", 2.71828182845904523536);

  /* Bitmath */
  josl_add_core_word (interp, "&", josl_w_bit_and);
  josl_add_core_word (interp, "|", josl_w_bit_or);
  josl_add_core_word (interp, "^", josl_w_bit_xor);
  josl_add_core_word (interp, "~", josl_w_bit_not);
  josl_add_core_word (interp, ">>", josl_w_shift_right);
  josl_add_core_word (interp, "<<", josl_w_shift_left);

  /* Conversion */
  josl_add_core_word (interp, ">str", josl_w_to_string);
  josl_add_core_word (interp, ">int", josl_w_to_integer);
  josl_add_core_word (interp, ">dbl", josl_w_to_double);

  /* Types */
  josl_add_core_word (interp, "integer?", josl_w_is_integer);
  josl_add_core_word (interp, "double?", josl_w_is_double);
  josl_add_core_word (interp, "number?", josl_w_is_number);
  josl_add_core_word (interp, "string?", josl_w_is_string);
  josl_add_core_word (interp, "dict?", josl_w_is_dict);
  josl_add_core_word (interp, "array?", josl_w_is_array);
  josl_add_core_word (interp, "list?", josl_w_is_list);
  josl_add_core_word (interp, "pointer?", josl_w_is_pointer);
  josl_add_core_word (interp, "null?", josl_w_is_null);
  josl_add_core_word (interp, "variable?", josl_w_is_variable);
  josl_add_core_word (interp, "word?", josl_w_is_word);
  josl_add_core_word (interp, "type", josl_w_type);
  josl_add_core_word (interp, "null", josl_w_null);

  /* String */
  josl_add_core_word (interp, "len", josl_w_len);
  josl_add_core_word (interp, "append", josl_w_append);
  josl_add_core_word (interp, "append#", josl_w_append_pound);
  josl_add_core_word (interp, "join", josl_w_join);
  josl_add_core_word (interp, "join#", josl_w_join_pound);
  josl_add_core_word (interp, "left", josl_w_left);
  josl_add_core_word (interp, "right", josl_w_right);
  josl_add_core_word (interp, "mid", josl_w_mid);
  josl_add_core_word (interp, "lower", josl_w_lower);
  josl_add_core_word (interp, "upper", josl_w_upper);
  josl_add_core_word (interp, "title", josl_w_title);
  josl_add_core_word (interp, "capitalize", josl_w_capitalize);
  josl_add_core_word (interp, "trim", josl_w_trim);
  josl_add_core_word (interp, "trim-head", josl_w_trim_head);
  josl_add_core_word (interp, "trim-tail", josl_w_trim_tail);
  josl_add_core_word (interp, "split", josl_w_split);
  josl_add_core_word (interp, "split#", josl_w_split_pound);
  josl_add_core_word (interp, "splitany", josl_w_splitany);
  josl_add_core_word (interp, "splitany#", josl_w_splitany_pound);
  josl_add_core_word (interp, "asc", josl_w_asc);
  josl_add_core_word (interp, "chr", josl_w_chr);
  josl_add_core_word (interp, "find", josl_w_find);
  josl_add_core_word (interp, "rfind", josl_w_rfind);
  josl_add_core_word (interp, "find#", josl_w_find_pound);
  josl_add_core_word (interp, "rfind#", josl_w_rfind_pound);
  josl_add_core_word (interp, "cut", josl_w_cut);
  josl_add_core_word (interp, "peel", josl_w_peel);
  josl_add_core_word (interp, "delete", josl_w_delete);
  josl_add_core_word (interp, "replace", josl_w_replace);
  josl_add_core_word (interp, "insert", josl_w_insert);
  josl_add_core_word (interp, "format", josl_w_format);
  josl_add_core_word (interp, "repeat", josl_w_repeat);
  josl_add_core_word (interp, "reverse", josl_w_reverse);
  josl_add_core_word (interp, "translate", josl_w_translate);
  josl_add_core_word (interp, "alnum?", josl_w_is_alnum);
  josl_add_core_word (interp, "alpha?", josl_w_is_alpha);
  josl_add_core_word (interp, "cntrl?", josl_w_is_cntrl);
  josl_add_core_word (interp, "digit?", josl_w_is_digit);
  josl_add_core_word (interp, "graph?", josl_w_is_graph);
  josl_add_core_word (interp, "lower?", josl_w_is_lower);
  josl_add_core_word (interp, "print?", josl_w_is_print);
  josl_add_core_word (interp, "punct?", josl_w_is_punct);
  josl_add_core_word (interp, "space?", josl_w_is_space);
  josl_add_core_word (interp, "upper?", josl_w_is_upper);
  josl_add_core_word (interp, "xdigit?", josl_w_is_xdigit);
  josl_add_core_word (interp, "prefix?", josl_w_has_prefix);
  josl_add_core_word (interp, "suffix?", josl_w_has_suffix);
  josl_add_core_word (interp, "at", josl_w_at);

  /* Date Words */
  josl_add_core_word (interp, "clock", josl_w_clock);
  josl_add_core_word (interp, "time", josl_w_time);

  josl_add_integer (interp, "CLOCKS-PER-SECOND", CLOCKS_PER_SEC);

  /* Intrinsic Type Words */
  josl_add_core_word (interp, "!", josl_w_set);
  josl_add_core_word (interp, "!!", josl_w_set_keep);
  josl_add_core_word (interp, "@", josl_w_get);
  josl_add_core_word (interp, "dict", josl_w_dict);
  josl_add_core_word (interp, "sized-dict", josl_w_sized_dict);
  josl_add_core_word (interp, "remove", josl_w_remove);
  josl_add_core_word (interp, "has-key?", josl_w_has_key);
  josl_add_core_word (interp, "copy", josl_w_copy);
  josl_add_core_word (interp, "array", josl_w_array);
  josl_add_core_word (interp, "list", josl_w_list);
  josl_add_core_word (interp, "push-head", josl_w_push_head);
  josl_add_core_word (interp, "push-tail", josl_w_push_tail);
  josl_add_core_word (interp, "current", josl_w_current);
  josl_add_core_word (interp, "forward", josl_w_forward);
  josl_add_core_word (interp, "backward", josl_w_backward);
  josl_add_core_word (interp, "head", josl_w_head);
  josl_add_core_word (interp, "tail", josl_w_tail);
  josl_add_core_word (interp, "go-head", josl_w_go_head);
  josl_add_core_word (interp, "go-tail", josl_w_go_tail);
  josl_add_core_word (interp, "pop-head", josl_w_pop_head);
  josl_add_core_word (interp, "pop-tail", josl_w_pop_tail);
  josl_add_core_word (interp, "sbuf", josl_w_sbuf);
  josl_add_core_word (interp, "sized-sbuf", josl_w_sized_sbuf);
  josl_add_core_word (interp, "re-compile", josl_w_re_compile);
  josl_add_core_word (interp, "regexp", josl_w_regexp);
  josl_add_core_word (interp, "regextract", josl_w_regextract);
  josl_add_core_word (interp, "regsub", josl_w_regsub);
  josl_add_core_word (interp, "keys", josl_w_keys);
  josl_add_core_word (interp, "values", josl_w_values);
  josl_add_core_word (interp, "capacity", josl_w_capacity);
  josl_add_core_word (interp, "array-resize", josl_w_array_resize);
  josl_add_core_word (interp, "sort", josl_w_sort);
  josl_add_core_word (interp, "sort-user", josl_w_sort_user);

  /* Functional Words */
  josl_add_core_word (interp, "map", josl_w_map);
  josl_add_core_word (interp, "filter", josl_w_filter);
  josl_add_core_word (interp, "fold", josl_w_fold);
  josl_add_core_word (interp, "foldr", josl_w_foldr);
}

/*!
  \ingroup intrinsic_setup

  Activate IO words

*/

void
josl_words_io (josl * interp)
{
  josl_add_core_word (interp, "print", josl_w_print);
  josl_add_core_word (interp, "println", josl_w_println);

  josl_add_core_word (interp, "file-open", josl_w_file_open);
  josl_add_core_word (interp, "close", josl_w_close);
  josl_add_core_word (interp, "eof", josl_w_eof);
  josl_add_core_word (interp, "read-char", josl_w_read_ch);
  josl_add_core_word (interp, "read-line", josl_w_read_line);
  josl_add_core_word (interp, "read-string", josl_w_read_str);
  josl_add_core_word (interp, "read-blob", josl_w_read_blob);
  josl_add_core_word (interp, "write", josl_w_write);
  josl_add_core_word (interp, "writeln", josl_w_writeln);

  josl_add_pointer (interp, "STDIN", stdin);
  josl_add_pointer (interp, "STDOUT", stdout);
  josl_add_pointer (interp, "STDERR", stderr);
}

void
josl_words_filesystem (josl * interp)
{
  josl_add_core_word (interp, "file-size", josl_w_file_size);
  josl_add_core_word (interp, "file-ctime", josl_w_file_create_time);
  josl_add_core_word (interp, "file-mtime", josl_w_file_modify_time);
  josl_add_core_word (interp, "file-atime", josl_w_file_access_time);
  josl_add_core_word (interp, "dir?", josl_w_is_dir);
  josl_add_core_word (interp, "readable?", josl_w_is_readable);
  josl_add_core_word (interp, "writable?", josl_w_is_writable);
  josl_add_core_word (interp, "executable?", josl_w_is_executable);
  josl_add_core_word (interp, "file-exists?", josl_w_file_exists);
  josl_add_core_word (interp, "file-copy", josl_w_file_copy);
  josl_add_core_word (interp, "file-remove", josl_w_file_remove);
  josl_add_core_word (interp, "file-move", josl_w_file_move);
  josl_add_core_word (interp, "dir-open", josl_w_dir_open);
  josl_add_core_word (interp, "dir-next", josl_w_dir_next);
  josl_add_core_word (interp, "dir-close", josl_w_dir_close);
  josl_add_core_word (interp, "dir-create", josl_w_dir_create);
  josl_add_core_word (interp, "dir-remove", josl_w_dir_remove);
}

/*!
  \ingroup intrinsic_setup

  Activate System words

*/

void
josl_words_system (josl * interp)
{
  josl_add_core_word (interp, "argc", josl_w_argc);
  josl_add_core_word (interp, "argv", josl_w_argv);
  josl_add_core_word (interp, "env", josl_w_env);
  josl_add_core_word (interp, "system", josl_w_system);
  josl_add_core_word (interp, "exit", josl_w_exit);
}

/*!
  \ingroup intrinsic_setup

  Activate socket words
*/

void
josl_words_socket (josl * interp)
{
  josl_add_core_word (interp, "socket", josl_w_sock_socket);
  josl_add_core_word (interp, "sock-open", josl_w_sock_connect);
  josl_add_core_word (interp, "select", josl_w_sock_select);
  josl_add_core_word (interp, "bind", josl_w_sock_bind);
  josl_add_core_word (interp, "listen", josl_w_sock_listen);
  josl_add_core_word (interp, "accept", josl_w_sock_accept);
  josl_add_core_word (interp, "udp-write", josl_w_sock_send_to);
  josl_add_core_word (interp, "udp-read", josl_w_sock_receive_from);
  josl_add_core_word (interp, "service-by-name", josl_w_sock_serv_by_name);
  josl_add_core_word (interp, "service-by-port", josl_w_sock_serv_by_port);
  josl_add_core_word (interp, "host-by-name", josl_w_sock_host_by_name);
  josl_add_core_word (interp, "host-by-address", josl_w_sock_host_by_addr);

  josl_add_integer (interp, "AF-UNSPEC", AF_UNSPEC);
  josl_add_integer (interp, "AF-UNIX", AF_UNIX);
  josl_add_integer (interp, "AF-INET", AF_INET);
  josl_add_integer (interp, "AF-INET6", AF_INET6);
  josl_add_integer (interp, "AF-APPLETALK", AF_APPLETALK);
  josl_add_integer (interp, "AF-BLUETOOTH", AF_BLUETOOTH);
  josl_add_integer (interp, "SOCK-STREAM", SOCK_STREAM);
  josl_add_integer (interp, "SOCK-DGRAM", SOCK_DGRAM);
  josl_add_integer (interp, "SOCK-RAW", SOCK_RAW);
  josl_add_integer (interp, "SOCK-RDM", SOCK_RDM);
  josl_add_integer (interp, "SOCK-SEQPACKET", SOCK_SEQPACKET);
  josl_add_integer (interp, "IPPROTO-IP", IPPROTO_IP);
  josl_add_integer (interp, "IPPROTO-ICMP", IPPROTO_ICMP);
  josl_add_integer (interp, "IPPROTO-IGMP", IPPROTO_IGMP);
  josl_add_integer (interp, "IPPROTO-GGP", IPPROTO_GGP);
  josl_add_integer (interp, "IPPROTO-TCP", IPPROTO_TCP);
  josl_add_integer (interp, "IPPROTO-PUP", IPPROTO_PUP);
  josl_add_integer (interp, "IPPROTO-UDP", IPPROTO_UDP);
  josl_add_integer (interp, "IPPROTO-IDP", IPPROTO_IDP);
  josl_add_integer (interp, "IPPROTO-ND", IPPROTO_ND);
  josl_add_integer (interp, "IPPROTO-RAW", IPPROTO_RAW);
  josl_add_integer (interp, "MSG-PEEK", MSG_PEEK);
  josl_add_integer (interp, "MSG-OOB", MSG_OOB);

  josl_add_integer (interp, "AddressFamilyNotSupported?",
                    josl_err_address_family_not_supported);
  josl_add_integer (interp, "AddressInUse?", josl_err_address_in_use);
  josl_add_integer (interp, "AddressNotAvailable?",
                    josl_err_address_not_available);
  josl_add_integer (interp, "BadProtocolOption?",
                    josl_err_bad_protocol_option);
  josl_add_integer (interp, "CallCancelled?", josl_err_call_cancelled);
  josl_add_integer (interp, "CannotTranslateName?",
                    josl_err_cannot_translate_name);
  josl_add_integer (interp, "ClassTypeNotFound?",
                    josl_err_class_type_not_found);
  josl_add_integer (interp, "ConnectionAborted?",
                    josl_err_connection_aborted);
  josl_add_integer (interp, "ConnectionRefused?",
                    josl_err_connection_refused);
  josl_add_integer (interp, "ConnectionReset?", josl_err_connection_reset);
  josl_add_integer (interp, "ConnectionTimedOut?",
                    josl_err_connection_timed_out);
  josl_add_integer (interp, "DatabaseQueryRefused?",
                    josl_err_database_query_refused);
  josl_add_integer (interp, "DestinationAddressRequired?",
                    josl_err_destination_address_required);
  josl_add_integer (interp, "DirectoryNotEmpty?", josl_err_dir_not_empty);
  josl_add_integer (interp, "DiskQuotaExceeded?",
                    josl_err_disk_quota_exceeded);
  josl_add_integer (interp, "HostIsDown?", josl_err_host_is_down);
  josl_add_integer (interp, "HostNotFound?", josl_err_host_not_found);
  josl_add_integer (interp, "HostUnreachable?", josl_err_host_unreachable);
  josl_add_integer (interp, "InvalidHandle?", josl_err_invalid_handle);
  josl_add_integer (interp, "InvalidParameter?", josl_err_invalid_parameter);
  josl_add_integer (interp, "IoIncomplete?", josl_err_io_incomplete);
  josl_add_integer (interp, "IoPending?", josl_err_io_pending);
  josl_add_integer (interp, "ItemIsRemote?", josl_err_item_is_remote);
  josl_add_integer (interp, "MessageTooLong?", josl_err_message_too_long);
  josl_add_integer (interp, "NameTooLong?", josl_err_name_too_long);
  josl_add_integer (interp, "NetworkDown?", josl_err_network_is_down);
  josl_add_integer (interp, "NetworkReset?", josl_err_network_reset);
  josl_add_integer (interp, "NetworkSubsystemUnavailable?",
                    josl_err_network_system_not_ready);
  josl_add_integer (interp, "NetworkUnreachable?",
                    josl_err_network_is_unreachable);
  josl_add_integer (interp, "NoBufferSpace?",
                    josl_err_no_buffer_space_available);
  josl_add_integer (interp, "NoDataRecord?", josl_err_no_data);
  josl_add_integer (interp, "NoMoreResults?", josl_err_no_more_results);
  josl_add_integer (interp, "NonauthorizedHostNotFound?",
                    josl_err_nonauthoritative_host_not_found);
  josl_add_integer (interp, "NonrecoverableError?",
                    josl_err_nonrecoverable_error);
  josl_add_integer (interp, "NotASocket?", josl_err_not_socket);
  josl_add_integer (interp, "NotInitialized?",
                    josl_err_winsock_not_initialized);
  josl_add_integer (interp, "OperationAborted?", josl_err_op_aborted);
  josl_add_integer (interp, "OperationInProgress?", josl_err_op_in_progress);
  josl_add_integer (interp, "OperationNotSupported?",
                    josl_err_op_not_supported);
  josl_add_integer (interp, "ProcedureTableInvalid?",
                    josl_err_procedure_table_invalid);
  josl_add_integer (interp, "ProtocolFamilyNotSupported?",
                    josl_err_protocol_family_not_supported);
  josl_add_integer (interp, "ProtocolNotSupported?",
                    josl_err_protocol_not_supported);
  josl_add_integer (interp, "ResourceTemporarilyUnavailable?",
                    josl_err_resource_temporarily_unavailable);
  josl_add_integer (interp, "ServiceInitFailed?",
                    josl_err_service_init_failed);
  josl_add_integer (interp, "ServiceNotFound?", josl_err_service_not_found);
  josl_add_integer (interp, "ServiceProviderInvalid?",
                    josl_err_service_invalid);
  josl_add_integer (interp, "ShutdownInProgress?",
                    josl_err_shutdown_in_progress);
  josl_add_integer (interp, "SocketAlreadyConnected?",
                    josl_err_socket_already_connected);
  josl_add_integer (interp, "SocketNotConnected?",
                    josl_err_socket_not_connected);
  josl_add_integer (interp, "SocketTypeNotSupported?",
                    josl_err_socket_type_not_supported);
  josl_add_integer (interp, "SocketWasShutdown?",
                    josl_err_socket_was_shutdown);
  josl_add_integer (interp, "StaleFileHandle?", josl_err_stale_file_handle);
  josl_add_integer (interp, "SystemCallFailed?",
                    josl_err_system_call_failure);
  josl_add_integer (interp, "TooManyProcesses?",
                    josl_err_too_many_references);
  josl_add_integer (interp, "TooManyReferences?",
                    josl_err_too_many_references);
  josl_add_integer (interp, "UserQuotaExceeded?",
                    josl_err_user_quota_exceeded);
  josl_add_integer (interp, "WinsockVersionError?",
                    josl_err_winsock_version_error);
  josl_add_integer (interp, "WindowsError?", josl_err_winspecific);
  josl_add_integer (interp, "WrongProtocol?", josl_err_wrong_protocol);
}

/*!
  \ingroup intrinsic_setup

  Activate debug/interactive words

*/

void
josl_words_interactive (josl * interp)
{
  josl_add_core_word (interp, "words", josl_w_words);
  josl_add_core_word (interp, "word-list", josl_w_word_list);
  josl_add_core_word (interp, "user-word?", josl_w_is_user_word);
  josl_add_core_word (interp, "dot-word", josl_w_word_dot);
  josl_add_core_word (interp, "word-show", josl_w_word_show);
  josl_add_core_word (interp, "stack-show", josl_w_stack_show);
  josl_add_core_word (interp, "test", josl_w_test);
  josl_add_core_word (interp, "test-fail-count", josl_w_test_fail_count);
  josl_add_core_word (interp, "test-pass-count", josl_w_test_pass_count);
  josl_add_core_word (interp, "test-total-count", josl_w_test_total_count);
  josl_add_core_word (interp, "test-report", josl_w_test_report);
  josl_add_core_word (interp, "test-verbose", josl_w_test_verbose);
  josl_add_core_word (interp, "test-is-verbose?", josl_w_test_is_verbose);
}

/*!
  \ingroup intrinsic_setup

  Add a new intrinsic word

*/
void
josl_add_word (josl * interp, const char *word)
{
}

/*!
  \ingroup intrinsic_setup

  Add a new intrinsic integer value
*/
void
josl_add_integer (josl * interp, const char *name, int64 v)
{
  josl_value *val = tgc_alloc (&gc, sizeof (josl_value));
  val->type = josl_vt_integer;
  val->value.i = v;

  hashtable_insert (interp->constants, (void *) name, (void *) val);
}

/*!
  \ingroup intrinsic_setup

  Add a new intrinsic double value

*/
void
josl_add_double (josl * interp, const char *name, double v)
{
  josl_value *val = tgc_alloc (&gc, sizeof (josl_value));
  val->type = josl_vt_double;
  val->value.d = v;

  hashtable_insert (interp->constants, (void *) name, (void *) val);
}

/*!
  \ingroup intrinsic_setup

  Add a new intrinsic string
*/
void
josl_add_string (josl * interp, const char *name, const char *v)
{
  josl_value *val = tgc_alloc (&gc, sizeof (josl_value));
  val->type = josl_vt_string;
  val->value.s = (char *) v;

  hashtable_insert (interp->constants, (void *) name, (void *) val);
}

/*!
  \ingroup intrinsic_setup

  Add a new intrinsic pointer
*/
void
josl_add_pointer (josl * interp, const char *name, const void *v)
{
  josl_value *val = tgc_alloc (&gc, sizeof (josl_value));
  val->type = josl_vt_pointer;
  val->value.p = (void *) v;

  hashtable_insert (interp->constants, (void *) name, (void *) val);
}

/*!

  Get the id for the given word.

  \param interp Josl interpreter structure to cleanup
  \param word Word to lookup
  \return word id or -1 for not found.

*/

josl_word_id
josl_word_get_id (josl * interp, const char *word)
{
  void *d = hashtable_search (interp->intrinsics, (void *) word);
  return (d == NULL ? -1 : (josl_word_id) d);
}

/* vim: sw=2: set expandtab: */
