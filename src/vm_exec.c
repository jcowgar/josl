/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*!

  \file vm_exec.c

  Virtual Machine Execution.

*/

#ifdef WINDOWS
#include <windows.h>
#endif /* WINDOWS */

#if HAVE_PCRE == 1
#include <pcre.h>
#endif /* HAVE_PCRE == 1 */

#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <ffi.h>

#include <josl.h>
#include <josl/hashtable.h>
#include <josl/hashtable_itr.h>
#include <josl/parse.h>
#include <josl/socket.h>
#include <josl/sort.h>
#include <josl/str.h>
#include <josl/vm.h>

bool verbose_tests = FALSE;
int failed_test_count = 0;
int passed_test_count = 0;

#ifdef WINDOWS

int josl_sock_did_init = 0;

static void
josl_sock_init (josl * vm)
{
  WORD wVersionRequested;
  WSADATA wsaData;
  int err;

  if (josl_sock_did_init == 1)
    return;

  wVersionRequested = MAKEWORD (2, 2);
  err = WSAStartup (wVersionRequested, &wsaData);
  if (err != 0)
    {
      /* TODO: real error */
      fprintf (stderr, "sock init failed\n");
      exit (0);
    }

  josl_sock_did_init = 1;
}

static void
josl_sock_cleanup (josl * vm)
{
  if (josl_sock_did_init == 1)
    WSACleanup ();
  josl_sock_did_init = 0;
}

#else /* WINDOWS */

static void
josl_sock_init (josl * vm)
{
}

static void
josl_sock_cleanup (josl * vm)
{
}

#endif /* WINDOWS */

#ifndef WINDOWS
static int
CopyFile (const char *from_filename, const char *to_filename, int ignore)
{
  FILE *fromFh, *toFh;
  char buf[4096];

  fromFh = fopen (from_filename, "rb");
  if (fromFh == NULL)
    return 0;

  toFh = fopen (to_filename, "wb");
  if (toFh == NULL)
    {
      fclose (fromFh);
      return 0;
    }

  size_t size;
  while ((size = fread (buf, 1, sizeof (buf), fromFh)) > 0)
    fwrite (buf, 1, size, toFh);
  fclose (fromFh);
  fclose (toFh);

  return 1;

}
#endif /* WINDOWS */

#ifdef FREEBSD
static double
log2 (double n)
{
  return log (n) / 0.6931471805599453;
}
#endif /* FREEBSD */

#define STACK(x) interp->s[x]
#define SP interp->sp

/* Null the value and ensure the GC will free memory for
 * any string, string buffer, list, etc...
 */
#define NULLVALUE(a)                            \
  a->type = josl_vt_null;                       \
  a->value.i = 0;

#define BREAK                                                           \
  if (interp->errorStack != NULL)                                       \
    {                                                                   \
      josl_error_new_word (interp, w, josl_err_call_stack,              \
                           "call stack");                               \
      return ;                                                          \
    }                                                                   \
  if (w->nextexec == NULL)                                              \
    goto done;                                                          \
  w=w->nextexec;                                                        \
  goto *dynids[w->word_id]

#define EXEC                                                            \
  if (interp->errorStack != NULL)                                       \
    {                                                                   \
      josl_error_new_word (interp, w, josl_err_call_stack,              \
                           "call stack");                               \
      return ;                                                          \
    }                                                                   \
  if (w == NULL)                                                        \
    goto done;                                                          \
  goto *dynids[w->word_id]

#define JUMP                                                            \
  if (interp->errorStack != NULL)                                       \
    {                                                                   \
      josl_error_new_word (interp, w, josl_err_call_stack,              \
                           "call stack");                               \
      return ;                                                          \
    }                                                                   \
  if (w->jump == NULL)                                                  \
    goto done;                                                          \
  w=w->jump;                                                            \
  goto *dynids[w->word_id]

#define RETRY goto *dynids[w->word_id]

#define THROW_EXC(errcode, errmsg)                              \
  switch (josl_throw_exception (interp, w, errcode, errmsg))    \
    {                                                           \
    case josl_exc_retry:                                        \
      RETRY;                                                    \
    case josl_exc_resume:                                       \
      return;                                                   \
    case josl_exc_resume_this:                                  \
    default:                                                    \
      BREAK;                                                    \
    }

#define THROW_ERRNO_EXC()                         \
  switch (josl_throw_errno_exception (interp, w)) \
    {                                             \
    case josl_exc_retry:                          \
      RETRY;                                      \
    case josl_exc_resume:                         \
      return;                                     \
    case josl_exc_resume_this:                    \
    default:                                      \
      BREAK;                                      \
    }

#define THROW_SOCK_EXC()                         \
  switch (josl_throw_sock_exception (interp, w)) \
    {                                            \
    case josl_exc_retry:                         \
      RETRY;                                     \
    case josl_exc_resume:                        \
      return;                                    \
    case josl_exc_resume_this:                   \
    default:                                     \
      BREAK;                                     \
    }

#define UNDERFLOW_CHECK(interp, word, x) {                              \
    if (SP < x)                                                         \
      {                                                                 \
        THROW_EXC (josl_err_stack_underflow, "stack underflow");        \
      }                                                                 \
  }

#define OVERFLOW_CHECK(interp, word, x) {                               \
    if ((SP + x) == JOSL_MAX_STACK)                                     \
      {                                                                 \
        THROW_EXC (josl_err_stack_overflow, "stack overflow");          \
      }                                                                 \
  }

#define TUNDERFLOW_CHECK(interp, word, x) {                             \
    if (interp->tsp < x)                                                \
      {                                                                 \
        THROW_EXC (josl_err_tstack_underflow,                           \
                   "temporary stack underflow");                        \
      }                                                                 \
  }

#define TOVERFLOW_CHECK(interp, word, x) {                              \
    if ((interp->tsp + x) == JOSL_MAX_TSTACK)                           \
      {                                                                 \
        THROW_EXC (josl_err_tstack_overflow,                            \
                   "temporary stack overflow");                         \
      }                                                                 \
  }

#define MOVE(x,y)                                               \
  STACK(y)->type = STACK(x)->type;                              \
  STACK(y)->value = STACK(x)->value;

#define SWAP(x,y)                               \
  {                                             \
    josl_value *tmpv;                           \
    tmpv = STACK (SP);                          \
    tmpv->type = STACK(x)->type;                \
    tmpv->value = STACK(x)->value;              \
    STACK(x)->type = STACK(y)->type;            \
    STACK(x)->value = STACK(y)->value;          \
    STACK(y)->type = tmpv->type;                \
    STACK(y)->value = tmpv->value;              \
  }

#define COPY(x,y)                                               \
  STACK(y)->type = STACK(x)->type;                              \
  STACK(y)->value = STACK(x)->value;

#define COPYV(x,y) y->type = x->type; y->value = x->value;

void
josl_ffi_callback (ffi_cif * cif, unsigned int *ret, void *args[],
                   void *userdata)
{
  josl_ffi *ffi = (josl_ffi *) userdata;
  int idx;
  josl_value *varg, *vresult;

  for (idx = 0; idx < ffi->argc; idx++)
    {
      varg = ffi->vm->s[ffi->vm->sp++];
      switch (ffi->argtypes[idx])
        {
        case JOSL_FFI_VOID:
          varg->type = josl_vt_null;
          break;

        case JOSL_FFI_INT:
        case JOSL_FFI_UINT:
        case JOSL_FFI_UINT8:
        case JOSL_FFI_INT8:
        case JOSL_FFI_UINT16:
        case JOSL_FFI_INT16:
        case JOSL_FFI_UINT32:
        case JOSL_FFI_INT32:
        case JOSL_FFI_UINT64:
        case JOSL_FFI_INT64:
        case JOSL_FFI_UCHAR:
        case JOSL_FFI_CHAR:
        case JOSL_FFI_USHORT:
        case JOSL_FFI_SHORT:
        case JOSL_FFI_ULONG:
        case JOSL_FFI_LONG:
          varg->type = josl_vt_integer;
          varg->value.i = *(int *) args[idx];
          break;

        case JOSL_FFI_FLOAT:
        case JOSL_FFI_DOUBLE:
          varg->type = josl_vt_double;
          varg->value.d = *(double *) args[idx];
          break;

        case JOSL_FFI_STRING:
          varg->type = josl_vt_string;
          varg->value.s = strdup (*(char **) args[idx]);
          break;

        case JOSL_FFI_POINTER:
          varg->type = josl_vt_pointer;
          varg->value.p = *(void **) args[idx];
          break;

        default:
          fprintf (stderr, "unknown argument type: %i\n", ffi->argtypes[idx]);
          break;
        }
    }

  josl_exec_word (ffi->vm, ffi->topword);

  switch (ffi->rettype)
    {
    case JOSL_FFI_INT:
    case JOSL_FFI_UINT:
    case JOSL_FFI_UINT8:
    case JOSL_FFI_INT8:
    case JOSL_FFI_UINT16:
    case JOSL_FFI_INT16:
    case JOSL_FFI_UINT32:
    case JOSL_FFI_INT32:
    case JOSL_FFI_UINT64:
    case JOSL_FFI_INT64:
    case JOSL_FFI_UCHAR:
    case JOSL_FFI_CHAR:
    case JOSL_FFI_USHORT:
    case JOSL_FFI_SHORT:
    case JOSL_FFI_ULONG:
    case JOSL_FFI_LONG:
      vresult = ffi->vm->s[--ffi->vm->sp];
      if (vresult->type != josl_vt_integer)
        {
          fprintf (stderr, "invalid return type\n");
        }
      else
        {
          *ret = vresult->value.i;
        }
      break;

    case JOSL_FFI_FLOAT:
    case JOSL_FFI_DOUBLE:
      vresult = ffi->vm->s[--ffi->vm->sp];
      if (vresult->type != josl_vt_double)
        {
          fprintf (stderr, "invalid return type\n");
        }
      else
        {
          *ret = vresult->value.d;
        }
      break;

    case JOSL_FFI_STRING:
      vresult = ffi->vm->s[--ffi->vm->sp];
      if (vresult->type != josl_vt_string)
        {
          fprintf (stderr, "invalid return type\n");
        }
      else
        {
          *ret = vresult->value.s;
        }
      break;

    case JOSL_FFI_POINTER:
      vresult = ffi->vm->s[--ffi->vm->sp];
      if (vresult->type != josl_vt_pointer)
        {
          fprintf (stderr, "invalid return type\n");
        }
      else
        {
          *ret = vresult->value.p;
        }
      break;

    default:
      *ret = NULL;
      break;
    }
}

static int
josl_throw_exception (josl * interp, josl_word * w,
                      josl_error_code err, const char *msg)
{
  josl_value *vexc;
  int code;

  interp->excCode = err;

  if (w->top->excH == NULL)
    {
      josl_error_new_word (interp, w, err, msg);
      return 0;
    }

  vexc = STACK (SP++);
  vexc->type = josl_vt_integer;
  vexc->value.i = err;

  josl_exec_word (interp, w->top->excH);

  if (interp->excCode > josl_exc_handled)
    {
      josl_error_new_word (interp, w, err, msg);
      return 0;
    }

  code = interp->excCode;
  interp->excCode = josl_ok;

  return code;
}

static int
josl_throw_errno_exception (josl * interp, josl_word * w)
{
  josl_error_code ecode;
  char *emsg;

  emsg = strerror (errno);

  switch (errno)
    {
    case EPERM:
      ecode = josl_err_op_not_permitted;
      break;

    case ENOENT:
      ecode = josl_err_no_such_file;
      break;

    case ESRCH:
      ecode = josl_err_no_such_process;
      break;

    case EINTR:
      ecode = josl_err_interrputed_func;
      break;

    case EIO:
      ecode = josl_err_io;
      break;

    case ENXIO:
      ecode = josl_err_no_such_device;
      break;

    case E2BIG:
      ecode = josl_err_arg_list_too_big;
      break;

    case ENOEXEC:
      ecode = josl_err_exec_format;
      break;

    case EBADF:
      ecode = josl_err_bad_file_no;
      break;

    case ECHILD:
      ecode = josl_err_no_spawned_child;
      break;

    case EAGAIN:
      ecode = josl_err_again;
      break;

    case ENOMEM:
      ecode = josl_err_not_enough_memory;
      break;

    case EACCES:
      ecode = josl_err_permission_denied;
      break;

    case EFAULT:
      ecode = josl_err_bad_address;
      break;

    case EBUSY:
      ecode = josl_err_device_resource_busy;
      break;

    case EEXIST:
      ecode = josl_err_already_exists;
      break;

    case EXDEV:
      ecode = josl_err_cross_device_link;
      break;

    case ENODEV:
      ecode = josl_err_no_such_device;
      break;

    case ENOTDIR:
      ecode = josl_err_not_a_directory;
      break;

    case EISDIR:
      ecode = josl_err_is_a_directory;
      break;

    case EINVAL:
      ecode = josl_err_invalid_argument;
      break;

    case ENFILE:
    case EMFILE:
      ecode = josl_err_too_many_files_opened;
      break;

    case ENOTTY:
      ecode = josl_err_no_tty;
      break;

    case EFBIG:
      ecode = josl_err_file_too_big;
      break;

    case ENOSPC:
      ecode = josl_err_no_space_left;
      break;

    case ESPIPE:
      ecode = josl_err_invalid_seek;
      break;

    case EROFS:
      ecode = josl_err_read_only_fs;
      break;

    case EMLINK:
      ecode = josl_err_too_many_links;
      break;

    case EPIPE:
      ecode = josl_err_broken_pipe;
      break;

    case EDOM:
      ecode = josl_err_invalid_argument;
      break;

    case ERANGE:
      ecode = josl_err_result_too_large;
      break;

    case EDEADLK:
      ecode = josl_err_resource_deadlock;
      break;

    case ENAMETOOLONG:
      ecode = josl_err_filename_too_long;
      break;

    case ENOLCK:
      ecode = josl_err_no_locks;
      break;

    case ENOSYS:
      ecode = josl_err_not_impl;
      break;

    case ENOTEMPTY:
      ecode = josl_err_dir_not_empty;
      break;

    case EILSEQ:
      ecode = josl_err_illegal_byte_sequence;
      break;

#ifndef WINDOWS

    case ELOOP:
      ecode = josl_err_too_many_links;
      break;

    case ENOMSG:
      ecode = josl_err_no_message;
      break;

    case ENOSTR:
      ecode = josl_err_not_a_stream;
      break;

    case ENODATA:
      ecode = josl_err_no_data;
      break;

    case ETIME:
      ecode = josl_err_timer_expired;
      break;

    case ENOSR:
      ecode = josl_err_no_streams_available;
      break;

    case EREMOTE:
      ecode = josl_err_item_is_remote;
      break;

    case ENOLINK:
      ecode = josl_err_no_link;
      break;

    case EPROTO:
      ecode = josl_err_protocol_error;
      break;

    case EBADMSG:
      ecode = josl_err_bad_message;
      break;

    case ENOTSOCK:
      ecode = josl_err_not_socket;
      break;

    case EDESTADDRREQ:
      ecode = josl_err_destination_address_required;
      break;

    case EMSGSIZE:
      ecode = josl_err_message_too_long;
      break;

    case EPROTOTYPE:
      ecode = josl_err_wrong_protocol;
      break;

    case EPROTONOSUPPORT:
      ecode = josl_err_protocol_not_supported;
      break;

    case ESOCKTNOSUPPORT:
      ecode = josl_err_socket_type_not_supported;
      break;

    case EPFNOSUPPORT:
      ecode = josl_err_protocol_family_not_supported;
      break;

    case EAFNOSUPPORT:
      ecode = josl_err_address_family_not_supported;
      break;

    case EADDRINUSE:
      ecode = josl_err_address_in_use;
      break;

    case EADDRNOTAVAIL:
      ecode = josl_err_address_not_available;
      break;

    case ENETDOWN:
      ecode = josl_err_network_is_down;
      break;

    case ENETUNREACH:
      ecode = josl_err_network_is_unreachable;
      break;

    case ENETRESET:
      ecode = josl_err_network_reset;
      break;

    case ECONNABORTED:
      ecode = josl_err_connection_aborted;
      break;

    case ECONNRESET:
      ecode = josl_err_connection_reset;
      break;

    case ENOBUFS:
      ecode = josl_err_no_buffer_space_available;
      break;

    case ESHUTDOWN:
      ecode = josl_err_socket_was_shutdown;
      break;

    case ETOOMANYREFS:
      ecode = josl_err_too_many_references;
      break;

    case ETIMEDOUT:
      ecode = josl_err_connection_timed_out;
      break;

    case ECONNREFUSED:
      ecode = josl_err_connection_refused;
      break;

    case EHOSTDOWN:
      ecode = josl_err_host_is_down;
      break;

    case EHOSTUNREACH:
      ecode = josl_err_host_unreachable;
      break;

    case EALREADY:
      ecode = josl_err_op_already_in_progress;
      break;

    case EINPROGRESS:
      ecode = josl_err_op_in_progress;
      break;

    case ESTALE:
      ecode = josl_err_stale_file_handle;
      break;

#endif /* ifndef WINDOWS */
#ifdef LINUX

    case ENONET:
      ecode = josl_err_not_on_network;
      break;

    case ENOTUNIQ:
      ecode = josl_err_name_not_unique;
      break;

    case EBADFD:
      ecode = josl_err_bad_file_no;
      break;

    case EREMCHG:
      ecode = josl_err_remote_address_changed;
      break;

#endif /* LINUX */

    default:
      ecode = josl_err_unknown;
      break;
    }

  return josl_throw_exception (interp, w, ecode, emsg);
}

static int
josl_throw_sock_exception (josl * interp, josl_word * w)
{
#ifdef WINDOWS
  josl_error_code ecode;
  char *emsg;

  switch (WSAGetLastError ())
    {
    case WSA_INVALID_HANDLE:
      ecode = josl_err_invalid_handle;
      emsg = "Specified event object handle is invalid.";
      break;

    case WSA_NOT_ENOUGH_MEMORY:
      ecode = josl_err_not_enough_memory;
      emsg = "Specified event object handle is invalid.";
      break;

    case WSA_INVALID_PARAMETER:
      ecode = josl_err_invalid_parameter;
      emsg = "One or more parameters are invalid.";
      break;

    case WSA_OPERATION_ABORTED:
      ecode = josl_err_op_aborted;
      emsg = "One or more parameters are invalid.";
      break;

    case WSA_IO_INCOMPLETE:
      ecode = josl_err_io_incomplete;
      emsg = "Overlapped I/O event object not in signaled state.";
      break;

    case WSA_IO_PENDING:
      ecode = josl_err_io_pending;
      emsg = "Overlapped operations will complete later.";
      break;

    case WSAEINTR:
      ecode = josl_err_interrputed_func;
      emsg = "Interrupted function call.";
      break;

    case WSAEBADF:
      ecode = josl_err_bad_file_no;
      emsg = "File handle is not valid.";
      break;

    case WSAEACCES:
      ecode = josl_err_permission_denied;
      emsg = "Permission denied.";
      break;

    case WSAEFAULT:
      ecode = josl_err_bad_address;
      emsg = "Bad address.";
      break;

    case WSAEINVAL:
      ecode = josl_err_invalid_argument;
      emsg = "Invalid Argument.";
      break;

    case WSAEMFILE:
      ecode = josl_err_too_many_files_opened;
      emsg = "Too many open files.";
      break;

    case WSAEWOULDBLOCK:
      ecode = josl_err_resource_temporarily_unavailable;
      emsg = "Resource temporarily unavailable.";
      break;

    case WSAEINPROGRESS:
      ecode = josl_err_op_in_progress;
      emsg = "Operation now in progress";
      break;

    case WSAEALREADY:
      ecode = josl_err_op_already_in_progress;
      emsg = "Operation already in progress.";
      break;

    case WSAENOTSOCK:
      ecode = josl_err_not_socket;
      emsg = "Socket operation on nonsocket.";
      break;

    case WSAEDESTADDRREQ:
      ecode = josl_err_destination_address_required;
      emsg = "Destination address required.";
      break;

    case WSAEMSGSIZE:
      ecode = josl_err_message_too_long;
      emsg = "Message too long.";
      break;

    case WSAEPROTOTYPE:
      ecode = josl_err_wrong_protocol;
      emsg = "Protocol wrong type for socket.";
      break;

    case WSAENOPROTOOPT:
      ecode = josl_err_bad_protocol_option;
      emsg = "Bad protocol option.";
      break;

    case WSAEPROTONOSUPPORT:
      ecode = josl_err_protocol_not_supported;
      emsg = "Protocol not supported.";
      break;

    case WSAESOCKTNOSUPPORT:
      ecode = josl_err_socket_type_not_supported;
      emsg = "Socket type not supported.";
      break;

    case WSAEOPNOTSUPP:
      ecode = josl_err_op_not_supported;
      emsg = "Operation not supported.";
      break;

    case WSAEPFNOSUPPORT:
      ecode = josl_err_protocol_family_not_supported;
      emsg = "Protocol family not supported.";
      break;

    case WSAEAFNOSUPPORT:
      ecode = josl_err_address_family_not_supported;
      emsg = "Address family not supported by protocol family.";
      break;

    case WSAEADDRINUSE:
      ecode = josl_err_address_in_use;
      emsg = "Address already in use.";
      break;

    case WSAEADDRNOTAVAIL:
      ecode = josl_err_address_not_available;
      emsg = "Cannot assign requested address.";
      break;

    case WSAENETDOWN:
      ecode = josl_err_network_is_down;
      emsg = "Network is down.";
      break;

    case WSAENETUNREACH:
      ecode = josl_err_network_is_unreachable;
      emsg = "Network is unreachable.";
      break;

    case WSAENETRESET:
      ecode = josl_err_network_reset;
      emsg = "Network dropped connection on reset.";
      break;

    case WSAECONNABORTED:
      ecode = josl_err_connection_aborted;
      emsg = "Software caused connection abort.";
      break;

    case WSAECONNRESET:
      ecode = josl_err_connection_reset;
      emsg = "Connection reset by peer.";
      break;

    case WSAENOBUFS:
      ecode = josl_err_no_buffer_space_available;
      emsg = "No buffer space available.";
      break;

    case WSAEISCONN:
      ecode = josl_err_socket_already_connected;
      emsg = "Socket is already connected.";
      break;

    case WSAENOTCONN:
      ecode = josl_err_socket_not_connected;
      emsg = "Socket is not connected.";
      break;

    case WSAESHUTDOWN:
      ecode = josl_err_socket_was_shutdown;
      emsg = "Cannot send after socket shutdown.";
      break;

    case WSAETOOMANYREFS:
      ecode = josl_err_too_many_references;
      emsg = "Too many references.";
      break;

    case WSAETIMEDOUT:
      ecode = josl_err_connection_timed_out;
      emsg = "Connection timed out.";
      break;

    case WSAECONNREFUSED:
      ecode = josl_err_connection_refused;
      emsg = "Connection refused.";
      break;

    case WSAELOOP:
      ecode = josl_err_cannot_translate_name;
      emsg = "Cannot translate name.";
      break;

    case WSAENAMETOOLONG:
      ecode = josl_err_name_too_long;
      emsg = "Name too long.";
      break;

    case WSAEHOSTDOWN:
      ecode = josl_err_host_is_down;
      emsg = "Host is down.";
      break;

    case WSAEHOSTUNREACH:
      ecode = josl_err_host_unreachable;
      emsg = "No route to host.";
      break;

    case WSAENOTEMPTY:
      ecode = josl_err_dir_not_empty;
      emsg = "Directory not empty.";
      break;

    case WSAEPROCLIM:
      ecode = josl_err_too_many_processes;
      emsg = "Too many processes.";
      break;

    case WSAEUSERS:
      ecode = josl_err_user_quota_exceeded;
      emsg = "User quota exceeded.";
      break;

    case WSAEDQUOT:
      ecode = josl_err_disk_quota_exceeded;
      emsg = "Disk quota exceeded.";
      break;

    case WSAESTALE:
      ecode = josl_err_stale_file_handle;
      emsg = "Stale file handle referenced.";
      break;

    case WSAEREMOTE:
      ecode = josl_err_item_is_remote;
      emsg = "Item is remote.";
      break;

    case WSASYSNOTREADY:
      ecode = josl_err_network_system_not_ready;
      emsg = "Network subsystem is unavailable.";
      break;

    case WSAVERNOTSUPPORTED:
      ecode = josl_err_winsock_version_error;
      emsg = "Winsock.dll version is out of range.";
      break;

    case WSANOTINITIALISED:
      ecode = josl_err_winsock_not_initialized;
      emsg = "Winsock is not yet initialized.";
      break;

    case WSAEDISCON:
      ecode = josl_err_shutdown_in_progress;
      emsg = "Shutdown in progresses.";
      break;

    case WSA_E_NO_MORE:
    case WSAENOMORE:
      ecode = josl_err_no_more_results;
      emsg = "No more results.";
      break;

    case WSA_E_CANCELLED:
    case WSAECANCELLED:
      ecode = josl_err_call_cancelled;
      emsg = "Call has been cancelled.";
      break;

    case WSAEINVALIDPROCTABLE:
      ecode = josl_err_procedure_table_invalid;
      emsg = "Procedure call table is invalid.";
      break;

    case WSAEINVALIDPROVIDER:
      ecode = josl_err_service_invalid;
      emsg = "Service provider is invalid.";
      break;

    case WSAEPROVIDERFAILEDINIT:
      ecode = josl_err_service_init_failed;
      emsg = "Service provider failed to initialize.";
      break;

    case WSASYSCALLFAILURE:
      ecode = josl_err_system_call_failure;
      emsg = "System call failure.";
      break;

    case WSASERVICE_NOT_FOUND:
      ecode = josl_err_service_not_found;
      emsg = "Service not found.";
      break;

    case WSATYPE_NOT_FOUND:
      ecode = josl_err_class_type_not_found;
      emsg = "Class type not found.";
      break;

    case WSAEREFUSED:
      ecode = josl_err_database_query_refused;
      emsg = "Database query was refused.";
      break;

    case WSAHOST_NOT_FOUND:
      ecode = josl_err_host_not_found;
      emsg = "Host not found.";
      break;

    case WSATRY_AGAIN:
      ecode = josl_err_nonauthoritative_host_not_found;
      emsg = "Nonauthoritative host not found.";
      break;

    case WSANO_RECOVERY:
      ecode = josl_err_nonrecoverable_error;
      emsg = "This is nonrecoverable error.";
      break;

    case WSANO_DATA:
      ecode = josl_err_no_data;
      emsg = "Valid name, no data record of requested type.";
      break;

    default:
      ecode = josl_err_unknown;
      emsg = strdup_printf ("Unknown exception, Winsock error code: %d",
                            WSAGetLastError ());
    }

  return josl_throw_exception (interp, w, ecode, emsg);

#else /* WINDOWS */

  return josl_throw_errno_exception (interp, w);

#endif /* WINDOWS */
}

static void
josl_dot_word (josl_top_word * top)
{
  FILE *f;
  josl_word *w = top->first;

  f = fopen (strdup_printf ("%s.dot", top->name), "w");

  fprintf (f, "digraph g {\n");

  while (w != NULL)
    {
      fprintf (f, "\"%p\" [\n", w);
      fprintf (f, "  label = \"<word> %s (%p) | <loc> %d,%d ", w->name, w,
               w->lineNo, w->colNo);
      if (w->next != NULL)
        fprintf (f, "| <next> next ");
      if (w->nextexec != NULL && w->next != w->nextexec)
        fprintf (f, "| <nextexec> nextexec ");
      if (w->jump != NULL)
        fprintf (f, "| <jump> jump");
      fprintf (f, "\"\n");
      fprintf (f, "  shape = \"record\"\n");
      fprintf (f, "  fontsize = 12\n");
      fprintf (f, "];\n");

      w = w->next;
    }

  w = top->first;

  while (w != NULL)
    {
      if (w->next != NULL)
        fprintf (f, "\"%p\":next -> \"%p\":word;\n", w, w->next);
      if (w->nextexec != NULL && w->next != w->nextexec)
        fprintf (f, "\"%p\":nextexec -> \"%p\":word;\n", w, w->nextexec);
      if (w->jump != NULL)
        fprintf (f, "\"%p\":jump -> \"%p\":word;\n", w, w->jump);

      w = w->next;
    }

  fprintf (f, "label = \"%s\";\n", top->name);
  fprintf (f, "}\n");
  fclose (f);
}

void
josl_exec_word (josl * interp, josl_top_word * top)
{
  register josl_word *w = top->first;
  josl_loop loops[10];
  int lp;

  for (lp = 0; lp < 10; lp++)
    loops[lp].content = NULL;
  lp = -1;

  /* *INDENT-OFF* */
  static void *dynids[] = {
    &&josl_w_push, &&josl_w_noop, &&josl_w_variable_access,
    &&josl_w_variable_assign, &&josl_w_variable_assign_keep,

    /* Exception Handling */
    &&josl_w_fail, &&josl_w_resume, &&josl_w_resume_this, &&josl_w_retry,

    /* System Information */
    &&josl_w_os,

    /* Memory */
    &&josl_w_mem_malloc,

    /* Dynamic Execution */
    &&josl_w_ffi_exec, &&josl_w_ffi_callback,
    &&josl_w_dynamic_word, &&josl_w_parse, &&josl_w_exec_word,
    &&josl_w_curry, &&josl_w_2curry, &&josl_w_3curry, &&josl_w_curry_pound,
    &&josl_w_curry_question, &&josl_w_compose,

    /* branch words */
    &&josl_w_if, &&josl_w_else, &&josl_w_end, &&josl_w_while, &&josl_w_do,
    &&josl_w_begin, &&josl_w_until,
    &&josl_w_each, &&josl_w_each_kv, &&josl_w_each_rev,
    &&josl_w_for, &&josl_w_times, &&josl_w_next,
    &&josl_w_plus_next, &&josl_w_vector,
    &&josl_w_return, &&josl_w_break, &&josl_w_continue,
    &&josl_w_i, &&josl_w_j, &&josl_w_k,
    &&josl_w_ii, &&josl_w_jj, &&josl_w_kk,
    &&josl_w_is, &&josl_w_do_is,

    /* non-dropping branch words */
    &&josl_w_star_if, &&josl_w_star_do, &&josl_w_star_until,

    /* stack words */
    &&josl_w_depth,
    &&josl_w_dup, &&josl_w_2dup, &&josl_w_3dup, &&josl_w_dup_pound,
    &&josl_w_drop, &&josl_w_2drop, &&josl_w_3drop, &&josl_w_drop_pound,
    &&josl_w_swap, &&josl_w_2swap, &&josl_w_3swap, &&josl_w_swap_pound,
    &&josl_w_over, &&josl_w_2over, &&josl_w_3over, &&josl_w_rot, &&josl_w_minus_rot,
    &&josl_w_2rot, &&josl_w_minus_2rot, &&josl_w_3rot, &&josl_w_minus_3rot, &&josl_w_roll,
    &&josl_w_minus_roll, &&josl_w_pick, &&josl_w_nip, &&josl_w_tuck, &&josl_w_dip,
    &&josl_w_2dip,
    &&josl_w_to_temp, &&josl_w_from_temp, &&josl_w_to_temp_count, &&josl_w_from_temp_count,

    /* Boolean */
    &&josl_w_equal, &&josl_w_star_equal,
    &&josl_w_equal_zero, &&josl_w_equal_one, &&josl_w_equal_minus_one,
    &&josl_w_not_equal, &&josl_w_star_not_equal,
    &&josl_w_not,
    &&josl_w_gt, &&josl_w_star_gt,
    &&josl_w_lt, &&josl_w_star_lt,
    &&josl_w_gt_equal, &&josl_w_star_gt_equal,
    &&josl_w_lt_equal, &&josl_w_star_lt_equal,
    &&josl_w_between, &&josl_w_star_between,
    &&josl_w_and, &&josl_w_or,

    /* Math */
    &&josl_w_even, &&josl_w_odd, &&josl_w_plus, &&josl_w_minus, &&josl_w_multiply,
    &&josl_w_divide, &&josl_w_mod,
    &&josl_w_one_plus, &&josl_w_one_minus, &&josl_w_two_plus,
    &&josl_w_two_minus, &&josl_w_negate, &&josl_w_abs,
    &&josl_w_min, &&josl_w_max, &&josl_w_ceil,
    &&josl_w_floor, &&josl_w_round, &&josl_w_round_to, &&josl_w_acos, &&josl_w_asin,
    &&josl_w_atan, &&josl_w_atan2, &&josl_w_cos, &&josl_w_cosh, &&josl_w_sin,
    &&josl_w_sinh, &&josl_w_tan, &&josl_w_tanh, &&josl_w_exp, &&josl_w_exp2,
    &&josl_w_expm1, &&josl_w_frexp,
    &&josl_w_log, &&josl_w_log1p, &&josl_w_log2, &&josl_w_log10, &&josl_w_modf,
    &&josl_w_pow, &&josl_w_sqrt, &&josl_w_fmod, &&josl_w_degrees, &&josl_w_radians,
    &&josl_w_rand, &&josl_w_srand,

    /* Bitmath */
    &&josl_w_bit_and, &&josl_w_bit_or, &&josl_w_bit_xor, &&josl_w_bit_not, &&josl_w_shift_right,
    &&josl_w_shift_left,

    /* Conversion */
    &&josl_w_to_string, &&josl_w_to_integer, &&josl_w_to_double,

    /* Type */
    &&josl_w_is_integer, &&josl_w_is_double, &&josl_w_is_number, &&josl_w_is_string,
    &&josl_w_is_pointer, &&josl_w_is_dict, &&josl_w_is_array, &&josl_w_is_list,
    &&josl_w_is_null, &&josl_w_is_variable, &&josl_w_is_word, &&josl_w_type, &&josl_w_null,

    /* String */
    &&josl_w_len, &&josl_w_append, &&josl_w_append_pound, &&josl_w_join,
    &&josl_w_join_pound, &&josl_w_left, &&josl_w_right, &&josl_w_mid,
    &&josl_w_lower, &&josl_w_upper, &&josl_w_title, &&josl_w_capitalize,
    &&josl_w_trim, &&josl_w_trim_head, &&josl_w_trim_tail, &&josl_w_split,
    &&josl_w_split_pound, &&josl_w_splitany, &&josl_w_splitany_pound, &&josl_w_asc,
    &&josl_w_chr, &&josl_w_find, &&josl_w_rfind, &&josl_w_find_pound, &&josl_w_rfind_pound,
    &&josl_w_cut, &&josl_w_peel, &&josl_w_delete, &&josl_w_replace, &&josl_w_insert,
    &&josl_w_format,
    &&josl_w_repeat, &&josl_w_reverse, &&josl_w_translate, &&josl_w_is_alnum,
    &&josl_w_is_alpha, &&josl_w_is_cntrl, &&josl_w_is_digit, &&josl_w_is_graph,
    &&josl_w_is_lower, &&josl_w_is_print, &&josl_w_is_punct, &&josl_w_is_space,
    &&josl_w_is_upper, &&josl_w_is_xdigit, &&josl_w_has_prefix, &&josl_w_has_suffix,
    &&josl_w_at,

    /* Date and time */
    &&josl_w_clock, &&josl_w_time,

    /* Input/Output */
    &&josl_w_print, &&josl_w_println,

    &&josl_w_file_open, &&josl_w_close, &&josl_w_eof, &&josl_w_read_ch, &&josl_w_read_line,
    &&josl_w_read_str, &&josl_w_read_blob, &&josl_w_write, &&josl_w_writeln,

    /* System */
    &&josl_w_argc, &&josl_w_argv, &&josl_w_env, &&josl_w_system, &&josl_w_exit,

    /* File System */
    &&josl_w_dir_open, &&josl_w_dir_next, &&josl_w_dir_close, &&josl_w_dir_create,
    &&josl_w_dir_remove, &&josl_w_file_size, &&josl_w_file_create_time, &&josl_w_file_access_time,
    &&josl_w_file_modify_time, &&josl_w_is_dir, &&josl_w_is_readable,
    &&josl_w_is_writable, &&josl_w_is_executable, &&josl_w_file_exists,
    &&josl_w_file_copy, &&josl_w_file_remove, &&josl_w_file_move,

    /* Misc Intrinsic Type Words */
    &&josl_w_variable, &&josl_w_dict, &&josl_w_sized_dict, &&josl_w_set,
    &&josl_w_set_keep, &&josl_w_get, &&josl_w_remove, &&josl_w_has_key,
    &&josl_w_copy, &&josl_w_array, &&josl_w_list, &&josl_w_push_head, &&josl_w_push_tail,
    &&josl_w_current, &&josl_w_forward, &&josl_w_backward, &&josl_w_head, &&josl_w_tail,
    &&josl_w_go_head, &&josl_w_go_tail, &&josl_w_pop_head, &&josl_w_pop_tail,
    &&josl_w_sbuf, &&josl_w_sized_sbuf, &&josl_w_keys, &&josl_w_values,
    &&josl_w_capacity, &&josl_w_array_resize, &&josl_w_sort, &&josl_w_sort_user,
    &&josl_w_place_0, &&josl_w_place_1, &&josl_w_place_2, &&josl_w_place_3,
    &&josl_w_place_4, &&josl_w_place_5, &&josl_w_place_6, &&josl_w_place_7,
    &&josl_w_place_8, &&josl_w_place_9,


    /* Functional Words */
    &&josl_w_map, &&josl_w_filter, &&josl_w_fold, &&josl_w_foldr,

    /* Regular Expressions */
    &&josl_w_re_compile, &&josl_w_regexp, &&josl_w_regsub, &&josl_w_regextract,

    /* Sockets */
    &&josl_w_sock_socket, &&josl_w_sock_bind, &&josl_w_sock_listen,
    &&josl_w_sock_accept, &&josl_w_sock_connect, &&josl_w_sock_select,
    &&josl_w_sock_send_to, &&josl_w_sock_receive_from,
    &&josl_w_sock_serv_by_name, &&josl_w_sock_serv_by_port,
    &&josl_w_sock_host_by_name, &&josl_w_sock_host_by_addr,

    /* Debugging */
    &&josl_w_words, &&josl_w_word_show, &&josl_w_is_user_word, &&josl_w_word_dot,
    &&josl_w_stack_show, &&josl_w_word_list,

    /* Unit Testing */
    &&josl_w_test, &&josl_w_test_fail_count, &&josl_w_test_pass_count,
    &&josl_w_test_total_count, &&josl_w_test_report, &&josl_w_test_verbose,
	&&josl_w_test_is_verbose,

    /* User */
    &&josl_w_user
  };
  /* *INDENT-ON* */

  if (w == NULL)
    return;

  goto *dynids[w->word_id];

#include "vm_exec_internal.c"

  /* Exception Handling */

josl_w_fail:                   /* ( s:message i:code -- ) */
  {
    josl_value *vmsg, *vcode;

    UNDERFLOW_CHECK (interp, w, 2);

    vcode = STACK (--SP);
    vmsg = STACK (--SP);

    if (vcode->type != josl_vt_integer)
      {
        josl_throw_exception (interp, w, josl_err_stack_type,
                              "expects an integer on TOS");
        return;
      }

    if (vmsg->type != josl_vt_string)
      {
        josl_throw_exception (interp, w, josl_err_stack_type,
                              "expects a string on NOS");
        return;
      }

    if (josl_throw_exception (interp, w, vcode->value.i, vmsg->value.s) == 0)
      return;
  }
  BREAK;

josl_w_resume:
  {
    interp->excCode = josl_exc_resume;
    JUMP;
  }

josl_w_resume_this:
  {
    interp->excCode = josl_exc_resume_this;
    JUMP;
  }

josl_w_retry:
  {
    interp->excCode = josl_exc_retry;
    JUMP;
  }

josl_w_mem_malloc:             /* ( i:size -- p:pointer ) */
  {
    josl_value *vsize, *vresult;
    void *p;

    UNDERFLOW_CHECK (interp, w, 1);

    vsize = STACK (--SP);
    p = tgc_alloc (&gc, vsize->value.i);
    vresult = STACK (SP++);
    vresult->type = josl_vt_pointer;
    vresult->value.p = p;

    BREAK;
  }

#include "vm_exec_sysinfo.c"
#include "vm_exec_dynamic.c"
#include "vm_exec_branch.c"
#include "vm_exec_stack.c"
#include "vm_exec_boolean.c"
#include "vm_exec_math.c"
#include "vm_exec_bitmath.c"
#include "vm_exec_conversion.c"
#include "vm_exec_type.c"
#include "vm_exec_string.c"
#include "vm_exec_datetime.c"
#include "vm_exec_io.c"
#include "vm_exec_system.c"
#include "vm_exec_filesys.c"
#include "vm_exec_misc.c"
#include "vm_exec_functional.c"
#include "vm_exec_regex.c"
#include "vm_exec_socket.c"
#include "vm_exec_debug.c"
#include "vm_exec_unittest.c"

done:
  ;                             /* all done */
}

void
josl_exec_named_word (josl * interp, const char *filename, const char *word)
{
  josl_top_word *w = NULL;
  josl_hash *filewords =
    hashtable_search (interp->filewords, (void *) filename);

  if (filewords == NULL)
    {
      fprintf (stderr, "couldn't locate file '%s'\n", filename);
      return;
    }

  w = hashtable_search (filewords, (void *) word);
  if (w == NULL)
    {
      fprintf (stderr, "couldn't locate word '%s'\n", word);
      return;
    }

  josl_exec_word (interp, w);
}
