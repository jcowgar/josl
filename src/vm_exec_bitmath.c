/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Bitmath

*/

josl_w_bit_and:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type == josl_vt_double)
    {
      b->type = josl_vt_integer;
      b->value.i = (int) b->value.d;
    }
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on NOS");
    }

  a->value.i = a->value.i & b->value.i;

  BREAK;
}

josl_w_bit_or:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type == josl_vt_double)
    {
      b->type = josl_vt_integer;
      b->value.i = (int) b->value.d;
    }
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on NOS");
    }

  a->value.i = a->value.i | b->value.i;

  BREAK;
}

josl_w_bit_xor:
{
  josl_value *a, *b;
  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type == josl_vt_double)
    {
      b->type = josl_vt_integer;
      b->value.i = (int) b->value.d;
    }
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on NOS");
    }

  a->value.i = a->value.i ^ b->value.i;

  BREAK;
}

josl_w_bit_not:
{
  josl_value *a;
  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }

  a->value.i = ~a->value.i;

  BREAK;
}

josl_w_shift_right:
{
  josl_value *a, *b;
  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type == josl_vt_double)
    {
      b->type = josl_vt_integer;
      b->value.i = (int) b->value.d;
    }
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on NOS");
    }

  a->value.i = a->value.i >> b->value.i;

  BREAK;
}

josl_w_shift_left:
{
  josl_value *a, *b;
  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type == josl_vt_double)
    {
      b->type = josl_vt_integer;
      b->value.i = (int) b->value.d;
    }
  if (a->type == josl_vt_double)
    {
      a->type = josl_vt_integer;
      a->value.i = (int) a->value.d;
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on TOS");
    }
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected a number on NOS");
    }

  a->value.i = a->value.i << b->value.i;

  BREAK;
}
