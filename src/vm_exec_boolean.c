/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Boolean Words (Compare)

*/

josl_w_star_equal:
josl_w_equal:
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_equal)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
    }
  
  vresult = STACK (SP++);
      
  if (a->type == b->type)
    {
      switch (a->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i == b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d == b->value.d;
          break;

        case josl_vt_string:
          vresult->value.i = strcmp (a->value.s, b->value.s) == 0;

          NULLVALUE (b);
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
    }
  else
    {
      vresult->value.i = 0;           /* Items of different types are not equal */
    }
  
  vresult->type = josl_vt_integer;

  BREAK;
}

josl_w_equal_zero:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = a->value.i == 0;
      break;

    case josl_vt_double:
      a->value.i = a->value.d == 0.0;
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected number");
      }
    }

  BREAK;
}

josl_w_equal_one:
{
  josl_value *a;
  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = a->value.i == 1;
      break;

    case josl_vt_double:
      a->value.i = a->value.d == 1.0;
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected number");
      }
    }

  BREAK;
}

josl_w_equal_minus_one:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = a->value.i == -1;
      break;

    case josl_vt_double:
      a->value.i = a->value.d == -1.0;
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected number");
      }
    }

  BREAK;
}

josl_w_star_not_equal:
josl_w_not_equal:
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_not_equal)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
    }
  
  vresult = STACK (SP++);
      
  if (a->type == b->type)
    {
      switch (a->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i != b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d != b->value.d;
          break;

        case josl_vt_string:
          vresult->value.i = strcmp (a->value.s, b->value.s) != 0;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
    }
  else
    {
      vresult->value.i = 1;           /* Items of different types are not equal */
    }

  vresult->type = josl_vt_integer;
  
  BREAK;
}

josl_w_not:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !a->value.i;
      break;

    case josl_vt_double:
      a->value.d = !a->value.d;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_gt:
josl_w_star_gt:
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_gt)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
    }
  
  vresult = STACK (SP++);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i > b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.i > b->value.d;
          break;
          
        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
          break;
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.d > b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d > b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
          break;
        }
      break;

    case josl_vt_string:
      if (b->type != josl_vt_string)
        {
          THROW_EXC (josl_err_not_comparable, "not comparable");
        }
      else
        {
          vresult->value.i = strcmp (a->value.s, b->value.s) > 0;
        }
      break;
    }
  
  vresult->type = josl_vt_integer;
  
  BREAK;
}

josl_w_lt:
josl_w_star_lt:
{
  josl_value *a, *b, *vresult;
  
  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_lt)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
    }
  
  vresult = STACK (SP++);
  
  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i < b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.i < b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
          break;
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.d < b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d < b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
          break;
        }
      break;

    case josl_vt_string:
      if (b->type != josl_vt_string)
        {
          THROW_EXC (josl_err_stack_type,
                     "a string can only be compared to a string");
        }
      else
        {
          vresult->value.i = strcmp (a->value.s, b->value.s) < 0;
        }
      break;
    }

  vresult->type = josl_vt_integer;
  
  BREAK;
}

josl_w_gt_equal:
josl_w_star_gt_equal:
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_gt_equal)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
      
      OVERFLOW_CHECK (interp, w, 1);
    }
  
  vresult = STACK (SP++);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i >= b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.i >= b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;

      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.d >= b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d >= b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
      break;

    case josl_vt_string:
      if (b->type != josl_vt_string)
        {
          THROW_EXC (josl_err_stack_type,
                     "a string can only be compared to a string");
        }
      else
        {
          vresult->value.i = strcmp (a->value.s, b->value.s) >= 0;
        }
      break;
    }
  
  vresult->type = josl_vt_integer;

  BREAK;
}

josl_w_lt_equal:
josl_w_star_lt_equal:
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);
  
  if (w->word_id == josl_w_lt_equal)
    {
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      b = STACK (SP-1);
      a = STACK (SP-2);
      
      OVERFLOW_CHECK (interp, w, 1);
    }
  
  vresult = STACK (SP++);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.i <= b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.i <= b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;

      switch (b->type)
        {
        case josl_vt_integer:
          vresult->value.i = a->value.d <= b->value.i;
          break;

        case josl_vt_double:
          vresult->value.i = a->value.d <= b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_comparable, "not comparable");
          }
        }
      break;

    case josl_vt_string:
      if (b->type != josl_vt_string)
        {
          THROW_EXC (josl_err_stack_type,
                     "a string can only be compared to a string");
        }
      else
        {
          vresult->value.i = strcmp (a->value.s, b->value.s) <= 0;
        }
      break;
    }
  
  vresult->type = josl_vt_integer;

  BREAK;
}

josl_w_between:
josl_w_star_between:
{
  josl_value *a, *b, *c, *vresult;
  double da, db, dc;

  UNDERFLOW_CHECK (interp, w, 3);

  /* ( a b c -- x ) a > b && a < c */
  
  if (w->word_id == josl_w_between)
    {
      c = STACK (--SP);
      b = STACK (--SP);
      a = STACK (--SP);
    }
  else
    {
      c = STACK (SP-1);
      b = STACK (SP-2);
      a = STACK (SP-3);
      
      OVERFLOW_CHECK (interp, w, 1);
    }
  
  vresult = STACK (SP++);
  
  if (a->type == josl_vt_integer) da = (double) a->value.i;
  if (a->type == josl_vt_double)  da = a->value.d;
  if (b->type == josl_vt_integer) db = (double) b->value.i;
  if (b->type == josl_vt_double)  db = b->value.d;
  if (c->type == josl_vt_integer) dc = (double) c->value.i;
  if (c->type == josl_vt_double)  dc = c->value.d;
  
  switch (a->type)
    {
    case josl_vt_integer:
    case josl_vt_double:
      if ((b->type != josl_vt_integer && b->type != josl_vt_double) ||
          (c->type != josl_vt_integer && c->type != josl_vt_double))
        {
          THROW_EXC (josl_err_not_comparable, "values are not comparable");
        }
      
      vresult->value.i = (da >= db) && (da <= dc);
      break;
      
    case josl_vt_string:
      if (b->type != josl_vt_string || c->type != josl_vt_string)
        {
          THROW_EXC (josl_err_not_comparable, "values are not comparable");
        }
      
      vresult->value.i = (strcmp (a->value.s, b->value.s) >= 0 &&
                          strcmp (a->value.s, c->value.s) <= 0);
      break;
      
    default:
      {
        THROW_EXC (josl_err_not_comparable, "values are not comparable");
      }
    }
  
  vresult->type = josl_vt_integer;
  
  BREAK;
}

josl_w_and:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i = a->value.i && b->value.i;
          break;

        case josl_vt_double:
          a->value.i = a->value.i && b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_boolean,
                       "expects boolean values on TOS and NOS");
          }
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i = a->value.d && b->value.i;
          break;

        case josl_vt_double:
          a->value.i = a->value.d && b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_boolean,
                       "expects boolean values on TOS and NOS");
          }
        }
      break;
    }

  BREAK;
}

josl_w_or:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i = a->value.i || b->value.i;
          break;

        case josl_vt_double:
          a->value.i = a->value.i || b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_boolean,
                       "expects boolean values on TOS and NOS");
          }
        }
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i = a->value.d || b->value.i;
          break;

        case josl_vt_double:
          a->value.i = a->value.d || b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_not_boolean,
                       "expects boolean values on TOS and NOS");
          }
          break;
        }
      break;
    }

  BREAK;
}

josl_w_even:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(a->value.i % 2);
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      a->value.i = !(((int) a->value.d) % 2);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number on TOS");
      }
      break;
    }

  BREAK;
}

josl_w_odd:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = (a->value.i % 2);
      break;

    case josl_vt_double:
      a->type = josl_vt_integer;
      a->value.i = (((int) a->value.d) % 2);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number on TOS");
      }
      break;
    }

  BREAK;
}
