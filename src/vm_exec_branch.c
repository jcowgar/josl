/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

   branching words

*/

josl_w_star_if:
josl_w_if:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  if (w->word_id == josl_w_star_if)
    a = STACK (SP - 1);
  else
    a = STACK (--SP);

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }
  else
    {
      if (a->value.i == 0)
        {
          JUMP;
        }
    }

  BREAK;
}

josl_w_else:
{
  BREAK;
}

josl_w_end:
{
  BREAK;
}

josl_w_while:
{
  BREAK;
}

josl_w_star_do:
josl_w_do:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  if (w->word_id == josl_w_star_do)
    a = STACK (SP - 1);
  else
    a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }
  else
    {
      if (a->value.i == 0)
        {
          JUMP;
        }
    }

  BREAK;
}

josl_w_begin:
{
  BREAK;
}

josl_w_star_until:
josl_w_until:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  if (w->word_id == josl_w_star_until)
    a = STACK (SP - 1);
  else
    a = STACK (--SP);
  switch (a->type)
    {
    case josl_vt_integer:
      if (!a->value.i)
        {
          JUMP;
        }
      break;

    case josl_vt_double:
      if (!a->value.d)
        {
          JUMP;
        }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "until requires a boolean, integer or double value on TOS");
      }
    }

  BREAK;
}

josl_w_each:                   /* ( al:content -- ) */
{
  josl_value *vcontent;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 1);

  vcontent = STACK (--SP);

  loop = &loops[++lp];
  loop->type = josl_w_each;
  if (loop->content == NULL)
    loop->content = tgc_alloc(&gc, sizeof (josl_value));
  loop->content->type = vcontent->type;
  loop->content->value = vcontent->value;

  switch (vcontent->type)
    {
    case josl_vt_string:
      loop->current = -1;
      loop->finish = strlen (vcontent->value.s);
      break;

    case josl_vt_array:
      loop->current = -1;
      loop->finish = vcontent->value.a->size;
      break;

    default:
      loop->current = -1;
      loop->finish = -1;
      break;
    }

  goto josl_w_next;

  BREAK;
}

josl_w_each_rev:                   /* ( al:content -- ) */
{
  josl_value *vcontent;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 1);

  vcontent = STACK (--SP);

  loop = &loops[++lp];
  loop->type = josl_w_each_rev;
  if (loop->content == NULL)
    loop->content = tgc_alloc(&gc, sizeof (josl_value));
  loop->content->type = vcontent->type;
  loop->content->value = vcontent->value;

  switch (vcontent->type)
    {
    case josl_vt_string:
      loop->current = strlen (vcontent->value.s);
      loop->finish = 0;
      break;

    case josl_vt_array:
      loop->current = vcontent->value.a->size;
      loop->finish = 0;
      break;

    default:
      loop->current = -1;
      loop->finish = -1;
      break;
    }

  goto josl_w_next;
}

josl_w_each_kv: /* ( d:dict -- key value ) */
{
  josl_value *vcontent;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 1);

  vcontent = STACK (--SP);

  loop = &loops[++lp];
  loop->type = josl_w_each_kv;
  if (loop->content == NULL)
    loop->content = tgc_alloc(&gc, sizeof (josl_value));
  loop->content->type = vcontent->type;
  loop->content->value = vcontent->value;

  switch (vcontent->type)
    {
    case josl_vt_string:
      loop->current = -1;
      loop->finish = strlen (vcontent->value.s);
      break;

    case josl_vt_array:
      loop->current = -1;
      loop->finish = vcontent->value.a->size;
      break;

    case josl_vt_dict:
      loop->content->type = josl_vt_dict_it;
      loop->content->value.hditr = hashtable_iterator (vcontent->value.hd);
      loop->current = -1;
      loop->finish = hashtable_count (vcontent->value.hd);
      break;

    default:
      /* TODO throw error, although if the parser is working correctly
         we should never get here as it traps this error */
      break;
    }

  goto josl_w_next;
}

josl_w_for:
{
  josl_value *a, *b;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (--SP);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  loop = &loops[++lp];
  loop->type = josl_w_for;
  loop->current = b->value.i;
  loop->finish = a->value.i;

  if (loop->current >= loop->finish)
    {
      lp--;

      JUMP;
    }

  BREAK;
}

josl_w_times:
{
  josl_value *vtimes;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 1);

  vtimes = STACK (--SP);

  if (vtimes->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  loop = &loops[++lp];
  loop->type = josl_w_times;
  loop->current = 0;
  loop->finish = vtimes->value.i;

  if (loop->current >= loop->finish)
    {
      lp--;

      JUMP;
    }

  BREAK;
}

josl_w_next:
{
  josl_loop *loop = &loops[lp];

  switch (loop->type)
    {
    case josl_w_times:
    case josl_w_for:
      {
        loop->current += 1;
        if (loop->current >= loop->finish)
          {
            lp--;
            JUMP;
          }
      }
      break;

    case josl_w_each:
      {
        josl_value *vresult;

        switch (loop->content->type)
          {
          case josl_vt_string:
            loop->current++;
            if (loop->current >= loop->finish)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = josl_vt_integer;
            vresult->value.i = loop->content->value.s[loop->current];
            break;

          case josl_vt_array:
            loop->current++;
            if (loop->current >= loop->finish)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.a->elems[loop->current]->type;
            vresult->value = loop->content->value.a->elems[loop->current]->value;
            break;

          case josl_vt_list:
            loop->content->type = josl_vt_list_item;
            loop->content->value.li = loop->content->value.l->head;

            if (loop->content->value.li == NULL)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.li->v->type;
            vresult->value = loop->content->value.li->v->value;
            break;

          case josl_vt_list_item:
            loop->content->value.li = loop->content->value.li->next;
            if (loop->content->value.li == NULL)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.li->v->type;
            vresult->value = loop->content->value.li->v->value;
            break;

          default:
            break;
          }
      }
      break;

    case josl_w_each_rev:
      {
        josl_value *vresult;

        switch (loop->content->type)
          {
          case josl_vt_string:
            loop->current--;
            if (loop->current < 0)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = josl_vt_integer;
            vresult->value.i = loop->content->value.s[loop->current];
            break;

          case josl_vt_array:
            loop->current--;
            if (loop->current < 0)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.a->elems[loop->current]->type;
            vresult->value = loop->content->value.a->elems[loop->current]->value;
            break;

          case josl_vt_list:
            loop->content->type = josl_vt_list_item;
            loop->content->value.li = loop->content->value.l->tail;

            if (loop->content->value.li == NULL)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.li->v->type;
            vresult->value = loop->content->value.li->v->value;
            break;

          case josl_vt_list_item:
            loop->content->value.li = loop->content->value.li->prev;
            if (loop->content->value.li == NULL)
              {
                lp--;

                JUMP;
              }

            OVERFLOW_CHECK (interp, w, 1);

            vresult = STACK (SP++);
            vresult->type = loop->content->value.li->v->type;
            vresult->value = loop->content->value.li->v->value;
            break;

          default:
            break;
          }
      }
      break;

    case josl_w_each_kv:
      {
        josl_value *vkey, *vvalue, *tmpv;

        loop->current++;

        if (loop->current >= loop->finish)
          {
            lp--;

            JUMP;
          }

        OVERFLOW_CHECK (interp, w, 2);
        vkey = STACK (SP++);
        vvalue = STACK (SP++);

        switch (loop->content->type)
          {
          case josl_vt_string:
            vkey->type = josl_vt_integer;
            vkey->value.i = loop->current;
            vvalue->type = josl_vt_integer;
            vvalue->value.i = loop->content->value.s[loop->current];
            break;

          case josl_vt_array:
            vkey->type = josl_vt_integer;
            vkey->value.i = loop->current;
            vvalue->type = loop->content->value.a->elems[loop->current]->type;
            vvalue->value = loop->content->value.a->elems[loop->current]->value;
            break;

          case josl_vt_dict_it:
            vkey->type = josl_vt_string;
            vkey->value.s = (char *) hashtable_iterator_key (loop->content->value.hditr);
            tmpv = (josl_value *) hashtable_iterator_value (loop->content->value.hditr);
            vvalue->type = tmpv->type;
            vvalue->value = tmpv->value;
            hashtable_iterator_advance (loop->content->value.hditr);
            break;

          default:
            /* TODO throw error, although if the parser is working correctly
               we should never get here as it traps this error */
            break;
          }
      }
      break;

    default:
      break;
    }

  BREAK;
}

josl_w_plus_next:
{
  josl_value *a;
  josl_loop *loop;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  loop = &loops[lp];
  loop->current += a->value.i;
  if (loop->current >= loop->finish)
    {
      lp--;

      JUMP;
    }

  BREAK;
}

josl_w_vector:
{
  josl_value *a;
  josl_word *jump = NULL;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  while ((w = w->next) && strcmp (w->name, "end"))
    {
      if (a->value.i == 0)
        jump = w;
      a->value.i--;
    }

  if (jump != NULL)
    {
      jump->nextexec = w;
      w = jump;
      EXEC;
    }

  BREAK;
}

josl_w_return:
{
  JUMP;
}

josl_w_break:
{
  josl_word *headw = w->jump;

  if (headw != NULL)
    {
      switch (headw->word_id)
        {
        case josl_w_times:
        case josl_w_for:
          headw->value->value.loop->current =
            headw->value->value.loop->finish;
          break;
        }
    }

  JUMP;
}

josl_w_continue:
{
  JUMP;
}

josl_w_i:
{
  josl_value *vi;

  OVERFLOW_CHECK (interp, w, 1);

  vi = STACK (SP++);
  vi->type = josl_vt_integer;
  vi->value.i = loops[lp].current;

  BREAK;
}

josl_w_ii:
{
  josl_value *vi;

  OVERFLOW_CHECK (interp, w, 1);

  vi = STACK (SP++);
  vi->type = josl_vt_integer;
  vi->value.i = loops[lp].finish - loops[lp].current;

  BREAK;
}

josl_w_j:
{
  josl_value *vi;

  OVERFLOW_CHECK (interp, w, 1);

  vi = STACK (SP++);
  vi->type = josl_vt_integer;
  vi->value.i = loops[lp-1].current;

  BREAK;
}

josl_w_jj:
{
  BREAK;
}

josl_w_k:
{
  josl_value *vi;

  OVERFLOW_CHECK (interp, w, 1);

  vi = STACK (SP++);
  vi->type = josl_vt_integer;
  vi->value.i = loops[lp-2].current;

  BREAK;
}

josl_w_kk:
{
  BREAK;
}

josl_w_is:
{
  BREAK;
}

josl_w_do_is:
{
  josl_value *visthis, *vvalue;

  UNDERFLOW_CHECK (interp, w, 1);

  visthis = STACK (--SP);
  vvalue = STACK (SP - 1);

  if (visthis->type == josl_vt_variable)
    {
      visthis = visthis->value.v;
    }

  switch (visthis->type)
    {
    case josl_vt_null:
      if (visthis->type != vvalue->type)
        {
          JUMP;
        }

      /* We know it's null because the types are both null */
      break;

    case josl_vt_integer:
      if (visthis->type != vvalue->type
          || vvalue->value.i != visthis->value.i)
        {
          JUMP;
        }
      break;

    case josl_vt_double:
      if (visthis->type != vvalue->type
          || vvalue->value.d != visthis->value.d)
        {
          JUMP;
        }
      break;

    case josl_vt_string:
      if (visthis->type != vvalue->type
          || strcmp (vvalue->value.s, visthis->value.s) != 0)
        {
          JUMP;
        }
      break;

    case josl_vt_sbuffer:
      if (visthis->type != vvalue->type
          || strcmp (vvalue->value.b->data, visthis->value.b->data) != 0)
        {
          JUMP;
        }
      break;

    case josl_vt_regex:
      {
        int rc;
        int ovector[30];

        if (vvalue->type != josl_vt_string)
          {
            JUMP;
          }

        rc = pcre_exec (visthis->value.regex, NULL, vvalue->value.s,
                        strlen (vvalue->value.s), 0, 0, ovector, 30);
        if (rc <= 0)
          {
            JUMP;
          }
      }
      break;

    case josl_vt_word:
      {
        josl_value *tmps;
        josl_top_word *theWord = visthis->value.wref->word;

        OVERFLOW_CHECK (interp, w, 1);

        tmps = STACK (SP++);
        tmps->type = vvalue->type;
        tmps->value = vvalue->value;

        josl_exec_word (interp, theWord);

        UNDERFLOW_CHECK (interp, w, 1);

        tmps = STACK (--SP);
        if (tmps->type != josl_vt_integer)
          {
            THROW_EXC (josl_err_stack_type, "type is not comparable via is");
          }

        if (tmps->value.i == 0)
          {
            JUMP;
          }
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "type is not comparable via is");
      }
    }

  /*
     To get here, we had a match, thus remove the original
     value from the stack.
   */

  SP--;

  BREAK;
}
