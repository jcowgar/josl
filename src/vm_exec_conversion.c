/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Conversion

*/
josl_w_to_string:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.s = strdup_printf ("%d", a->value.i);
      break;

    case josl_vt_double:
      if (a->value.d > 0.000009)
        a->value.s = strdup_printf ("%.6f", a->value.d);
      else
        a->value.s = strdup_printf ("%e", a->value.d);
      break;

    case josl_vt_string:
      break;

    case josl_vt_sbuffer:
      a->value.s = a->value.b->data;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "value must be convertable to a string");
      }
    }

  a->type = josl_vt_string;

  BREAK;
}

josl_w_to_integer:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      break;

    case josl_vt_double:
      a->value.i = (int) a->value.d;
      break;

    case josl_vt_string:
      a->value.i = string_to_int64 (a->value.s);
      break;

    case josl_vt_sbuffer:
      a->value.i = string_to_int64 (a->value.b->data);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "value must be integer, double or string");
      }
    }

  a->type = josl_vt_integer;

  BREAK;
}

josl_w_to_double:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = (double) a->value.i;
      break;

    case josl_vt_double:
      break;

    case josl_vt_string:
      a->value.d = string_to_double (a->value.s);
      break;

    case josl_vt_sbuffer:
      a->value.d = string_to_double (a->value.b->data);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "value must be integer, double or string");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}
