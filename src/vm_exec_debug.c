/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Debugging

*/

josl_w_word_list:
{
  josl_value *a;
  josl_hash_itr *itr, *itr2;
  int count = 0;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);

  a->type = josl_vt_list;
  a->value.l = new_list();

  itr = hashtable_iterator (interp->intrinsics);

  do
    {
	  list_append_string(a->value.l, strdup(hashtable_iterator_key(itr)));

      count++;
    }
  while (hashtable_iterator_advance (itr));

  char *currentFile = w->top->filename;
  josl_hash *tmp = hashtable_search (interp->filewords, (void *) currentFile);

  if (tmp != NULL) {
	  itr = hashtable_iterator (tmp);

	  do
		{
		  list_append_string(a->value.l, strdup(hashtable_iterator_key(itr)));

		  count++;
		}
	  while (hashtable_iterator_advance (itr));
  }

  BREAK;
}

josl_w_words:
{
  josl_hash_itr *itr, *itr2;
  int count = 0;

  itr = hashtable_iterator (interp->intrinsics);

  fprintf (stdout, "global words: ");
  do
    {
      fprintf (stdout, "%s%s", count == 0 ? "" : ", ",
               hashtable_iterator_key (itr));
      count++;
    }
  while (hashtable_iterator_advance (itr));

  fprintf (stdout, "\n");

  itr = hashtable_iterator (interp->filewords);
  do
    {
      char *fname = hashtable_iterator_key (itr);
      fprintf (stdout, "%s words: ", fname);

      count = 0;
      itr2 = hashtable_iterator (hashtable_iterator_value (itr));
      if (itr2 != NULL)
        {
          do
            {
              fprintf (stdout, "%s%s", count == 0 ? "" : ", ",
                       hashtable_iterator_key (itr2));
              count++;
            }
          while (hashtable_iterator_advance (itr2));
        }
      fprintf (stdout, "\n");
    }
  while (hashtable_iterator_advance (itr));

  BREAK;
}

josl_w_word_show:
{
  josl_throw_exception (interp, w, josl_err_not_impl, "not yet implemented");
  BREAK;
}

josl_w_is_user_word:
{
  josl_top_word *uwc = NULL;

  UNDERFLOW_CHECK (interp, w, 1);

  uwc = hashtable_search (interp->filewords, STACK (SP - 1)->value.s);
  STACK (SP - 1)->type = josl_vt_integer;
  STACK (SP - 1)->value.i = uwc != NULL;

  BREAK;
}

josl_w_word_dot:
{
  josl_value *vword;
  josl_hash *words;
  josl_top_word *uwc;

  UNDERFLOW_CHECK (interp, w, 1);

  vword = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expected a word on TOS");
    }

  josl_dot_word (vword->value.wref->word);

  BREAK;
}

josl_w_stack_show:
{
  int tsp;

  printf ("stack (%d) ", SP);

  for (tsp = 0; tsp < SP; tsp++)
    {
      switch (STACK (tsp)->type)
        {
        case josl_vt_null:
          printf ("NULL ");
          break;

        case josl_vt_integer:
          printf ("i%lld ", STACK (tsp)->value.i);
          break;

        case josl_vt_double:
          if (interp->s[tsp]->value.d > 0.000009)
            printf ("d%.6f ", STACK (tsp)->value.d);
          else
            printf ("d%e ", STACK (tsp)->value.d);
          break;

        case josl_vt_string:
          printf ("s\"%s\" ", STACK (tsp)->value.s);
          break;

        case josl_vt_pointer:
          printf ("p%p ", STACK (tsp)->value.p);
          break;

        case josl_vt_variable:
          printf ("v%p ", STACK (tsp)->value.v);
          break;

        case josl_vt_dict:
          printf ("d%p ", STACK (tsp)->value.hd);
          break;

        case josl_vt_array:
          printf ("a%p ", STACK (tsp)->value.a);
          break;

        case josl_vt_list:
          printf ("l%p ", STACK (tsp)->value.l);
          break;

        case josl_vt_sbuffer:
          printf ("b%p ", STACK (tsp)->value.b);
          break;

        case josl_vt_word:
          printf ("w%p ", STACK (tsp)->value.wref);
          break;

        case josl_vt_regex:
          printf ("r%p ", STACK (tsp)->value.regex);
          break;

        case josl_vt_socket:
          printf ("sk%p ", STACK (tsp)->value.sock);
          break;

        case josl_vt_ffi:
          printf ("ff%p ", STACK (tsp)->value.ffi);
          break;

        default:
          printf ("not-printable ");
        }
    }

  printf ("tstack (%d) ", interp->tsp);

  for (tsp = 0; tsp < interp->tsp; tsp++)
    {
      switch (interp->ts[tsp]->type)
        {
        case josl_vt_integer:
          printf ("i%lld ", interp->ts[tsp]->value.i);
          break;

        case josl_vt_double:
          if (interp->ts[tsp]->value.d > 0.000009)
            printf ("d%.6f ", interp->ts[tsp]->value.d);
          else
            printf ("d%e ", interp->ts[tsp]->value.d);
          break;

        case josl_vt_string:
          printf ("s\"%s\" ", interp->ts[tsp]->value.s);
          break;

        case josl_vt_pointer:
          printf ("p%p ", interp->ts[tsp]->value.p);
          break;

        case josl_vt_variable:
          printf ("v%p ", interp->ts[tsp]->value.v);
          break;

        case josl_vt_dict:
          printf ("d%p ", interp->ts[tsp]->value.hd);
          break;

        case josl_vt_array:
          printf ("a%p ", interp->ts[tsp]->value.a);
          break;

        case josl_vt_list:
          printf ("l%p ", interp->ts[tsp]->value.l);
          break;

        case josl_vt_word:
          printf ("w%p ", interp->ts[tsp]->value.wref);
          break;

        case josl_vt_regex:
          printf ("r%p ", interp->ts[tsp]->value.regex);
          break;

        case josl_vt_socket:
          printf ("sk%p ", interp->ts[tsp]->value.sock);
          break;

        case josl_vt_ffi:
          printf ("ff%p ", interp->ts[tsp]->value.ffi);
          break;

        default:
          printf ("not-printable ");
        }
    }

  printf ("\n");

  BREAK;
}
