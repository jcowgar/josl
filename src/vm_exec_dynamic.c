/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

   Dynamic Execution

*/

josl_w_ffi_exec: /* ( ... ffi-word -- ?result ) */
{
  josl_value *vffi, *vtmp, *vresult;
  void **values;
  int idx;

  UNDERFLOW_CHECK (interp, w, 1);

  vffi = STACK (--SP);
  if (vffi->type != josl_vt_ffi)
    {
      THROW_EXC (josl_err_stack_type, "ffi-word must be a ffi type");
    }

  values = tgc_alloc(&gc, sizeof (void *) * vffi->value.ffi->argc);

  UNDERFLOW_CHECK (interp, w, vffi->value.ffi->argc);

  for (idx=0; idx < vffi->value.ffi->argc; idx++)
    {
      vtmp = STACK (--SP);
      switch (vtmp->type)
        {
        case josl_vt_null:
          values[idx] = &vtmp->value.p;
          break;

        case josl_vt_integer:
          values[idx] = &vtmp->value.i;
          break;

        case josl_vt_double:
          values[idx] = &vtmp->value.d;
          break;

        case josl_vt_string:
          values[idx] = &vtmp->value.s;
          break;

        case josl_vt_sbuffer:
          values[idx] = &vtmp->value.b->data;
          break;

        case josl_vt_pointer:
          {
            values[idx] = &vtmp->value.p;
          }
          break;
        }
    }

  switch (vffi->value.ffi->rettype)
    {
    case JOSL_FFI_INT:
    case JOSL_FFI_UINT:
    case JOSL_FFI_UINT8:
    case JOSL_FFI_INT8:
    case JOSL_FFI_UINT16:
    case JOSL_FFI_INT16:
    case JOSL_FFI_UINT32:
    case JOSL_FFI_INT32:
    case JOSL_FFI_UINT64:
    case JOSL_FFI_INT64:
    case JOSL_FFI_UCHAR:
    case JOSL_FFI_CHAR:
    case JOSL_FFI_USHORT:
    case JOSL_FFI_SHORT:
    case JOSL_FFI_ULONG:
    case JOSL_FFI_LONG:
      vresult = STACK (SP++);
      vresult->type = josl_vt_integer;
      ffi_call (vffi->value.ffi->cif, vffi->value.ffi->funcref, &vresult->value.i, values);
      break;

    case JOSL_FFI_FLOAT:
    case JOSL_FFI_DOUBLE:
      vresult = STACK (SP++);
      vresult->type = josl_vt_double;
      ffi_call (vffi->value.ffi->cif, vffi->value.ffi->funcref, &vresult->value.d, values);
      break;

    case JOSL_FFI_STRING:
      vresult = STACK (SP++);
      vresult->type = josl_vt_string;
      ffi_call (vffi->value.ffi->cif, vffi->value.ffi->funcref, &vresult->value.s, values);
      break;

    case JOSL_FFI_POINTER:
      vresult = STACK (SP++);
      vresult->type = josl_vt_pointer;
      ffi_call (vffi->value.ffi->cif, vffi->value.ffi->funcref, &vresult->value.p, values);
      break;

    case JOSL_FFI_VOID:
      ffi_call (vffi->value.ffi->cif, vffi->value.ffi->funcref, NULL, values);
      break;
    }

  BREAK;
}

josl_w_ffi_callback: /* ( w:josl-word p:ffi-callback -- p:final-callback ) */
{
  josl_value *vword, *vcallback, *vresult;
  ffi_closure *closure;
  int rc;

  void (*v)(void);

  UNDERFLOW_CHECK (interp, w, 2);

  vcallback = STACK (--SP);
  vword = STACK (--SP);

  if (vcallback->type != josl_vt_ffi)
    {
      THROW_EXC (josl_err_stack_type, "ffi-callback expects a ffi value on TOS");
    }
  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "ffi-callback expects quotation on NOS");
    }

  closure = ffi_closure_alloc (sizeof (ffi_closure), &v);
  if (!closure)
    {
      THROW_EXC (josl_err_not_enough_memory, "not enough memory for callback allocation");
    }

  vcallback->value.ffi->topword = vword->value.wref->word;

  if (ffi_prep_closure_loc (closure, vcallback->value.ffi->cif, josl_ffi_callback,
                            (void *) vcallback->value.ffi, v) != FFI_OK)
    {
      THROW_EXC (josl_err_not_enough_memory, "ffi_prep_closure_loc failed");
    }

  vresult = STACK(SP++);
  vresult->type = josl_vt_pointer;
  vresult->value.p = v;

  BREAK;
}

josl_w_dynamic_word:
{
  josl_hash *str = hashtable_search (interp->filewords, "<str>");
  josl_top_word *newtopword;

  if (str == NULL)
    {
      THROW_EXC (josl_err_unknown_word, "Dynamic word could not be located");
    }

  newtopword = (josl_top_word *) hashtable_search (str, w->name);
  if (newtopword == NULL)
    {
      THROW_EXC (josl_err_unknown_word, "Dynamic word could not be located");
    }

  w->value = tgc_alloc(&gc, sizeof (josl_value));
  w->value->type = josl_vt_pointer;
  w->value->value.p = newtopword;
  w->word_id = josl_w_user;

  josl_exec_word (interp, (josl_top_word *) w->value->value.p);

  BREAK;
}

josl_w_parse:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  josl_parse_string (interp, a->value.s);
  if (interp->errorStack != NULL)
    {
      fprintf (stderr, "we have errors\n");
    }

  BREAK;
}

josl_w_exec_word:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  switch (a->type)
    {
    case josl_vt_word:
      josl_exec_word (interp, a->value.wref->word);
      break;

    case josl_vt_string:
      {
        void *word = NULL;
        josl_hash *words = NULL;

        words =
          hashtable_search (interp->filewords, (void *) w->top->filename);
        if (words != NULL)
          word = hashtable_search (words, (void *) a->value.s);
        if (word == NULL)
          {
            words = hashtable_search (interp->filewords, (void *) "<str>");
            if (words != NULL)
              word = hashtable_search (words, (void *) a->value.s);
          }
        if (word == NULL)
          {
            word = hashtable_search (interp->intrinsics, (void *) a->value.s);
            if (word != NULL)
              {
                josl_top_word *tw = tgc_alloc(&gc, sizeof (josl_top_word));

                tw->name = strdup ("str-word");
                tw->filename = strdup ("<str>");
                tw->lineNo = -1;
                tw->first = tgc_alloc(&gc, sizeof (josl_word));
                tw->last = NULL;

                tw->first->word_id = (josl_word_id) word;
                tw->first->name = a->value.s;
                tw->first->lineNo = -1;

                word = tw;
              }
          }

        if (word == NULL)
          {
            THROW_EXC (josl_err_invalid_word_reference,
                       "dynamic word could not be found");
          }

        josl_exec_word (interp, word);
      }
      break;

    defaul:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects a word reference or string on TOS");
      }
    }

  BREAK;
}

josl_w_curry:                  /* ( x:value q:quotation -- q:quotation ) */
josl_w_2curry:                 /* ( x:v1 x:v2 q:quotation -- q:quotation ) */
josl_w_3curry:                 /* ( x:v1 x:v2 x:v3 q:quotation -- q:quotation ) */
josl_w_curry_pound:            /* ( x:... i:count q:quotation -- q:quotation ) */
{
  josl_value *vvalue, *vquot, *vresult;
  josl_top_word *wtop;
  josl_word *curryw;
  josl_word_ref *wref;
  int count;

  switch (w->word_id)
    {
    case josl_w_curry:
      count = 1;
      break;

    case josl_w_2curry:
      count = 2;
      break;

    case josl_w_3curry:
      count = 3;
      break;

    case josl_w_curry_pound:
      {
        josl_value *curryCount;

        UNDERFLOW_CHECK (interp, w, 1);

        curryCount = STACK (--SP);
        if (curryCount->type != josl_vt_integer)
          {
            THROW_EXC (josl_err_stack_type,
                       "expects an integer reference on TOS");
          }
        count = curryCount->value.i;
      }
    }

  UNDERFLOW_CHECK (interp, w, count + 1);

  vquot = STACK (--SP);

  if (vquot->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  wref = vquot->value.wref;

  wtop = tgc_alloc(&gc, sizeof (josl_top_word));
  wtop->name = strdup_printf ("quot%p", wtop);
  wtop->filename = wref->word->filename;
  wtop->lineNo = wref->word->lineNo;
  wtop->first = wref->word->first;
  wtop->last = wref->word->last;

  while (count > 0)
    {
      count--;

      vvalue = STACK (--SP);

      curryw = tgc_alloc(&gc, sizeof (josl_word));
      curryw->top = wtop;
      curryw->name = strdup ("<curry_val>");
      curryw->lineNo = w->lineNo;
      curryw->colNo = w->colNo;
      curryw->word_id = josl_w_push;
      curryw->next = wtop->first;
      curryw->nextexec = wtop->first;
      curryw->jump = NULL;
      curryw->value = tgc_alloc(&gc, sizeof (josl_value));
      curryw->value->type = vvalue->type;
      curryw->value->value = vvalue->value;

      wtop->first = curryw;
    }

  vresult = STACK (SP++);
  vresult->type = josl_vt_word;
  vresult->value.wref = tgc_alloc(&gc, sizeof (josl_word_ref));
  vresult->value.wref->word = wtop;
  vresult->value.wref->word_id = josl_w_user;

  BREAK;
}

josl_w_curry_question: /* ( ?... q -- q ) */
{
  josl_value *vquote, *vresult;
  josl_word *wit;
  int max = -1, thisnum;

  UNDERFLOW_CHECK (interp, w, 1);

  vquote = tgc_alloc(&gc, sizeof (josl_value));
  SP--;
  COPYV (STACK (SP), vquote);

  if (vquote->type != josl_vt_word)
    {
      SP--;
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  wit = vquote->value.wref->word->first;
  while (wit != NULL)
    {
      switch (wit->word_id)
        {
        case josl_w_place_0:
          thisnum = 1;
          break;

        case josl_w_place_1:
          thisnum = 2;
          break;

        case josl_w_place_2:
          thisnum = 3;
          break;

        case josl_w_place_3:
          thisnum = 4;
          break;

        case josl_w_place_4:
          thisnum = 5;
          break;

        case josl_w_place_5:
          thisnum = 6;
          break;

        case josl_w_place_6:
          thisnum = 7;
          break;

        case josl_w_place_7:
          thisnum = 8;
          break;

        case josl_w_place_8:
          thisnum = 9;
          break;

        case josl_w_place_9:
          thisnum = 10;
          break;

        default:
          thisnum = 0;
          break;
        }

      if (thisnum != 0)
        {
          if (max < thisnum)
            {
              max = thisnum;
            }

          if (wit->value == NULL)
            {
              wit->value = tgc_alloc(&gc, sizeof (josl_value));
            }

          COPYV (STACK (SP-thisnum), wit->value);
        }

      wit = wit->next;
    }

  if (max > 0)
    SP -= max;

  COPYV (vquote, STACK (SP));
  SP++;

  BREAK;
}

josl_w_place_0:
josl_w_place_1:
josl_w_place_2:
josl_w_place_3:
josl_w_place_4:
josl_w_place_5:
josl_w_place_6:
josl_w_place_7:
josl_w_place_8:
josl_w_place_9:
{
  OVERFLOW_CHECK (interp, w, 1);

  if (w->value == NULL)
    {
      josl_value *vsrc, *vres;
      vsrc = STACK (SP - (w->word_id - josl_w_place_0) - 1);
      vres = STACK (SP++);
      vres->type = vsrc->type;
      vres->value = vsrc->value;
    }
  else
    {
      josl_value *v;
      v = STACK (SP++);
      COPYV (w->value, v);
    }

  BREAK;
}

josl_w_compose:                /* ( q:q1 q:q2 -- q ) */
{
  josl_value *vq1, *vq2, *vresult;
  josl_top_word *wtop;
  josl_word *tmpw;

  UNDERFLOW_CHECK (interp, w, 1);

  vq2 = STACK (--SP);
  vq1 = STACK (--SP);

  if (vq2->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }
  if (vq1->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on NOS");
      return;
    }

  wtop = tgc_alloc(&gc, sizeof (josl_top_word));
  wtop->name = strdup_printf ("quot%p", wtop);
  wtop->filename = w->top->filename;
  wtop->lineNo = w->lineNo;
  wtop->first = NULL;
  wtop->last = NULL;

  tmpw = tgc_alloc(&gc, sizeof (josl_word));
  tmpw->top = wtop;
  tmpw->name = strdup_printf ("q1%p", tmpw);
  tmpw->lineNo = w->lineNo;
  tmpw->colNo = w->colNo;
  tmpw->word_id = josl_w_push;
  tmpw->next = NULL;
  tmpw->nextexec = NULL;
  tmpw->jump = NULL;
  tmpw->value = tgc_alloc(&gc, sizeof (josl_value));
  tmpw->value->type = vq1->type;
  tmpw->value->value = vq1->value;

  wtop->first = tmpw;
  wtop->last = tmpw;

  tmpw = tgc_alloc(&gc, sizeof (josl_word));
  tmpw->top = wtop;
  tmpw->name = strdup ("exec-word");
  tmpw->lineNo = w->lineNo;
  tmpw->colNo = w->colNo;
  tmpw->word_id = josl_w_exec_word;
  tmpw->next = NULL;
  tmpw->nextexec = NULL;
  tmpw->jump = NULL;
  tmpw->value = NULL;

  wtop->last->next = tmpw;
  wtop->last->nextexec = tmpw;
  wtop->last = tmpw;

  tmpw = tgc_alloc(&gc, sizeof (josl_word));
  tmpw->top = wtop;
  tmpw->name = strdup_printf ("q2%p", tmpw);
  tmpw->lineNo = w->lineNo;
  tmpw->colNo = w->colNo;
  tmpw->word_id = josl_w_push;
  tmpw->next = NULL;
  tmpw->nextexec = NULL;
  tmpw->jump = NULL;
  tmpw->value = tgc_alloc(&gc, sizeof (josl_value));
  tmpw->value->type = vq2->type;
  tmpw->value->value = vq2->value;

  wtop->last->next = tmpw;
  wtop->last->nextexec = tmpw;
  wtop->last = tmpw;

  tmpw = tgc_alloc(&gc, sizeof (josl_word));
  tmpw->top = wtop;
  tmpw->name = strdup ("exec-word");
  tmpw->lineNo = w->lineNo;
  tmpw->colNo = w->colNo;
  tmpw->word_id = josl_w_exec_word;
  tmpw->next = NULL;
  tmpw->nextexec = NULL;
  tmpw->jump = NULL;
  tmpw->value = NULL;

  wtop->last->next = tmpw;
  wtop->last->nextexec = tmpw;
  wtop->last = tmpw;

  vresult = STACK (SP++);
  vresult->type = josl_vt_word;
  vresult->value.wref = tgc_alloc(&gc, sizeof (josl_word_ref));
  vresult->value.wref->word_id = josl_w_user;
  vresult->value.wref->word = wtop;

  BREAK;
}
