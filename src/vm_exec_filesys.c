/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  File System Words

*/

josl_w_dir_open:               /* ( s:dir-name -- p:dir-handle ) */
{
  josl_value *vdir, *vresult;
  DIR *d;

  UNDERFLOW_CHECK (interp, w, 1);

  vdir = STACK (--SP);
  if (vdir->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  d = opendir (vdir->value.s);
  if (d == NULL)
    {
      THROW_ERRNO_EXC ();
    }

  if (readdir (d) == NULL)
    {
      THROW_ERRNO_EXC ();
    }

  vresult = STACK (SP++);
  vresult->type = josl_vt_pointer;
  vresult->value.p = d;

  BREAK;
}

josl_w_dir_next:               /* ( p:dir-handle -- s:filename ) */
{
  josl_value *vdir, *vresult;
  DIR *d;
  struct dirent *e;

  UNDERFLOW_CHECK (interp, w, 1);

  vdir = STACK (--SP);
  if (vdir->type != josl_vt_pointer)
    {
      THROW_EXC (josl_err_stack_type,
                 "expects a directory handle pointer on TOS");
    }

  vresult = STACK (SP++);

  if (vdir->value.p == NULL)
    {
      NULLVALUE (vresult);
    }
  else
    {
      e = readdir (vdir->value.p);
      if (e == NULL)
        {
          NULLVALUE (vresult);
        }
      else
        {
          vresult->type = josl_vt_string;
          vresult->value.s = e->d_name;
        }
    }

  BREAK;
}

josl_w_dir_close:              /* ( p:dir-handle -- ) */
{
  josl_value *a;
  DIR *d;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_pointer)
    {
      THROW_EXC (josl_err_stack_type,
                 "expects a directory handle pointer on TOS");
    }

  if (a->value.p != NULL)
    closedir (a->value.p);

  BREAK;
}

josl_w_dir_create:             /* ( s:dir-name -- ) */
{
  josl_value *vdir;

  UNDERFLOW_CHECK (interp, w, 1);

  vdir = STACK (--SP);
  if (vdir->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

#ifdef WINDOWS
  if (CreateDirectory (vdir->value.s, 0) == 0)
    {
      char *msg = tgc_alloc(&gc, sizeof (char) * 128);
      snprintf (msg, 128, "could not create directory (windows code: %i)",
                GetLastError());

      THROW_EXC (josl_err_winspecific, msg);
    }
#else
  if (mkdir (vdir->value.s, 0660) != 0)
    {
      THROW_ERRNO_EXC ();
    }
#endif

  BREAK;
}

josl_w_dir_remove:             /* ( s:dir-name -- ) */
{
  josl_value *vdir;

  UNDERFLOW_CHECK (interp, w, 1);

  vdir = STACK (--SP);
  if (vdir->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

#ifdef WINDOWS
  if (RemoveDirectory (vdir->value.s) == 0)
    {
      char *msg = tgc_alloc(&gc, sizeof (char) * 128);
      snprintf (msg, 128, "could not remove directory (windows code: %i)",
                GetLastError());

      THROW_EXC (josl_err_winspecific, msg);
    }
#else
  if (rmdir (vdir->value.s) != 0)
    {
      THROW_ERRNO_EXC ();
    }
#endif

  BREAK;
}

josl_w_file_size:              /* ( s:file-name -- i:size ) */
josl_w_file_create_time:       /* ( s:file-name -- x:result ) */
josl_w_file_access_time:
josl_w_file_modify_time:
josl_w_is_dir:
{
  josl_value *vfname, *vresult;
  struct stat stat_p;
  int result;

  UNDERFLOW_CHECK (interp, w, 1);

  vfname = STACK (--SP);
  if (vfname->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (stat (vfname->value.s, &stat_p) == 0)
    {
      switch (w->word_id)
        {
        case josl_w_file_size:
          result = stat_p.st_size;
          break;

        case josl_w_file_create_time:
          result = stat_p.st_ctime;
          break;

        case josl_w_file_modify_time:
          result = stat_p.st_mtime;
          break;

        case josl_w_file_access_time:
          result = stat_p.st_atime;
          break;

        case josl_w_is_dir:
          result = S_ISDIR (stat_p.st_mode);
          break;

        default:
          {
            THROW_EXC (josl_err_invalid_word_reference, "invalid word");
          }
        }
    }
  else
    result = -1;

  vresult = STACK (SP++);
  vresult->type = josl_vt_integer;
  vresult->value.i = result;

  BREAK;
}

josl_w_is_readable:            /* ( s:file-name -- b:readable? ) */
josl_w_is_writable:
josl_w_is_executable:
josl_w_file_exists:
{
  josl_value *vfname, *vresult;
  int check, result;

  UNDERFLOW_CHECK (interp, w, 1);

  vfname = STACK (--SP);
  if (vfname->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  switch (w->word_id)
    {
    case josl_w_is_readable:
      check = R_OK;
      break;
    case josl_w_is_writable:
      check = W_OK;
      break;
    case josl_w_is_executable:
      check = X_OK;
      break;
    case josl_w_file_exists:
      check = F_OK;
      break;
    default:
      check = R_OK;
    }

  vresult = STACK (SP++);
  vresult->value.i = (access (vfname->value.s, check) == 0);
  vresult->type = josl_vt_integer;

  BREAK;
}

josl_w_file_copy:              /* ( s:from-filename s:to-filename -- b:ok? ) */
{
  josl_value *from, *to, *vresult;
  int result;

  UNDERFLOW_CHECK (interp, w, 2);

  to = STACK (--SP);
  from = STACK (--SP);

  if (from->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (to->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  if (CopyFile (from->value.s, to->value.s, 0) == 0)
    {
#ifndef WINDOWS
      THROW_ERRNO_EXC ();
#else
      char *msg = tgc_alloc(&gc, sizeof (char) * 128);
      snprintf (msg, 128, "could not copy file: %i", GetLastError ());
      THROW_EXC (josl_err_winspecific, msg);
#endif
    }

  BREAK;
}

josl_w_file_remove:            /* ( s:filename -- ) */
{
  josl_value *vfname;

  UNDERFLOW_CHECK (interp, w, 1);

  vfname = STACK (--SP);
  if (vfname->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

#ifdef WINDOWS
  if (DeleteFile (vfname->value.s) == 0)
    {
      char *msg = tgc_alloc(&gc, sizeof (char) * 128);
      snprintf (msg, 128, "could not remove file: %i", GetLastError ());
      THROW_EXC (josl_err_winspecific, msg);
    }
#else
  if (unlink (vfname->value.s) != 0)
    {
      THROW_ERRNO_EXC ();
    }
#endif

  BREAK;
}

josl_w_file_move:              /* ( s:from-filename s:to-filename -- ) */
{
  josl_value *from, *to;

  UNDERFLOW_CHECK (interp, w, 2);

  to = STACK (--SP);
  from = STACK (--SP);

  if (from->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (to->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

#ifdef WINDOWS
  if (MoveFile (from->value.s, to->value.s) == 0)
    {
      char *msg = tgc_alloc(&gc, sizeof (char) * 128);
      snprintf (msg, 128, "could not move file: %i", GetLastError ());
      THROW_EXC (josl_err_winspecific, msg);
    }
#else
  if (rename (from->value.s, to->value.s) == -1)
    {
      THROW_ERRNO_EXC ();
    }
#endif

  BREAK;
}
