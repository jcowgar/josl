/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

   Functional Words

*/

josl_w_map:                    /* ( al:content w:word -- al:result ) */
{
  josl_value *vcontent, *vword, *vresult, *vtmp;
  josl_word_ref *mapWordRef;
  int idx;

  UNDERFLOW_CHECK (interp, w, 2);

  vword = STACK (--SP);
  vcontent = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  mapWordRef = vword->value.wref;

  switch (vcontent->type)
    {
    case josl_vt_string:
      {
        char *s = vcontent->value.s;
        charbuf *cb;
        int len = strlen (s);

        cb = cbuf_alloc (len);

        OVERFLOW_CHECK (interp, w, 1);

        for (idx = 0; idx < len; idx++)
          {
            vtmp = STACK (SP++);
            vtmp->type = josl_vt_integer;
            vtmp->value.i = s[idx];

            josl_exec_word (interp, mapWordRef->word);

            vtmp = STACK (--SP);
            switch (vtmp->type)
              {
              case josl_vt_integer:
                cbuf_append_ch (cb, vtmp->value.i);
                break;

              case josl_vt_string:
                cbuf_append_str (cb, vtmp->value.s, -1);
                break;

              case josl_vt_sbuffer:
                cbuf_append_str (cb, vtmp->value.b->data, -1);
                break;

              default:
                THROW_EXC (josl_err_stack_type,
                           "expects an integer, string or buffer "
                           "as a result of map quotation");
                break;
              }
          }

        vresult = STACK (SP++);
        vresult->type = josl_vt_string;
        vresult->value.s = cb->data;
      }
      break;

    case josl_vt_array:
      {
        josl_array *ary = vcontent->value.a, *newAry;

        newAry = tgc_alloc(&gc, sizeof (josl_array));
        newAry->size = ary->size;
        newAry->capacity = ary->capacity;
        newAry->elems = tgc_alloc(&gc, sizeof (void *) * newAry->size);
        OVERFLOW_CHECK (interp, w, 1);
        for (idx = 0; idx < ary->size; idx++)
          {
            vtmp = STACK (SP++);
            vtmp->type = ary->elems[idx]->type;
            vtmp->value = ary->elems[idx]->value;

            josl_exec_word (interp, mapWordRef->word);

            newAry->elems[idx] = tgc_alloc(&gc, sizeof (josl_value));

            vtmp = STACK (--SP);
            newAry->elems[idx]->type = vtmp->type;
            newAry->elems[idx]->value = vtmp->value;
          }

        vresult = STACK (SP++);
        vresult->type = josl_vt_array;
        vresult->value.a = newAry;
      }
      break;

    case josl_vt_list:
      {
        josl_list *lst = vcontent->value.l, *newLst;
        josl_list_item *item, *newItem;

        newLst = tgc_alloc(&gc, sizeof (josl_list));
        newLst->size = lst->size;
        newLst->head = NULL;
        newLst->tail = NULL;
        newLst->cur = NULL;

        OVERFLOW_CHECK (interp, w, 1);

        item = lst->head;
        while (item != NULL)
          {
            vtmp = STACK (SP++);
            vtmp->type = item->v->type;
            vtmp->value = item->v->value;

            josl_exec_word (interp, mapWordRef->word);

            vtmp = STACK (--SP);

            newItem = tgc_alloc(&gc, sizeof (josl_list_item));
            newItem->v = tgc_alloc(&gc, sizeof (josl_value));
            newItem->v->type = vtmp->type;
            newItem->v->value = vtmp->value;
            newItem->prev = newLst->tail;
            newItem->next = NULL;
            if (newLst->tail != NULL)
              {
                newLst->tail->next = newItem;
                newItem->prev = newLst->tail;
              }
            if (newLst->head == NULL)
              {
                newLst->head = newItem;
                newLst->cur = newItem;
              }
            newLst->tail = newItem;
            item = item->next;
          }

        newLst->cur = NULL;

        vresult = STACK (SP++);
        vresult->type = josl_vt_list;
        vresult->value.l = newLst;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects an array or list on NOS");
      }
    }

  BREAK;
}

josl_w_filter:                 /* ( al:content w:word -- al:result ) */
{
  josl_value *vcontent, *vword, *vresult, *vtmp;
  josl_word_ref *wordRef;
  int idx;

  UNDERFLOW_CHECK (interp, w, 2);

  vword = STACK (--SP);
  vcontent = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  wordRef = vword->value.wref;

  switch (vcontent->type)
    {
    case josl_vt_string:
      {
        char *ns, *s = vcontent->value.s;
        int cnt = 0, len = strlen (s);

        ns = tgc_alloc(&gc, sizeof (char) * len);

        for (idx = 0; idx < len; idx++)
          {
            vtmp = STACK (SP++);
            vtmp->type = josl_vt_integer;
            vtmp->value.i = s[idx];


            josl_exec_word (interp, wordRef->word);

            vtmp = STACK (--SP);
            if (vtmp->value.i != 0)
              {
                ns[cnt] = s[idx];
                cnt++;
              }
          }
        ns[cnt] = 0;

        vresult = STACK (SP++);
        vresult->type = josl_vt_string;
        vresult->value.s = ns;
      }
      break;

    case josl_vt_array:
      {
        josl_array *ary = vcontent->value.a, *newAry;
        josl_value **tmpv;

        newAry = tgc_alloc(&gc, sizeof (josl_array));
        newAry->size = 0;
        newAry->capacity = ary->size;
        newAry->elems = tgc_alloc(&gc, sizeof (void *) * ary->size);

        OVERFLOW_CHECK (interp, w, 1);

        for (idx = 0; idx < ary->size; idx++)
          {
            vtmp = STACK (SP++);
            vtmp->type = ary->elems[idx]->type;
            vtmp->value = ary->elems[idx]->value;

            josl_exec_word (interp, wordRef->word);

            vtmp = STACK (--SP);
            if (vtmp->value.i != 0)
              {
                newAry->elems[newAry->size] = tgc_alloc(&gc, sizeof (josl_value));
                newAry->elems[newAry->size]->type = ary->elems[idx]->type;
                newAry->elems[newAry->size]->value = ary->elems[idx]->value;
                newAry->size++;
              }
          }

        tmpv = tgc_realloc (&gc, newAry->elems, sizeof (void *) * newAry->size);
        if (tmpv != NULL)
          {
            newAry->elems = tmpv;
          }

        vresult = STACK (SP++);
        vresult->type = josl_vt_array;
        vresult->value.a = newAry;
      }
      break;

    case josl_vt_list:
      {
        josl_list *lst = vcontent->value.l, *newLst;
        josl_list_item *item, *newItem;

        newLst = tgc_alloc(&gc, sizeof (josl_list));
        newLst->size = 0;
        newLst->head = NULL;
        newLst->tail = NULL;
        newLst->cur = NULL;

        OVERFLOW_CHECK (interp, w, 1);

        item = lst->head;
        while (item != NULL)
          {
            vtmp = STACK (SP++);
            vtmp->type = item->v->type;
            vtmp->value = item->v->value;

            josl_exec_word (interp, wordRef->word);

            vtmp = STACK (--SP);

            if (vtmp->value.i != 0)
              {
                newItem = tgc_alloc(&gc, sizeof (josl_list_item));
                newItem->v = item->v;
                newItem->prev = newLst->tail;
                newItem->next = NULL;
                if (newLst->tail != NULL)
                  {
                    newLst->tail->next = newItem;
                    newItem->prev = newLst->tail;
                  }
                if (newLst->head == NULL)
                  {
                    newLst->head = newItem;
                    newLst->cur = newItem;
                  }
                newLst->tail = newItem;
                newLst->size++;
              }

            item = item->next;
          }

        newLst->cur = NULL;

        vresult = STACK (SP++);
        vresult->type = josl_vt_list;
        vresult->value.l = newLst;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects an array or list on NOS");
      }
    }

  BREAK;
}

josl_w_fold:                   /* ( al:content w:word -- x:result ) */
{
  josl_value *vcontent, *vword, *vresult, *vtmpA, *vtmpB;
  josl_word_ref *wordRef;
  int idx;

  UNDERFLOW_CHECK (interp, w, 2);

  vword = STACK (--SP);
  vcontent = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  wordRef = vword->value.wref;

  OVERFLOW_CHECK (interp, w, 2);

  switch (vcontent->type)
    {
    case josl_vt_string:
      {
        char *s = vcontent->value.s;
        int len = strlen (s);

        if (len == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
          }
        else if (len == 1)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_integer;
            vresult->value.i = s[0];
          }
        else
          {
            vtmpA = STACK (SP++);
            vtmpB = STACK (SP++);

            vtmpA->type = josl_vt_integer;
            vtmpA->value.i = s[0];
            vtmpB->type = josl_vt_integer;
            vtmpB->value.i = s[1];

            josl_exec_word (interp, wordRef->word);

            for (idx = 2; idx < len; idx++)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = josl_vt_integer;
                vtmpA->value.i = s[idx];

                josl_exec_word (interp, wordRef->word);
              }
          }
      }
      break;

    case josl_vt_array:
      {
        josl_array *ary = vcontent->value.a;

        if (ary->size == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
          }
        else if (ary->size == 1)
          {
            vresult = STACK (SP++);
            vresult->type = ary->elems[0]->type;
            vresult->value = ary->elems[0]->value;
          }
        else
          {
            vtmpA = STACK (SP++);
            vtmpB = STACK (SP++);

            vtmpA->type = ary->elems[0]->type;
            vtmpA->value = ary->elems[0]->value;
            vtmpB->type = ary->elems[1]->type;
            vtmpB->value = ary->elems[1]->value;

            josl_exec_word (interp, wordRef->word);

            for (idx = 2; idx < ary->size; idx++)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = ary->elems[idx]->type;
                vtmpA->value = ary->elems[idx]->value;

                josl_exec_word (interp, wordRef->word);
              }
          }
      }
      break;

    case josl_vt_list:
      {
        josl_list *lst = vcontent->value.l;
        josl_list_item *itemA, *itemB;

        if (lst->size == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
            vresult->value.i = 0;
          }
        else if (lst->size == 1)
          {
            vresult = STACK (SP++);
            vresult->type = lst->head->v->type;
            vresult->value = lst->head->v->value;
          }
        else
          {
            vtmpA = STACK (SP++);
            vtmpB = STACK (SP++);

            itemA = lst->head;
            itemB = itemA->next;

            vtmpA->type = itemA->v->type;
            vtmpA->value = itemA->v->value;
            vtmpB->type = itemB->v->type;
            vtmpB->value = itemB->v->value;

            josl_exec_word (interp, wordRef->word);

            itemA = itemB->next;
            while (itemA != NULL)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = itemA->v->type;
                vtmpA->value = itemA->v->value;

                josl_exec_word (interp, wordRef->word);
                itemA = itemA->next;
              }
          }
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects an array or list on NOS");
      }
    }

  BREAK;
}

josl_w_foldr:                  /* ( al:content w:word -- x:result ) */
{
  josl_value *vcontent, *vword, *vresult, *vtmpA, *vtmpB;
  josl_word_ref *wordRef;
  int idx;

  UNDERFLOW_CHECK (interp, w, 2);

  vword = STACK (--SP);
  vcontent = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "expects a word reference on TOS");
    }

  wordRef = vword->value.wref;

  OVERFLOW_CHECK (interp, w, 2);

  switch (vcontent->type)
    {
    case josl_vt_string:
      {
        char *s = vcontent->value.s;
        int len = strlen (s);

        if (len == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
          }
        else if (len == 1)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_integer;
            vresult->value.i = s[0];
          }
        else
          {
            vtmpA = STACK (SP++);
            vtmpB = STACK (SP++);

            vtmpA->type = josl_vt_integer;
            vtmpA->value.i = s[len-1];
            vtmpB->type = josl_vt_integer;
            vtmpB->value.i = s[len-2];

            josl_exec_word (interp, wordRef->word);

            for (idx = len - 3; idx >= 0; idx--)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = josl_vt_integer;
                vtmpA->value.i = s[idx];

                josl_exec_word (interp, wordRef->word);
              }
          }
      }
      break;

    case josl_vt_array:
      {
        josl_array *ary = vcontent->value.a;

        if (ary->size == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
          }
        else if (ary->size == 1)
          {
            vresult = STACK (SP++);
            vresult->type = ary->elems[0]->type;
            vresult->value = ary->elems[0]->value;
          }
        else
          {
            vtmpB = STACK (SP++);
            vtmpA = STACK (SP++);

            vtmpB->type = ary->elems[ary->size - 1]->type;
            vtmpB->value = ary->elems[ary->size - 1]->value;
            vtmpA->type = ary->elems[ary->size - 2]->type;
            vtmpA->value = ary->elems[ary->size - 2]->value;

            josl_exec_word (interp, wordRef->word);

            for (idx = ary->size - 3; idx >= 0; idx--)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = ary->elems[idx]->type;
                vtmpA->value = ary->elems[idx]->value;

                josl_exec_word (interp, wordRef->word);
              }
          }
      }
      break;

    case josl_vt_list:
      {
        josl_list *lst = vcontent->value.l;
        josl_list_item *itemA, *itemB;

        if (lst->size == 0)
          {
            vresult = STACK (SP++);
            vresult->type = josl_vt_null;
            vresult->value.i = 0;
          }
        else if (lst->size == 1)
          {
            vresult = STACK (SP++);
            vresult->type = lst->head->v->type;
            vresult->value = lst->head->v->value;
          }
        else
          {
            vtmpB = STACK (SP++);
            vtmpA = STACK (SP++);

            itemB = lst->tail;
            itemA = itemB->prev;

            vtmpA->type = itemA->v->type;
            vtmpA->value = itemA->v->value;
            vtmpB->type = itemB->v->type;
            vtmpB->value = itemB->v->value;

            josl_exec_word (interp, wordRef->word);

            itemA = itemA->prev;
            while (itemA != NULL)
              {
                vtmpA = STACK (SP++);
                vtmpA->type = itemA->v->type;
                vtmpA->value = itemA->v->value;

                josl_exec_word (interp, wordRef->word);
                itemA = itemA->prev;
              }
          }
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects an array or list on NOS");
      }
    }

  BREAK;
}
