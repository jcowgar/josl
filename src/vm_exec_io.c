/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Input/Output

*/

/* Console print/println */

josl_w_print:
josl_w_println:
{
  josl_value *vstdout;

  OVERFLOW_CHECK (interp, w, 1);

  vstdout = STACK (SP++);
  vstdout->type = josl_vt_pointer;
  vstdout->value.p = (void *) stdout;

  goto josl_w_write;
}

josl_w_file_open:              /* ( s:filename s:mode -- p:fh ) */
{
  FILE *fh;
  josl_value *vfilename, *vmode, *vfh;

  UNDERFLOW_CHECK (interp, w, 2);

  vmode = STACK (--SP);
  vfilename = STACK (--SP);

  if (vmode->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expected a string on TOS");
    }
  if (vfilename->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expected a string on TOS");
    }

  fh = fopen (vfilename->value.s, vmode->value.s);
  if (fh == NULL)
    {
      THROW_ERRNO_EXC ();
    }
  else
    {
      vfh = STACK (SP++);
      vfh->value.p = fh;
      vfh->type = josl_vt_pointer;
    }

  BREAK;
}

josl_w_close:                  /* ( x:obj -- ) */
{
  josl_value *vobj;

  UNDERFLOW_CHECK (interp, w, 1);

  vobj = STACK (SP - 1);

  switch (vobj->type)
    {
    case josl_vt_socket:
      goto josl_w_close_socket;
      break;

    case josl_vt_pointer:
      goto josl_w_close_file;
      break;

    default:
      {
        --SP;

        THROW_EXC (josl_err_stack_type, "invalid type on TOS");
      }
    }

  BREAK;
}

josl_w_close_file:
{
  josl_value *vobj;

  vobj = STACK (--SP);

  if (vobj->value.p != NULL)
    {
      if (fclose (vobj->value.p) != 0 && errno != 0)
        {
          THROW_ERRNO_EXC ();
        }
    }

  BREAK;
}

josl_w_eof:                    /* ( x:from -- b:eof? ) */
{
  josl_value *vfrom;

  UNDERFLOW_CHECK (interp, w, 1);

  vfrom = STACK (SP - 1);

  switch (vfrom->type)
    {
    case josl_vt_pointer:
      goto josl_w_eof_file;

    case josl_vt_socket:
      goto josl_w_eof_socket;

    default:
      {
        --SP;                   /* take vfrom off the stack */

        THROW_EXC (josl_err_stack_type, "expected file or socket on TOS");
      }
    }
}

josl_w_eof_file:
{
  josl_value *vfrom, *veof;
  FILE *fh;

  vfrom = STACK (--SP);
  fh = (FILE *) vfrom->value.p;

  veof = STACK (SP++);
  veof->type = josl_vt_integer;
  veof->value.i = feof (fh) != 0;

  BREAK;
}

josl_w_read_ch:                /* ( x:from -- i:char ) */
{
  josl_value *vfrom;

  UNDERFLOW_CHECK (interp, w, 1);

  vfrom = STACK (SP - 1);

  switch (vfrom->type)
    {
    case josl_vt_pointer:
      goto josl_w_read_ch_file;

    case josl_vt_socket:
      goto josl_w_read_ch_socket;

    default:
      {
        --SP;                   /* take vfrom off the stack */

        THROW_EXC (josl_err_stack_type, "expected file or socket on TOS");
      }
    }
}

josl_w_read_ch_file:           /* ( p:fh -- i:char ) */
{
  FILE *fh;
  josl_value *vfrom, *vchar;
  int ch;

  vfrom = STACK (--SP);

  fh = (FILE *) vfrom->value.p;

  ch = fgetc (fh);

  if (ch == -1 && errno != 0)
    {
      THROW_ERRNO_EXC ();
    }
  else
    {
      vchar = STACK (SP++);
      vchar->type = josl_vt_integer;
      vchar->value.i = ch;
    }

  BREAK;
}

josl_w_read_line:              /* ( x:from -- s:line ) */
{
  josl_value *vfrom;

  UNDERFLOW_CHECK (interp, w, 1);

  vfrom = STACK (SP - 1);

  switch (vfrom->type)
    {
    case josl_vt_pointer:
      goto josl_w_read_line_file;

    case josl_vt_socket:
      goto josl_w_read_line_socket;

    default:
      {
        --SP;                   /* take vfrom off the stack */

        THROW_EXC (josl_err_stack_type, "expected file or socket on TOS");
      }
    }
}

josl_w_read_line_file:
{
  josl_value *vfrom, *vline = NULL;
  FILE *fh;
  char line[16384];

  vfrom = STACK (--SP);

  fh = (FILE *) vfrom->value.p;

  if (fgets (line, 16384, fh) == NULL)
    {
      if (errno == 0)
        {
          vline = STACK (SP++);
          vline->type = josl_vt_string;
          vline->value.s = strdup ("");
        }
      else
        {
          THROW_ERRNO_EXC ();
        }
    }
  else
    {
      int len = strlen (line);
      if (len == 16383)
        {
          /* we didn't read the whole line, try again */
          charbuf *cb = cbuf_alloc (32768);
          cbuf_assign (cb, line, -1);

          while (len == 16383)
            {
              if (fgets (line, 16384, fh) == NULL)
                {
                  THROW_ERRNO_EXC ();
                }

              cbuf_append_str (cb, line, -1);
              len = strlen (line);
            }

          vline = STACK (SP++);
          vline->type = josl_vt_string;
          vline->value.s = cb->data;
        }
      else
        {
          vline = STACK (SP++);
          vline->type = josl_vt_string;
          vline->value.s = strdup (line);
        }
    }

  /* Remove newlines */
  if (vline != NULL)
    {
      int idx = strlen (vline->value.s);

      while (idx >= 0 &&
             (vline->value.s[idx] == '\n' || vline->value.s[idx] == '\r'))
        {
          vline->value.s[idx--] = 0;
        }
    }

  BREAK;
}

josl_w_read_str:               /* ( i:count x:from -- s:content ) */
{
  josl_value *tos, *nos;

  UNDERFLOW_CHECK (interp, w, 2);

  tos = STACK (SP - 1);
  nos = STACK (SP - 2);

  switch (tos->type)
    {
    case josl_vt_pointer:
      goto josl_w_read_str_file;

    case josl_vt_socket:
      goto josl_w_read_str_socket;

    case josl_vt_integer:
      if (nos->type == josl_vt_socket)
        goto josl_w_read_str_socket_opts;

    default:
      {
        --SP;                   /* take vfrom off the stack */
        --SP;                   /* take vcount off the stack */

        THROW_EXC (josl_err_stack_type, "expected file or socket on TOS");
      }
    }
}

josl_w_read_str_file:          /* ( i:count x:from -- s:content ) */
{
  josl_value *vfrom, *vcount, *vcontent;
  FILE *fh;
  char *content = NULL;
  size_t len, readlen;

  UNDERFLOW_CHECK (interp, w, 2);

  vfrom = STACK (--SP);
  vcount = STACK (--SP);

  if (vcount->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected an integer on NOS");
    }

  content = tgc_alloc(&gc, sizeof (char) * (vcount->value.i + 1));
  fh = (FILE *) vfrom->value.p;

  readlen = fread (content, sizeof (char), vcount->value.i, fh);
  if (readlen > 0)
    {
      vcontent = STACK (SP++);
      vcontent->type = josl_vt_string;
      vcontent->value.s = content;
    }
  else
    {
      THROW_ERRNO_EXC ();
    }

  BREAK;
}

josl_w_read_blob:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_write:                  /* ( x:content x:where-to -- ) */
josl_w_writeln:
{
  josl_value *vwhere;

  UNDERFLOW_CHECK (interp, w, 1);

  vwhere = STACK (SP - 1);

  switch (vwhere->type)
    {
    case josl_vt_pointer:
      goto josl_w_write_file;
      break;

    case josl_vt_socket:
      goto josl_w_write_socket;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected file or socket on TOS");
      }
    }

  BREAK;
}

josl_w_write_file:
{
  josl_value *vwhere, *vcontent;
  FILE *fh;
  int result = -1;

  UNDERFLOW_CHECK (interp, w, 1);

  vwhere = STACK (--SP);
  vcontent = STACK (--SP);

  fh = (FILE *) vwhere->value.p;

  switch (vcontent->type)
    {
    case josl_vt_integer:
      result = fprintf (fh, "%lld", vcontent->value.i);
      break;

    case josl_vt_double:
      if (vcontent->value.d > 0.000009)
        result = fprintf (fh, "%.6f", vcontent->value.d);
      else
        result = fprintf (fh, "%e", vcontent->value.d);
      break;

    case josl_vt_string:
      if (vcontent->value.s == NULL)
        result = fprintf (fh, "NULL");
      else
        result = fprintf (fh, "%s", vcontent->value.s);
      break;

    case josl_vt_sbuffer:
      result = fprintf (fh, "%s", vcontent->value.b->data);
      break;

    case josl_vt_dict:
      result = fprintf (fh, "dict(%p)", vcontent->value.hd);
      break;

    case josl_vt_array:
      result = fprintf (fh, "array(%p)", vcontent->value.a);
      break;

    case josl_vt_list:
      result = fprintf (fh, "list(%p)", vcontent->value.l);
      break;

    case josl_vt_pointer:
      result = fprintf (fh, "pointer(%p)", vcontent->value.p);
      break;

    case josl_vt_word:
      result = fprintf (fh, "word(%p)", vcontent->value.wref);
      break;

    case josl_vt_regex:
      result = fprintf (fh, "regex(%p)", vcontent->value.regex);
      break;

    case josl_vt_ffi:
      result = fprintf (fh, "ffi(%p)", vcontent->value.ffi);
      break;

    case josl_vt_null:
      result = fprintf (fh, "NULL");
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "unknown stack type");
      }
    }

  NULLVALUE (vcontent);

  if (result >= 0)
    {
      if (w->word_id == josl_w_println || w->word_id == josl_w_writeln)
        result = fprintf (fh, "\n");
    }

  if (result < 0)
    {
      THROW_ERRNO_EXC ();
    }

  BREAK;
}
