/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Math  Words

*/

josl_w_plus:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i += b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_double;
          a->value.d += b->value.d;
          break;
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d += (double) b->value.i;
          break;

        case josl_vt_double:
          a->value.d += b->value.d;
          break;
        }
      break;
    }

  BREAK;
}

josl_w_minus:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i -= b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_double;
          a->value.d -= b->value.d;
          break;
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d -= (double) b->value.i;
          break;

        case josl_vt_double:
          a->value.d -= b->value.d;
          break;
        }
      break;
    }

  BREAK;
}

josl_w_multiply:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i *= b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_double;
          a->value.d *= b->value.d;
          break;
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d *= (double) b->value.i;
          break;

        case josl_vt_double:
          a->value.d *= b->value.d;
          break;
        }
      break;
    }
}

BREAK;

josl_w_divide:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i /= b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_double;
          a->value.d = (double) a->value.i / b->value.d;
          break;
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d /= (double) b->value.i;
          break;

        case josl_vt_double:
          a->value.d /= b->value.d;
          break;
        }
      break;
    }

  BREAK;
}

josl_w_mod:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.i = a->value.i % b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_integer;
          a->value.i = (int64) a->value.d % (int64) b->value.d;
          break;
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->type = josl_vt_integer;
          a->value.i = (int64) a->value.d % (int64) b->value.i;
          break;

        case josl_vt_double:
          a->type = josl_vt_integer;
          a->value.i = (int64) a->value.d % (int64) b->value.d;
          break;
        }
      break;
    }

  BREAK;
}

josl_w_one_plus:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i++;
      break;

    case josl_vt_double:
      a->value.d++;
      break;
    }

  BREAK;
}

josl_w_one_minus:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i--;
      break;

    case josl_vt_double:
      a->value.d--;
      break;
    }

  BREAK;
}

josl_w_two_plus:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i += 2;
      break;

    case josl_vt_double:
      a->value.d += 2.0;
      break;
    }

  BREAK;
}

josl_w_two_minus:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i -= 2;
      break;

    case josl_vt_double:
      a->value.d -= 2.0;
      break;
    }

  BREAK;
}

josl_w_negate:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = -(a->value.i);
      break;

    case josl_vt_double:
      a->value.d = -(a->value.d);
      break;
    }

  BREAK;
}

josl_w_abs:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      if (a->value.i < 0)
        a->value.i = -a->value.i;
      break;

    case josl_vt_double:
      if (a->value.d < 0)
        a->value.d = -a->value.d;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_min:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          if (a->value.i > b->value.i)
            a->value.i = b->value.i;
          break;

        case josl_vt_double:
          if (a->value.i > b->value.d)
            {
              a->value.d = b->value.d;
              a->type = josl_vt_double;
            }
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          if (a->value.d > b->value.i)
            {
              a->value.i = b->value.i;
              a->type = josl_vt_integer;
            }
          break;

        case josl_vt_double:
          if (a->value.d > b->value.d)
            a->value.d = b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }
      break;
    }

  BREAK;
}

josl_w_max:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          if (a->value.i < b->value.i)
            a->value.i = b->value.i;
          break;

        case josl_vt_double:
          if (a->value.i < b->value.d)
            {
              a->value.d = b->value.d;
              a->type = josl_vt_double;
            }
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          if (a->value.d < b->value.i)
            {
              a->value.i = b->value.i;
              a->type = josl_vt_integer;
            }
          break;

        case josl_vt_double:
          if (a->value.d < b->value.d)
            a->value.d = b->value.d;
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }
      break;
    }

  BREAK;
}

josl_w_ceil:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = (double) a->value.i;
      break;

    case josl_vt_double:
      if (((int) a->value.d) < a->value.d)
        a->value.d = (double) ((int) (a->value.d + 1));
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_floor:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = (double) a->value.i;
      break;

    case josl_vt_double:
      a->value.d = (double) ((int) a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_round:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = (double) a->value.i;
      break;

    case josl_vt_double:
      a->value.d = round (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_round_to:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type,
                 "expects an integer as the 'round-to' value");
    }

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = (double) a->value.i;
      break;

    case josl_vt_double:
      {
        double to = pow (10, b->value.i);
        a->value.d = floor (a->value.d * to + 0.5) / to;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_acos:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = acos ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = acos (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_asin:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = asin ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = asin (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_atan:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = atan ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = atan (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_atan2:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = atan2 ((double) a->value.i, (double) b->value.i);
          break;

        case josl_vt_double:
          a->value.d = atan2 ((double) a->value.i, b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }

      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = atan2 (a->value.d, (double) b->value.i);
          break;

        case josl_vt_double:
          a->value.d = atan2 (a->value.d, b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expects a number");
          }
        }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_cos:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = cos ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = cos (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_cosh:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = cosh ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = cosh (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_sin:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = sin ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = sin (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_sinh:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = sinh ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = sinh (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_tan:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = tan ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = tan (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_tanh:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = tanh ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = tanh (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_exp:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = exp ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = exp (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_exp2:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = exp2 ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = exp2 (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_expm1:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = expm1 ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = expm1 (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_frexp:
{
  josl_value *a, *b;
  int exp;

  UNDERFLOW_CHECK (interp, w, 1);
  OVERFLOW_CHECK (interp, w, 2);

  a = STACK (SP - 1);
  b = STACK (SP++);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = frexp ((double) a->value.i, &exp);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = frexp (a->value.d, &exp);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  b->type = josl_vt_integer;
  b->value.i = exp;

  BREAK;
}

josl_w_log:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = log ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = log (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_log1p:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = log1p ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = log1p (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_log2:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = log2 ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = log2 (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_log10:
{
  josl_value *a;
  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = log10 ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = log10 (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_modf:
{
  josl_value *a, *b;
  double iptr;

  UNDERFLOW_CHECK (interp, w, 1);
  OVERFLOW_CHECK (interp, w, 2);

  a = STACK (SP - 1);
  b = STACK (SP++);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = modf ((double) a->value.i, &iptr);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = modf (a->value.d, &iptr);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  b->type = josl_vt_double;
  b->value.d = iptr;

  BREAK;
}

josl_w_pow:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = pow ((double) a->value.i, (double) b->value.i);
          break;

        case josl_vt_double:
          a->value.d = pow ((double) a->value.i, b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expected number");
          }
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = pow (a->value.d, b->value.i);
          break;

        case josl_vt_double:
          a->value.d = pow (a->value.d, (double) b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expected number");
          }
        }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_sqrt:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d = sqrt ((double) a->value.i);
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d = sqrt (a->value.d);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_fmod:
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = fmod ((double) a->value.i, (double) b->value.i);
          break;

        case josl_vt_double:
          a->value.d = fmod ((double) a->value.i, b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expected number");
          }
        }
      break;

    case josl_vt_double:
      switch (b->type)
        {
        case josl_vt_integer:
          a->value.d = fmod (a->value.d, b->value.i);
          break;

        case josl_vt_double:
          a->value.d = fmod (a->value.d, (double) b->value.d);
          break;

        default:
          {
            THROW_EXC (josl_err_stack_type, "expected number");
          }
        }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected number");
      }
    }

  a->type = josl_vt_double;

  BREAK;
}

josl_w_degrees:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d *= 57.29577951308232090712;
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d *= 57.29577951308232090712;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_radians:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.d *= 0.01745329251994329576;
      a->type = josl_vt_double;
      break;

    case josl_vt_double:
      a->value.d *= 0.01745329251994329576;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a number");
      }
    }

  BREAK;
}

josl_w_rand:
{
  josl_value *vrand;
  
  OVERFLOW_CHECK (interp, w, 1);
  
  vrand = STACK (SP++);
  vrand->type = josl_vt_integer;
  vrand->value.i = rand();
  
  BREAK;
}

josl_w_srand: /* ( i -- ) */
{
  josl_value *vseed;
  
  UNDERFLOW_CHECK (interp, w, 1);
  
  vseed = STACK (--SP);
  if (vseed->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "srand expects an integer on TOS");
    }
  
  srand (vseed->value.i);
  
  BREAK;
}

