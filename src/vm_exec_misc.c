/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Misc Intrinsic Type Words

*/

josl_w_variable:
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);
  a->type = josl_vt_variable;
  a->value.v = w->value;

  BREAK;
}

josl_w_dict:
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);
  a->type = josl_vt_dict;
  a->value.hd = create_hashtable (16, strhash, strhasheq);

  BREAK;
}

josl_w_sized_dict:             /* ( i:size -- d ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }
  a->type = josl_vt_dict;
  a->value.hd = create_hashtable (a->value.i, strhash, strhasheq);

  BREAK;
}

josl_w_keys:                   /* ( d:dict -- a:keys ) */
{
  josl_value *vdict, *vresult;
  josl_array *keys;
  josl_hash_itr *itr;
  int i;

  UNDERFLOW_CHECK (interp, w, 1);

  vdict = STACK (--SP);

  if (vdict->type != josl_vt_dict)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  keys = tgc_alloc(&gc, sizeof (josl_array));
  keys->size = hashtable_count (vdict->value.hd);
  keys->capacity = keys->size;
  keys->elems = tgc_alloc(&gc, sizeof (char *) * keys->size);

  itr = hashtable_iterator (vdict->value.hd);

  for (i = 0; i < keys->size; i++)
    {
      keys->elems[i] = tgc_alloc(&gc, sizeof (josl_value));
      keys->elems[i]->type = josl_vt_string;
      keys->elems[i]->value.s = hashtable_iterator_key (itr);
      hashtable_iterator_advance (itr);
    }

  vresult = STACK (SP++);
  vresult->type = josl_vt_array;
  vresult->value.a = keys;

  BREAK;
}

josl_w_values:                 /* ( d:dict -- a:values ) */
{
  josl_value *vdict, *vresult;
  josl_array *values;
  josl_hash_itr *itr;
  int i;

  UNDERFLOW_CHECK (interp, w, 1);

  vdict = STACK (--SP);

  if (vdict->type != josl_vt_dict)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  values = tgc_alloc(&gc, sizeof (josl_array));
  values->size = hashtable_count (vdict->value.hd);
  values->capacity = values->size;
  values->elems = tgc_alloc(&gc, sizeof (josl_value *) * values->size);

  itr = hashtable_iterator (vdict->value.hd);

  for (i = 0; i < values->size; i++)
    {
      josl_value *v = hashtable_iterator_value (itr);

      values->elems[i] = tgc_alloc(&gc, sizeof (josl_value));
      values->elems[i]->type = v->type;
      values->elems[i]->value = v->value;
      hashtable_iterator_advance (itr);
    }

  vresult = STACK (SP++);
  vresult->type = josl_vt_array;
  vresult->value.a = values;

  BREAK;
}

josl_w_set:                    /* ( ... v:variable  -- ) */
josl_w_set_keep:
{
  josl_value *var;

  UNDERFLOW_CHECK (interp, w, 1);

  var = STACK (--SP);
  switch (var->type)
    {
    case josl_vt_variable:     /* ( x:value v:variable -- ) */
      {
        josl_value *val;

        UNDERFLOW_CHECK (interp, w, 1);

        if (w->word_id == josl_w_set)
          val = STACK (--SP);
        else
          val = STACK (SP - 1);

        var->value.v->type = val->type;
        var->value.v->value = val->value;
      }
      break;

    case josl_vt_dict:         /* ( x:value s:key v:variable -- ) */
      {
        josl_value *key, *val, *tmp;

        UNDERFLOW_CHECK (interp, w, 2);

        key = STACK (--SP);

        if (w->word_id == josl_w_set)
          val = STACK (--SP);
        else
          val = STACK (SP - 1);

        if (key->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type, "expects a string on NOS");
          }

        tmp = tgc_alloc(&gc, sizeof (josl_value));
        tmp->type = val->type;
        tmp->value = val->value;

        hashtable_remove (var->value.hd, (void *) key->value.s);
        hashtable_insert (var->value.hd, (void *) key->value.s, (void *) tmp);
      }
      break;

    case josl_vt_array:        /* ( x:value i:index v:variable -- ) */
      {
        josl_value *idx, *val;
        int64 i;

        UNDERFLOW_CHECK (interp, w, 2);

        idx = STACK (--SP);

        if (w->word_id == josl_w_set)
          val = STACK (--SP);
        else
          val = STACK (SP - 1);

        if (idx->type != josl_vt_integer)
          {
            THROW_EXC (josl_err_stack_type,
                       "expects an integer index on NOS");
          }

        i = idx->value.i;
        if (i > var->value.a->capacity || i < 0)
          {
            THROW_EXC (josl_err_index_out_of_bounds,
                       "index out of bounds");
          }

        var->value.a->elems[i]->type = val->type;
        var->value.a->elems[i]->value = val->value;

        if (i + 1 > var->value.a->size)
          var->value.a->size = i + 1;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a dict or variable on TOS");
      }
    }

  BREAK;
}

josl_w_get:                    /* ( v:variable -- x ) */
{
  josl_value *var, *vresult;

  UNDERFLOW_CHECK (interp, w, 1);

  var = STACK (--SP);
  switch (var->type)
    {
    case josl_vt_variable:
      vresult = STACK (SP++);
      vresult->type = var->value.v->type;
      vresult->value = var->value.v->value;
      break;

    case josl_vt_pointer:
      {
        unsigned long *peekaddr;
        peekaddr = (unsigned long *) var->value.p;

        vresult = STACK (SP++);
        vresult->type = josl_vt_pointer;
        vresult->value.p = (void *) *peekaddr;
      }
      break;

    case josl_vt_dict:
      {
        josl_value *key, *tmp;

        UNDERFLOW_CHECK (interp, w, 1);

        key = STACK (--SP);
        if (key->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type, "expects a string key on NOS");
          }

        tmp = hashtable_search (var->value.hd, (void *) key->value.s);
        vresult = STACK (SP++);

        if (tmp == NULL)
          {
            NULLVALUE (vresult);
          }
        else
          {
            vresult->type = tmp->type;
            vresult->value = tmp->value;
          }
      }
      break;

    case josl_vt_array:
      {
        josl_value *idx, *tmp;
        int64 i;

        UNDERFLOW_CHECK (interp, w, 1);

        idx = STACK (--SP);
        if (idx->type != josl_vt_integer)
          {
            THROW_EXC (josl_err_stack_type,
                       "expects an integer index on NOS");
          }

        i = idx->value.i;
        if (i > var->value.a->size)
          {
            THROW_EXC (josl_err_index_out_of_bounds, "index out of bounds");
          }

        vresult = STACK (SP++);
        vresult->type = var->value.a->elems[i]->type;
        vresult->value = var->value.a->elems[i]->value;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a dict or variable on TOS");
      }
    }

  BREAK;
}

josl_w_remove:                 /* ( x:value -- ) */
{
  josl_value *val;

  UNDERFLOW_CHECK (interp, w, 1);

  val = STACK (--SP);

  switch (val->type)
    {
    case josl_vt_dict:
      {
        josl_value *key = STACK (--SP);
        if (key->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type, "expects a string key on NOS");
          }

        hashtable_remove (val->value.hd, (void *) key->value.s);
      }
      break;

    case josl_vt_list:
      if (val->value.l->cur != NULL)
        {
          josl_list_item *cur = val->value.l->cur;
          if (cur->prev != NULL)
            cur->prev->next = cur->next;
          if (cur->next != NULL)
            cur->next->prev = cur->prev;
          val->value.l->cur = cur->prev;
          val->value.l->size--;
        }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects a dictionary or list on TOS");
      }
    }

  BREAK;
}

josl_w_has_key:
{
  josl_value *dict, *key, *tmp;

  UNDERFLOW_CHECK (interp, w, 2);

  dict = STACK (--SP);
  key = STACK (SP - 1);

  if (dict->type != josl_vt_dict)
    {
      THROW_EXC (josl_err_stack_type, "expects a dictionary on TOS");
    }

  if (key->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string key on NOS");
    }

  tmp = hashtable_search (dict->value.hd, (void *) key->value.s);
  key->type = josl_vt_integer;
  key->value.i = !(tmp == NULL);

  BREAK;
}

josl_w_copy:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_dict:
      {
        int size;
        char *key;
        josl_hash *newhash;
        josl_hash_itr *itr;

        size = hashtable_count (a->value.hd);
        newhash =
          create_hashtable (size, strhash, strhasheq);

        if (size > 0)
          {
            itr = hashtable_iterator (a->value.hd);
            do
              {
                key = strdup (hashtable_iterator_key (itr));

                hashtable_insert (newhash, (void *) key,
                                  hashtable_iterator_value (itr));
              }
            while (hashtable_iterator_advance (itr));
          }

        a->value.hd = newhash;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a dict on TOS");
      }
    }

  BREAK;
}

josl_w_array:                  /* ( i:capacity -- a ) */
{
  josl_value *vcapacity, *vresult;
  int64 i, capacity;

  UNDERFLOW_CHECK (interp, w, 1);

  vcapacity = STACK (--SP);
  if (vcapacity->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "capacity expects an integer");
    }

  capacity = vcapacity->value.i;

  vresult = STACK (SP++);
  vresult->value.a = tgc_alloc(&gc, sizeof (josl_array));
  vresult->value.a->size = 0;
  vresult->value.a->capacity = capacity;
  vresult->value.a->elems = tgc_alloc(&gc, sizeof (void *) * capacity);
  for (i = 0; i < capacity; i++)
    {
      vresult->value.a->elems[i] = tgc_alloc(&gc, sizeof (josl_value));
      vresult->value.a->elems[i]->type = josl_vt_null;
    }

  vresult->type = josl_vt_array;

  BREAK;
}

josl_w_append_array: /* ( a:array x:value -- a:array ) */
{
  /* Underflow checking already done in josl_w_append */

  josl_value *vary, *vval;

  vval = STACK (--SP);
  vary = STACK (SP - 1);

  if (vary->value.a->size + 1 >= vary->value.a->capacity)
    {
      int idx, newcap;
      josl_value **vals;

      newcap = (vary->value.a->capacity + 1) * 1.5;

      vals = tgc_realloc (&gc, vary->value.a->elems,
                         sizeof (josl_value *) * newcap);

      if (vals == NULL)
        {
          THROW_EXC (josl_err_not_enough_memory, "array resize failed, not enough memory");
        }

      for (idx = vary->value.a->capacity; idx < newcap; idx++)
        vals[idx] = tgc_alloc(&gc, sizeof (josl_value));

      vary->value.a->elems = vals;
      vary->value.a->capacity = newcap;
    }

  vary->value.a->elems[vary->value.a->size]->type = vval->type;
  vary->value.a->elems[vary->value.a->size]->value = vval->value;
  vary->value.a->size++;

  BREAK;
}

josl_w_array_resize: /* ( a:array i:rsize -- ) */
{
  josl_value *vary, *vsize;

  vsize = STACK (--SP);
  vary = STACK (--SP);

  if (vsize->type == josl_vt_double)
    {
      vsize->type = josl_vt_integer;
      vsize->value.i = (int) vsize->value.d;
    }

  if (vsize->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "size should be an integer");
    }
  if (vary->type != josl_vt_array)
    {
      THROW_EXC (josl_err_stack_type, "array must be an array type");
    }

  if (vsize->value.i > vary->value.a->capacity)
    {
      josl_value **vals;
      int idx;

      vals = tgc_realloc (&gc, vary->value.a->elems,
                         sizeof (josl_value *) * vsize->value.i);
      if (vals == NULL)
        {
          THROW_EXC (josl_err_not_enough_memory, "array resize failed, not enough memory");
        }

      for (idx = vary->value.a->capacity; idx < vsize->value.i; idx++)
        vals[idx] = tgc_alloc(&gc, sizeof (josl_value));

      vary->value.a->elems = vals;
      vary->value.a->capacity = vsize->value.i;
    }

  BREAK;
}

josl_w_list:                   /* ( -- l ) */
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);

  a->type = josl_vt_list;
  a->value.l = tgc_alloc(&gc, sizeof (josl_list));
  a->value.l->size = 0;
  a->value.l->head = NULL;
  a->value.l->tail = NULL;
  a->value.l->cur = NULL;

  BREAK;
}

josl_w_push_head:              /* ( x l -- ) */
{
  josl_value *l, *x;
  josl_list_item *li;

  UNDERFLOW_CHECK (interp, w, 2);

  l = STACK (--SP);
  x = STACK (--SP);

  if (l->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  li = tgc_alloc(&gc, sizeof (josl_list_item));
  li->v = tgc_alloc(&gc, sizeof (josl_value));
  li->v->type = x->type;
  li->v->value = x->value;
  li->next = l->value.l->head;
  li->prev = NULL;
  l->value.l->head = li;
  l->value.l->size++;

  if (l->value.l->tail == NULL)
    {
      l->value.l->tail = li;
      l->value.l->cur = li;
    }
  else
    li->next->prev = li;

  BREAK;
}

josl_w_push_tail:              /* ( x l -- ) */
{
  josl_value *l, *x;
  josl_list_item *li;

  UNDERFLOW_CHECK (interp, w, 2);

  l = STACK (--SP);
  x = STACK (--SP);

  if (l->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  li = tgc_alloc(&gc, sizeof (josl_list_item));
  li->v = tgc_alloc(&gc, sizeof (josl_value));
  li->v->type = x->type;
  li->v->value = x->value;
  li->next = NULL;
  li->prev = l->value.l->tail;
  l->value.l->tail = li;
  l->value.l->size++;

  if (l->value.l->head == NULL)
    {
      l->value.l->head = li;
      l->value.l->cur = li;
    }
  else
    li->prev->next = li;

  BREAK;
}

josl_w_current:                /* ( l -- x ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  if (a->value.l->cur == NULL)
    a->type = josl_vt_null;
  else
    {
      a->type = a->value.l->cur->v->type;
      a->value = a->value.l->cur->v->value;
    }

  BREAK;
}

josl_w_forward:                /* ( l -- x  ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      josl_throw_exception (interp, w, josl_err_stack_type,
                            "expects a list on TOS");
      return;
    }

  if (a->value.l->cur == NULL)
    a->value.l->cur = a->value.l->head;
  else
    a->value.l->cur = a->value.l->cur->next;

  if (a->value.l->cur == NULL)
    {
      a->type = josl_vt_null;
      a->value.i = 0;
    }
  else
    {
      a->type = a->value.l->cur->v->type;
      a->value = a->value.l->cur->v->value;
    }

  BREAK;
}

josl_w_backward:               /* ( l -- x  ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  if (a->value.l->cur == NULL)
    a->value.l->cur = a->value.l->tail;
  else
    a->value.l->cur = a->value.l->cur->prev;

  if (a->value.l->cur == NULL)
    {
      a->type = josl_vt_null;
      a->value.i = 0;
    }
  else
    {
      a->type = a->value.l->cur->v->type;
      a->value = a->value.l->cur->v->value;
    }

  BREAK;
}

josl_w_head:                   /* ( l -- x ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  a->value.l->cur = a->value.l->head;
  a->type = a->value.l->head->v->type;
  a->value = a->value.l->head->v->value;

  BREAK;
}

josl_w_tail:                   /* ( l -- x ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  a->value.l->cur = a->value.l->tail;
  a->type = a->value.l->tail->v->type;
  a->value = a->value.l->tail->v->value;

  BREAK;
}

josl_w_go_tail:                /* ( l -- ) */
josl_w_go_head:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  a->value.l->cur = NULL;

  BREAK;
}

josl_w_pop_head:               /* ( l -- ) */
{
  josl_value *a;
  josl_list *lst;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  lst = a->value.l;
  if (lst->head == NULL)
    {
      a->value.l = NULL;
      a->type = josl_vt_null;
    }
  else
    {
      a->type = lst->head->v->type;
      a->value = lst->head->v->value;
      lst->head = lst->head->next;
      lst->size--;
    }

  BREAK;
}

josl_w_pop_tail:               /* ( l -- ) */
{
  josl_value *a;
  josl_list *lst;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_list)
    {
      THROW_EXC (josl_err_stack_type, "expects a list on TOS");
    }

  lst = a->value.l;
  if (lst->tail == NULL)
    {
      a->value.l = NULL;
      a->type = josl_vt_null;
    }
  else
    {
      a->type = lst->tail->v->type;
      a->value = lst->tail->v->value;
      lst->tail = lst->tail->prev;
      lst->size--;

	  if (lst->tail == NULL) {
		  lst->head = NULL;
	  }
    }

  BREAK;
}

josl_w_sbuf:                   /* ( -- b ) */
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);
  a->type = josl_vt_sbuffer;
  a->value.b = cbuf_alloc (128);

  BREAK;
}

josl_w_sized_sbuf:             /* ( i -- b ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  a->type = josl_vt_sbuffer;
  a->value.b = cbuf_alloc (a->value.i);

  BREAK;
}

josl_w_capacity: /* ( a -- i ) */
{
  josl_value *vary, *vresult;

  UNDERFLOW_CHECK (interp, w, 1);

  vary = STACK (--SP);
  switch (vary->type)
    {
    case josl_vt_array:
      vresult = STACK(SP++);
      vresult->type = josl_vt_integer;
      vresult->value.i = vary->value.a->capacity;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expected an array");
      }
    }

  BREAK;
}

josl_w_sort: /* ( collection -- sorted-collection ) */
{
  josl_value *vcol;

  UNDERFLOW_CHECK (interp, w, 1);

  vcol = STACK (SP - 1);

  switch (vcol->type)
    {
    case josl_vt_array:
      {
        josl_qsort (vcol->value.a->elems, vcol->value.a->size, NULL);
      }
      break;

    case josl_vt_list:
      {
        josl_list_item *head = vcol->value.l->head;

        head = josl_list_sort (head, 0, 1, NULL);

        vcol->value.l->head = head;

        /* TODO patch up tail */
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expected an array or list on TOS");
      }
    }

  BREAK;
}

josl_w_sort_user: /* ( collection w:word -- sorted-collection ) */
{
  josl_value *vword, *vcol, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);

  vword = STACK (--SP);
  vcol = STACK (--SP);

  if (vword->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type,
                 "expected a word reference on TOS");
    }

  switch (vcol->type)
    {
    case josl_vt_array:
      {
        josl_array *ary;
        josl_user_sort us;
        us.interp = interp;
        us.topword = vword->value.wref->word;

        ary = vcol->value.a;

        josl_qsort (vcol->value.a->elems, vcol->value.a->size, &us);

        vresult = STACK (SP++);
        vresult->type = josl_vt_array;
        vresult->value.a = ary;
      }
      break;

    case josl_vt_list:
      {
        josl_list *l = vcol->value.l;
        josl_list_item *head = l->head;
        josl_user_sort us;
        us.interp = interp;
        us.topword = vword->value.wref->word;

        l->head = josl_list_sort (head, 0, 1, &us);

        vresult = STACK (SP++);
        vresult->type = josl_vt_list;
        vresult->value.l = l;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expected an array or list on NOS");
      }
    }

  BREAK;
}
