/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

Regular Expressions

*/

#if HAVE_PCRE == 1
josl_w_re_compile:             /* ( s:str-expression -- r:expression ) */
{
  josl_value *vstrexp;
  const char *errmsg = NULL;
  int erroffset = 0;

  UNDERFLOW_CHECK (interp, w, 1);

  vstrexp = STACK (SP - 1);

  if (vstrexp->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  vstrexp->type = josl_vt_regex;
  vstrexp->value.regex =
    pcre_compile (vstrexp->value.s, 0, &errmsg, &erroffset, NULL);

  if (vstrexp->value.regex == NULL)
    {
      vstrexp->type = josl_vt_string;
      vstrexp->value.s = strdup (errmsg);
      vstrexp = STACK (SP++);
      vstrexp->type = josl_vt_integer;
      vstrexp->value.i = 0;
    }

  BREAK;
}

josl_w_regexp:                 /* ( s:haystack r:expression -- i:match-count ) */
{
  josl_value *vre, *vsubj, *tmp;
  int rc;
  int ovector[30];

  UNDERFLOW_CHECK (interp, w, 2);

  vre = STACK (--SP);
  vsubj = STACK (--SP);

  if (vre->type != josl_vt_regex)
    {
      THROW_EXC (josl_err_stack_type, "expects a regular expression on TOS");
    }

  if (vsubj->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  rc = pcre_exec (vre->value.regex, NULL, vsubj->value.s,
                  strlen (vsubj->value.s), 0, 0, ovector, 30);

  if (rc > 0)
    {
      int idx, veccount;
      veccount = rc * 2;

      OVERFLOW_CHECK (interp, w, veccount + 1);

      for (idx = veccount - 1; idx >= 0; idx--)
        {
          tmp = STACK (SP++);
          tmp->type = josl_vt_integer;
          tmp->value.i = ovector[idx];
        }

      tmp = STACK (SP++);
      tmp->type = josl_vt_integer;
      tmp->value.i = rc;
    }
  else
    {
      tmp = STACK (SP++);
      tmp->type = josl_vt_integer;
      tmp->value.i = rc == -1 ? 0 : rc; /* 0 matches or negative error msg */
    }

  BREAK;
}

josl_w_regsub:                 /* ( s:haystack s:replacement i:limit i:from r:expression -- s:result ) */
{
  josl_value *vhay, *vrepl, *vlimit, *vfrom, *vexp, *vresult;
  char *haystack, *out = 0;
  int slen, rc, ovector[30], idx, st, n, sidx, eidx;
  charbuf *cb;

  UNDERFLOW_CHECK (interp, w, 5);

  vexp = STACK (--SP);
  vfrom = STACK (--SP);
  vlimit = STACK (--SP);
  vrepl = STACK (--SP);
  vhay = STACK (SP - 1);
  vresult = vhay;

  if (vexp->type != josl_vt_regex)
    {
      THROW_EXC (josl_err_stack_type, "expects a regular expression on TOS");
    }
  if (vfrom->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }
  if (vlimit->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on 3rd from TOS");
    }
  if (vrepl->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on 4th from TOS");
    }
  if (vhay->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on 5th from TOS");
    }

  sidx = 0;
  eidx = 0;
  haystack = strdup (vhay->value.s);
  slen = strlen (haystack);
  cb = cbuf_alloc (slen);       /* starting point */

  for (;;)
    {
      rc = pcre_exec (vexp->value.regex, NULL, vhay->value.s, slen,
                      vfrom->value.i, 0, ovector, 30);
      if (rc <= 0 || vlimit->value.i == 0)
        break;

      if (ovector[0] > eidx)
        {
          cbuf_append_str (cb, vhay->value.s + eidx, ovector[0] - eidx);
        }

      for (idx = 0; idx < strlen (vrepl->value.s); idx++)
        {
          if (vrepl->value.s[idx] == '\\')
            {
              int n = abs (48 - vrepl->value.s[idx + 1]);
              if (n >= 0 && n <= 9)
                {
                  idx++;
                  n += n;

                  cbuf_append_str (cb, vhay->value.s + ovector[n],
                                   ovector[n + 1] - ovector[n]);
                }
            }
          else
            {
              cbuf_append_ch (cb, vrepl->value.s[idx]);
            }
        }

      vfrom->value.i = ovector[1];
      vlimit->value.i--;

      eidx = ovector[1];
    }

  if (eidx < slen)
    cbuf_append_str (cb, vhay->value.s + eidx, slen - eidx);

  vresult->type = josl_vt_string;
  vresult->value.s = cb->data;

  BREAK;
}

josl_w_regextract:             /* ( s:haystack i:limit i:from r:expression -- (?s...) i:count ) */
{
  josl_value *vhay, *vlimit, *vfrom, *vexp, *vresult;
  char *haystack;
  int ovector[30], limit, from, haylen, rc, count;
  pcre *re;

  UNDERFLOW_CHECK (interp, w, 4);

  vexp = STACK (--SP);
  vfrom = STACK (--SP);
  vlimit = STACK (--SP);
  vhay = STACK (--SP);

  if (vexp->type != josl_vt_regex)
    {
      THROW_EXC (josl_err_stack_type, "expects a regular expression on TOS");
    }
  if (vfrom->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }
  if (vlimit->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on 3rd from TOS");
    }
  if (vhay->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on 4th from TOS");
    }

  count = 0;
  re = vexp->value.regex;
  haystack = strdup (vhay->value.s);
  haylen = strlen (vhay->value.s);
  from = vfrom->value.i;
  limit = vlimit->value.i;

  for (;;)
    {
      rc = pcre_exec (re, NULL, haystack, haylen, from, 0, ovector, 30);

      if (rc <= 0 || vlimit->value.i == 0)
        break;

      OVERFLOW_CHECK (interp, w, 1);

      vresult = STACK (SP++);
      vresult->type = josl_vt_string;
      vresult->value.s =
        strndup (haystack + ovector[0], ovector[1] - ovector[0]);

      from = ovector[1];

      count++;
      limit--;

      if (limit == 0)
        break;
    }

  OVERFLOW_CHECK (interp, w, 1);

  vresult = STACK (SP++);
  vresult->type = josl_vt_integer;
  vresult->value.i = count;

  BREAK;
}

#else /* HAVE_PCRE == 1 */

josl_w_re_compile:
josl_w_regexp:
josl_w_regsub:
josl_w_regextract:
{
  THROW_EXC (josl_err_regex_not_supported,
             "Regular expressions not support in this compilation of Josl");
}

#endif /* HAVE_PCRE == 1 */
