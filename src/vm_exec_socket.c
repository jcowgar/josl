/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Socket Words

*/

/*
 * General Socket Words
 */

josl_w_sock_socket:            /* ( i:protocol i:type i:domain -- p:socket ) */
{
  josl_value *vresult, *vproto, *vtype, *vdomain;
  SOCKET sock;

  UNDERFLOW_CHECK (interp, w, 3);

  vdomain = STACK (--SP);
  vtype = STACK (--SP);
  vproto = STACK (--SP);

  if (vdomain->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }
  if (vtype->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }
  if (vproto->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer third from TOS");
    }

  josl_sock_init (interp);

  sock = socket (vdomain->value.i, vtype->value.i, vproto->value.i);
  if (sock == INVALID_SOCKET)
    {
      THROW_SOCK_EXC ();
    }
  else
    {
      vresult = STACK (SP++);
      vresult->type = josl_vt_socket;
      vresult->value.sock = tgc_alloc(&gc, sizeof (josl_socket));
      vresult->value.sock->sock = sock;
      vresult->value.sock->addr = tgc_alloc(&gc, sizeof (struct sockaddr_in));
      vresult->value.sock->addr->sin_family = vdomain->value.i;
      vresult->value.sock->addr->sin_port = 0;
    }

  BREAK;
}

josl_w_close_socket:           /* ( sk:socket -- ) */
{
  josl_value *vsock;

  vsock = STACK (--SP);

  if (closesocket (vsock->value.sock->sock) != 0)
    {
      THROW_SOCK_EXC ();
    }

  BREAK;
}

/*
 * Server Based Words
 */

josl_w_sock_bind:              /* ( i:port is:address sk:socket -- ) */
{
  josl_value *vport, *vaddr, *vsock;

  UNDERFLOW_CHECK (interp, w, 3);

  vsock = STACK (--SP);
  vaddr = STACK (--SP);
  vport = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }
  if (vport->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer third from TOS");
    }
  switch (vaddr->type)
    {
    case josl_vt_string:
      vsock->value.sock->addr->sin_addr.s_addr = inet_addr (vaddr->value.s);
      break;

    case josl_vt_integer:
      vsock->value.sock->addr->sin_addr.s_addr = htonl (INADDR_ANY);
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects an integer or string on NOS");
      }
    }

  vsock->value.sock->addr->sin_port = htons (vport->value.i);

  if (bind (vsock->value.sock->sock,
            (struct sockaddr *) vsock->value.sock->addr,
            sizeof (SOCKADDR)) == SOCKET_ERROR)
    {
      THROW_SOCK_EXC ();
    }

  BREAK;
}

josl_w_sock_listen:            /* ( i:backlog sk:socket -- ) */
{
  josl_value *vback, *vsock;
  int result;

  UNDERFLOW_CHECK (interp, w, 2);

  vsock = STACK (--SP);
  vback = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }
  if (vback->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  if (listen (vsock->value.sock->sock, vback->value.i) == SOCKET_ERROR)
    {
      THROW_SOCK_EXC ();
    }

  BREAK;
}

josl_w_sock_accept:            /* ( sk:socket -- sk:client ) */
{
  SOCKET client;
  struct sockaddr_in addr;
  int addrLen;
  josl_value *vsock;

  UNDERFLOW_CHECK (interp, w, 1);

  vsock = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }

  addrLen = sizeof (addr);
  client =
    accept (vsock->value.sock->sock, (struct sockaddr *) &addr, &addrLen);

  if (client == INVALID_SOCKET)
    {
      THROW_SOCK_EXC ();
    }
  else
    {
      vsock = STACK (SP++);
      vsock->value.sock = tgc_alloc(&gc, sizeof (josl_socket));
      vsock->value.sock->sock = client;
    }

  BREAK;
}

/*
 * General Words
 */

josl_w_sock_select:
/* ( errd writable readable i:timeout -- a:errd-status a:writable-status a:readable-status ) */
{
  SOCKET tmpsock;
  josl_value *vtimeout, *vreadable, *vwritable, *verrd;
  int rcount, wcount, ecount, idx, maxsock, result;
  fd_set readable, writable, errd;
  struct timeval tv_timeout;

  UNDERFLOW_CHECK (interp, w, 4);

  vtimeout = STACK (--SP);
  vreadable = STACK (SP - 1);
  vwritable = STACK (SP - 2);
  verrd = STACK (SP - 3);
  maxsock = 0;

  FD_ZERO (&readable);
  FD_ZERO (&writable);
  FD_ZERO (&errd);

  if (vtimeout->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "timeout must be an integer");
    }

  tv_timeout.tv_sec = 0;
  tv_timeout.tv_usec = vtimeout->value.i;

  switch (vreadable->type)
    {
    case josl_vt_null:
      rcount = 0;
      break;

    case josl_vt_socket:
      rcount = 1;
      tmpsock = vreadable->value.sock->sock;
      FD_SET (tmpsock, &readable);
      maxsock = (maxsock > tmpsock) ? maxsock : tmpsock;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "readable must be an array, list or socket");
      }
    }

  switch (vwritable->type)
    {
    case josl_vt_null:
      wcount = 0;
      break;

    case josl_vt_socket:
      wcount = 1;
      tmpsock = vwritable->value.sock->sock;
      FD_SET (tmpsock, &writable);
      maxsock = (maxsock > tmpsock) ? maxsock : tmpsock;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "writable must be an array, list or socket");
      }
    }

  switch (verrd->type)
    {
    case josl_vt_null:
      ecount = 0;
      break;

    case josl_vt_socket:
      ecount = 1;
      tmpsock = vwritable->value.sock->sock;
      FD_SET (tmpsock, &errd);
      maxsock = (maxsock > tmpsock) ? maxsock : tmpsock;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "errd must be an array, list or socket");
      }
    }

  result = select (maxsock + 1, &readable, &writable, &errd, &tv_timeout);

  if (result == -1)
    {
      THROW_SOCK_EXC ();
    }

  switch (verrd->type)
    {
    case josl_vt_null:
      break;

    case josl_vt_socket:
      tmpsock = verrd->value.sock->sock;
      verrd->type = josl_vt_integer;
      verrd->value.i = FD_ISSET (tmpsock, &errd) != 0;
      break;

    default:
      /* validation was already done, this is here
         to prevent compiler warnings */
      break;
    }

  switch (vwritable->type)
    {
    case josl_vt_null:
      break;

    case josl_vt_socket:
      tmpsock = vwritable->value.sock->sock;
      vwritable->type = josl_vt_integer;
      vwritable->value.i = FD_ISSET (tmpsock, &writable) != 0;
      break;

    default:
      /* validation was already done, this is here
         to prevent compiler warnings */
      break;
    }

  switch (vreadable->type)
    {
    case josl_vt_null:
      break;

    case josl_vt_socket:
      tmpsock = vreadable->value.sock->sock;
      vreadable->type = josl_vt_integer;
      vreadable->value.i = FD_ISSET (tmpsock, &readable) != 0;
      break;

    default:
      /* validation was already done, this is here
         to prevent compiler warnings */
      break;
    }

  BREAK;
}

josl_w_write_socket:           /* ( s:content i:flags sk:socket -- ) */
{
  josl_value *vcontent, *vflags, *vsock;
  char *buf;

  UNDERFLOW_CHECK (interp, w, 3);

  vsock = STACK (--SP);
  vflags = STACK (--SP);
  vcontent = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }
  if (vflags->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  switch (vcontent->type)
    {
    case josl_vt_integer:
      buf = tgc_alloc(&gc, sizeof (char) * 256);
      snprintf (buf, 256, "%lld", vcontent->value.i);
      break;

    case josl_vt_double:
      buf = tgc_alloc(&gc, sizeof (char) * 256);
      if (vcontent->value.d > 0.000009)
        snprintf (buf, 256, "%.6f", vcontent->value.d);
      else
        snprintf (buf, 256, "%e", vcontent->value.d);
      break;

    case josl_vt_string:
      buf = vcontent->value.s;
      break;

    case josl_vt_sbuffer:
      buf = vcontent->value.b->data;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "content must be an integer, double, string or buffer");
      }
    }

  if (send (vsock->value.sock->sock, vcontent->value.s,
            strlen (vcontent->value.s), vflags->value.i) == SOCKET_ERROR)
    {
      THROW_SOCK_EXC ();
    }

josl_w_sock_write_done:
  BREAK;
}

josl_w_read_str_socket:        /* ( i:buffer-size sk:sock -- s:content ) */
{
  josl_value *vflags;

  OVERFLOW_CHECK (interp, w, 1);

  vflags = STACK (SP++);
  vflags->type = josl_vt_integer;
  vflags->value.i = 0;

  /* FALL-THRU */
}

josl_w_read_str_socket_opts:   /* ( i:buffer-size sk:sock i:flags -- s:content ) */
{
  josl_value *vflags, *vsock, *vbuffsize, *vcontent;

  char *buf;
  int result;

  UNDERFLOW_CHECK (interp, w, 3);

  vflags = STACK (--SP);
  vsock = STACK (--SP);
  vbuffsize = STACK (--SP);

  if (vbuffsize->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "buffer size must be an integer");
    }

  buf = tgc_alloc(&gc, sizeof (char) * vbuffsize->value.i);

  result =
    recv (vsock->value.sock->sock, buf, vbuffsize->value.i, vflags->value.i);

  if (result > 0)
    {
      vcontent = STACK (SP++);
      vcontent->type = josl_vt_string;
      vcontent->value.s = buf;
    }
  else if (result == 0)
    {
      vcontent = STACK (SP++);
      NULLVALUE (vcontent);
    }
  else
    {
      THROW_SOCK_EXC ();
    }

  BREAK;
}

/*
 * Client Words
 */

josl_w_sock_connect:           /* ( i:port s:address sk:socket -- ) */
{
  josl_value *vport, *vaddr, *vsock;

  UNDERFLOW_CHECK (interp, w, 3);

  vsock = STACK (--SP);
  vaddr = STACK (--SP);
  vport = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }
  if (vaddr->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }
  if (vport->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer third from TOS");
    }

  vsock->value.sock->addr->sin_addr.s_addr = inet_addr (vaddr->value.s);
  vsock->value.sock->addr->sin_port = htons (vport->value.i);

  if (connect (vsock->value.sock->sock,
               (struct sockaddr *) vsock->value.sock->addr,
               sizeof (SOCKADDR)) == SOCKET_ERROR)
    {
      THROW_SOCK_EXC ();
    }

  BREAK;
}

/*
 * UDP Words
 */

josl_w_sock_send_to:           /* ( i:flags i:port s:address s:content sk:socket -- ) */
{
  josl_value *vflags, *vport, *vaddr, *vcontent, *vsock;
  struct sockaddr_in addr;
  int rc;

  vsock = STACK (--SP);
  vcontent = STACK (--SP);
  vaddr = STACK (--SP);
  vport = STACK (--SP);
  vflags = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }
  if (vcontent->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }
  if (vaddr->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string 2nd from TOS");
    }
  if (vport->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer 3rd from TOS");
    }
  if (vflags->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer 4th from TOS");
    }

  addr.sin_family = AF_INET;
  addr.sin_port = htons (vport->value.i);
  addr.sin_addr.s_addr = inet_addr (vaddr->value.s);

  if (sendto (vsock->value.sock->sock, vcontent->value.s,
              strlen (vcontent->value.s), vflags->value.i,
              (SOCKADDR *) & addr, sizeof (addr)) == SOCKET_ERROR)
    {
      NULLVALUE (vcontent);

      THROW_SOCK_EXC ();
    }
  else
    NULLVALUE (vcontent);

  BREAK;
}

josl_w_sock_receive_from:      /* ( i:buff-size sk:socket ?i:flags -- s:content ) */
{
  josl_value *vtmp, *vsock, *vbuffsize, *vresult;
  char *buff;
  struct sockaddr_in addr;
  int flags, addrSize, rc;

  UNDERFLOW_CHECK (interp, w, 2);

  vtmp = STACK (--SP);
  if (vtmp->type == josl_vt_integer)
    {
      flags = vtmp->value.i;
      vsock = STACK (--SP);
    }
  else
    {
      flags = 0;
      vsock = vtmp;
    }

  vbuffsize = STACK (--SP);

  if (vsock->type != josl_vt_socket)
    {
      THROW_EXC (josl_err_stack_type, "expects a socket on TOS");
    }

  if (vbuffsize->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "buffer size should be an integer");
    }


  buff = tgc_alloc(&gc, sizeof (char) * vbuffsize->value.i);
  addrSize = sizeof (addr);

  if (recvfrom (vsock->value.sock->sock, buff, vbuffsize->value.i,
                flags, (SOCKADDR *) & addr, &addrSize) == SOCKET_ERROR)
    {
      THROW_SOCK_EXC ();
    }
  else
    {
      vresult = STACK (SP++);
      vresult->type = josl_vt_string;
      vresult->value.s = buff;
    }

  BREAK;
}

/*
 * Information Words
 */

josl_w_sock_serv_by_name:      /* ( s:name -- i:port ) */
{
  josl_value *vname, *vresult;
  struct servent *ent;

  UNDERFLOW_CHECK (interp, w, 1);

  vname = STACK (--SP);

  if (vname->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string  on TOS");
    }

  josl_sock_init (interp);
  ent = getservbyname (vname->value.s, 0);

  vresult = STACK (SP++);
  vresult->type = josl_vt_integer;
  vresult->value.i = (ent == NULL) ? -1 : htons (ent->s_port);

  BREAK;
}

josl_w_sock_serv_by_port:      /* ( i:port -- s:name ) */
{
  josl_value *vport, *vresult;
  struct servent *ent;

  UNDERFLOW_CHECK (interp, w, 1);

  vport = STACK (--SP);

  if (vport->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer  on TOS");
    }

  josl_sock_init (interp);
  ent = getservbyport (ntohs (vport->value.i), 0);

  vresult = STACK (SP++);

  if (ent == NULL)
    {
      NULLVALUE (vresult);
    }
  else
    {
      vresult->type = josl_vt_string;
      vresult->value.s = ent->s_name;
    }

  BREAK;
}

josl_w_sock_host_by_name:      /* ( s:name -- (?s:addr ...) i:count ) */
{
  josl_value *vname, *vresult;
  struct hostent *ent;
  struct in_addr addr;
  char **addrs;
  int count = 0;

  UNDERFLOW_CHECK (interp, w, 1);

  vname = STACK (--SP);

  if (vname->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  josl_sock_init (interp);
  ent = gethostbyname (vname->value.s);
  if (ent == NULL)
    {
      vresult = STACK (SP++);
      vresult->type = josl_vt_integer;
      vresult->value.i = 0;
    }
  else
    {
      addrs = ent->h_addr_list;
      while (*addrs != 0)
        {
          addr.s_addr = *(unsigned long *) *addrs;

          OVERFLOW_CHECK (interp, w, 1);

          vresult = STACK (SP++);
          vresult->type = josl_vt_string;
          vresult->value.s = strdup (inet_ntoa (addr));

          count++;
          addrs++;
        }

      OVERFLOW_CHECK (interp, w, 1);

      vresult = STACK (SP++);
      vresult->type = josl_vt_integer;
      vresult->value.i = count;
    }

  BREAK;
}

josl_w_sock_host_by_addr:      /* ( s:addr -- (?s:name ...) i:count ) */
{
  josl_value *vaddr, *vresult;
  struct hostent *ent;
  struct in_addr addr;
  char **aliases;
  int count = 0;

  UNDERFLOW_CHECK (interp, w, 1);

  vaddr = STACK (--SP);

  if (vaddr->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  josl_sock_init (interp);
  addr.s_addr = inet_addr (vaddr->value.s);
  if (addr.s_addr == INADDR_NONE)
    {
      OVERFLOW_CHECK (interp, w, 1);

      vresult = STACK (SP++);
      vresult->type = josl_vt_integer;
      vresult->value.i = -1;
    }
  else
    {
      ent = gethostbyaddr ((char *) &addr, 4, AF_INET);
      if (ent == NULL)
        {
          vresult = STACK (SP++);
          vresult->type = josl_vt_integer;
          vresult->value.i = 0;
        }
      else
        {
          aliases = ent->h_aliases;
          while (*aliases != 0)
            {
              OVERFLOW_CHECK (interp, w, 1);

              vresult = STACK (SP++);
              vresult->type = josl_vt_string;
              vresult->value.s = strdup (*aliases);

              count++;
              aliases++;
            }

          OVERFLOW_CHECK (interp, w, 1);

          vresult = STACK (SP++);
          vresult->type = josl_vt_string;
          vresult->value.s = strdup (ent->h_name);

          count++;

          OVERFLOW_CHECK (interp, w, 1);

          vresult = STACK (SP++);
          vresult->type = josl_vt_integer;
          vresult->value.i = count;
        }
    }

  BREAK;
}

josl_w_eof_socket:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_read_ch_socket:         /* ( i:options sk:sock -- i:char ) */
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_read_line_socket:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_read_blob_socket:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}
