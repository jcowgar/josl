/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  stack words

*/

josl_w_depth:
{
  OVERFLOW_CHECK (interp, w, 1);

  STACK (SP)->type = josl_vt_integer;
  STACK (SP)->value.i = SP;

  SP++;

  BREAK;
}

josl_w_dup:
{
  OVERFLOW_CHECK (interp, w, 1);

  STACK (SP)->type = STACK (SP - 1)->type;
  STACK (SP)->value = STACK (SP - 1)->value;

  SP++;

  BREAK;
}

josl_w_2dup:
{
  OVERFLOW_CHECK (interp, w, 2);
  UNDERFLOW_CHECK (interp, w, 2);

  STACK (SP)->type = STACK (SP - 2)->type;
  STACK (SP++)->value = STACK (SP - 2)->value;
  STACK (SP)->type = STACK (SP - 2)->type;
  STACK (SP++)->value = STACK (SP - 2)->value;

  BREAK;
}

josl_w_3dup:
{
  OVERFLOW_CHECK (interp, w, 3);
  UNDERFLOW_CHECK (interp, w, 3);

  STACK (SP)->type = STACK (SP - 3)->type;
  STACK (SP++)->value = STACK (SP - 3)->value;
  STACK (SP)->type = STACK (SP - 3)->type;
  STACK (SP++)->value = STACK (SP - 3)->value;
  STACK (SP)->type = STACK (SP - 3)->type;
  STACK (SP++)->value = STACK (SP - 3)->value;

  BREAK;
}

josl_w_dup_pound:
{
  josl_value *a;
  int count, total;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  total = a->value.i;

  UNDERFLOW_CHECK (interp, w, total);
  OVERFLOW_CHECK (interp, w, total);

  for (count = 0; count < total; count++)
    {
      STACK (SP)->type = STACK (SP - total)->type;
      STACK (SP)->value = STACK (SP - total)->value;
      SP++;
    }

  BREAK;
}


josl_w_drop:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  NULLVALUE (a);

  BREAK;
}

josl_w_2drop:
{
  josl_value *a;
  UNDERFLOW_CHECK (interp, w, 2);

  a = STACK (--SP);
  NULLVALUE (a);

  a = STACK (--SP);
  NULLVALUE (a);

  BREAK;
}


josl_w_3drop:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 3);

  a = STACK (--SP);
  NULLVALUE (a);

  a = STACK (--SP);
  NULLVALUE (a);

  a = STACK (--SP);
  NULLVALUE (a);

  BREAK;
}

josl_w_drop_pound:             /* ( ... i -- ) */
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  UNDERFLOW_CHECK (interp, w, a->value.i);

  for (; a->value.i > 0; a->value.i--)
    {
      b = STACK (--SP);
      NULLVALUE (b);
    }

  BREAK;
}


josl_w_swap:
{
  UNDERFLOW_CHECK (interp, w, 2);

  SWAP (SP - 1, SP - 2);

  BREAK;
}

josl_w_2swap:                  /* ( x1 x2 x3 x4 -- x3 x4 x1 x2 ) */
{
  UNDERFLOW_CHECK (interp, w, 4);

  SWAP (SP - 1, SP - 3);
  SWAP (SP - 2, SP - 4);

  BREAK;
}

josl_w_3swap:                  /* ( x1 x2 x3 x4 x5 x6 -- x4 x5 x6 x1 x2 x3 ) */
{
  UNDERFLOW_CHECK (interp, w, 6);

  SWAP (SP - 1, SP - 4);
  SWAP (SP - 2, SP - 5);
  SWAP (SP - 3, SP - 6);

  BREAK;
}

josl_w_swap_pound:             /* ( ... i -- ... ) */
{
  josl_value *a;
  int count, i;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  count = a->value.i;

  UNDERFLOW_CHECK (interp, w, count);

  for (i = 0; i < count; i++)
    {
      SWAP (SP - i - 1, SP - i - count - 1);
    }

  BREAK;
}

josl_w_over:
{
  OVERFLOW_CHECK (interp, w, 1);
  UNDERFLOW_CHECK (interp, w, 2);

  COPY (SP - 2, SP);
  SP++;

  BREAK;
}

josl_w_2over:
{
  OVERFLOW_CHECK (interp, w, 2);
  UNDERFLOW_CHECK (interp, w, 4);

  COPY (SP - 4, SP);
  SP++;

  COPY (SP - 4, SP);
  SP++;

  BREAK;
}

josl_w_3over:
{
  OVERFLOW_CHECK (interp, w, 3);
  UNDERFLOW_CHECK (interp, w, 6);

  COPY (SP - 6, SP);
  SP++;

  COPY (SP - 6, SP);
  SP++;

  COPY (SP - 6, SP);
  SP++;

  BREAK;
}

josl_w_rot:                    /* ( x1 x2 x3 -- x2 x3 x1 ) */
{
  UNDERFLOW_CHECK (interp, w, 3);

  SWAP (SP - 3, SP - 1);
  SWAP (SP - 2, SP - 3);

  BREAK;
}

josl_w_minus_rot:              /* ( x1 x2 x3 -- x3 x1 x2 ) */
{
  UNDERFLOW_CHECK (interp, w, 3);

  SWAP (SP - 2, SP - 3);
  SWAP (SP - 3, SP - 1);

  BREAK;
}

josl_w_2rot:                   /* ( x1 x2 x3 x4 x5 x6 -- x3 x4 x5 x6 x1 x2 ) */
{
  UNDERFLOW_CHECK (interp, w, 6);

  SWAP (SP - 1, SP - 3);
  SWAP (SP - 2, SP - 4);
  SWAP (SP - 1, SP - 5);
  SWAP (SP - 2, SP - 6);

  BREAK;
}

josl_w_minus_2rot:
{
  UNDERFLOW_CHECK (interp, w, 6);

  SWAP (SP - 2, SP - 6);
  SWAP (SP - 1, SP - 5);
  SWAP (SP - 2, SP - 4);
  SWAP (SP - 1, SP - 3);

  BREAK;
}

josl_w_3rot:                   /* ( x1 x2 x3 x4 x5 x6 x7 x8 x9 -- x4 x5 x6 x7 x8 x9 x1 x2 x3 ) */
{
  UNDERFLOW_CHECK (interp, w, 9);

  SWAP (SP - 1, SP - 4);
  SWAP (SP - 2, SP - 5);
  SWAP (SP - 3, SP - 6);
  SWAP (SP - 1, SP - 7);
  SWAP (SP - 2, SP - 8);
  SWAP (SP - 3, SP - 9);

  BREAK;
}

josl_w_minus_3rot:             /* ( x1 x2 x3 x4 x5 x6 x7 x8 x9 -- x7 x8 x9 x1 x2 x3 x4 x5 x6 ) */
{
  UNDERFLOW_CHECK (interp, w, 9);

  SWAP (SP - 3, SP - 9);
  SWAP (SP - 2, SP - 8);
  SWAP (SP - 1, SP - 7);
  SWAP (SP - 3, SP - 6);
  SWAP (SP - 2, SP - 5);
  SWAP (SP - 1, SP - 4);

  BREAK;
}

josl_w_roll:                   /* ( ... i -- ... ) */
{
  josl_value *a;
  int i, count;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  count = a->value.i;

  a->value = STACK (SP - count - 1)->value;     /* bottom */
  a->type = STACK (SP - count - 1)->type;

  for (i = count; i > 0; i--)
    SWAP (SP - i - 1, SP - i);

  STACK (SP - 1)->type = a->type;
  STACK (SP - 1)->value = a->value;

  BREAK;
}

josl_w_minus_roll:
{
  josl_value *a;
  int i, count;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  count = a->value.i;

  a->value = STACK (SP - 1)->value;     /* top */
  a->type = STACK (SP - 1)->type;

  for (i = 1; i <= count; i++)
    SWAP (SP - i, SP - i - 1);

  STACK (SP - count - 1)->type = a->type;
  STACK (SP - count - 1)->value = a->value;

  BREAK;
}

josl_w_pick:                   /* ( ... i -- ... ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "an integer expected on TOS");
    }

  UNDERFLOW_CHECK (interp, w, a->value.i + 1);
  OVERFLOW_CHECK (interp, w, a->value.i + 1);

  COPY (SP - a->value.i - 1, SP);
  SP++;

  BREAK;
}

josl_w_nip:                    /* ( x1 x2 -- x2 ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 2);

  MOVE (SP - 1, SP - 2);
  a = STACK (--SP);
  NULLVALUE (a);

  BREAK;
}

josl_w_tuck:                   /* ( x1 x2 -- x2 x1 x2 ) */
{
  UNDERFLOW_CHECK (interp, w, 2);
  OVERFLOW_CHECK (interp, w, 1);

  SWAP (SP - 1, SP - 2);
  COPY (SP - 2, SP);
  SP++;

  BREAK;
}

josl_w_dip: /* ( ... x1 q -- ... x1 ) */
{
  josl_value *vquot, *vtos;

  UNDERFLOW_CHECK (interp, w, 2);

  vquot = STACK (--SP);
  SP--;

  if (vquot->type != josl_vt_word)
    {
      THROW_EXC (josl_err_stack_type, "dip expects a quotation on TOS");
    }

  vtos = tgc_alloc(&gc, sizeof (josl_value));

  COPYV (STACK (SP), vtos);

  josl_exec_word (interp, vquot->value.wref->word);

  COPYV (vtos, STACK (SP));
  SP++;

  BREAK;
}

josl_w_2dip:
{
  josl_value *vquot, *vtos, *vnos;

  UNDERFLOW_CHECK (interp, w, 3);

  vquot = STACK (--SP);
  if (vquot->type != josl_vt_word)
    {
      SP--;
      SP--;
      THROW_EXC (josl_err_stack_type, "2dip expects a quotation on TOS");
    }

  vtos = tgc_alloc(&gc, sizeof (josl_value));
  vnos = tgc_alloc(&gc, sizeof (josl_value));

  SP--;
  COPYV (STACK (SP), vtos);
  SP--;
  COPYV (STACK (SP), vnos);

  josl_exec_word (interp, vquot->value.wref->word);

  COPYV (vnos, STACK (SP));
  SP++;
  COPYV (vtos, STACK (SP));
  SP++;

  BREAK;
}

josl_w_to_temp:
{
  josl_value *vfrom, *vto;

  UNDERFLOW_CHECK (interp, w, 1);
  TOVERFLOW_CHECK (interp, w, 1);

  vfrom = STACK (--SP);
  vto = interp->ts[interp->tsp++];

  vto->type = vfrom->type;
  vto->value = vfrom->value;

  BREAK;
}

josl_w_from_temp:
{
  josl_value *vfrom, *vto;

  TUNDERFLOW_CHECK (interp, w, 1);
  OVERFLOW_CHECK (interp, w, 1);

  vto = STACK (SP++);
  vfrom = interp->ts[--interp->tsp];

  vto->type = vfrom->type;
  vto->value = vfrom->value;

  BREAK;
}

josl_w_to_temp_count:
{
  josl_value *vcnt, *vfrom, *vto;
  int count;

  UNDERFLOW_CHECK (interp, w, 1);

  vcnt = STACK (--SP);
  if (vcnt->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected an integer on TOS");
    }

  count = vcnt->value.i;

  UNDERFLOW_CHECK (interp, w, count);
  TOVERFLOW_CHECK (interp, w, count);

  for (; count > 0; count--)
    {
      vfrom = STACK (--SP);
      vto = interp->ts[interp->tsp++];

      vto->type = vfrom->type;
      vto->value = vfrom->value;
    }

  BREAK;
}

josl_w_from_temp_count:
{
  josl_value *vcnt, *vfrom, *vto;
  int count;

  UNDERFLOW_CHECK (interp, w, 1);

  vcnt = STACK (--SP);
  if (vcnt->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expected an integer on TOS");
    }

  count = vcnt->value.i;

  OVERFLOW_CHECK (interp, w, count);
  TUNDERFLOW_CHECK (interp, w, count);

  for (; count > 0; count--)
    {
      vto = STACK (SP++);
      vfrom = interp->ts[--interp->tsp];

      vto->type = vfrom->type;
      vto->value = vfrom->value;
    }

  BREAK;
}
