/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  String

*/

josl_w_len:                    /* ( s -- i ) */
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_string:
      a->value.i = strlen (a->value.s);
      break;

    case josl_vt_sbuffer:
      a->value.i = strlen (a->value.b->data);
      break;

    case josl_vt_dict:
      a->value.i = hashtable_count (a->value.hd);
      break;

    case josl_vt_array:
      a->value.i = a->value.a->size;
      break;

    case josl_vt_list:
      a->value.i = a->value.l->size;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "len cannot be determined for TOS");
      }
    }

  a->type = josl_vt_integer;

  BREAK;
}

josl_w_append:                 /* ( s1 s2 -- s ) */
{
  josl_value *a, *b, *vresult;

  UNDERFLOW_CHECK (interp, w, 2);

  if (STACK (SP - 2)->type == josl_vt_array)
    {
      goto josl_w_append_array;
    }

  b = STACK (--SP);
  a = STACK (--SP);

  switch (b->type)
    {
    case josl_vt_string:
      {
        if (a->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type, "expects a string on NOS");
          }

        vresult = STACK (SP++);
        vresult->type = josl_vt_string;
        vresult->value.s = strdup_printf ("%s%s", a->value.s, b->value.s);

        NULLVALUE (b);
      }
      break;

    case josl_vt_sbuffer:
      {
        switch (a->type)
          {
          case josl_vt_integer:
            cbuf_append_ch (b->value.b, a->value.i);
            break;

          case josl_vt_string:
            cbuf_append_str (b->value.b, a->value.s, -1);
            break;

          default:
            {
              THROW_EXC (josl_err_stack_type,
                         "expects an integer or string on NOS");
            }
          }

        vresult = STACK (SP++);
        vresult->type = josl_vt_sbuffer;
        vresult->value.b = b->value.b;

        NULLVALUE (b);
      }
      break;
    }

  BREAK;
}

josl_w_append_pound:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_join:                   /* ( s1 s2 s:joinby -- s ) */
{
  josl_value *by, *a, *b;

  UNDERFLOW_CHECK (interp, w, 3);

  by = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (by->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string third down from TOS");
    }

  a->value.s = strdup_printf ("%s%s%s", a->value.s, by->value.s, b->value.s);
  NULLVALUE (b);
  NULLVALUE (by);

  BREAK;
}

josl_w_join_pound:
{
  THROW_EXC (josl_err_not_impl, "not yet implemented");
}

josl_w_left:                   /* ( s n -- s ) */
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  a->value.s = strndup (a->value.s, b->value.i);

  BREAK;
}

josl_w_right:                  /* ( s n -- s ) */
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  a->value.s = strdup (a->value.s + (strlen (a->value.s) - b->value.i));

  BREAK;
}

josl_w_mid:                    /* ( s n1 n2 -- s ) */
{
  josl_value *a, *b, *c;

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string third from TOS");
    }

  a->value.s = strndup (a->value.s + b->value.i, c->value.i - b->value.i + 1);

  BREAK;
}

josl_w_lower:                  /* ( s -- s ) */
{
  josl_value *a;
  int i;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = tolower (a->value.i);
      break;

    case josl_vt_string:
      a->value.s = strdup (a->value.s);
      for (i = 0; a->value.s[i]; i++)
        a->value.s[i] = tolower (a->value.s[i]);
      break;

    default:
      THROW_EXC (josl_err_stack_type,
                 "expects an integer character code or "
                 "a string on TOS");
      break;
    }

  BREAK;
}

josl_w_upper:
{
  josl_value *a;
  int i;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = toupper (a->value.i);
      break;

    case josl_vt_string:
      a->value.s = strdup (a->value.s);
      for (i = 0; a->value.s[i]; i++)
        a->value.s[i] = toupper (a->value.s[i]);
      break;

    default:
      THROW_EXC (josl_err_stack_type,
                 "expects an integer character code or "
                 "a string on TOS");
      break;
    }

  BREAK;
}

josl_w_title:
{
  josl_value *a;
  int i;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  a->value.s = strdup (a->value.s);
  a->value.s[0] = toupper (a->value.s[0]);
  for (i = 1; a->value.s[i]; i++)
    {
      if (isspace (a->value.s[i - 1]))
        a->value.s[i] = toupper (a->value.s[i]);
      else
        a->value.s[i] = tolower (a->value.s[i]);

    }

  BREAK;
}

josl_w_capitalize:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  a->value.s = strdup (a->value.s);
  a->value.s[0] = toupper (a->value.s[0]);

  BREAK;
}

josl_w_trim:
{
  josl_value *a;
  int s = 0, e;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  while (a->value.s[s] && isspace (a->value.s[s]))
    s++;
  e = strlen (a->value.s) - 1;

  while (a->value.s[e] && isspace (a->value.s[e]))
    e--;

  a->value.s = strndup (a->value.s + s, e - s + 1);

  BREAK;
}

josl_w_trim_head:
{
  josl_value *a;
  int s = 0;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  while (a->value.s[s] && isspace (a->value.s[s]))
    s++;
  a->value.s = strdup (a->value.s + s);

  BREAK;
}

josl_w_trim_tail:
{
  josl_value *a;
  int e;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  e = strlen (a->value.s) - 1;
  while (a->value.s[e] && isspace (a->value.s[e]))
    e--;

  a->value.s = strndup (a->value.s, e + 1);

  BREAK;
}

josl_w_split:                  /* ( s:content s:by -- s... i ) */
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  /* Add -1 to the stack and fall through to split# */
  a = STACK (SP++);
  a->type = josl_vt_integer;
  a->value.i = -1;
}

josl_w_split_pound:            /* ( s:content s:by i:max-splits -- s... i ) */
{
  josl_value *a, *b, *c, *x;
  int pos = 0, len = 0, numsplit = 0, count = 0, bylen;
  char *content, *by, *tmp;

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (--SP);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string third from TOS");
    }

  count = c->value.i + 1;
  content = strdup (a->value.s);
  by = strdup (b->value.s);
  bylen = strlen (by);

  while (--count != 0 && (tmp = strstr (content + pos, by)))
    {
      OVERFLOW_CHECK (interp, w, 1);
      len = tmp - (content + pos);

      x = STACK (SP++);
      x->type = josl_vt_string;
      x->value.s = strndup (content + pos, len);
      pos = pos + len + bylen;

      numsplit++;
    }

  if (pos < strlen (content))
    {
      OVERFLOW_CHECK (interp, w, 1);
      x = STACK (SP++);
      x->type = josl_vt_string;
      x->value.s = strdup (content + pos);
      numsplit++;
    }

  OVERFLOW_CHECK (interp, w, 1);

  x = STACK (SP++);
  x->type = josl_vt_integer;
  x->value.i = numsplit;

  BREAK;
}

josl_w_splitany:               /* ( s:content s:by -- s... i ) */
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  /* Add -1 to the stack and fall through to split# */
  a = STACK (SP++);
  a->type = josl_vt_integer;
  a->value.i = -1;
}

josl_w_splitany_pound:         /* ( s:content s:by i:max-splits -- s... i ) */
{
  josl_value *a, *b, *c, *x;
  int numsplit = 0, count = 0, bylen;
  char *content, *by, *tmp;

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (--SP);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string third from TOS");
    }

  count = c->value.i;
  content = strdup (a->value.s);
  by = strdup (b->value.s);
  bylen = strlen (by);

  tmp = strtok (content, by);
  while (tmp != NULL)
    {
      OVERFLOW_CHECK (interp, w, 1);

      x = STACK (SP++);
      x->type = josl_vt_string;
      x->value.s = strdup (tmp);

      numsplit++;
      count--;

      if (count == 0)
        tmp = strtok (NULL, "\0");
      else
        tmp = strtok (NULL, by);
    }

  OVERFLOW_CHECK (interp, w, 1);

  x = STACK (SP++);
  x->type = josl_vt_integer;
  x->value.i = numsplit;

  BREAK;
}

josl_w_asc:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  a->type = josl_vt_integer;
  a->value.i = a->value.s[0];

  BREAK;
}

josl_w_chr:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  a->value.s = strdup_printf ("%c", a->value.i);
  a->type = josl_vt_string;

  BREAK;
}

josl_w_find:                   /* ( s:haystack si:needle  -- i ) */
{
  josl_value *a, *b;
  char *tmp, *tmp2, tmp3[1];

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (b->type)
    {
    case josl_vt_integer:
      tmp3[0] = b->value.i;
      tmp3[1] = 0;
      b->value.s = tmp3;
      break;

    case josl_vt_string:
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects an integer or string on TOS");
      }
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  tmp = a->value.s;
  tmp2 = strstr (a->value.s, b->value.s);
  if (tmp2 == NULL)
    a->value.i = -1;
  else
    a->value.i = tmp2 - tmp;
  a->type = josl_vt_integer;

  NULLVALUE (b);

  BREAK;
}

josl_w_rfind:                  /* ( s si:needle -- i ) */
{
  josl_value *a, *b;
  char *tmp, *tmp2, tmp3[1];

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  switch (b->type)
    {
    case josl_vt_integer:
      tmp3[0] = b->value.i;
      tmp3[1] = 0;
      b->value.s = tmp3;
      break;

    case josl_vt_string:
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects an integer or string on TOS");
      }
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  tmp = a->value.s;
  tmp2 = strrstr (a->value.s, b->value.s);
  if (tmp2 == NULL)
    a->value.i = -1;
  else
    a->value.i = tmp2 - tmp;
  a->type = josl_vt_integer;

  NULLVALUE (b);

  BREAK;
}

josl_w_find_pound:             /* ( s si:needle i -- i ) */
{
  josl_value *a, *b, *c;
  char *tmp, *tmp2, tmp3[1];

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  switch (b->type)
    {
    case josl_vt_integer:
      tmp3[0] = b->value.i;
      tmp3[1] = 0;
      b->value.s = tmp3;
      break;

    case josl_vt_string:
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects an integer or string on TOS");
      }
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on third from TOS");
    }

  if (c->value.i >= strlen (a->value.s))
    {
      a->value.i = -1;

      NULLVALUE (b);
    }
  else
    {
      tmp = a->value.s;
      tmp2 = strstr (a->value.s + c->value.i, b->value.s);
      if (tmp2 == NULL)
        a->value.i = -1;
      else
        a->value.i = tmp2 - tmp;
      a->type = josl_vt_integer;

      NULLVALUE (b);
    }

  BREAK;
}

josl_w_rfind_pound:            /* ( s si:needle i -- i ) */
{
  josl_value *a, *b, *c;
  char *tmp, *tmp2, tmp3[1];

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  switch (b->type)
    {
    case josl_vt_integer:
      tmp3[0] = b->value.i;
      tmp3[1] = 0;
      b->value.s = tmp3;
      break;

    case josl_vt_string:
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type,
                   "expects an integer or string on TOS");
      }
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on third from TOS");
    }

  a->type = josl_vt_integer;

  if (c->value.i >= strlen (a->value.s))
    c->value.i = strlen (a->value.s);

  tmp = strndup (a->value.s, c->value.i + 1);
  tmp2 = strrstr (tmp, b->value.s);

  if (tmp2 == NULL)
    a->value.i = -1;
  else
    a->value.i = tmp2 - tmp;

  NULLVALUE (b);

  BREAK;
}

josl_w_cut:                    /* ( s i -- s s ) */
{
  josl_value *a, *b;
  int i;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (SP - 1);
  a = STACK (SP - 2);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  i = b->value.i;
  b->type = josl_vt_string;
  b->value.s = strdup (a->value.s + i);
  a->value.s = strndup (a->value.s, i);

  BREAK;
}

josl_w_peel:                   /* ( s -- s i ) */
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 1);
  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  b = STACK (SP++);
  b->type = josl_vt_integer;
  b->value.i = a->value.s[0];
  a->value.s = strdup (a->value.s + 1);

  BREAK;
}

josl_w_delete:                 /* ( s:content i:startpos i:endpos -- s ) */
{
  josl_value *a, *b, *c;
  char *tmp, *it;
  int i, contentlen;

  UNDERFLOW_CHECK (interp, w, 3);

  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }
  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string third from TOS");
    }

  i = strlen (a->value.s);

  if (b->value.i > i)
    {
      /* nothing needs to be done as start index > length of the string */
      BREAK;
    }

  if (c->value.i >= i)
    c->value.i = i;

  contentlen = strlen (a->value.s) - (c->value.i - b->value.i);
  tmp = tgc_alloc(&gc, sizeof (char) * (contentlen + 1));
  if (tmp == NULL)
    {
      THROW_EXC (josl_err_memory, "could not allocate memory for new string");
    }

  it = tmp;
  for (i = 0; i < b->value.i; i++)
    *it++ = a->value.s[i];
  for (i = c->value.i; a->value.s[i]; i++)
    *it++ = a->value.s[i];
  tmp[contentlen] = 0;
  a->value.s = tmp;

  BREAK;
}

josl_w_replace:                /* ( s:content s:new startpos:position i:endpos -- s ) */
{
  josl_value *a, *b, *c, *d;
  char *tmp, *it;
  int newlen, i;

  UNDERFLOW_CHECK (interp, w, 4);

  d = STACK (--SP);
  c = STACK (--SP);
  b = STACK (--SP);
  a = STACK (SP - 1);

  if (d->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (c->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string thrid from TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string forth from TOS");
    }

  newlen =
    strlen (a->value.s) + strlen (b->value.s) - (d->value.i - c->value.i);
  tmp = tgc_alloc(&gc, sizeof (char) * (newlen + 1));
  if (tmp == NULL)
    {
      THROW_EXC (josl_err_memory, "could not allocate memory for new string");
    }
  it = tmp;

  for (i = 0; i < c->value.i; i++)
    *it++ = a->value.s[i];
  for (i = 0; i < strlen (b->value.s); i++)
    *it++ = b->value.s[i];
  for (i = d->value.i; a->value.s[i]; i++)
    *it++ = a->value.s[i];
  *it++ = 0;
  a->value.s = tmp;

  BREAK;
}

josl_w_insert:
{
  josl_value *val;

  UNDERFLOW_CHECK (interp, w, 1);

  val = STACK (--SP);

  switch (val->type)
    {
    case josl_vt_integer:      /* position for string insert */
      {
        /* ( s:content s:new i:position -- s ) */
        josl_value *a, *b, *c;
        char *tmp, *it;
        int newlen, i;

        UNDERFLOW_CHECK (interp, w, 2);

        c = val;
        b = STACK (--SP);
        a = STACK (SP - 1);

        if (b->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type, "expects a string on NOS");
          }

        if (a->type != josl_vt_string)
          {
            THROW_EXC (josl_err_stack_type,
                       "expects a string third from TOS");
          }

        newlen = strlen (a->value.s) + strlen (b->value.s);
        tmp = tgc_alloc(&gc, sizeof (char) * (newlen + 1));
        if (tmp == NULL)
          {
            THROW_EXC (josl_err_memory,
                       "could not allocate memory for new string");
          }

        it = tmp;
        for (i = 0; i < c->value.i; i++)
          *it++ = a->value.s[i];
        for (i = 0; i < strlen (b->value.s); i++)
          *it++ = b->value.s[i];
        for (i = c->value.i; a->value.s[i]; i++)
          *it++ = a->value.s[i];
        *it++ = 0;
        a->value.s = tmp;
      }
      break;

    case josl_vt_list:         /* ( x l:list -- ) */
      {
        josl_value *insval;
        josl_list_item *ni;

        UNDERFLOW_CHECK (interp, w, 1);
        insval = STACK (--SP);

        ni = tgc_alloc(&gc, sizeof (josl_list_item));
        ni->v = tgc_alloc(&gc, sizeof (josl_value));
        ni->v->type = insval->type;
        ni->v->value = insval->value;

        if (val->value.l->cur != NULL)
          {
            ni->next = val->value.l->cur;
            ni->prev = val->value.l->cur->prev;
          }
        else
          {
            ni->next = val->value.l->head;
            ni->prev = NULL;
          }

        if (ni->next != NULL)
          ni->next->prev = ni;
        if (ni->prev != NULL)
          ni->prev->next = ni;
        val->value.l->cur = ni;
      }
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expected an integer or list on TOS");
      }
    }

  BREAK;
}

josl_w_format:                 /* ( ... s:format (b:charbuf) -- s ) */
{
  josl_value *a, *x;
  charbuf *cb = NULL;
  char itemfmt[12];
  char *format;
  int i, j, formatlen, maxpos, fromCharBuf = 0;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (--SP);

  if (a->type == josl_vt_sbuffer)
    {
      cb = a->value.b;
      a = STACK (--SP);
      fromCharBuf = 1;

      UNDERFLOW_CHECK (interp, w, 1);
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  format = a->value.s;
  formatlen = strlen (format);
  if (fromCharBuf == 0)
    cb = cbuf_alloc (formatlen * 2);
  i = 0;
  maxpos = 0;

  while (i < formatlen)
    {
      if (format[i] == '{')
        {
          if (format[i + 1] == '{')
            {
              /* An escaped {, allow it */
              i++;
              cbuf_append_ch (cb, '{');
            }
          else
            {
              int pos;
              i++;              /* Skip our { */
              pos = atoi (format + i);
              if (pos > 99)
                i += 3;
              else if (pos > 9)
                i += 2;
              else
                i += 1;

              if (pos > maxpos)
                maxpos = pos;

              UNDERFLOW_CHECK (interp, w, pos + 1);

              x = STACK (SP - (pos + 1));

              if (format[i] == '}')
                {
                  /* No format given, use our own */
                  switch (x->type)
                    {
                    case josl_vt_integer:
                      cbuf_append_printf (cb, "%lld", x->value.i);
                      break;

                    case josl_vt_double:
                      cbuf_append_printf (cb, "%f", x->value.d);
                      break;

                    case josl_vt_string:
                      cbuf_append_str (cb, x->value.s, -1);
                      break;
                    }
                }
              else
                {
                  /* We have something that looks like {1:%s} */
                  i++;          /* Skip the : */

                  /* Find the trailing } */
                  for (j = i; j < formatlen; j++)
                    {
                      if (format[j] == '}')
                        {
                          break;
                        }
                    }

                  if (format[j] != '}')
                    {
                      THROW_EXC (josl_err_format, "malformed format");
                    }

                  j -= (i - 1);
                  itemfmt[0] = '%';
                  itemfmt[1] = 0;
                  strncat (itemfmt, format + i, j);
                  itemfmt[j] = 0;

                  switch (x->type)
                    {
                    case josl_vt_integer:
                      cbuf_append_printf (cb, itemfmt, x->value.i);
                      break;

                    case josl_vt_double:
                      cbuf_append_printf (cb, itemfmt, x->value.d);
                      break;

                    case josl_vt_string:
                      cbuf_append_printf (cb, itemfmt, x->value.s);
                      break;
                    }

                  i += j - 1;   /* Skip the length of our format */
                }
            }
        }
      else
        {
          cbuf_append_ch (cb, format[i]);
        }

      i++;
    }

  SP -= maxpos + 1;
  if (fromCharBuf == 0)
    {
      x = STACK (SP++);
      x->type = josl_vt_string;
      x->value.s = cb->data;
    }

  BREAK;
}

josl_w_repeat:                 /* ( s i -- s ) */
{
  josl_value *a, *b;
  charbuf *cb;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  cb = cbuf_alloc ((b->value.i * strlen (a->value.s)) + 1);
  if (cb == NULL)
    {
      THROW_EXC (josl_err_memory, "Could not allocate memory for new string");
    }

  while (--b->value.i >= 0)
    cbuf_append_str (cb, a->value.s, -1);

  a->value.s = cb->data;

  BREAK;
}

josl_w_reverse:                /* ( s -- s ) */
{
  josl_value *a;
  int i = 0, n;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  a->value.s = strdup (a->value.s);
  n = strlen (a->value.s);
  while (i < n / 2)
    {
      *(a->value.s + n) = *(a->value.s + i);
      *(a->value.s + i) = *(a->value.s + n - i - 1);
      *(a->value.s + n - i - 1) = *(a->value.s + n);
      i++;
    }
  *(a->value.s + n) = '\0';

  BREAK;
}

josl_w_translate:              /* ( s:value s:map -- s ) */
{
  josl_value *a, *b;
  char *value, *map;
  int i, j, maplen;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  value = strdup (a->value.s);
  map = b->value.s;
  maplen = strlen (map);
  for (i = 0; value[i]; i++)
    {
      for (j = 0; j < maplen - 1; j += 2)
        {
          if (value[i] == map[j])
            {
              value[i] = map[j + 1];
              break;
            }
        }
    }

  a->value.s = value;

  BREAK;
}

josl_w_is_alnum:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isalnum (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isalnum (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_alpha:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isalpha (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isalpha (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_cntrl:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(iscntrl (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(iscntrl (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_digit:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isdigit (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isdigit (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_graph:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isgraph (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isgraph (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_lower:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(islower (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(islower (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_print:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isprint (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isprint (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_punct:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(ispunct (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(ispunct (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_space:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isspace (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isspace (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_upper:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isupper (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isupper (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_is_xdigit:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);

  switch (a->type)
    {
    case josl_vt_integer:
      a->value.i = !(isxdigit (a->value.i) == 0);
      break;

    case josl_vt_string:
      a->value.i = !(isxdigit (a->value.s[0]) == 0);
      a->type = josl_vt_integer;
      break;

    default:
      {
        THROW_EXC (josl_err_stack_type, "expects a string or integer on TOS");
      }
    }

  BREAK;
}

josl_w_has_prefix:             /* ( s s -- i ) */
{
  josl_value *a, *b;
  char *tmp, *tmp2;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  tmp = a->value.s;
  tmp2 = strstr (a->value.s, b->value.s);
  if (tmp2 == NULL)
    a->value.i = 0;
  else
    a->value.i = tmp2 == tmp;
  a->type = josl_vt_integer;

  NULLVALUE (b);

  BREAK;
}

josl_w_has_suffix:             /* ( s s -- i ) */
{
  josl_value *a, *b;
  char *tmp, *tmp2;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (SP - 1);

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (a->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on NOS");
    }

  tmp = a->value.s;
  tmp2 = strrstr (a->value.s, b->value.s);
  if (tmp2 == NULL)
    a->value.i = 0;
  else
    a->value.i = tmp2 == (tmp + strlen (tmp) - strlen (b->value.s));
  a->type = josl_vt_integer;

  NULLVALUE (b);

  BREAK;
}

josl_w_at:                     /* ( i s -- i ) */
{
  josl_value *a, *b;

  UNDERFLOW_CHECK (interp, w, 2);

  b = STACK (--SP);
  a = STACK (--SP);

  if (b->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  if (a->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on NOS");
    }

  if (a->value.i >= strlen (b->value.s))
    {
      THROW_EXC (josl_err_index_out_of_bounds, "index out of bounds");
    }
  else
    {
      a = STACK (SP++);
      a->value.i = b->value.s[a->value.i];
      a->type = josl_vt_integer;

      NULLVALUE (b);
    }

  BREAK;
}
