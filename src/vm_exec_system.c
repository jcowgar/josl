/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  System

*/

josl_w_argc:
{
  josl_value *vargc;

  OVERFLOW_CHECK (interp, w, 1);

  vargc = STACK (SP++);
  vargc->type = josl_vt_integer;
  vargc->value.i = josl_argc;

  BREAK;
}

josl_w_argv:
{
  josl_value *vidx, *vresult;
  int idx;

  UNDERFLOW_CHECK (interp, w, 1);

  vidx = STACK (--SP);
  if (vidx->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "argv$ expects an integer on TOS");
    }

  idx = vidx->value.i;

  vresult = STACK (SP++);

  if (idx < josl_argc)
    {
      vresult->type = josl_vt_string;
      vresult->value.s = josl_argv[idx];
    }
  else
    {
      NULLVALUE (vresult);
    }

  BREAK;
}

josl_w_env:
{
  josl_value *vvar, *vresult;
  UNDERFLOW_CHECK (interp, w, 1);

  vvar = STACK (--SP);
  if (vvar->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  vresult = STACK (SP++);

  vresult->value.s = getenv (vvar->value.s);
  if (vresult->value.s == NULL)
    {
      NULLVALUE (vresult);
    }
  else
    {
      vresult->type = josl_vt_string;
    }

  BREAK;
}

josl_w_system:
{
  josl_value *vcmd, *vresult;
  UNDERFLOW_CHECK (interp, w, 1);

  vcmd = STACK (--SP);
  if (vcmd->type != josl_vt_string)
    {
      THROW_EXC (josl_err_stack_type, "expects a string on TOS");
    }

  vresult = STACK (SP++);
  vresult->type = josl_vt_integer;
  vresult->value.i = system (vcmd->value.s);

  BREAK;
}

josl_w_exit:
{
  josl_value *vcode;

  UNDERFLOW_CHECK (interp, w, 1);

  vcode = STACK (--SP);
  if (vcode->type != josl_vt_integer)
    {
      THROW_EXC (josl_err_stack_type, "expects an integer on TOS");
    }

  exit (vcode->value.i);

  BREAK;
}
