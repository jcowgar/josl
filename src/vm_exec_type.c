/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Type

*/

josl_w_is_integer:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_integer;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_double:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_double;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_number:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = (a->type == josl_vt_integer || a->type == josl_vt_double);
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_string:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_string;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_pointer:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_pointer;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_dict:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_dict;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_array:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_array;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_list:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_list;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_null:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_null;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_variable:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_variable;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_is_word:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type == josl_vt_word;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_type:
{
  josl_value *a;

  UNDERFLOW_CHECK (interp, w, 1);

  a = STACK (SP - 1);
  a->value.i = a->type;
  a->type = josl_vt_integer;

  BREAK;
}

josl_w_null:
{
  josl_value *a;

  OVERFLOW_CHECK (interp, w, 1);

  a = STACK (SP++);
  a->type = josl_vt_null;
  a->value.p = NULL;

  BREAK;
}
