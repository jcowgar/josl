/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Unit Testing

*/

josl_w_test:
{
  josl_value *msg, *result, *expected;
  char resultFS[18], expectedFS[18];
  int tsp, cnt;
  bool passed = TRUE;

  msg = STACK (--SP);
  cnt = SP / 2;

  if ((SP % 2) != 0)
    {
      passed = FALSE;
    }
  else
    {
      for (tsp = SP; tsp > cnt; tsp--)
        {
          expected = STACK (tsp - 1);
          result = STACK (tsp - 1 - cnt);

          if (result->type != expected->type)
            passed = FALSE;
          else
            {
              switch (result->type)
                {
                case josl_vt_integer:
                  if (result->value.i != expected->value.i)
                    passed = FALSE;
                  break;

                case josl_vt_double:
                  snprintf (resultFS, 18, "%.8f", result->value.d);
                  snprintf (expectedFS, 18, "%.8f", expected->value.d);
                  if (strcmp (resultFS, expectedFS) != 0)
                    passed = FALSE;
                  break;

                case josl_vt_string:
                  if (strcmp (result->value.s, expected->value.s) != 0)
                    passed = FALSE;
                  break;

                case josl_vt_pointer:
                  if (result->value.p != expected->value.p)
                    passed = FALSE;
                  break;

                case josl_vt_null:
                  if (expected->type != josl_vt_null)
                    passed = FALSE;
                  break;

                default:
                  fprintf (stdout,
                           "  value type is not yet comparable by test\n");
                  passed = FALSE;
                  break;
                }
            }
        }
    }

  if (passed)
    {
      if (verbose_tests == TRUE)
        fprintf (stdout, "  ok....... %s\n", msg->value.s);
      passed_test_count++;
    }
  else
    {
      failed_test_count++;

      fprintf (stdout, "  FAILED... %s\n", msg->value.s);

      if ((SP % 2) != 0)
        fprintf (stdout,
                 "       stack does not have an even number of values\n");
      else
        {
          for (tsp = SP; tsp > cnt; tsp--)
            {
              expected = STACK (tsp - 1);
              result = STACK (tsp - 1 - cnt);

              fprintf (stdout, "      got ");

              switch (result->type)
                {
                case josl_vt_integer:
                  fprintf (stdout, "'%lld'", result->value.i);
                  break;

                case josl_vt_double:
                  fprintf (stdout, "'%e'", result->value.d);
                  break;

                case josl_vt_string:
                  fprintf (stdout, "'%s'", result->value.s);
                  break;

                case josl_vt_pointer:
                  fprintf (stdout, "'%p'", result->value.p);
                  break;

                case josl_vt_null:
                  fprintf (stdout, "NULL");
                  break;

                default:
                  fprintf (stdout, "type not comparable (%i)", result->type);
                  break;
                }

              fprintf (stdout, " expected ");
              switch (expected->type)
                {
                case josl_vt_integer:
                  fprintf (stdout, "'%lld'", expected->value.i);
                  break;

                case josl_vt_double:
                  fprintf (stdout, "'%e'", expected->value.d);
                  break;

                case josl_vt_string:
                  fprintf (stdout, "'%s'", expected->value.s);
                  break;

                case josl_vt_pointer:
                  fprintf (stdout, "'%p'", expected->value.p);
                  break;

                case josl_vt_null:
                  fprintf (stdout, "NULL");
                  break;

                default:
                  fprintf (stdout, "type not comparable (%i)",
                           expected->type);
                  break;
                }

              fprintf (stdout, "\n");
            }
        }
    }

  SP = 0;

  BREAK;
}

josl_w_test_fail_count:
{
  OVERFLOW_CHECK (interp, w, 1);

  STACK (SP)->type = josl_vt_integer;
  STACK (SP++)->value.i = failed_test_count;

  BREAK;
}

josl_w_test_pass_count:
{
  OVERFLOW_CHECK (interp, w, 1);

  STACK (SP)->type = josl_vt_integer;
  STACK (SP++)->value.i = passed_test_count;

  BREAK;
}

josl_w_test_total_count:
{
  OVERFLOW_CHECK (interp, w, 1);

  STACK (SP)->type = josl_vt_integer;
  STACK (SP++)->value.i = passed_test_count + failed_test_count;

  BREAK;
}

josl_w_test_report:
{
  int total_tests = passed_test_count + failed_test_count;

  fprintf (stdout, "%d total test%s ", total_tests,
           total_tests > 1 || total_tests == 0 ? "s" : "");
  fprintf (stdout, "%d passed test%s ", passed_test_count,
           passed_test_count > 1 || passed_test_count == 0 ? "s" : "");
  fprintf (stdout, "%d failed test%s ", failed_test_count,
           failed_test_count > 1 || failed_test_count == 0 ? "s" : "");
  fprintf (stdout, "%.1f%% success rate\n",
           (passed_test_count + 0.0) / (total_tests + 0.0) * 100.0);

  BREAK;
}

josl_w_test_verbose:
{
  verbose_tests = TRUE;

  BREAK;
}

josl_w_test_is_verbose:
{
	STACK(SP)->type = josl_vt_integer;
	STACK(SP++)->value.i = verbose_tests;

	BREAK;
}
