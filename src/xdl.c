/*
 * Copyright (c) 2008,2010 Jeremy Cowgar.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of Jeremy Cowgar nor the names of its contributors may
 *     be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*

  Cross Platform DLL/SO loading

*/

#ifdef WINDOWS
#include <stdio.h>
#include <windows.h>

void *
dlopen (const char *filename, int mode)
{
  void *result;

  result = (void *) LoadLibraryEx (filename, NULL, 0);
  if (result)
    {
      SetLastError (ERROR_SUCCESS);
    }

  return result;
}

int
dlclose (void * dll)
{
  return FreeLibrary ((HANDLE) dll);
}

const char *
dlerror (void)
{
  DWORD error;
  LPSTR buffer;

  error = GetLastError();
  if (error == ERROR_SUCCESS)
    return NULL;

  FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER
                 | FORMAT_MESSAGE_IGNORE_INSERTS
                 | FORMAT_MESSAGE_FROM_SYSTEM,
                 NULL, error,
                 MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
                 (LPSTR) &buffer, 0, NULL);

  return buffer;
}

void *
dlsym (void *dll, const char * symbol)
{
  if (dll == 0)
    dll = GetModuleHandle (NULL);

  return (void *) GetProcAddress ((HANDLE) dll, symbol);
}

#endif
