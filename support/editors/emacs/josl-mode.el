;;; josl-mode-el -- Major mode for editing Josl files

;; Author: Jeremy Cowgar <jeremy@cowgar.com>
;; Created: 25 Mar 2010
;; Keywords: Josl major-mode

;; Copyright (C) 2010 Jeremy Cowgar <jeremy@cowgar.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

;;; Commentary:
;;
;; This mode provides syntax highlighting and automatic indentation
;; for Josl source files.
;;
;; Josl can be learned about at http://www.josl.org

;;; Code:
(defvar josl-mode-hook nil)
(defvar josl-mode-map
  (let ((josl-mode-map (make-keymap)))
    (define-key josl-mode-map "\C-j" 'newline-and-indent)
    (define-key josl-mode-map "\C-c" 'comment-region)
    (define-key josl-mode-map "\C-\c" 'uncomment-region)
    josl-mode-map)
  "Keymap for Josl major mode")

(add-to-list 'auto-mode-alist '("\\.j\\'" . josl-mode))

;; Keywords:
;;
;; (regexp-opt '("begin" "do" "each" "else" "end" "for" "if" "is" "times" 
;;   "next" "+next" "select" "until" "vector" "while"))
;;

(defconst josl-font-lock-keywords-1
  (list
   ; These define control words
   '("\\<#.*" . font-lock-comment-face)
   '("\\<\"[^\"]*\"" . font-lock-string-face)
   '("\\<[+-]?[0-9,_\\.]+\\>" . font-lock-constant-face)
   '("\\<[A-Z0-9][^ \t\n\r]*\\>" . font-lock-constant-face)
   '("^-[^ \t\n]+\\>" . font-lock-reference-face)
   '("^[^ \t\n]+\\>" . font-lock-function-name-face)
   '("\\<\\(\\+next\\|begin\\|do\\|e\\(?:ach\\|lse\\|nd\\)\\|for\\|i[fs]\\|next\\|select\\|times\\|until\\|vector\\|while\\)\\>" . font-lock-builtin-face)
   )
  "Minimal highlighting for Josl mode.")

(defvar josl-font-lock-keywords josl-font-lock-keywords-1)

(defvar josl-mode-syntax-table nil)

(if (not josl-mode-syntax-table)
    (progn
      (setq josl-mode-syntax-table (make-syntax-table))
      (let ((char 0))
	(while (< char ?!)
	  (modify-syntax-entry char " " josl-mode-syntax-table)
	  (setq char (1+ char)))
	(while (< char 256)
	  (modify-syntax-entry char "w" josl-mode-syntax-table)
	  (setq char (1+ char))))
      ))

(defun josl-mode ()
  (interactive)
  (kill-all-local-variables)
  (use-local-map josl-mode-map)
  (set-syntax-table josl-mode-syntax-table)
  (set (make-local-variable 'font-lock-defaults)
       '(josl-font-lock-keywords))
  (make-local-variable 'comment-start)
  (setq comment-start "# ")
  (setq major-mode 'josl-mode)
  (setq mode-name "Josl")
  (run-hooks 'josl-mode-hook))

(provide 'josl-mode)
