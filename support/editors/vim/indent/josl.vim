" Vim indent file
"
" Language:		Josl
" Maintainer:	Jeremy Cowgar <jeremy@cowgar.com>
" Last Change:	Thu Mar 18 01:10:00 2010 EST
" Notes:		Josl: http://www.josl.org
"

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
	finish
endif
let b:did_indent = 1

setlocal indentexpr=GetJoslIndent()
setlocal indentkeys+==if,=else,=end,=do,=while,=until,=begin,=vector,=times,=next,=for

" Only define the function once
if exists("*GetJoslIndent")
	finish
endif

function GetJoslIndent()
	" Find a non-blank line above the current line.
	let lnum = prevnonblank(v:lnum - 1)

	" Hit the start of the file, use zero indent.
	if lnum == 0
		return 0
	endif

	let line  = getline(lnum)     " Last line
	let cline = getline(v:lnum)   " Current line
	let pline = getline(lnum - 1) " Previous to last line
	let ind   = indent(lnum)

	if line =~ '\(if\|else\|while\|begin\|times\|for\|vector\)$'
		let ind = ind + &sw
	endif

	if cline =~ '\(else\|until\|next\|end\)$'
		let ind = ind - &sw
	endif

	return ind
endfunction

" vim: set ts=4 sw=4:

