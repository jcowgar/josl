" Vim syntax file
"
" Language:		Josl
" Maintainer:	Jeremy Cowgar <jeremy@cowgar.com>
" Last Change:	Thu Mar 31 10:41:00 2010 EST
" Notes:		Josl: http://www.josl.org
"
" Install:
"
" Add something like the following to your ~/.vim/filetype.vim
"
" augroup filetypedetect
"   au! BufRead,BufNewFile *.j setfiletype josl
" augroup END
"
" Notes:
"
" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
	syntax clear
elseif exists("b:current_syntax")
	finish
endif

" Characters allowed in keywords
if version >= 600
	setlocal iskeyword=33-126
else
	set iskeyword=33-126
endif

" Josl Keywords
syn keyword joslKeywords +next
syn keyword joslKeywords if is
syn keyword joslKeywords begin
syn keyword joslKeywords do
syn keyword joslKeywords each else end
syn keyword joslKeywords for
syn keyword joslKeywords next
syn keyword joslKeywords select
syn keyword joslKeywords times
syn keyword joslKeywords vector
syn keyword joslKeywords until
syn keyword joslKeywords while

syn keyword joslDirective -include
syn keyword joslDirective -public

syn match joslWordDefine '^[^ \t"-][^ \t]\+'
syn match joslNumber "\<\d\+\>"
syn match joslNumber "\<\d+\.\d*\>"
syn match joslNumber "\<\.\d+\>"

syn region joslString start=+"+ skip=+\\\\\|\\"+ end=+"+

syn keyword joslTodo contained TODO FIXME XXX

syn region joslComment oneline contains=joslTodo start="#" end="$"
syn sync ccomment joslComment

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version > 508 || !exists("did_josl_syntax_inits")
	if version < 508
		let did_josl_syntax_inits = 1
		command -nargs=+ HiLink hi link <args>
	else
		command -nargs=+ HiLink hi def link <args>
	endif

	HiLink joslDirective		Keyword
	HiLink joslWordDefine       Define
	HiLink joslKeywords         Keyword
	HiLink joslNumber           Number
	HiLink joslComment          Comment
	HiLink joslString           String
	HiLink joslTodo             ToDo

	delcommand HiLink
endif

let b:current_syntax = "josl"

" vim: ts=4
