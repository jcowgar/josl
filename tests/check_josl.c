#include <stdlib.h>
#include <string.h>
#include <check.h>

#include "test_setup.h"

josl *vm;

Suite *make_stack_suite (void);
Suite *make_str_suite (void);
Suite *make_vm_suite (void);

void
setup (void)
{
  fail_unless (josl_init (&vm) == TRUE, "josl init failed");
  fail_unless (vm->sp == 0, "stack pointer not set to zero initially");
  fail_if (vm->s == NULL, "stack was not initialized");
}

void
teardown (void)
{
  josl_cleanup (&vm);

  fail_unless (vm == NULL, "cleanup did not NULL vm pointer");
}

Suite *
make_master_suite (void)
{
  Suite *s = suite_create ("Josl");

  suite_add_tcase (s, tcase_create ("Master"));

  return s;
}

int
main (void)
{
  int number_failed;
  SRunner *sr = srunner_create (make_master_suite ());
  srunner_add_suite (sr, make_str_suite ());
  srunner_add_suite (sr, make_vm_suite ());
  srunner_add_suite (sr, make_stack_suite ());

  srunner_set_log (sr, "check_josl.log");
  srunner_run_all (sr, CK_ENV);
  number_failed = srunner_ntests_failed (sr);
  srunner_free (sr);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
