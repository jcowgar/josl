#include <stdlib.h>

#include <check.h>

#include <josl/vm.h>
#include <josl/stack.h>
#include <josl/value.h>

#include "test_setup.h"

START_TEST (test_depth)
{
  fail_unless (josl_stack_depth (vm) == 0,
               "depth is not reporting an empty stack correctly");

  vm->sp++;
  fail_unless (josl_stack_depth (vm) == 1,
               "depth does not report one value correctly");

  vm->sp += 500;
  fail_unless (josl_stack_depth (vm) == 501, "stack depth should be 501");
}

END_TEST;

START_TEST (test_push_integer)
{
  josl_stack_push_integer (vm, 10);
  fail_unless (vm->sp == 1, "stack pointer was not incremented");
  fail_unless (vm->s[vm->sp - 1]->type == josl_vt_integer,
               "tos does not contain the pushed type");
  fail_unless (vm->s[vm->sp - 1]->value.i == 10,
               "tos does not contain the pushed value");
}

END_TEST;

START_TEST (test_push_double)
{
  josl_stack_push_double (vm, 10.5);
  fail_unless (vm->sp == 1, "stack pointer was not incremented");
  fail_unless (vm->s[vm->sp - 1]->type == josl_vt_double,
               "tos does not contain the pushed type");
  fail_unless (vm->s[vm->sp - 1]->value.d == 10.5,
               "tos does not contain the pushed value");
}

END_TEST;

START_TEST (test_pop_integer)
{
  gint64 v;
  josl_stack_push_integer (vm, 10);
  josl_stack_pop_integer (vm, &v);

  fail_unless (v == 10, "pop did not get value pushed");

  josl_stack_push_double (vm, 10.5);
  josl_stack_pop_integer (vm, &v);

  fail_if (vm->errorStack == NULL, "tried popping a double as an integer and no error");
}

END_TEST;

START_TEST (test_pop_double)
{
  gdouble v;
  josl_stack_push_double (vm, 10.5);
  josl_stack_pop_double (vm, &v);

  fail_unless (v == 10.5, "pop did not get value pushed");

  josl_stack_push_integer (vm, 10);
  josl_stack_pop_double (vm, &v);

  fail_if (vm->errorStack == NULL, "tried popping an integer as a double and no error");
}

END_TEST;

START_TEST (test_drop)
{
  josl_stack_push_integer (vm, 1);
  josl_stack_push_integer (vm, 2);
  fail_unless (josl_stack_depth (vm) == 2,
               "2 items were pushed yet depth is not 2");

  josl_stack_drop (vm);
  fail_unless (josl_stack_depth (vm) == 1,
               "1 item was dropped, depth should be 1 but is not");

  josl_stack_drop (vm);
  fail_unless (josl_stack_depth (vm) == 0,
               "1 more item was dropped, depth should be 0 but is not");

  josl_stack_drop (vm);
  fail_if (vm->errorStack == NULL,
           "dropped too many, should be an error but is not");
}

END_TEST;

START_TEST (test_swap)
{
  gint64 a = 1, b = 2, x;
/* ( a b -- b a ) */
  josl_stack_push_integer (vm, a);
  josl_stack_push_integer (vm, b);
  josl_stack_swap (vm);
  josl_stack_pop_integer (vm, &x);
  fail_unless (x == a, "tos was not swapped with nos");

  josl_stack_pop_integer (vm, &x);
  fail_unless (x == b, "nos was not swapped with tos");

  josl_stack_swap (vm);
  fail_if (vm->errorStack == NULL,
           "swapped with nothing on the stack, should be an error but is not");

  josl_stack_push_integer (vm, a);
  josl_stack_swap (vm);
  fail_if (vm->errorStack == NULL,
           "swapped with one item on the stack, should be an error but is not");
}

END_TEST;

Suite *
make_stack_suite (void)
{
  Suite *s = suite_create ("Stack");
  TCase *tc_depth = tcase_create ("Depth");
  tcase_add_checked_fixture (tc_depth, setup, teardown);
  tcase_add_test (tc_depth, test_push_integer);
  suite_add_tcase (s, tc_depth);

  TCase *tc_pushing = tcase_create ("Pushing");
  tcase_add_checked_fixture (tc_pushing, setup, teardown);
  tcase_add_test (tc_pushing, test_push_integer);
  tcase_add_test (tc_pushing, test_push_double);
  suite_add_tcase (s, tc_pushing);

  TCase *tc_popping = tcase_create ("Popping");
  tcase_add_checked_fixture (tc_popping, setup, teardown);
  tcase_add_test (tc_popping, test_pop_integer);
  tcase_add_test (tc_popping, test_pop_double);
  suite_add_tcase (s, tc_popping);

  TCase *tc_basics = tcase_create ("Basics");
  tcase_add_checked_fixture (tc_basics, setup, teardown);
  tcase_add_test (tc_basics, test_drop);
  tcase_add_test (tc_basics, test_swap);
  suite_add_tcase (s, tc_basics);

  return s;
}
