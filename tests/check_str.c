#include <stdlib.h>
#include <check.h>

#include <josl/str.h>

START_TEST (test_string_to_int64)
{
  gint64 i;

  i = string_to_int64 ("1");
  fail_unless (i == 1, "'1' failed to convert");

  i = string_to_int64 ("9223372036854775807");
  fail_unless (i == 9223372036854775807, "'9223372036854775807' failed to convert");

  i = string_to_int64 ("9223372036854775808");
  fail_unless (i == 9223372036854775807, "'9223372036854775808' failed to be set to MAX");

  i = string_to_int64 ("-9223372036854775807");
  fail_unless (i == -9223372036854775807, "'-9223372036854775807' failed to convert");

  i = string_to_int64 ("-9223372036854775808");
  fail_unless (i == -9223372036854775807, "'-9223372036854775808' failed to be set to MAX");

  i = string_to_int64 ("9,223,372,036,854,775,807");
  fail_unless (i == 9223372036854775807, "'9,223,372,036,854,775,807' failed to convert");

  i = string_to_int64 ("-9,223,372,036,854,775,807");
  fail_unless (i == -9223372036854775807, "'-9,223,372,036,854,775,807' failed to convert");

  i = string_to_int64 ("9_223_372_036_854_775_807");
  fail_unless (i == 9223372036854775807, "'9_223_372_036_854_775_807' failed to convert");

  i = string_to_int64 ("-9_223_372_036_854_775_807");
  fail_unless (i == -9223372036854775807, "'-9_223_372_036_854_775_807' failed to convert");
}

END_TEST;

START_TEST (test_string_to_double)
{
  gdouble d;

  d = string_to_double("1");
  fail_unless (d == 1.0, "'1' failed to convert");

  d = string_to_double("1.1");
  fail_unless (d == 1.1, "'1.1' failed to convert");

  d = string_to_double("1,234.56789");
  fail_unless (d == 1234.56789, "'1,234.56789' failed to convert");

  d = string_to_double("1,234.567,89");
  fail_unless (d == 1234.56789, "'1,234.567,89' failed to convert");

  d = string_to_double("1_234.567_89");
  fail_unless (d == 1234.56789, "'1_234.567_89' failed to convert");

  d = string_to_double("-1");
  fail_unless (d == -1.0, "'-1' failed to convert");

  d = string_to_double("-1.1");
  fail_unless (d == -1.1, "'-1.1' failed to convert");

  d = string_to_double("-1,234.56789");
  fail_unless (d == -1234.56789, "'-1,234.56789' failed to convert");

  d = string_to_double("-1,234.567,89");
  fail_unless (d == -1234.56789, "'-1,234.567,89' failed to convert");

  d = string_to_double("-1_234.567_89");
  fail_unless (d == -1234.56789, "'-1_234.567_89' failed to convert");
}
END_TEST;

Suite *
make_str_suite (void)
{
  Suite *s = suite_create ("String");
  TCase *tc_string_to = tcase_create ("String>???");
  tcase_add_test (tc_string_to, test_string_to_int64);
  tcase_add_test (tc_string_to, test_string_to_double);
  suite_add_tcase (s, tc_string_to);

  return s;
}
