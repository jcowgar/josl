#include <stdlib.h>
#include <check.h>

#include <josl.h>
#include <josl/vm.h>

#include "test_setup.h"

/* These are tested by setup and teardown */
START_TEST (test_josl_init)
{
}

END_TEST;

/* These are tested by setup and teardown */
START_TEST (test_josl_cleanup)
{
}

END_TEST;

START_TEST (test_core_word_lookup)
{
  fail_unless (josl_word_get_id (vm, "dup") == josl_w_dup,
               "dup lookup did not equal josl_w_dup");
  fail_unless (josl_word_get_id (vm, ">s") == josl_w_from_temp,
               ">s did not equal josl_w_to_stack");
  fail_unless (josl_word_get_id (vm, "does-not-exist") == -1,
               "non-existiant word lookup did not result in -1");
}

END_TEST;

Suite *
make_vm_suite (void)
{
  Suite *s = suite_create ("Vm");
  TCase *tc_init = tcase_create ("Init/Cleanup");
  tcase_add_checked_fixture (tc_init, setup, teardown);
  tcase_add_test (tc_init, test_josl_init);
  tcase_add_test (tc_init, test_josl_cleanup);
  suite_add_tcase (s, tc_init);

  TCase *tc_word_setup = tcase_create ("Word Setup");
  tcase_add_checked_fixture (tc_word_setup, setup, teardown);
  tcase_add_test (tc_word_setup, test_core_word_lookup);
  suite_add_tcase (s, tc_word_setup);

  return s;
}
