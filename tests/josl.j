#
# Tests for the Josl interpreter
#

-include t_array.j
-include t_break.j
-include t_compare.j
-include t_constant.j
-include t_continue.j
-include t_dict.j
-include t_dynamic.j
-include t_each.j
-include t_enum.j
-include t_filesys.j
-include t_for.j
-include t_functional.j
-include t_if.j
-include t_ijk.j
-include t_in.j
-include t_io.j
-include t_is.j
-include t_list.j
-include t_math.j
-include t_regex.j
-include t_rename.j
-include t_stack.j
-include t_string.j
-include t_times.j
-include t_tstack.j
-include t_type.j
-include t_until.j
-include t_variable.j
-include t_vector.j
-include t_while.j

main argc if test-verbose end
  t_array
  t_break
  t_compare
  t_constant
  t_continue
  t_dict
  t_dynamic
  t_each
  t_enum
  t_filesys
  t_for
  t_functional
  t_if
  t_is
  t_ijk
  t_in
  t_io
  t_list
  t_math
  t_regex
  t_rename
  t_stack
  t_string
  t_times
  t_tstack
  t_type
  t_until
  t_while
  t_variable
  t_vector

  # Final test report
  test-report

  # Exit with 1 on failure or 0 on success
  test-fail-count 0 > if 1 else 0 end exit

