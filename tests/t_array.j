#
# Array Tests
#

-include test.j
-public t_array

-variable ary

user-cmp-rev
  2dup > if 2drop -1 return end
  2dup < if 2drop 1 return end
  2drop 0

t_array
  "Array" test-header
  10 array ary !
  ary @ capacity 10 "array create/capacity" test
  0 0 ary @ !
  1 1 ary @ !
  "John" 2 ary @ !
  0 ary @ @
  1 ary @ @
  2 ary @ @
  0 1 "John" "array set/get" test
  1 array 3 times i append next
  each next 0 1 2 "array append" test
  1 array 18 append 2 append "John" append "Apple" append sort
  each next 2 18 "Apple" "John" "array sort" test
  1 array 8 append 2 append 9 append @user-cmp-rev sort-user
  each next 9 8 2 "array sort-user" test
