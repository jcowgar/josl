#
# Test the break word
#

-include test.j
-public t_break

t_break
  "Break" test-header
  0 10 times i 8 = if break end 1+ next
  8 "break" test
