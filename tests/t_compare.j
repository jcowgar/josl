#
# Compare Tests
#

-include test.j
-public t_compare

t_compare
  "Compare" test-header
  1 1 = 1    "1 1 =" test
  1 2 = 0    "1 2 =" test
  1 1 not= 0 "1 1 not=" test
  1 0 not= 1 "1 0 not=" test
  0 -1= 0    "0 -1=" test
  -1 -1= 1   "-1 -1=" test
  0 0= 1     "0 0=" test
  1 0= 0     "1 0=" test
  0 1= 0     "0 1=" test
  1 1= 1     "1 1=" test
  0 1 > 0    "0 1 >" test
  0 1 < 1    "0 1 <" test
  1 0 > 1    "1 0 >" test
  1 0 < 0    "1 0 <" test
  5 3 7 between? 1 "5 3 7 between?" test
  1 3 7 between? 0 "1 3 7 between?" test

  1 1 and? 1 "1 1 and?" test
  1 0 and? 0 "1 0 and?" test
  0 0 and? 0 "0 0 and?" test
  0 1 and? 0 "0 1 and?" test
  1 1 or? 1 "1 1 or?" test
  1 0 or? 1 "1 0 or?" test
  0 0 or? 0 "0 0 or?" test
  0 1 or? 1 "0 1 or?" test

  1.0 1.0 = 1    "1.0 1.0 =" test
  1.0 2.0 = 0    "1.0 2.0 =" test
  1.0 1.0 not= 0 "1.0 1.0 not=" test
  1.0 0.0 not= 1 "1.0 0.0 not=" test
  0.0 -1= 0    "0.0 -1=" test
  -1.0 -1= 1   "-1.0 -1=" test
  0.0 0= 1     "0.0 0=" test
  1.0 0= 0     "1.0 0=" test
  0.0 1= 0     "0.0 1=" test
  1.0 1= 1     "1.0 1=" test
  0.0 1.0 > 0    "0.0 1.0 >" test
  0.0 1.0 < 1    "0.0 1.0 <" test
  1.0 0.0 > 1    "1.0 0.0 >" test
  1.0 0.0 < 0    "1.0 0.0 <" test
  5.0 3.0 7.0 between? 1 "5.0 3.0 7.0 between?" test
  1.0 3.0 7.0 between? 0 "1.0 3.0 7.0 between?" test
  
  1 1 *= 1 1 1 "*= 1" test
  1 0 *= 1 0 0 "*= 2" test
  1 1 *not= 1 1 0 "*not= 1" test
  1 0 *not= 1 0 1 "*not= 2" test
  1 0 *> 1 0 1 "*> 1" test
  0 1 *> 0 1 0 "*> 2" test
  1 0 *< 1 0 0 "*< 1" test
  0 1 *< 0 1 1 "*< 2" test
  1 0 *<= 1 0 0 "*<= 1" test
  0 1 *<= 0 1 1 "*<= 2" test
  1 1 *<= 1 1 1 "*<= 3" test
  1 0 *>= 1 0 1 "*>= 1" test
  0 1 *>= 0 1 0 "*>= 2" test
  1 1 *>= 1 1 1 "*>= 3" test
  5 3 7 *between? 5 3 7 1 "*between 1" test
  5 6 7 *between? 5 6 7 0 "*between 2" test

