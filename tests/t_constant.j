#
# Test the constant system
#

-include test.j
-public t_constant
-constant a 10
-constant b 20 c 30
-constant name "John Doe"
-constant hourly-wage 3.45

t_constant
  "Constant" test-header
  a 10 "basic" test
  b c 20 30 "multiple on one line" test
  name "John Doe" "constant string" test
  hourly-wage 3.45 "constant double" test
