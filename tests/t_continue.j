#
# Test the continue word
#

-include test.j
-public t_continue

t_continue
  "Continue" test-header
  0 10 times i 5 = if continue end 1+ next
  9 "continue" test
