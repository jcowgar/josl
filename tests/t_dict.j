#
# Dict Tests
#

-include test.j
-public t_dict
-variable d d2

t_dict
  "Dict" test-header
  dict d !
  12 "age" d @ !
  d @ copy d2 !
  d @ len 1 "len" test
  "age" d @ has-key? 1 "set/has-key?" test
  "age" d @ @ 12 "get" test
  "abc" d @ has-key? 0 "has-key?" test
  "age" d @ remove
  "age" d @ has-key? 0 "remove" test
  10 sized-dict d !
  15 "age" d @ !
  "age" d @ @ 15 "get from sized-dict" test
  d2 @ len
  "age" d2 @ @ 1 12 "copy" test
  "John" "name" d2 @ !
  d2 @ keys each next
  "age" "name" "keys" test
  d2 @ values each next
  12 "John" "values" test
  1 dict copy drop 1 "copy empty dict" test
