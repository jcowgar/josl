#
# Dynamic Word Tests
#

-include test.j
-include ../lib/dynamic.j

-public t_dynamic

one 1

t_dynamic
  "Dynamic" test-header
  @one word? 1 "word? 1" test
  10 word? 0 "word? 2" test

  @one exec-word 1 "exec-word" test
  
  10 "dup" exec-word 10 10 "exec-word 2" test

  "20 dup" exec-str 20 20 "exec-str" test
  
  10 { - } curry
  100 swap exec-word 90 "curry" test

  10 9 { - - } 2curry
  100 swap exec-word 99 "2curry" test

  10 3 2 { - - - } 3curry
  100 swap exec-word 91 "3curry" test

  { - } { + } compose >t 50 20 10 >s exec-word
  60 "compose" test
  
  3 2 1 0 { '0 '1 '2 '3 } curry? exec-word
  0 1 2 3 "curry?" test

