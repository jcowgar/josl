#
# Each/Next testing
#

-include test.j
-public t_each

, over push-tail
a, 2 pick !
d, 2 pick !

test-list list 10 , 20 , 30 ,
test-array 3 array 10 0 a, 20 1 a, 30 2 a,
test-dict dict 10 "a" d, 20 "b" d, 30 "c" d,

t_each
  "Each/Next" test-header
  test-list each next 10 20 30 "list each" test
  test-array each next 10 20 30 "array each" test
  "abc" each next 97 98 99 "string each" test
  test-list each-rev next 30 20 10 "list each-rev" test
  test-array each-rev next 30 20 10 "array each-rev" test
  "abc" each-rev next 99 98 97 "string each-rev" test
  test-dict each-kv next "c" 30 "b" 20 "a" 10 "dict each-kv" test
  test-array each-kv next 0 10 1 20 2 30 "array each-kv" test
  "abc" each-kv next 0 97 1 98 2 99 "string each-kv" test
