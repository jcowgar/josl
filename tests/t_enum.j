#
# Test the enum system
#

-include test.j
-public t_enum

-enum a1 b1 c1
-enum 10 a2 b2 c2
-enum 0 -1 a3 b3 c3
-enum 1 2 * a4 b4 c4
-enum 1 a5 b5 c5 100 d5 e5 f5

t_enum
  "Enum" test-header
  a1 b1 c1 0 1 2 "basic" test
  a2 b2 c2 10 11 12 "starting value" test
  a3 b3 c3 0 -1 -2  "starting value and incrementor" test
  a4 b4 c4 1 2 4 "starting value and multiplier" test
  a5 b5 c5 d5 e5 f5 1 2 3 100 101 102
    "starting then changing mid stream" test



