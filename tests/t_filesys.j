#
# Test File Words
#

-include test.j
-public t_filesys
-variable found-filesys did-fail


test-file-remove@failed? drop 1 did-fail ! resume
test-file-remove 0 did-fail ! file-remove

test-file-move@failed? drop 1 did-fail ! resume
test-file-move 0 did-fail ! file-move

test-file-copy@failed? drop 1 did-fail ! resume
test-file-copy 0 did-fail ! file-copy

t_filesys
  "File System" test-header
  0 did-fail !

  "t_filesys.j" file-ctime 0 > 1 "file-ctime" test
  "t_filesys.j" file-mtime 0 > 1 "file-mtime" test
  "t_filesys.j" file-atime 0 > 1 "file-atime" test
  "t_filesys.j" dir? 0 "dir? 1" test
  "." dir? 1 "dir 2" test
  "t_filesys.j" readable? 1 "readable? 1" test
  "badname"     readable? 0 "readable? 0" test
  "t_filesys.j" writable? 1 "writable? 1" test
  "badname"     writable? 0 "writable? 0" test
  "t_filesys.j" executable?
  1 os = if 1 else 0 end # Windows always says files are executable
  "executable? 1" test
  "badname"     executable? 0 "executable? 0" test
  "t_filesys.j" file-exists? 1 "file-exists? 1" test
  "badname"     file-exists? 0 "file-exists? 0" test

  "badname" "goodname" test-file-copy did-fail @
  1 "file-copy 1" test

  "t_filesys.tmp" file-exists?
  "t_filesys.j" "t_filesys.tmp" test-file-copy did-fail @
  "t_filesys.tmp" file-exists?
  0 0 1 "file-copy 2" test

  "t_filesys.tmp2" file-exists?
  "t_filesys.tmp" "t_filesys.tmp2" test-file-move did-fail @
  "t_filesys.tmp2" file-exists?
  0 0 1 "file-move 1" test

  "badname" "goodname" test-file-move did-fail @
  1 "file-move 2" test

  "t_filesys.tmp2" file-exists?
  "t_filesys.tmp2" test-file-remove did-fail @
  "t_filesys.tmp2" file-exists?
  1 0 0 "file-remove 1" test

  "badname" test-file-remove did-fail @
  1 "file-remove 2" test

  "dira" file-exists?
  "dira" dir-create
  "dira" file-exists?
  "dira" dir?
  0 1 1 "dir-create" test

  "dira" file-exists?
  "dira" dir-remove
  "dira" file-exists?
  1 0 "dir-remove" test

  0 found-filesys !
  "." dir-open
  while dup dir-next dup null? 0= do
    "t_filesys.j" = if 1 found-filesys ! end
  end drop dir-close
  found-filesys @ 1 "dir-open, dir-next, dir-close" test

