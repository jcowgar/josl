# 
# Test the functional words
#

-include test.j
-public t_functional
-variable tmp

,  over push-tail
a, 2 pick !

test-array 5 array 1 0 a, 2 1 a, 3 2 a, 4 3 a, 5 4 a,
test-list  list 1 , 2 , 3 , 4 , 5 ,

t_functional
  "Functional" test-header
  test-list { 10 + } map tmp !!
  dup go-head len times tmp @ forward next
  11 12 13 14 15 "map list" test
  
  test-array { 10 + } map tmp !!
  len times i tmp @ @ next
  11 12 13 14 15 "map array" test  

  test-list { * } fold 120 "fold list" test
  test-array { * } fold 120 "fold array" test
  test-list { - } fold -13 "fold list 2" test
  test-list { - } foldr -5 "foldr list" test
  test-array { - } fold -13 "fold array 2" test
  test-array { - } foldr -5 "foldr array" test
  0 array { - } fold null? 1 "fold empty array" test
  1 array 10 0 a, { - } fold 10 "fold one-element array" test
  0 array { - } foldr null? 1 "foldr empty array" test
  1 array 10 0 a, { - } foldr 10 "foldr one-element array" test
  list { - } fold null? 1 "fold empty list" test
  list 10 , { - } fold 10 "fold one-element list" test
  list { - } foldr null? 1 "foldr empty list" test
  list 10 , { - } foldr 10 "foldr one-element list" test

  test-list { odd? } filter tmp !!
  dup go-head len times tmp @ forward next 
  1 3 5 "filter list" test

  test-array { odd? } filter tmp !!
  len times i tmp @ @ next
  1 3 5 "filter array" test
