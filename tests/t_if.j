#
# Test the if system
#

-include test.j
-public t_if

t_if
  "If/Else/End" test-header
  1 1 if 1+ end 2 "1 if" test
  0 1 if 1+ end 1 "0 if" test
  1 1 if 1+ else 1+ 1+ end 2 "1 if/else" test
  1 0 if 1+ else 1+ 1+ end 3 "0 if/else" test
  1 1 *if 2 else 3 end 1 1 2 "1 *if/else" test
  1 0 *if 2 else 3 end 1 0 3 "0 *if/else" test


