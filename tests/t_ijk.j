#
# i, j and k tests
#

-include test.j
-public t_ijk

t_ijk
  "ijk" test-header
  3 0 for i next 0 1 2 "i with for" test
  2 0 for 2 0 for i j next next
  0 0 1 0 0 1 1 1 "i j with for" test
  2 0 for 2 0 for 2 0 for i j k next next next
  0 0 0
  1 0 0
  0 1 0
  1 1 0
  0 0 1
  1 0 1
  0 1 1
  1 1 1
  "i j k with for" test
  3 times i next 0 1 2 "i with times" test
