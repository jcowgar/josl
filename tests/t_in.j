#
# In Testing
#

-include test.j
-public t_in

t_in
  10 is
    2 do -2 end
    3 do -3 end
    10 do 1 end
    r/[a-z]/ do -4 end
  else
    -100
  end 1 "10 is" test

  "john" is
    "jim" do 0 end
    "joe" do 0 end
    "john" do 1 end 
    "jane" do 0 end
  end 1 "john is" test
  
  10 is
    20 do 0 end
    { 4 > } do 1 end  # should hit, not the next
    10 do 0 end       # should have been skipped
  end 1 "10 is #2" test
