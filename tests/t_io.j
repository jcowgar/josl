#
# Tests for IO
#

-include test.j
-public t_io

t_io
  "IO" test-header
  "t_io.tmp" "w" file-open dup "Hello, World!" swap write close
  "t_io.tmp" "r" file-open dup read-line swap close
  "Hello, World!" "file-open, write, read-line, close" test
  "t_io.tmp" file-remove 1 1 "file-remove" test
