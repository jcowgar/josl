#
# Test the is word
#

-include test.j

-public t_is

t_is_simple
  "Is" test-header
  0 is
    0 do 0 end
	1 do 1 end
	2 do 2 end
  end 0 "0 is simple" test

  1 is
    0 do 0 end
	1 do 1 end
	2 do 2 end
  end 1 "1 is simple" test

  2 is
    0 do 0 end
	1 do 1 end
	2 do 2 end
  end 2 "2 is simple" test

t_is_else
  "Is Else" test-header
  0 is
    0 do 0 end
  else
    drop 1
  end 0 "0 is else" test

  1 is
    0 do 0 end
	2 do 2 end
  else
    drop 1
  end 1 "1 is else" test

is-0 0 =
is-1 1 =
is-2 2 =

t_is_word
  "Is word" test-header
  0 is
    { is-0 } do 0 end
	{ is-1 } do 1 end
	{ is-2 } do 2 end
  end 0 "0 is word" test

  1 is
    { is-0 } do 0 end
	{ is-1 } do 1 end
	{ is-2 } do 2 end
  end 1 "1 is word" test

  2 is
    { is-0 } do 0 end
	{ is-1 } do 1 end
	{ is-2 } do 2 end
  end 2 "2 is word" test

t_is
  t_is_simple
  t_is_else
  t_is_word

