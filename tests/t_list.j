-include test.j
-public t_list
-variable l

user-cmp-rev
  2dup > if 2drop -1 return end
  2dup < if 2drop 1 return end
  2drop 0

t_list_basic
  "List" test-header
  list l !
  l @ list? 1 "list?" test
  5 times i 1+ l @ push-tail next
  l @ len 5 "len" test
  l @ head 1 "head" test
  l @ tail 5 "tail" test
  l @ go-head
  l @ forward 1 "forward 1" test
  l @ forward 2 "forward 2" test
  l @ forward 3 "forward 3" test
  l @ forward 4 "forward 4" test
  l @ forward 5 "forward 5" test
  l @ forward null "forward 6" test
  l @ go-tail
  l @ backward 5 "backward 5" test
  l @ backward 4 "backward 4" test
  l @ backward 3 "backward 3" test
  l @ backward 2 "backward 2" test
  l @ backward 1 "backward 1" test
  l @ backward null "backward 6" test
  l @ go-head
  l @ forward drop
  l @ forward drop
  l @ forward drop
  l @ remove
  l @ len 4 "len after remove" test
  l @ go-head
  l @ forward 1 "forward 1, 2" test
  l @ forward 2 "forward 2, 2" test
  l @ forward 4 "forward 4, 2" test
  l @ forward 5 "forward 5, 2" test
  l @ forward null "forward 6, 2" test
  l @ go-head
  l @ forward drop
  l @ forward drop
  100 l @ insert
  l @ go-head
  l @ forward 1 "forward 1, 3" test
  l @ forward 100 "forward 100, 3" test
  l @ forward 2 "forward 2, 3" test
  l @ forward 4 "forward 4, 3" test
  l @ forward 5 "forward 5, 3" test
  l @ forward null "forward 6, 3" test
  l @ go-tail
  l @ backward 5 "backward 5, 3" test
  l @ backward 4 "backward 4, 3" test
  l @ backward 2 "backward 2, 3" test
  l @ backward 100 "backward 100, 3" test
  l @ backward 1 "backward 1, 3" test
  l @ backward null "backward 0, 3" test
  l @ go-head
  l @ forward drop
  l @ current 1 "current" test
  list l !
  3 times i l @ push-tail next
  3 times l @ pop-head next
  0 1 2 "pop-head" test
  3 times l @ pop-tail next
  2 1 0 "pop-tail" test
  list 18 over push-tail "John" over push-tail 2 over push-tail "Apple" over push-tail sort
  each next 2 18 "Apple" "John" "list sort" test
  list 8 over push-tail 2 over push-tail 9 over push-tail @user-cmp-rev sort-user
  each next 9 8 2 "list sort-user" test

test-bug-6
  "test-bug-6" test-header
  list dup 10 over push-tail pop-tail drop 10 over push-tail pop-tail 10 "bug #6" test

t_list
  t_list_basic
  test-bug-6
