#
# Tests for math words

-include test.j
-public t_math

t_math
  "Math" test-header
  1 1 + 2                "1 1 +" test
  1 1 - 0                "1 1 -" test
  2 2 * 4                "2 2 *" test
  4 2 / 2                "4 2 /" test
  1 1+ 2                 "1 1+" test
  1 1- 0                 "1 1-" test
  1 2+ 3                 "1 2+" test
  1 2- -1                "1 2-" test
  1 negate -1            "1 negate" test
  -1 negate 1            "-1 negate" test
  1 abs 1                "1 abs" test
  1.1 abs 1.1            "1.1 abs" test
  -1 abs 1               "-1 abs" test
  -1.1 abs 1.1           "-1.1 abs" test
  1 2 min 1              "1 2 min" test
  2 1 min 1              "2 1 min" test
  1.1 2 min 1.1          "1.1 2 min" test
  2 1.1 min 1.1          "2 1.1 min" test
  1.1 2.1 min 1.1        "1.1 2.1 min" test
  2.1 1.1 min 1.1        "2.1 1.1 min" test
  1 2 max 2              "1 2 max" test
  2 1 max 2              "2 1 max" test
  1.1 2 max 2            "1.1 2 max" test
  2 1.1 max 2            "2 1.1 max" test
  1.1 2.1 max 2.1        "1.1 2.1 max" test
  2.1 1.1 max 2.1        "2.1 1.1 max" test
  1 ceil 1.0             "1 ceil" test
  1.1 ceil 2.0           "1.1 ceil" test
  1 floor 1.0            "1 floor" test
  1.1 floor 1.0          "1.1 floor" test
  1 round 1.0            "1 round" test
  2 round 2.0            "2 round" test
  1.1 round 1.0          "1.1 round" test
  1.5 round 2.0          "1.5 round" test
  1.1 0 round-to 1.0     "1.1 0 round-to" test
  1.5 0 round-to 2.0     "1.5 0 round-to" test
  1.54 1 round-to 1.5    "1.54 1 round-to" test
  1.55 1 round-to 1.6    "1.55 1 round-to" test
  1.554 2 round-to 1.55  "1.554 2 round-to" test
  0.5 acos 1.0471975511965979       "0.5 acos" test
  0.5 asin 0.52359877559829893      "0.5 asin" test
  0.5 atan 0.46364760900080609      "0.5 atan" test
  0.5 0.5 atan2 0.78539816339744828 "0.5 0.5 atan2" test
  0.5 cos 0.87758256189037276       "0.5 cos" test
  0.5 cosh 1.1276259652063807       "0.5 cosh" test
  0.5 sin 0.47942553860420301       "0.5 sin" test
  0.5 sinh 0.52109530549374738      "0.5 sinh" test
  0.5 tan 0.54630248984379048       "0.5 tan" test
  0.5 tanh 0.46211715726000974      "0.5 tanh" test
  10.5 exp 36315.502674246636       "10.5 exp" test
  1.0 exp2 2.0                      "1.0 exp2" test
  1.7 expm1 4.473947391727          "1.7 expm1" test
  5 frexp 0.625 3                   "5 frexp" test
  10 log 2.3025850929940459         "10 log" test
  0.45 log1p 0.371563556432         "10 log1p" test
  8.4 log2 3.070389327891            "10 log2" test
  12 log10 1.0791812460476249       "12 log10" test
  12.5 modf 0.5 12.0                "12.5 modf" test
  2 8 fmod 2.0                      "2 8 fmod" test
  2 2 pow 4.0                       "2 2 pow" test
  2 sqrt 1.4142135623730951         "2 sqrt" test
  0 degrees 0.0                     "0 degrees" test
  2.0 degrees 114.59155902616465    "2.0 degrees" test
  114.59 radians 1.9999727898603024 "114.59 radians" test
  PI 3.1415926535897932384          "PI" test
  2PI 6.28318530717958647692        "2PI" test
  PI/2 1.57079632679489661923       "PI/2" test
  E 2.71828182845904523536          "E" test
  rand 0 > 1                        "rand" test
  0 100 seed-rand 0                 "seed-rand" test
