#
# Regular Expression Tests
#

-include test.j
-include ../lib/regex.j
-public t_regex

t_regex
  "Regular Expressions" test-header

  "hello" "[a-z]" re-compile regexp 1 0 1 "re-compile" test

  "hello" r/[a-z]/ regexp 1 0 1 "regexp 1" test
  "hello world" r/([a-z]+) ([a-z]+)/ regexp 11 6 5 0 11 0 3 "regexp 2" test
  "Hello World" r/([a-z]+) ([a-z]+)/i regexp 11 6 5 0 11 0 3 "regex 3" test

  "100 200 300" -1 0 r/[0-9]+/ regextract
  "100" "200" "300" 3 "regextract" test

  "100 200 300" r/[0-9]+/ regextract-first
  "100" 1 "regextract-first" test

  "100 200 300" 2 r/[0-9]+/ regextract-limit
  "100" "200" 2 "regextract-limit" test

  "100 200 300" r/[0-9]+/ regextract-all
  "100" "200" "300" 3 "regextract-all" test

  "hello Jeremy Cowgar" "Hello" -1 0 r/hello/ regsub
  "Hello Jeremy Cowgar" "regsub 1" test

  "Jeremy Cowgar John Doe" "\\2, \\1" -1 0 r/([A-Z][a-z]+) ([A-Z][a-z]+)/ regsub
  "Cowgar, Jeremy Doe, John" "regsub 2" test

  "What's up Jeremy Cowgar? What's up John Doe?" "\\1" -1 0 r/([A-Z][a-z]+) ([A-Z][a-z]+)/ regsub
  "What's up Jeremy? What's up John?" "regsub 3" test

  "Hello Hello Hello" "a" 1 0 r/e/ regsub
  "Hallo Hello Hello" "regsub 4" test

  "Hello Hello Hello" "a" 1 3 r/e/ regsub
  "Hello Hallo Hello" "regsub 5" test

  "Hello Hello Hello" "a" r/e/ regsub-first
  "Hallo Hello Hello" "regsub-first" test

  "Hello Hello Hello" "a" 1 r/e/ regsub-limit
  "Hallo Hello Hello" "regsub-limit" test

  "Hello Hello Hello" "a" r/e/ regsub-all
  "Hallo Hallo Hallo" "regsub-all" test
