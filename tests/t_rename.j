#
# Rename Tests
#

-include test.j
-public t_rename

-rename + j+

+ 10 j+ j+

t_rename
  "Rename" test-header

  10 20 + 40 "-rename 1" test
  10 20 j+ 30 "-rename 2" test
