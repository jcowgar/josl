#
# Stack Testing
#

-include test.j
-public t_stack

t_stack
  1 2 3 1 2 3 "general push" test
  1 2 depth 1 2 2 "depth" test
  1 2 3 drop 1 2 "drop" test
  1 2 3 2drop 1 "2drop" test
  1 2 3 4 3drop 1 "3drop" test
  1 2 3 2 drop# 1 "drop#" test
  1 dup "dup" test
  1 2 2dup "2dup" test
  1 2 3 3dup "3dup" test
  1 2 3 4 4 dup# "dup#" test
  1 2 swap 2 1 "swap" test
  1 2 3 4 2swap 3 4 1 2 "2swap" test
  1 2 3 4 5 6 3swap 4 5 6 1 2 3 "3swap" test
  1 2 3 4 5 6 7 8 4 swap# 5 6 7 8 1 2 3 4 "swap#" test
  1 2 over 1 2 1 "over" test
  1 2 3 4 2over 1 2 3 4 1 2 "2over" test
  1 2 3 4 5 6 3over 1 2 3 4 5 6 1 2 3 "3over" test
  1 2 3 rot 2 3 1 "rot" test
  1 2 3 -rot 3 1 2 "-rot" test
  1 2 3 4 5 6 2rot 3 4 5 6 1 2 "2rot" test
  1 2 3 4 5 6 -2rot 5 6 1 2 3 4 "-2rot" test
  1 2 3 4 5 6 7 8 9 3rot 4 5 6 7 8 9 1 2 3 "3rot" test
  1 2 3 4 5 6 7 8 9 -3rot 7 8 9 1 2 3 4 5 6 "-3rot" test
  1 2 1 pick 1 2 1 "pick" test
  1 2 nip 2 "nip" test
  1 2 tuck 2 1 2 "tuck" test
  1 2 3 4 5 3 roll 1 3 4 5 2 "roll" test
  1 2 3 4 5 3 -roll 1 5 2 3 4 "-roll" test
  0 '0 0 0 "'0" test
  1 0 '1 1 0 1 "'1" test
  2 1 0 '2 2 1 0 2 "'2" test
  3 2 1 0 '3 3 2 1 0 3 "'3" test
  4 3 2 1 0 '4 4 3 2 1 0 4 "'4" test
  5 4 3 2 1 0 '5 5 4 3 2 1 0 5 "'5" test
  6 5 4 3 2 1 0 '6 6 5 4 3 2 1 0 6 "'6" test
  7 6 5 4 3 2 1 0 '7 7 6 5 4 3 2 1 0 7 "'7" test
  8 7 6 5 4 3 2 1 0 '8 8 7 6 5 4 3 2 1 0 8 "'8" test
  9 8 7 6 5 4 3 2 1 0 '9 9 8 7 6 5 4 3 2 1 0 9 "'9" test
  10 20 { dup } dip 10 10 20 "dip" test

