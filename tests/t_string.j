#
# Tests for strings
#

-include test.j
-public t_string

t_string
  "String" test-header
  "A" "A" = 1                   "A A =" test
  "A" "C" = 0                   "A C =" test
  "A" "C" > 0                   "A C >" test
  "A" "C" < 1                   "A C <" test
  "C" "A" > 1                   "C A >" test
  "C" "A" < 0                   "C A <" test
  "A" "A" >= 1                  "A A >=" test
  "A" "C" >= 0                  "A C >=" test
  "C" "A" >= 1                  "C A >=" test
  "A" "A" <= 1                  "A A <=" test
  "A" "C" <= 1                  "A C <=" test
  "C" "A" <= 0                  "C A <=" test
  "F" "E" "G" between? 1        "F E G between?" test
  "A" "E" "G" between? 0        "A E G between?" test
  "H" "E" "G" between? 0        "H E G between?" test
  "John" len 4                  "John len" test
  "John" "Doe" append "JohnDoe" "John Doe append" test
  "John" "Doe" " " join         "John Doe" "John Doe \" \" join" test
  "John Doe" 4 left             "John" "\"John Doe\" 4 left" test
  "John Smith Doe" 5 9 mid      "Smith" "\"John Smith Doe\" 5 9 mid" test
  "John Doe" 3 right            "Doe" "\"John Doe\" 3 right" test
  "John" lower                  "john" "John lower" test
  "John" upper                  "JOHN" "John upper" test
  "john doe" title              "John Doe" "\"john doe\" title" test
  "he had a dog." capitalize    "He had a dog." "\"he had a dog.\" capitalize" test
  " John " trim                 "John" "\" John \" trim" test
  " John " trim-head            "John " "\" John \" trim-head" test
  " John " trim-tail            " John" "\" John \" trim-tail" test
  "John Doe" "o" find 1         "\"John Doe\" \"o\" find" test
  "John Doe" "o" rfind 6        "\"John Doe\" \"o\" rfind" test
  "John Doe" "o" 2 find# 6      "\"John Doe\" \"o\" 2 find#" test
  "John Doe" "o" 5 rfind# 1     "\"John Doe\" \"o\" 5 rfind#" test
  "John Doe" 111 find 1         "\"John Doe\" 111 find" test
  "John Doe" 111 rfind 6        "\"John Doe\" 111 rfind" test
  "John Doe" 111 2 find# 6      "\"John Doe\" 111 2 find#" test
  "John Doe" 111 5 rfind# 1        "\"John Doe\" 111 5 rfind#" test
  "Dr. Jay Jr." "Dr." prefix? 1 "\"Dr. Jay Jr.\" \"Dr.\" prefix?" test
  "Dr. Jay Jr." "Jr." prefix? 0 "\"Dr. Jay Jr.\" \"Jr.\" prefix?" test
  "Dr. Jay Jr." "Jr." suffix? 1 "\"Dr. Jay Jr.\" \"Jr.\" suffix?" test
  "Dr. Jay Jr." "Dr." suffix? 0 "\"Dr. Jay Jr.\" \"Dr.\" suffix?" test
  "*" 3 repeat "***"            "\"*\" 3 repeat" test
  "JohnDoe" 4 cut "John" "Doe"  "\"JohnDoe\" 4 cut" test
  "John" 4 times peel swap next drop
  74 111 104 110 "peel" test
  "John Don Doe" " "    split "John" "Don" "Doe" 3 "split" test
  "John Don Doe" " " 1  split# "John" "Don Doe" 2 "split#" test
  "John,Don|Doe" ",|"   splitany "John" "Don" "Doe" 3 "splitany" test
  "John,Don|Doe" ",|" 1 splitany# "John" "Don|Doe" 2 "splitany#" test
  "John Doe" 4 5 delete "JohnDoe" "delete 1" test
  "John Doe" 4 8 delete "John" "delete 2" test
  "John Doe" 0 5 delete "Doe" "delete 3" test
  "John Doe" 4 1,000 delete "John" "delete 4 1,000" test
  "John Doe" 20 30 delete "John Doe" "delete 20 30" test
  "John Doe" "Don " 5 insert "John Don Doe" "insert 1" test
  "John Don Doe" "Smith" 5 8 replace "John Smith Doe" "replace 1" test
  "John Doe" reverse "eoD nhoJ" "\"John Doe\" reverse" test
  "Tom" "TDmn" translate "Don" "translate 1" test
  "John" "Doe" "{1} {0}" format "John Doe" "format 1" test
  3 "John" "{0} {1}" format "John 3" "format 2" test
  3 "John" "{1:03lld} {0:s}" format "003 John" "format 3" test
  1 "{0} {{ Hello" format "1 { Hello" "format 4" test
