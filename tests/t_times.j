#
# Times/Next testing
#

-include test.j
-public t_times

t_times
  "Times/(+)Next" test-header
  0 5 times 1+ next 5  "5 times" test
  0 0 times 1+ next 0  "0 times" test
  0 -5 times 1+ next 0 "-5 times" test
  0 10 times 1+ 2 +next 5 "+times" test

