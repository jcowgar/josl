#
# Tests for the temporary stack
#

-include test.j
-public t_tstack

t_tstack
  "Temporary Stack" test-header
  1 2 >t 1 "1 2 >t" test
  1 >t >s 1 "1 >t >s" test
  0 1 2 3 3 >t# 0 "1 2 3 3 >t#" test
  1 2 3 3 >t# 3 >s# 1 2 3 "1 2 3 3 >t# 3 >s#" test

  1 array >t >s array? 1 ">t >s array" test
  1 array 1 array 2 >t# 2 >s# array? swap array? 1 1 ">t# >s# array" test
