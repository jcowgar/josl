#
# Type testing tests
#

-include test.j
-public t_type

-variable a

t_type
  "Type Testing" test-header

  # Integer checking
  1 integer?   1 "1 integer?" test
  1.1 integer? 0 "1.1 integer?" test
  "h" integer? 0 "\"h\" integer?" test

  # Double checking
  1 double?    0 "1 double?" test
  1.1 double?  1 "1.1 double?" test
  "h" double?  0 "\"h\" double?" test

  # Number checking
  1 number?    1 "1 number?" test
  1.1 number?  1 "1.1 number?" test
  "h" number?  0 "\"h\" number?" test

  # String checking
  1 string?    0 "1 string?" test
  1.1 string?  0 "1.1 string?" test
  "h" string?  1 "\"h\" string?" test

  # Dict checking
  0 dict?      0 "0 dict?" test
  dict dict?   1 "dict dict?" test

  # Array checking
  5 array array? 1 "array array?" test
  0 array? 0 "0 array?" test

  # List checking
  list list? 1 "list list?" test
  0 list? 0 "0 list?" test

  # Variable checking
  0 variable?  0 "0 variable?" test
  a variable?  1 "a variable?" test

  # Null checking
  0 null?      0 "0 null?" test
  null null?   1 "null null?" test
