#
# Test Variables
#

-include test.j
-public t_variable
-variable a b

t_variable
  "Variables" test-header
  0 a ! 1 a @ 1 0 "set/get" test
  0 a !! 1 a @ 0 1 0 "set-keep/get" test
  b variable? 1 "variable?" test
  b @ null? 1 "null? 1" test
  a null? 0 "null? 2" test
