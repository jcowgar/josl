#
# Test Vector
#

-include test.j
-public t_vector

one   1
two   2
three 3

t_vector
  "Vector" test-header
  0 vector one two three end 1 "0 vector" test
  1 vector one two three end 2 "1 vector" test
  2 vector one two three end 3 "2 vector" test
